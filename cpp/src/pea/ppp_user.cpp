
#include "observations.hpp"
#include "streamTrace.hpp"
#include "linearCombo.hpp"
#include "corrections.hpp"
#include "navigation.hpp"
#include "testUtils.hpp"
#include "acsConfig.hpp"
#include "constants.h"
#include "satStat.hpp"
#include "preceph.hpp"
#include "station.hpp"
#include "algebra.hpp"
#include "constants.h"
#include "common.hpp"
#include "wancorr.h"
#include "antenna.h"
#include "tides.hpp"
#include "enums.h"
#include "ppp.hpp"
#include "vmf3.h"
#include "trop.h"

#include "eigenIncluder.hpp"

#define MIN_NSAT_SOL	4				// min satellite number for solution


#define VAR_CLK     	SQR(2)       	// init variance receiver clock (m^2)
#define VAR_DCB     	SQR(30.0)       // init variance dcb (m^2)
#define VAR_GLO_IFB 	SQR( 0.6)       // variance of glonass ifb

/* write solution status for PPP
 */
void pppoutstat(
	Trace&		trace,
	KFState&	kfState,
	bool		rts)
{
	int solStat = 0;

	int week;
	double tow = time2gpst(kfState.time, &week);
//
// 	double* x = rtk->sol.stat == SOLQ_FIX ? rtk->xa : rtk->xx;
//
	for (auto& [kfKey, index] : kfState.kfIndexMap)
	{
		KFKey key = kfKey;
		if	( key.type	== KF::REC_POS
			&&key.num	== 0)
		{
			double x[3]	= {};
			double v[3]	= {};

			for (key.num = 0; key.num < 3; key.num++)
			{
				kfState.getKFValue(key, x[key.num], &v[key.num]);
			}
			tracepdeex(1, trace, "\r\n$POS,%d,%.3f,%d,%.4f,%.4f,%.4f,%.14f,%.14f,%.14f\n",
						week,
						tow,
						solStat,
						x[0],
						x[1],
						x[2],
						v[0],
						v[1],
						v[2]);
		}

		if (key.type == KF::PHASE_BIAS)
		{
			double phase_bias		= 0;
			double phase_biasVar	= 0;
			kfState.getKFValue(key, phase_bias, &phase_biasVar);
			tracepdeex(1, trace, "$AMB,%d,%.3f,%d,%s,%d,%.4f,%f\n",
						week,
						tow,
						solStat,
						key.Sat.id().c_str(),
						key.num,
						phase_bias,
						phase_biasVar);
		}

		if (key.type == KF::TROP)
		{
			double trop		= 0;
			double tropVar	= 0;
			kfState.getKFValue(key, trop, &tropVar);
			tracepdeex(1, trace, "$TROP,%d,%.3f,%d,%d,%f,%.14f\n",
					week,
					tow,
					solStat,
					1,
					trop,
					tropVar);
		}

		if	( key.type	== KF::REC_SYS_BIAS
			&&key.num	== SatSys(E_Sys::GPS).biasGroup())
		{
			double rClk		= 0;
			double rClkG	= 0;
			double rClkVar	= 0;
			double rClkGVar	= 0;
			kfState.getKFValue(key, rClk,	&rClkVar);
			key.num = SatSys(E_Sys::GLO).biasGroup();
			kfState.getKFValue(key, rClkG,	&rClkGVar);

			tracepdeex(1, trace, "$CLK,%d,%.3f,%d,%d,%.4f,%.4f,%.4f,%.4f\n",
					week,
					tow,
					solStat,
					1,
					rClk		* 1E9 / CLIGHT,
					rClkG		* 1E9 / CLIGHT,
					rClkVar		* 1E9 / CLIGHT,
					rClkGVar	* 1E9 / CLIGHT);
		}
	}
//
// 	/* receiver velocity and acceleration */
// 	{
// 		ecef2pos(rtk->sol.rr, pos);
// 		ecef2enu(pos, rtk->xx + 3, vel);
// 		ecef2enu(pos, rtk->xx + 6, acc);
// 		p += sprintf(p, "$VELACC,%d,%.3f,%d,%.4f,%.4f,%.4f,%.5f,%.5f,%.5f,%.4f,%.4f,"
// 		             "%.4f,%.5f,%.5f,%.5f\n", week, tow, rtk->sol.stat, vel[0], vel[1],
// 		             vel[2], acc[0], acc[1], acc[2], 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
// 	}

// 	if (acsConfig.tropOpts.corr_mode == TROPOPT_ESTG)
// 	{
// 		double tropG1		= 0;
// 		double tropG1Var	= 0;
// 		double tropG2		= 0;
// 		double tropG2Var	= 0;
// 		kfState.getKFValue({KF::TROP, {}, "", 1}, 	tropG1, &tropG1Var);
// 		kfState.getKFValue({KF::TROP, {}, "", 2}, 	tropG2, &tropG2Var);
//
// 		tracepdeex(1, trace, "$TRPG,%d,%.3f,%d,%d,%.5f,%.5f,%f,%f\n",
// 			week, tow, solStat, 1,
// 			tropG1,
// 			tropG2,
// 			tropG1Var,
// 			tropG2Var);
// 	}
}



/* temporal update of phase biases -------------------------------------------*/
void udbias_ppp(
	Trace&		trace,
	rtk_t&		rtk,
	ObsList&	obsList)
{
	tracepde(3,trace, "udbias  : n=%d\n", obsList.size());

	//handle day-boundary clock jump
	bool clk_jump = false;
	if (acsConfig.clock_jump)
	{
		if (ROUND(time2gpst(obsList.front().time, NULL) * 10) % (60*60*24) == 0)
		{
			clk_jump = true;
		}
	}

	for (auto& [key, index] : rtk.pppState.kfIndexMap)
	{
		if (key.type != KF::PHASE_BIAS)
		{
			continue;
		}

		SatStat& satStat = rtk.satStatMap[key.Sat];
		SigStat& sigStat = satStat.sigStatMap[(E_FType)key.num];

		if (sigStat.phaseRejectCount >= acsConfig.pppOpts.phase_reject_count)
		{
			sigStat.phaseRejectCount = 0;

			trace << "Removing " << key << " due to repeated rejections > " << acsConfig.pppOpts.phase_reject_count << std::endl;
			rtk.pppState.removeState(key);
		}

		if	(clk_jump)
		{
			rtk.pppState.removeState(key);
		}

		sigStat.phaseOutageCount++;
		if (sigStat.phaseOutageCount == acsConfig.pppOpts.outage_reset_count)
		{
			sigStat.phaseOutageCount = 0;

			trace << "Removing " << key << " due to extended outage > " << acsConfig.pppOpts.outage_reset_count << std::endl;
			rtk.pppState.removeState(key);
		}
	}

	//Get sums and nums for averages of each frequency
	map<E_FType, double>	sumMap;
	map<E_FType, int>		numMap;

	for (auto& obs			: obsList)
	for (auto& [ft, sig]	: obs.Sigs)
	{
		if (obs.exclude)
		{
			continue;
		}

		if	( (acsConfig.ionoOpts.corr_mode == E_IonoMode::IONO_FREE_LINEAR_COMBO && ft != FTYPE_IF12)
			||(acsConfig.ionoOpts.corr_mode != E_IonoMode::IONO_FREE_LINEAR_COMBO && ft == FTYPE_IF12)
			||(sig.L_corr_m == 0)
			||(sig.P_corr_m == 0))
		{
			continue;
		}

		E_FType l = obs.Sat.sys == +E_Sys::GAL ? F5 : F2;

		double	bias	= 0;
		int		slip	= obs.satStat_ptr->sigStatMap[ft].slip.any;
		double* lam		= obs.satNav_ptr->lam;

		if		(acsConfig.ionoOpts.corr_mode == E_IonoMode::IONO_FREE_LINEAR_COMBO)
		{
			bias = sig.L_corr_m - sig.P_corr_m;
		}
		else if (acsConfig.ionoOpts.corr_mode == E_IonoMode::ESTIMATE)
		{
			if	( obs.Sigs[F1].P	== 0
				||obs.Sigs[l].P		== 0
				||lam[F1]			== 0
				||lam[l]			== 0
				||lam[ft]			== 0)
			{
				continue;
			}

			double ion = (obs.Sigs[F1].P - obs.Sigs[l].P) / (1 - SQR(lam[l] / lam[F1]));

			bias	= obs.Sigs[ft].L_corr_m
					- obs.Sigs[ft].P_corr_m
					+ 2 * ion * SQR(lam[ft] / lam[F1]);
		}
		else
		{
			if	( obs.Sigs[ft].P	== 0
				||obs.Sigs[ft].L	== 0
				||lam[ft]			== 0)
			{
				continue;
			}

			if (obs.satStat_ptr->extionovar <= 0)
			{
				continue;
			}

			double ion = obs.satStat_ptr->extiono;

			bias	= obs.Sigs[ft].L_corr_m
					- obs.Sigs[ft].P_corr_m
					+ 2 * ion * SQR(lam[ft] / lam[F1]);
		}

		KFKey phaseBiasKey;
		phaseBiasKey.type	= KF::PHASE_BIAS;
		phaseBiasKey.Sat	= obs.Sat;
		phaseBiasKey.num	= ft;
		phaseBiasKey.str	= obs.mount;

		if	( bias
			&&slip)
		{
			// reinitialize phase-bias if detecting cycle slip
			rtk.pppState.removeState(phaseBiasKey);

			tracepde(2, trace, "udbias_ppp: sat=%s bias=%.3f\n", obs.Sat.id().c_str(), bias);
			continue;
		}

		double oldBias;
		bool initialised = rtk.pppState.getKFValue(phaseBiasKey, oldBias);

		if	( initialised	== false
			||slip			!= false
			||bias			== 0)
		{
			continue;
		}

		sumMap[ft] += bias - oldBias;
		numMap[ft] ++;
	}

	for (auto& obs			: obsList)
	for (auto& [ft, sig]	: obs.Sigs)
	{
		if (obs.exclude)
		{
			continue;
		}

		double averageOffset = sumMap[ft] / numMap[ft];			//todo aaron, this probably needs bias group separation, not just frequency?

		/* correct phase-code jump to ensure phase-code coherency */
		if	( numMap[ft] >= 2
			&&fabs(averageOffset) > 0.0005 * CLIGHT)		//todo aaron, need define , half millisecond?
		{
			for (auto& [key, index] : rtk.pppState.kfIndexMap)
			{
				if (key.type == KF::PHASE_BIAS)
				{
					double oldBias;
					rtk.pppState.getKFValue(key, oldBias);

					double newBias = oldBias + averageOffset;
					rtk.pppState.setKFValue(key, newBias);
				}
			}

			tracepde(2,trace, "phase-code jump corrected: %s n=%2d dt=%12.9fs\n", rtk.sol.time.to_string(0).c_str(), numMap[ft], averageOffset / CLIGHT);
		}
	}
}



/* phase and code residuals --------------------------------------------------*/
int ppp_filter(
    Trace&		trace,
	ObsList&	obsList,
	Vector3d&	dr,
    rtk_t&		rtk,
    sta_t&		refstat)
{
	TestStack ts(__FUNCTION__);

	KFState&	kfState = rtk.pppState;

	kfState.initFilterEpoch();

	char str[32];
	time2str(obsList.front().time, str, 2);

	auto& recOpts = acsConfig.getRecOpts(obsList.front().mount);

	//Get the previous estimate of position or load from the point positioning solution if not initialised.
	Vector3d x0;
	for (int i = 0; i < 3; i++)
	{
		KFKey xKey;
		xKey.type	= KF::REC_POS;
		xKey.str	= obsList.front().mount;
		xKey.num	= i;
		bool pass = kfState.getKFValue(xKey, x0[i]);
		if (pass == false)
		{
			x0[i] = rtk.sol.sppRRec[i];
		}
	}

	double pos[3];
	ecef2pos(x0.data(), pos);

    // update receiver position with tide and antenna delta
	Vector3d dr1;
    enu2ecef(pos, rtk.opt.antdel.data(), dr1.data());

	//remove elements of receiver position that we dont want included in the state estimate
	Vector3d rRec	= x0
					+ dr
					+ dr1;

	pppCorrections(trace, obsList, rRec, rtk, refstat);

	udbias_ppp(trace, rtk, obsList);

	KFMeasEntryList		kfMeasEntryList;
	map<KFKey, bool>	measuredStates;

	for (auto& obs : obsList)
	{
		if (obs.exclude)
		{
			continue;
		}

		int			sys			= obs.Sat.sys;
		double*		lam			= obs.satNav_ptr->lam;
		SatStat&	satStat		= *(obs.satStat_ptr);
		int			biasGroup	= obs.Sat.biasGroup();

		//Satellite already has precise clocks if available
		double C_dtSat = CLIGHT * obs.dtSat[0];

		//try to get precicse clock for receiver//todo aaron, there is a comment about precise ephemeris in original code

		double precDtRec	= 0;
		double precDtRecVar	= 0;
		pephclk(rtk.sol.time, obs.mount, nav, &precDtRec, &precDtRecVar);
		if (precDtRec != 0)
		{
// 			C_dtRec = CLIGHT * precDtRec;
		}

		// tropospheric  model
		double dTrop			= 0;
		double varTrop			= 0;
		double tropStates[3]	= {};
		double dTropDx[3]		= {};

		//get the previous filter states for linearisation around this operating point
		for (short i = 0; i < 3; i++)
		{
			bool pass = kfState.getKFValue({KF::TROP, {}, obs.mount, i}, tropStates[i]);
			if	( (i	== 0)
				&&(pass == false))
			{
				double ztd = sbstropcorr(rtk.sol.time, rRec, PI/2);
				tropStates[i] = ztd;
			}

		}

		//calculate the trop values, variances, and gradients at the operating points
		if	( !model_trop(obs.time, pos, satStat.azel, tropStates,	dTropDx, 	dTrop, varTrop))
		{
			continue;
		}

		//when gradients aren't being estimated set their partials to zero
		if (acsConfig.tropOpts.corr_mode != TROPOPT_ESTG)
		{
			dTropDx[1] = 0;
			dTropDx[2] = 0;
		}

		// ionospheric model
		double dIono			= 0;
		double varIono			= 0;
		double ionoState		= 0;

		//calculate the iono values amd variances at the operating points
		if (acsConfig.ionoOpts.corr_mode == E_IonoMode::ESTIMATE)
		{
			kfState.getKFValue({KF::IONOSPHERIC, obs.Sat}, ionoState);
			dIono	= ionoState;
			varIono	= 0;
		}
		else
		{
			if (satStat.extionovar < 0)
				continue;
			dIono	= satStat.extiono;
			varIono	= satStat.extionovar;
		}

		for (auto& [ft, sig] : obs.Sigs)
		{
			SigStat& sigStat = satStat.sigStatMap[ft];

			if (acsConfig.ionoOpts.corr_mode != E_IonoMode::IONO_FREE_LINEAR_COMBO)
			{
				if	( ft == FTYPE_IF12
					||ft == FTYPE_IF15)
				{
					continue;
				}
			}

			if (acsConfig.ionoOpts.corr_mode == E_IonoMode::IONO_FREE_LINEAR_COMBO)
			{
				if	( ft != FTYPE_IF12
					&&ft != FTYPE_IF15)
				{
					continue;
				}

				if	( (acsConfig.ionoOpts.iflc_freqs == E_LinearCombo::L1L2_ONLY && ft != FTYPE_IF12)
					||(acsConfig.ionoOpts.iflc_freqs == E_LinearCombo::L1L5_ONLY && ft != FTYPE_IF15))
				{
					continue;
				}
			}

			if (acsConfig.ionoOpts.corr_mode == E_IonoMode::TOTAL_ELECTRON_CONTENT)
			{
				if	(ft != F1)
				{
					continue;  // for now, vtec maps are only valid for F1 frequency
				}
			}

			if	( (sig.P_corr_m == 0)
				||(sig.L_corr_m == 0))
			{
				continue;
			}

			// Ionosphere dependent values
			double ionC			= 0;
			double phaseBias	= 0;
			double ionInitValue	= 0;

			switch (acsConfig.ionoOpts.corr_mode)
			{
				case E_IonoMode::IONO_FREE_LINEAR_COMBO:
				{
					ionC			= 0;
					ionInitValue	= 0;
					phaseBias		= sig.L_corr_m - sig.P_corr_m;
					break;
				}
				case E_IonoMode::ESTIMATE:
				{
					E_FType ft2;
					if (obs.Sat.sys == +E_Sys::GAL)	ft2 = F5;
					else							ft2 = F2;

					if	( obs.Sigs[F1].	P_corr_m == 0
						||obs.Sigs[ft2].P_corr_m == 0)
					{
						continue;
					}

					ionC = SQR(lam[ft] / lam[F1]) * ionmapf(rRec, satStat.azel);		//todo aaron, change to linear combos

					ionInitValue = obs.Sigs[F1].P_corr_m - obs.Sigs[ft2].P_corr_m;								// code GF
					ionInitValue *= SQR(lam[ft]) / (SQR(lam[F1])-SQR(lam[ft2]));								// from GF to slant Iono(ft)
					phaseBias = obs.Sigs[ft].L_corr_m - obs.Sigs[ft].P_corr_m + 2 * ionInitValue;				// Bias = L-P+2*iono

					ionInitValue /= ionC;																		// slant iono to estimated iono (vert Iono(F1))

					break;
				}
				case E_IonoMode::OFF:
				{
					break;
				}
				default:
				{
 					ionC			= SQR(lam[ft] / lam[F1]);
 					ionInitValue	= 0;
 					phaseBias		= sig.L_corr_m - sig.P_corr_m + 2 * satStat.extiono * ionC;
				}
			}

			double varSysIFB = 0;
			if	(sys == E_Sys::GLO)
			{
				varSysIFB = VAR_GLO_IFB;
			}


			//Do some kalman filtering to do the PPP

			// PR	=	R	+ C*dtRec	- C*dtSat	+ dTrop	+ dIono*K + dcb			+ innovation;
			// P	=	R	+ C*dtRec	- C*dtSat	+ dTrop	- dIono*K + ambiguity	+ innovation;


			//Prepare keys for filter states

			KFKey posKeys[3];
			for (short i = 0; i < 3; i++)
			{
				posKeys[i].type		= KF::REC_POS;
				posKeys[i].num		= i;
				posKeys[i].str		= obs.mount;
			}

			KFKey posRateKeys[3];
			for (short i = 0; i < 3; i++)
			{
				posRateKeys[i].type	= KF::REC_POS_RATE;
				posRateKeys[i].num	= i;
				posRateKeys[i].str	= obs.mount;
			}

			KFKey tropKeys[3];
			for (short i = 0; i < 3; i++)
			{
				tropKeys[i].type	= KF::TROP;
				tropKeys[i].num		= i;
				tropKeys[i].str		= obs.mount;
			}

			KFKey phaseBiasKey;
			{
				phaseBiasKey.type	= KF::PHASE_BIAS;
				phaseBiasKey.Sat	= obs.Sat;
				phaseBiasKey.num	= ft;
				phaseBiasKey.str	= obs.mount;
			}

			KFKey systemBiasKey;
			{
				systemBiasKey.type	= KF::REC_SYS_BIAS;
				systemBiasKey.num	= biasGroup;
				systemBiasKey.str	= obs.mount;
			}

			KFKey systemBiasRateKey;
			{
				systemBiasRateKey.type	= KF::REC_SYS_BIAS_RATE;
				systemBiasRateKey.num	= biasGroup;
				systemBiasRateKey.str	= obs.mount;
			}

			KFKey dcbKey;
			{
				dcbKey.type			= KF::DCB;
				dcbKey.num			= ft;
				dcbKey.str			= obs.mount;
			}

			KFKey ionoKey;
			{
				ionoKey.type 		= KF::IONOSPHERIC;
				ionoKey.Sat			= obs.Sat;
				ionoKey.str			= obs.mount;
			}

			//get any values required from state, and use variances in the filter instead of below
			double dcb			= 0;
			double C_dtRec		= 0;					//todo aaron, there is a comment about precise ephemeris in original code

			kfState.getKFValue(dcbKey,			dcb);
			kfState.getKFValue(phaseBiasKey,	phaseBias);
			kfState.getKFValue(systemBiasKey,	C_dtRec);

			double C_dtRecAdj	= rtk.sol.dtRec_m			[biasGroup]
								- rtk.sol.dtRec_m_ppp_old	[biasGroup];
			C_dtRec += C_dtRecAdj;

			InitialState systemBiasInit;
			systemBiasInit.x = C_dtRec;
			systemBiasInit.P = VAR_CLK;
			kfState.resetKFValue(systemBiasKey, systemBiasInit);

			//Prepare the measurement innovation values and sigmas

			double codeMeasured	= sig.P_corr_m;
			double phasMeasured	= sig.L_corr_m;

			double codeComputed	= sig.Range
								+ C_dtRec
								- C_dtSat
								+ dTrop
								+ dIono * ionC
								+ dcb;

			double phasComputed	= sig.Range
								+ C_dtRec
								- C_dtSat
								+ dTrop
								- dIono * ionC
								+ phaseBias;

			tracepde(4, trace, "   PPP_states %s %d %8.4f  %9.4f %9.4f  %9.4f %8.4f %8.4f %9.4f %9.4f %.6e\n", obs.Sat.id(), ft, satStat.el, codeMeasured-sig.Range+C_dtSat, phasMeasured-sig.Range+C_dtSat, C_dtRec, dTrop,ionC*dIono,dcb,phaseBias, sqrt(varIono) * ionC);

			double codeInnov	= codeMeasured - codeComputed;
			double phasInnov	= phasMeasured - phasComputed;

			double codeVar		= sig.codeVar
								+ obs.var
								+ varTrop
								+ varIono * SQR(ionC);

			double phasVar		= sig.phasVar
								+ obs.var
								+ varTrop
								+ varIono * SQR(ionC)
								+ varSysIFB;

			ObsKey obsKeyCode = {obs.Sat, "", "P", ft};
			ObsKey obsKeyPhas = {obs.Sat, "", "L", ft};

			KFMeasEntry	codeMeas(&kfState, obsKeyCode);
			KFMeasEntry	phasMeas(&kfState, obsKeyPhas);

			codeMeas.setInnov(codeInnov);
			phasMeas.setInnov(phasInnov);

			codeMeas.setNoise(codeVar);
			phasMeas.setNoise(phasVar);

			codeMeas.metaDataMap["obs_ptr"] = &obs;
			phasMeas.metaDataMap["obs_ptr"] = &obs;

			// (Re)Initialise any states with values, covariances, and process noises

			InitialState posInits[3];
			for (int i = 0; i < 3; i++)
			{
				posInits[i]					= initialStateFromConfig(recOpts.pos,			i);

				posInits[i].x = rtk.sol.sppRRec[i];
			}

			InitialState posRateInits[3];
			for (int i = 0; i < 3; i++)
			{
				posRateInits[i]				= initialStateFromConfig(recOpts.pos_rate,		i);

// 				posRateInits[i].x = rtk.sol.sppRRec[i];	//todo aaron, get from doppler
			}

			InitialState clockBiasRateInit	= initialStateFromConfig(recOpts.clk_rate);

			InitialState phaseBiasInit		= initialStateFromConfig(recOpts.amb);
			{
				phaseBiasInit.x = phaseBias;
			}

			InitialState ionoInit			= initialStateFromConfig(acsConfig.ionFilterOpts.ion);
			{
				ionoInit.x = ionInitValue;
				ionoInit.Q = SQR(acsConfig.ionFilterOpts.ion.proc_noise[0] / sin(satStat.el));
			}

			InitialState tropInits[3];
			for (int i = 0; i < 1; i++)
			{
				tropInits[i]				= initialStateFromConfig(recOpts.trop,			i);

				tropInits[i].x = tropStates[0];
				tropInits[i].P = varTrop;
			}
			for (int i = 0; i < 2; i++)
			{
				tropInits[i+1]				= initialStateFromConfig(recOpts.trop_grads,	i);
			}

			InitialState dcbInit;
			{
				switch (ft)
				{
					case F2:
					{
						//use dcb from station file
						double dcb;
						if (sys == +E_Sys::GLO)		dcb = -refstat.rbias[1][0];
						else						dcb = -refstat.rbias[0][0];

						dcbInit.x = dcb;
						dcbInit.P = SQR(0.0001);
						dcbInit.Q = 0;
						break;
					}
					case F5:
					{
						//estimate dcbIndex
						dcbInit.x = 0;
						dcbInit.P = VAR_DCB;
						dcbInit.Q = 0;			//todo aaron, had zero process noise before, check?
						break;
					}
					default:
					{
						//do not use these frequencies for dcb.
						break;
					}
				}
			}

			//Add the entries to the design matrix
			if (recOpts.pos.estimate)
			for (int i = 0; i < 3; i++)
			{
				codeMeas.addDsgnEntry(posKeys[i],		-satStat.e[i],		posInits[i]);
				phasMeas.addDsgnEntry(posKeys[i],		-satStat.e[i],		posInits[i]);

				if (recOpts.pos_rate.estimate)
				{
					kfState.setKFTransRate(posKeys[i],		posRateKeys[i],		1,	posRateInits[i]);
				}
			}

			if (recOpts.clk.estimate)
			{
				codeMeas.addDsgnEntry(systemBiasKey,	1,					systemBiasInit);
				phasMeas.addDsgnEntry(systemBiasKey,	1,					systemBiasInit);

				if (recOpts.clk_rate.estimate)
				{
					kfState.setKFTransRate(systemBiasKey,	systemBiasRateKey,	1,	clockBiasRateInit);
				}
			}

			if (acsConfig.ionoOpts.corr_mode == E_IonoMode::ESTIMATE)
			{
				codeMeas.addDsgnEntry(ionoKey,			-ionC,				ionoInit);
				phasMeas.addDsgnEntry(ionoKey,			+ionC,				ionoInit);
			}

			if	( (ft != F1)
				&&(ft != FTYPE_IF12)
				&&(ft != FTYPE_IF15))
			{
				codeMeas.addDsgnEntry(dcbKey,			1,					dcbInit);
			}

			if (recOpts.amb.estimate)
			{
				phasMeas.addDsgnEntry(phaseBiasKey,		1,					phaseBiasInit);
				measuredStates[phaseBiasKey] = true;
			}

			if	( (acsConfig.tropOpts.corr_mode == TROPOPT_EST)
				||(acsConfig.tropOpts.corr_mode == TROPOPT_ESTG))
			for (int i = 0; i < 3; i++)
			{
				codeMeas.addDsgnEntry(tropKeys[i],		dTropDx[i],			tropInits[i]);
				phasMeas.addDsgnEntry(tropKeys[i],		dTropDx[i],			tropInits[i]);
			}

			if	(   acsConfig.max_inno	> 0
				&&( fabs(codeInnov)		> acsConfig.max_inno
				  ||fabs(phasInnov)		> acsConfig.max_inno))
			{
				tracepde(2, std::cout, "outlier rejected  sat=%s %d res=%9.4f %9.4f el=%4.1f\n", obs.Sat.id().c_str(), ft, codeInnov, phasInnov, satStat.el * R2D);
				obs.excludeOutlier = true;
				continue;			//todo aaron, moved to filer
			}

			//Add the completed measurements to the filter

			kfMeasEntryList.push_back(phasMeas);
			kfMeasEntryList.push_back(codeMeas);

			tracepde(4, trace, "%s sat=%2d P%d res=%9.4f sig=%9.4f el=%4.1f\n", str, obs.Sat, ft, codeInnov, sqrt(codeVar), satStat.el * R2D);
			tracepde(4, trace, "%s sat=%2d L%d res=%9.4f sig=%9.4f el=%4.1f\n", str, obs.Sat, ft, phasInnov, sqrt(phasVar), satStat.el * R2D);

			sig.vsig = true;
		}
	}

	if	( recOpts.pos_rate.estimate
		&&acsConfig.pppOpts.ballistics)
	{
		Vector3d gravity		= rRec.normalized() * -1 * EARTH_G_CONST / rRec.dot(rRec);
		Vector3d refOmega		= {0, 0, OMGE};
		Vector3d centrifugal	= - rRec.cross(refOmega).cross(refOmega);
		Vector3d acceleration	= gravity + centrifugal;

		for (int i = 0; i < 3; i++)
		{
			KFKey posRateKey;
			posRateKey.type	= KF::REC_POS_RATE;
			posRateKey.num	= i;
			posRateKey.str	= obsList.front().mount;
			kfState.setKFTransRate(posRateKey,	{KF::ONE},		acceleration[i]);
		}
	}

	removeUnmeasuredAmbiguities(rtk.pppState, measuredStates);

	//add process noise to existing states as per their initialisations.
	kfState.stateTransition(std::cout, rtk.tt);

	//combine the measurement list into a single matrix
	KFMeas combinedMeas = kfState.combineKFMeasList(kfMeasEntryList);
	combinedMeas.time = obsList.front().time;

	if (combinedMeas.V.rows() == 0)
	{
		return SOLQ_NONE;
	}

	//if there are uninitialised state values, estimate them using least squared
	if (kfState.lsqRequired)
	{
		kfState.lsqRequired = false;
		trace << "\r\n -------INITIALISING USING LEAST SQUARES--------" << std::endl;
		kfState.leastSquareInitStates(trace, combinedMeas);
	}

	//perform kalman filtering
	trace << "\r\n -------DOING KALMAN FILTER --------" << std::endl;
	kfState.filterKalman(trace, combinedMeas, true);

	rtk.sol.dtRec_m_ppp_old = rtk.sol.dtRec_m;

	//todo aaron, add pointer to reject counter for bad kf measurements

//reject satellite with large and max post-fit residual */
//satStat.sigStatMap[ft].rejectCount++;

	TestStack::testMat("combinedMeas.V", combinedMeas.V);
	TestStack::testMat("combinedMeas.A", combinedMeas.A);
	TestStack::testMat("kfState.x", kfState.x, 0, &kfState.P);

	return SOLQ_PPP;
}


/* update solution status ----------------------------------------------------*/
void update_stat(
	rtk_t&		rtk,
	ObsList&	obsList,
	int			stat)
{
	//test # of valid satellites
	rtk.sol.numSats = 0;

	for (auto& obs : obsList)
	{
		if (obs.exclude)
		{
			continue;
		}

		bool hasValidSig = false;

		for (auto& [key, Sig] : obs.Sigs)
		{
			if (Sig.vsig == false)
				continue;

			hasValidSig = true;

			SatStat& satStat = *obs.satStat_ptr;
			SigStat& sigStat = satStat.sigStatMap[key];

			sigStat.phaseOutageCount = 0;
			if (Sig.phaseError)		sigStat.phaseRejectCount++;
			else 					sigStat.phaseRejectCount = 0;

		}

		if (hasValidSig)
		{
			rtk.sol.numSats++;
		}
	}

	if (rtk.sol.numSats >= MIN_NSAT_SOL)	rtk.sol.stat = stat;
	else									rtk.sol.stat = SOLQ_NONE;

	for (short i = 0; i < 3; i++)	rtk.pppState.getKFValue({KF::REC_POS,		{}, "",i}, rtk.sol.pppRRec		[i]);
	for (short i = 0; i < 2; i++)	rtk.pppState.getKFValue({KF::REC_SYS_BIAS,	{}, "",i}, rtk.sol.pppdtRec_m	[i]);

	//remove gps from glonass bias
	rtk.sol.pppdtRec_m[1] -= rtk.sol.pppdtRec_m[0];
}

/* precise point positioning -------------------------------------------------*/
void pppos(
	Trace&		trace,
	rtk_t&		rtk,
	ObsList&	obsList,
	sta_t&		refstat)
{
	TestStack ts(__FUNCTION__);

	if (rtk.sol.stat == SOLQ_NONE)
	{
		return;
	}

	const prcopt_t* opt = &rtk.opt;
	char str[32];

	time2str(obsList.front().time, str, 2);
// 	tracepde(2, trace, "pppos   : time=%s nx=%d n=%d\n", str, rtk.nx, obsList.size());

	/* satellite positions and clocks */
	satposs(trace, obsList.front().time, obsList, nav, acsConfig.ppp_ephemeris);

	double pos[3];
	ecef2pos(rtk.sol.sppRRec.data(), pos);

	double ionoState = 0;
	for (auto& obs : obsList)
	{
		if (obs.exclude)
		{
			continue;
		}

		SatStat& satStat	= *(obs.satStat_ptr);

		bool pass = model_iono(obs.time, pos, satStat.azel, ionoState, satStat.extiono, satStat.extionovar);
		if (pass == false)
		{
			satStat.extionovar = -1;
		}
		tracepde(5, trace, "   extion %s : %.4f %.4f\n", obs.Sat.id(), satStat.extiono, satStat.extionovar);
	}

	/* earth tides correction */
	Vector3d dTide = Vector3d::Zero();
	if	( acsConfig.tide_solid
		||acsConfig.tide_otl
		||acsConfig.tide_pole)
	{
		tidedisp(trace, gpst2utc(obsList.front().time), rtk.sol.sppRRec, &nav.erp, opt->odisp[0], dTide);
	}

	int stat = ppp_filter(trace, obsList, dTide, rtk, refstat);

	if (stat == SOLQ_PPP)
	{
		/* update solution status */
		update_stat(rtk, obsList, stat);

		if (rtk.sol.stat)
		{
			pppoutstat(trace, rtk.pppState);
			rtk.pppState.outputStates(trace);
		}
	}
}

/** \file
* ppp.c : precise point positioning
*
* references :
*    [1] D.D.McCarthy, IERS Technical Note 21, IERS Conventions 1996, July 1996
*    [2] D.D.McCarthy and G.Petit, IERS Technical Note 32, IERS Conventions
*        2003, November 2003
*    [3] D.A.Vallado, Fundamentals of Astrodynamics and Applications 2nd ed,
*        Space Technology Library, 2004
*    [4] J.Kouba, A Guide to using International GNSS Service (IGS) products,
*        May 2009
*    [5] RTCM Paper, April 12, 2010, Proposed SSR Messages for SV Orbit Clock,
*        Code Biases, URA
*    [6] MacMillan et al., Atmospheric gradients and the VLBI terrestrial and
*        celestial reference frames, Geophys. Res. Let., 1997
*    [7] G.Petit and B.Luzum (eds), IERS Technical Note No. 36, IERS
*         Conventions (2010), 2010
*    [8] J.Kouba, A simplified yaw-attitude model for eclipsing GPS satellites,
*        GPS Solutions, 13:1-12, 2009
*    [9] F.Dilssner, GPS IIF-1 satellite antenna phase center and attitude
*        modeling, InsideGNSS, September, 2010
*    [10] F.Dilssner, The GLONASS-M satellite yaw-attitude model, Advances in
*        Space Research, 2010
*    [11] IGS MGEX (http://igs.org/mgex)
*/

#include <vector>

using std::vector;

#include "observations.hpp"
#include "streamTrace.hpp"
#include "linearCombo.hpp"
#include "corrections.hpp"
#include "navigation.hpp"
#include "testUtils.hpp"
#include "acsConfig.hpp"
#include "constants.h"
#include "satStat.hpp"
#include "preceph.hpp"
#include "station.hpp"
#include "algebra.hpp"
#include "constants.h"
#include "common.hpp"
#include "wancorr.h"
#include "antenna.h"
#include "tides.hpp"
#include "enums.h"
#include "ppp.hpp"
#include "vmf3.h"
#include "trop.h"

#include "eigenIncluder.hpp"

#define VAR_IONO    	SQR(60.0)       // init variance iono-delay
#define VAR_IONEX   	SQR(0.0)
#define EFACT_GPS_L5 	10.0           	// error factor of GPS/QZS L5
#define ERR_BRDCI   	0.5             // broadcast iono model error factor

map<string, vector<double>> plotVectors;


/** exclude meas of eclipsing satellite (block IIA)
 */
void testeclipse(
	ObsList&	obsList)
{
	double erpv[5] = {0};

	/* unit vector of sun direction (ecef) */
	Vector3d rsun;
	sunmoonpos(gpst2utc(obsList.front().time), erpv, rsun.data(), NULL, NULL);
	Vector3d esun = rsun.normalized();

	for (auto& obs : obsList)
	{
		if (obs.exclude)
		{
			continue;
		}

		double r = obs.rSat.norm();
		if (r <= 0)
			continue;

		/* only block IIA */
// 		if (obs.satNav_ptr->pcv.type == "BLOCK IIA")		//todo take from the satSys object
// 			continue;

		/* sun-earth-satellite angle */
		double cosa = obs.rSat.dot(esun) / r;
		if (cosa < -1)	cosa = -1;
		if (cosa > +1)	cosa = +1;
		double ang = acos(cosa);

		/* test eclipse */
		if (ang < PI / 2
			|| r * sin(ang) > RE_WGS84)
			continue;

// 		trace(3, "eclipsing sat excluded %s sat=%s\n", obs.time.to_string(0).c_str(), obs.Sat.id().c_str());

		obs.excludeEclipse = true;
	}
}

/** Calculate nominal yaw-angle
 */
double yaw_nominal(
	double	beta,
	double	mu)
{
	if	( fabs(beta)	< 1E-12
		&&fabs(mu)		< 1E-12)
		return PI;

	return atan2(-tan(beta), sin(mu)) + PI;
}

/** Satellite attitude model
 */
int sat_yaw(
	GTime		time,
	Vector3d&	rSat,		///< Satellite position (ECEF)
	Vector3d&	vSat,		///< Satellite velocity (ECEF)
	Vector3d&	exs,		///< Output unit vector XS
	Vector3d&	eys)		///< Output unit vector YS
{
	Vector3d	rSun;
	double		erpv[5] = {};
	sunmoonpos(gpst2utc(time), erpv, rSun.data(), NULL, NULL);

	Vector3d vSatPrime = vSat;

	/* beta and orbit angle */
	vSatPrime[0] -= OMGE * rSat[1];
	vSatPrime[1] += OMGE * rSat[0];

	Vector3d n = rSat.	cross(vSatPrime);
	Vector3d p = rSun.	cross(n);

	Vector3d es		= rSat.	normalized();
	Vector3d eSun	= rSun.	normalized();
	Vector3d en		= n.	normalized();
	Vector3d ep		= p.	normalized();

	double beta	= PI / 2 - acos(eSun.dot(en));
	double E	= acos(es.dot(ep));
	double mu	= PI / 2 + (es.dot(eSun) <= 0 ? -E : E);
	if      (mu < -PI / 2) mu += 2 * PI;
	else if (mu >= PI / 2) mu -= 2 * PI;

	/* yaw-angle of satellite */
	double yaw = yaw_nominal(beta, mu);

	/* satellite fixed x,y-vector */
	Vector3d ex = en.cross(es);

	double cosy = cos(yaw);
	double siny = sin(yaw);

	eys = -cosy * en - siny * ex;
	exs = -siny * en + cosy * ex;

	return 1;
}

/** phase windup model
 */
int model_phw(
	GTime		time,	///< Time
	Obs&		obs,	///< Observation detailing the satellite to apply model to
	Vector3d&	rRec,	///< Position of receiver (ECEF)
	double&		phw)	///< Output of phase windup result
{
	/* satellite yaw attitude model */
	Vector3d exs;
	Vector3d eys;
	if (!sat_yaw(time, obs.rSat, obs.satVel, exs, eys))
		return 0;

	/* non block IIR satellites, to be refined for other constellations */
	/* if ((!strstr(type,"BLOCK IIR"))&&(sys!=SYS_GAL)) { */

	/* all the satellite orientation follow block II-A, 21/03/2019*/
	if (1)
	{
		exs *= -1;
		eys *= -1;
	}

	/* unit vector satellite to receiver */
	Vector3d r = rRec - obs.rSat;
	Vector3d ek = r.normalized();

	/* unit vectors of receiver antenna */
	double E[9];
	double pos[3];
	ecef2pos(rRec.data(), pos);
	xyz2enu(pos, E);
	Vector3d exr;
	Vector3d eyr;
	exr(0) =  E[1];
	exr(1) =  E[4];
	exr(2) =  E[7]; /* x = north */

	eyr(0) = -E[0];
	eyr(1) = -E[3];
	eyr(2) = -E[6]; /* y = west  */

	/* phase windup effect */
	Vector3d eks = ek.cross(eys);
	Vector3d ekr = ek.cross(eyr);

	Vector3d ds = exs - ek * ek.dot(exs) - eks;
	Vector3d dr = exr - ek * ek.dot(exr) + ekr;
	double cosp = ds.dot(dr) / ds.norm() / dr.norm();
	if      (cosp < -1) cosp = -1;
	else if (cosp > +1) cosp = +1;
	double ph = acos(cosp) / 2 / PI;
	Vector3d drs = ds.cross(dr);

	if (ek.dot(drs) < 0)
		ph *= -1;

	phw = ph + floor(phw - ph + 0.5); /* in cycle */

	return 1;
}

/** get meterological parameters
 */
void getmet(
	double	lat,
	double*	met)
{
	const double metprm[][10] = /* lat=15,30,45,60,75 */
	{
		{1013.25, 299.65, 26.31, 6.30E-3, 2.77,  0.00, 0.00, 0.00, 0.00E-3, 0.00},
		{1017.25, 294.15, 21.79, 6.05E-3, 3.15, -3.75, 7.00, 8.85, 0.25E-3, 0.33},
		{1015.75, 283.15, 11.66, 5.58E-3, 2.57, -2.25, 11.00, 7.24, 0.32E-3, 0.46},
		{1011.75, 272.15, 6.78, 5.39E-3, 1.81, -1.75, 15.00, 5.36, 0.81E-3, 0.74},
		{1013.00, 263.65, 4.11, 4.53E-3, 1.55, -0.50, 14.50, 3.39, 0.62E-3, 0.30}
	};

	lat = fabs(lat);

	if      (lat <= 15) for (int i = 0; i < 10; i++) met[i] = metprm[0][i];
	else if (lat >= 75) for (int i = 0; i < 10; i++) met[i] = metprm[4][i];
	else
	{
		int j = (int)(lat / 15);
		double a = (lat - j * 15) / 15.0;

		for (int i = 0; i < 10; i++)
		{
			met[i] = (1 - a) * metprm[j - 1][i] + a * metprm[j][i];
		}
	}
}

/* tropospheric delay correction -----------------------------------------------
* compute sbas tropospheric delay correction (mops model)
* args   : gtime_t time     I   time
*          double   *pos    I   receiver position {lat,lon,height} (rad/m)
*          double   *azel   I   satellite azimuth/elavation (rad)
*          double   *var    O   variance of troposphric error (m^2)
* return : slant tropospheric delay (m)
*-----------------------------------------------------------------------------*/
double sbstropcorr(
	GTime			time,			///< Time
	Vector3d&		rRec,			///< Receiver position (ECEF)
	double			el,				///< Satellite elevation
	double*			var)			///< Optional variance output
{
	double pos[3];
	ecef2pos(rRec.data(), pos);
	const double k1	= 77.604;
	const double k2	= 382000;
	const double rd	= 287.054;
	const double gm	= 9.784;
	const double g	= 9.80665;

// 	trace(4, "sbstropcorr: pos=%.3f %.3f azel=%.3f\n",
// 			pos[0]*R2D,
// 			pos[1]*R2D,
// 			el*R2D);

	if	( pos[2]	< -100
		||pos[2]	> +10000
		||el		<= 0)
	{
		if (var)
			*var = 0;

		return 0;
	}

	double met[10];
	getmet(pos[0] * R2D, met);

	double c = cos(2 * PI * (time2doy(time) - (pos[0] >= 0 ? 28 : 211)) / 365.25);
	for (int i = 0; i < 5; i++)
	{
		met[i] -= met[i + 5] * c;
	}
	double zh = 1E-6 * k1 * rd * met[0] / gm;
	double zw = 1E-6 * k2 * rd / (gm * (met[4] + 1.0) - met[3] * rd) * met[2] / met[1];

	double h = pos[2];
	zh *= pow(1 - met[3] * h / met[1], g / (rd * met[3]));
	zw *= pow(1 - met[3] * h / met[1], (met[4] + 1) * g / (rd * met[3]) - 1);

	double sinel = sin(el);
	double m = 1.001 / sqrt(0.002001 + sinel * sinel);
	if (var)
		*var = SQR(0.12 * m);
	return (zh + zw) * m;
}

/** Antenna corrected measurement
 */
void corr_meas(
	Trace&		trace,		///< Trace file to output to
	Obs&		obs,		///< Observation to correct measurements of
	E_FType		ft,			///< Frequency type to correct
	double		el,			///< Satellite elevation
	double		dAntRec,
	double		dAntSat,
	double		phw,		///< Phase wind up
	ClockJump&	cj,			///< Clock jump
	sta_t&		station)	//todo aaron, is this for this station, or a reference?
{
	TestStack ts(__FUNCTION__);

	int lv = 3;

	E_Sys	sys = obs.Sat.sys;
	int		prn = obs.Sat.prn;

	double ep[6];
	time2epoch(obs.time, ep);
	double	mjd = ymdhms2jd(ep) - JD2MJD;

	/* receiver clock millisecond jump (m) */
	double jump = cj.msJump * CLIGHT * 1e-3;

	SatNav& satNav = *obs.satNav_ptr;

// 	for (auto& [ft, sig] : obs.Sigs)
	Sig& sig = obs.Sigs[ft];
	for (int o = 0; o < 1; o++)
	{
		double lam = obs.satNav_ptr->lam[ft];
		if	( lam	== 0
			||sig.L	== 0
			||sig.P	== 0)
			continue;

		if (testsnr(0, 0, el, sig.snr, &acsConfig.snrmask)) 	//todo aaron, get rid of snr scaling everywhere
			continue;

		double dcbfact = SQR(lam) / (SQR(satNav.lam[F1]) - SQR(satNav.lam[F2]));

		/* antenna phase center and phase windup correction */	//todo aaron, dAntSat and dAntRec need to be signal tied
		if (1)
		{
			sig.L_corr_m = sig.L * lam	- dAntSat - dAntRec - phw * lam; 	//+(cj[1]==1?jump:0);
			sig.P_corr_m = sig.P       	- dAntSat - dAntRec; 				//+jump;		//todo aaron, clock jumps not used?
		}
		else
		{
			sig.L_corr_m = sig.L * lam	- dAntSat - dAntRec + jump - phw * lam; 	//+(cj[1]==1?jump:0);
			sig.P_corr_m = sig.P       	- dAntSat - dAntRec + jump; 				//+jump;		//todo aaron, clock jumps not used?
		}


		E_ObsCode code = sig.code;

		tracepde(lv, trace, " %.6f %s  OBS TYPE L%d          = %3d\n",	  mjd, obs.Sat.id().c_str(), ft + 1, sig.code);
		tracepde(lv, trace, " %.6f %s  Rec clock jump       = %14.4f\n",  mjd, obs.Sat.id().c_str(), jump);

		/* RINEX 2 DCB */
		if (nav.optbias != 1)
		{
			int dcbsys;
			if 		(sys == +E_Sys::GPS)	dcbsys = 0;
			else if (sys == +E_Sys::GLO)	dcbsys = 1;
			else 							continue;	//todo aaron other sys?

			//todo aaron, this used to enumerate only a few options, should it be as general as it is now?

			int dcbIndex;
			if 		(ft == F1)		dcbIndex = 1;	//todo aaron enums please
			else if (ft == F2)		dcbIndex = 2;
			else 						continue;		//todo aaron, L5 corrections?

			sig.P_corr_m += satNav.	cbias			[dcbIndex];    		/* sat dcb */
			sig.P_corr_m += station.rbias[dcbsys]	[dcbIndex];			/* rec dcb */


			tracepde(lv,trace,"   P1-P2 DCB %s %2d %.6f %.6f %.6f\n",obs.Sat.id().c_str(),ft, dcbfact,satNav.cbias[0],station.rbias[dcbsys][0]);
			if (acsConfig.ionoOpts.corr_mode == E_IonoMode::TOTAL_ELECTRON_CONTENT) /* dcb for ionosphere: warning, this will only work for F1 and F2 */
			{
				sig.P_corr_m -= dcbfact * satNav. cbias			[0];    		/* sat p1-p2 dcb for ionosphere */
				sig.P_corr_m -= dcbfact * station.rbias[dcbsys]	[0];			/* rec p1-p2 dcb for ionosphere */
			}

			/* P1-C1,P2-C2 dcb correction (C1->P1,C2->P2) */
			tracepde(lv,trace," %.6f %s  DCB L%d               = %14.4f %14.4f %3s\n",
					mjd, obs.Sat.id().c_str(), dcbIndex,
					satNav.cbias[dcbIndex],
					station.rbias[dcbsys][dcbIndex],
					station.name);
		}
		/* RINEX 3 MGEX DCB CAS or DLR products */
		else
		{
			double dcb[2]		= {};
			double iondcb[2]	= {};		/* ADDED FOR IONOSF */
			string code1		= "";

			if (sys == +E_Sys::GPS)
			{
				/* find code type */
				if 		(code == +E_ObsCode::L2X) 	code1 = "C2X";			//todo aaron, lookup table/enum strings.
				else if (code == +E_ObsCode::L2L) 	code1 = "C2L";
				else if (code == +E_ObsCode::L2S)	code1 = "C2S";
				else if (code == +E_ObsCode::L1C) 	code1 = "C1C";
				else
				{
					if 	( (ft == F1 && code != +E_ObsCode::L1W)
						||(ft == F2 && code != +E_ObsCode::L2W))
					{
						fprintf(stdout, "No mgex dcb for sys %s, obs %s!!\n", sys._to_string(), code._to_string());
					}

					continue;
				}

				/* GPS C1C-C1W, C2() -C2W */
				if 		(ft == F1)	searchdcb(&nav.mbias, NULL, 'G', obs.Sat.prn, code1, "C1W", dcb);
				else if (ft == F2)	searchdcb(&nav.mbias, NULL, 'G', obs.Sat.prn, code1, "C2W", dcb);

				if (acsConfig.ionoOpts.corr_mode == E_IonoMode::TOTAL_ELECTRON_CONTENT) /* dcb for ionosphere: warning, this will only work for F1 and F2 */
				{
					searchdcb(&nav.mbias, NULL, 'G', obs.Sat.prn, "C1W", "C2W", iondcb);
				}
			}
			else if (sys == +E_Sys::CMP)
			{
				double corr = 0;

				/* satellite-induced code bias */
				if	( code == +E_ObsCode::L1I
					||code == +E_ObsCode::L7I
					||code == +E_ObsCode::L6I)
				{
					corr = wancorr(prn, el * R2D, ft + 1);
					sig.P_corr_m += corr;
				}

				if 	( code == +E_ObsCode::L1Q
					||code == +E_ObsCode::L2Q)
				{
					code1 = "C2Q";
				}
				else if (code == +E_ObsCode::L6Q)	code1 = "C6Q";
				else if (code == +E_ObsCode::L7Q)	code1 = "C7Q";
				else
				{
					if 	( (ft == F1 && code != +E_ObsCode::L1I)
						||(ft == F2 && code != +E_ObsCode::L7I))
					{
						fprintf(stdout, "No mgex dcb for sys %s, obs %s!!\n", sys._to_string(), code._to_string());
					}

					continue;
				}

				/* align signal to C2I or align signal to C7I */
				if 		(ft == F1) searchdcb(&nav.mbias, NULL, 'C', obs.Sat.prn, code1, "C2I", dcb);
				else if (ft == F2) searchdcb(&nav.mbias, NULL, 'C', obs.Sat.prn, code1, "C7I", dcb);

				if (acsConfig.ionoOpts.corr_mode == E_IonoMode::TOTAL_ELECTRON_CONTENT) /* dcb for ionosphere: warning, this will only work for F1 and F2 */
				{
					searchdcb(&nav.mbias, NULL, 'C', obs.Sat.prn, "C2I", "C7I", iondcb);
				}
			}
			else if (sys == +E_Sys::GAL)
			{
				if 		(ft == F1) searchdcb(&nav.mbias, NULL, 'E', obs.Sat.prn, code1, "C1C", dcb);
				else if (ft == F5) searchdcb(&nav.mbias, NULL, 'E', obs.Sat.prn, code1, "C5Q", dcb);

				if (acsConfig.ionoOpts.corr_mode == E_IonoMode::TOTAL_ELECTRON_CONTENT) /* dcb for ionosphere: warning, this will only work for F1 and F2 */
				{
					dcbfact = SQR(lam) / (SQR(satNav.lam[F1]) - SQR(satNav.lam[F5]));	//todo aaron, does this 'lam' have to refer to one of the other? does it mean anything in this case? refactor?
					searchdcb(&nav.mbias, NULL, 'E', obs.Sat.prn, "C1C", "C5Q", iondcb);
				}
			}

			if (iondcb[0] == 0)
				iondcb[0] = satNav.cbias[0];    						/* sat p1-p2 dcb for ionosphere from IONEX*/

			sig.P_corr_m -= dcb[0];
			sig.P_corr_m -= dcbfact * iondcb[0];
			tracepde(lv,trace,"   SINEX DCB %s %2d %.6f %.6f %.6f\n",obs.Sat.id().c_str(),ft+1, dcbfact,satNav.cbias [0],dcb[0]);

			//todo aaron, need variance here?
		}
	}
}


/* satellite antenna phase center variation ----------------------------------*/
void satantpcv(
	Vector3d&	rs,
	Vector3d&	rr,
	pcvacs_t&	pcv,
	double*		dAntSat,
	double*		nad = nullptr)
{
	Vector3d ru = rr - rs;
	Vector3d rz = -rs;
	Vector3d eu = ru.normalized();
	Vector3d ez = rz.normalized();

	double cosa = eu.dot(ez);
	if (cosa < -1)	cosa = -1;
	if (cosa > +1)	cosa = +1;

	double nadir = acos(cosa);
	if (nad)
		*nad = nadir * R2D;

	interp_satantmodel(&pcv, nadir, dAntSat);
	//antmodel_s(pcv,nadir,dAntSat);
}

/* precise tropospheric model ------------------------------------------------*/
double trop_model_prec(
	gtime_t		time,
	double*		pos,
	double*		azel,
	double*		tropStates,
	double*		dTropDx,
	double&		var)
{
	double map[2] = {0};

	/* zenith hydrostatic delay */
	double zhd = tropacs(pos, azel, map);

	if (acsConfig.process_user)
	{
		/* mapping function */
		double m_w;
		double m_h = tropmapf(time, pos, azel, &m_w);

		if (azel[1] > 0)
		{
			/* m_w=m_0+m_0*cot(el)*(Gn*cos(az)+Ge*sin(az)): ref [6] */
			double cotz = 1 / tan(azel[1]);
			double grad_n = m_w * cotz * cos(azel[0]);
			double grad_e = m_w * cotz * sin(azel[0]);

			m_w += grad_n * tropStates[1];
			m_w	+= grad_e * tropStates[2];

			dTropDx[1] = grad_n * (tropStates[0] - zhd);
			dTropDx[2] = grad_e * (tropStates[0] - zhd);
		}

		dTropDx[0]	= m_w;
		var			= SQR(0.01);		//todo aaron, move this somewhere else, should use trop state variance?
		double value	= m_h * zhd
						+ m_w * (tropStates[0] - zhd);
		return value;
	}
	else
	{
		/* wet mapping function */
		dTropDx[0]	= map[1];
		var			= SQR(0.01);

		return map[0] * zhd;
	}
}

/* tropospheric model ---------------------------------------------------------*/
int model_trop(
	gtime_t		time,
	double*		pos,
	double*		azel,
	double*		tropStates,
	double*		dTropDx,
	double&		dTrp,
	double&		var)
{
	if	( acsConfig.tropOpts.corr_mode == TROPOPT_EST
		||acsConfig.tropOpts.corr_mode == TROPOPT_ESTG)
	{
		dTrp = trop_model_prec(time, pos, azel, tropStates, dTropDx, var);
		return 1;
	}

	return 1;
}

/* ionospheric model ---------------------------------------------------------*/
int model_iono(
	gtime_t		time,
	double*		pos,
	double*		azel,
	double		ionoState,
	double&		dion,
	double&		var)
{
	switch (acsConfig.ionoOpts.corr_mode)
	{
		case E_IonoMode::TOTAL_ELECTRON_CONTENT:
		{
			int res = iontec(time, &nav, pos, azel, 1, dion, var);
			if (res)	var +=	VAR_IONEX;			// adding some extra errors to reflect modelling errors
			else		var =	VAR_IONO;

			return res;
		}
		case E_IonoMode::BROADCAST:
		{
			dion	= ionmodel(time, nav.ion_gps, pos, azel);
			var		= SQR(dion * ERR_BRDCI);

			return 1;
		}
		case E_IonoMode::ESTIMATE:
		{
			dion	= ionoState;
			var		= 0;

			return 1;
		}
		case E_IonoMode::IONO_FREE_LINEAR_COMBO:
		{
			dion	= 0;
			var		= 0;

			return 1;
		}
	}

	return 0;
}

void pppCorrections(
	Trace&		trace,
	ObsList&	obsList,
	Vector3d&	rRec,
	rtk_t&		rtk,
    sta_t&		refstat)
{
	double pos[3];
	ecef2pos(rRec.data(), pos);

	tracepde(3,trace, "pppCorrections  : n=%d\n", obsList.size());

	for (auto& obs : obsList)
	{
		if (obs.exclude)
		{
			continue;
		}

		TestStack ts(obs.Sat);
		SatStat&	satStat	= *(obs.satStat_ptr);
		double*		lam		= obs.satNav_ptr->lam;

		double r = geodist(obs.rSat, rRec, satStat.e);
		if 	( r <= 0
			||satStat.el < acsConfig.elevation_mask)
		{
			obs.excludeElevation = true;
			continue;
		}

		if	(satexclude(obs.Sat, obs.svh))
		{
			obs.exclude = true;
			continue;
		}

// 		if (acsConfig.antexacs == 0)
// 		{
// 			std::cout << " Using antmodel() " << std::endl;			//todo aaron, broke this.
// 			antmodel(opt->pcvr, opt->antdel, obs.azel, opt->posopt[1], dAntRec);
// 		}

		//satellite and receiver antenna model
		double dAntSat[NFREQ] = {};
		if	(acsConfig.satpcv)
		{
			double ep[6];
			time2epoch(obs.time, ep);

			string nullStr;
			pcvacs_t* pcvsat = findantenna(nullStr, obs.Sat.id(), ep, nav.pcvList, 1);
			if (pcvsat)
			{
				satantpcv(obs.rSat, rRec, *pcvsat, dAntSat);
			}
			else
			{
				tracepde(1, trace,	"Warning: no satellite (%s) pcv information\n",	obs.Sat.id().c_str());
				continue;
			}
		}

		// phase windup model
		if (acsConfig.phase_windup)
		{
			bool pass = model_phw(rtk.sol.time, obs, rRec, satStat.phw);
			if (pass == false)
			{
				continue;
			}
		}

		for (auto& [ft, sig] : obs.Sigs)
		{
			sig.Range = r;

			double dAntRec = 0;

			/* receiver pco correction to the coordinates */
			if (rtk.pcvrec)
			{
				if (ft < rtk.pcvrec->nf)
				{
					Vector3d pco_r;
					Vector3d dr2;

					recpco(rtk.pcvrec, obs.Sat.sysChar(), ft, pco_r);
	                enu2ecef(pos, pco_r.data(), dr2.data());    /* convert enu to xyz */

					/* get rec position and geometric distance for each frequency */
					Vector3d rRecFreq = rRec + dr2;

	                sig.Range = geodist(obs.rSat, rRecFreq, satStat.e);

					/* calculate pcv */
					double azDeg = satStat.az * R2D;
					double elDeg = satStat.el * R2D;
					recpcv(rtk.pcvrec, obs.Sat.sysChar(), ft, elDeg, azDeg, dAntRec);
				}
			}
			// corrected phase and code measurements
			ClockJump cj = {};
			corr_meas(trace, obs, ft, satStat.el, dAntRec, dAntSat[ft], satStat.phw, cj, refstat);
		}

		if (acsConfig.ionoOpts.corr_mode == E_IonoMode::IONO_FREE_LINEAR_COMBO)
		for (E_FType ft : {F2, F5})
		{
			/* iono-free LC */
			Sig sig1 = obs.Sigs[F1];
			Sig sig2 = obs.Sigs[ft];

			if	( lam[F1] == 0
				||lam[ft] == 0)
				continue;

			if	( (sig1.L_corr_m == 0)
				||(sig1.P_corr_m == 0)
				||(sig2.L_corr_m == 0)
				||(sig2.P_corr_m == 0))
			{
				continue;
			}

			double c1;
			double c2;
			S_LC lc = getLC(sig1.L_corr_m,	sig2.L_corr_m,
							sig1.P_corr_m,	sig2.P_corr_m,
							lam[F1],		lam[ft],
							&c1, 			&c2);

			Sig lcSig = {};
			lcSig.L_corr_m = lc.IF_Phas_m;
			lcSig.P_corr_m = lc.IF_Code_m;

			E_FType newType;
			switch (ft)
			{
				case F2: newType = FTYPE_IF12;	break;
				case F5: newType = FTYPE_IF15;	break;
				default: continue;
			}

			//update distance measurement for iflc
			lcSig.Range	= sig1.Range * c1
						- sig2.Range * c2;

			lcSig.codeVar	= POW4(lam[F1]) * sig1.codeVar / POW2(POW2(lam[F1]) - POW2(lam[ft]))
							+ POW4(lam[ft]) * sig2.codeVar / POW2(POW2(lam[F1]) - POW2(lam[ft]));

			lcSig.phasVar 	= POW4(lam[F1]) * sig1.phasVar / POW2(POW2(lam[F1]) - POW2(lam[ft]))
							+ POW4(lam[ft]) * sig2.phasVar / POW2(POW2(lam[F1]) - POW2(lam[ft]));

			obs.Sigs[newType] = lcSig;

			obs.satStat_ptr->sigStatMap[newType].slip.any	= obs.satStat_ptr->sigStatMap[F1].slip.any
															| obs.satStat_ptr->sigStatMap[ft].slip.any;
		}
	}
}

bool deweightMeas(
	Trace&		trace,
	KFState&	kfState,
	KFMeas&		kfMeas,
	int			index)
{
	trace << std::endl << "Deweighting " << kfMeas.obsKeys[index] << std::endl;

	kfMeas.R[index] *= SQR(acsConfig.deweight_factor);

	return true;
}

bool countSignalErrors(
	Trace&		trace,
	KFState&	kfState,
	KFMeas&		kfMeas,
	int			index)
{
	map<string, void*>& metaDataMap = kfMeas.metaDataMaps[index];

	Obs* obs_ptr = (Obs*) metaDataMap["obs_ptr"];

	if (obs_ptr == nullptr)
	{
		return true;
	}

	ObsKey&		obsKey	= kfMeas.obsKeys[index];
	Obs&		obs		= *obs_ptr;

	if (obsKey.type == "L")
	{
		//this is a phase observation
		obs.Sigs[(E_FType)obsKey.num].phaseError = true;
	}

	return true;
}





#include "ionoModel.hpp"
#include "enums.h"
#include "algebra.hpp"
#include "acsConfig.hpp"

#include <list>

using std::list;

static int yrstart		= 0;
static int doystart		= 0;
static int todstart		= 0;

static map<SatSys, sinexbias_t> sinexBiasList;

gtime_t tlast_sinex = {};


static void printExponent(Trace& trace, double d, bool sigma)
{
	int		exponent	= 0;
	double	base   		= 0;
	if (d != 0)
	{
		exponent	= (int) floor( log10( fabs(d) ) );
		base		= d * pow(10, -1 * exponent);
	}

	if (sigma)	tracepdeex(0, trace, " %8.6fE%+01d",	base, exponent);
	else		tracepdeex(0, trace, " %18.15fE%+01d",	base, exponent);
// 	tracepdeex(0, trace, " %*.*fE%+01d", w1, w2, base, exponent);
}

static void ionex_time(
	gtime_t time,
	int& year,
	int& doy,
	int& tod)
{
	double ep0[6] = {2000, 1, 1, 0, 0, 0};	//todo aaron, move somewhere else

	double ep[6];
	time2epoch(time, ep);

	ep0[0] = ep[0];

	int toy = (int) timediff(time, epoch2time(ep0));

	year = (int) ep[0];
	doy = toy / 86400 + 1;		//doy[0]=toy/86400;		//todo aaron, check this
	tod = toy % 86400;
}

static void write_bSINEX_head(
	gtime_t time,
	Trace& iondsb)
{
	int yr;
	int doy;
	int tod;
	gtime_t now = timeget();
	ionex_time(now, yr, doy, tod);

	tracepdeex(0, iondsb, "%%=BIA 1.00 AUS %4d:%03d:%05d AUS %4d:%03d:%05d", yr, doy, tod, yrstart, doystart, todstart);

	ionex_time(time, yr, doy, tod);
	tracepdeex(0, iondsb, " %4d:%03d:%05d %8d\n", yr, doy, tod, sinexBiasList.size());
	tracepdeex(0, iondsb, "*-------------------------------------------------------------------------------\n");
	tracepdeex(0, iondsb, "+FILE/REFERENCE\n");
	tracepdeex(0, iondsb, " DESCRIPTION       AUS, Position Australia, Geoscience Australia\n");			//todo aaron, get name from config
	tracepdeex(0, iondsb, " OUTPUT            AUS DSB estimates for day %3d, %4d\n", doystart, yrstart);
	tracepdeex(0, iondsb, " CONTACT           ken.harima@rmit.edu.au\n");
	tracepdeex(0, iondsb, " SOFTWARE          GINANionex\n");
	tracepdeex(0, iondsb, " INPUT             Daily 30-sec observations from AUSCORS stations\n");
	tracepdeex(0, iondsb, "-FILE/REFERENCE\n");
	tracepdeex(0, iondsb, "*-------------------------------------------------------------------------------\n");
	tracepdeex(0, iondsb, "+BIAS/DESCRIPTION\n");
	tracepdeex(0, iondsb, " OBSERVATION_SAMPLING                     %12d\n", 30);
	tracepdeex(0, iondsb, " PARAMETER_SPACING                        %12d\n", 86400);
	tracepdeex(0, iondsb, " BIAS_MODE                                RELATIVE\n");
	tracepdeex(0, iondsb, " TIME_SYSTEM                              G\n");
	tracepdeex(0, iondsb, "*-------------------------------------------------------------------------------\n");
}

static int new_sat_bias(
	SatSys Sat,
	gtime_t time,
	double dcb,
	double sigma)
{
	sinexbias_t sinexbias = {};

	sinexbias.Sat = Sat;
	strcpy(sinexbias.station, " ");

	switch (Sat.sys)
	{
		case E_Sys::GPS: strcpy(sinexbias.cod1, "C1C"); strcpy(sinexbias.cod2, "C2W"); break;
		case E_Sys::SBS: strcpy(sinexbias.cod1, "C1P"); strcpy(sinexbias.cod2, "C2P"); break;
		case E_Sys::GLO: strcpy(sinexbias.cod1, "C1X"); strcpy(sinexbias.cod2, "C5X"); break;
		case E_Sys::GAL: strcpy(sinexbias.cod1, "C1C"); strcpy(sinexbias.cod2, "C2X"); break;
		case E_Sys::QZS: strcpy(sinexbias.cod1, "C2I"); strcpy(sinexbias.cod1, "C7I"); break;
		case E_Sys::CMP: strcpy(sinexbias.cod1, "C2I"); strcpy(sinexbias.cod1, "C7I"); break;
		default: return 0;
	}

	sinexbias.bias		= dcb	* (1e9 / CLIGHT);
	sinexbias.biasstd	= sigma	* (1e9 / CLIGHT);
	sinexbias.slope		= 0;
	sinexbias.slopestd	= 0;

	ionex_time(tlast_sinex, sinexbias.tini[0], sinexbias.tini[1], sinexbias.tini[2]);
	ionex_time(time,		sinexbias.tend[0], sinexbias.tend[1], sinexbias.tend[2]);

	sinexBiasList[Sat] = sinexbias;

	return 1;
}

static void write_bSINEX_line(sinexbias_t& sinexbias, Trace& iondsb)
{
	tracepdeex(0, iondsb, " %4s %4s %3s %9s %4s %4s",
			"DSB ",
			sinexbias.Sat.svn().c_str(),
			sinexbias.Sat.id().c_str(),
			sinexbias.station,
			sinexbias.cod1,
			sinexbias.cod2);


	tracepdeex(0, iondsb, " %4d:%03d:%05d %4d:%03d:%05d %4s",
			sinexbias.tini[0],
			sinexbias.tini[1],
			sinexbias.tini[2],
			sinexbias.tend[0],
			sinexbias.tend[1],
			sinexbias.tend[2], "ns");

	printExponent(iondsb, sinexbias.bias,		false);
	printExponent(iondsb, sinexbias.biasstd,	true);
	/*Printbias(fout, sinexbias->slope);
	Printstd(fout, sinexbias->slopestd);*/
	tracepdeex(0, iondsb, "\n");
}

void iono_write_DSB(
	Trace&		trace,
	gtime_t		time,
	KFState&	kfState,
	bool		end)
{
	tracepdeex(2, trace, "  ..Loading Bias values.. \n");

    std::ofstream dsbfile(acsConfig.iondsb_filename, std::ofstream::app);

	if (yrstart == 0)
	{
		ionex_time(time, yrstart, doystart, todstart);
		tlast_sinex = time;
		tracepde(1, trace, "Initial time: %4d:%03d:%04d\n", yrstart, doystart, todstart);
		return;
	}

	for (auto& [key, index] : kfState.kfIndexMap)
	{
		if	( (key.type	== KF::DCB)
			&&(key.Sat	!= SatSys()))
		{
			double value = 0;
			double sigma = 0;
			kfState.getKFValue(key, value, &sigma);

			if (sigma > 1)
			{
				continue;
			}

			new_sat_bias(key.Sat, time, value, sigma);
		}
	}

	tracepde(1, trace, "New epoch: %4d:%03d:%04d  nbia:%d\n", yrstart, doystart, todstart, sinexBiasList.size());
	//tlast_sinex = time;

	if (end)
	{
		tracepdeex(2, trace, "  ..Writing Bias SINEX File.. \n");
		write_bSINEX_head(time, dsbfile);

		for (auto& [Sat,sinexBias] : sinexBiasList)
		{
			write_bSINEX_line(sinexBias, dsbfile);
		}

		tracepdeex(0, dsbfile, "*-------------------------------------------------------------------------------\n");
		tracepdeex(0, dsbfile, "%%=ENDBIA\n");
		yrstart = 0;
	}
}

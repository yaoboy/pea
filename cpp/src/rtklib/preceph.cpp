/*------------------------------------------------------------------------------
* preceph.c : precise ephemeris and clock functions
*
*          Copyright (C) 2007-2013 by T.TAKASU, All rights reserved.
*
* references :
*     [1] S.Hilla, The Extended Standard Product 3 Orbit Format (SP3-c),
*         12 February, 2007
*     [2] J.Ray, W.Gurtner, RINEX Extensions to Handle Clock Information,
*         27 August, 1998
*     [3] D.D.McCarthy, IERS Technical Note 21, IERS Conventions 1996, July 1996
*     [4] D.A.Vallado, Fundamentals of Astrodynamics and Applications 2nd ed,
*         Space Technology Library, 2004
*-----------------------------------------------------------------------------*/

#include <unordered_map>
#include <ctype.h>

#include "eigenIncluder.hpp"
#include "gaTime.hpp"
#include "acsStream.hpp"
#include "enums.h"
#include "common.hpp"
#include "constants.h"
#include "streamTrace.hpp"
#include "algebra.hpp"
#include "tides.hpp"


#define NMAX        10              /* order of polynomial interpolation */
#define MAXDTE      900.0           /* max time difference to ephem time (s) */
#define EXTERR_CLK  1E-3            /* extrapolation error for clock (m/s) */
#define EXTERR_EPH  5E-7            /* extrapolation error for ephem (m/s^2) */

/* satellite code to satellite system ----------------------------------------*/
E_Sys code2sys(char code)
{
    if (code=='G'||code==' ') return E_Sys::GPS;
    if (code=='R') return E_Sys::GLO;
    if (code=='E') return E_Sys::GAL; /* extension to sp3-c */
    if (code=='J') return E_Sys::QZS; /* extension to sp3-c */
    if (code=='C') return E_Sys::CMP; /* extension to sp3-c */
    if (code=='L') return E_Sys::LEO; /* extension to sp3-c */
    return E_Sys::NONE;
}
/* read sp3 header -----------------------------------------------------------*/
int readsp3h(FILE *fp, gtime_t *time, char *type, int *sats,
                    double *bfact, char *tsys)
{
    int i,j,k=0,ns=0,prn;
    char buff[1024];

//     trace(3,"readsp3h:\n");
    for (i=0;i<22;i++) {
        if (!fgets(buff,sizeof(buff),fp)) break;

        if (i==0) {
            *type=buff[2];
            if (str2time(buff,3,28,time)) return 0;
        }
        else if (2<=i&&i<=6) {
            if (i==2) {
                ns=(int)str2num(buff,4,2);
            }
            for (j=0;j<17&&k<ns;j++) {
                E_Sys sys=code2sys(buff[9+3*j]);
                prn=(int)str2num(buff,10+3*j,2);
                if (k<MAXSAT) sats[k++]=SatSys(sys,prn);
            }
        }
        else if (i==12) {
            strncpy(tsys,buff+9,3); tsys[3]='\0';
        }
        else if (i==14) {
            bfact[0]=str2num(buff, 3,10);
            bfact[1]=str2num(buff,14,12);
        }
    }
    return ns;
}

/* add precise ephemeris -----------------------------------------------------*/
int addpeph(nav_t *nav, Peph& peph)
{
 	nav->pephMap[peph.Sat][peph.time] = peph;

    return 1;
}

/* read sp3 body -------------------------------------------------------------*/
void readsp3b(FILE *fp, char type, int *sats, int ns, double *bfact,
                     char *tsys, int index, int opt, nav_t *nav)
{
    gtime_t time;
    double val,std,base;
    int i,j,prn,n=ns*(type=='P'?1:2),pred_o,pred_c;
    char buff[1024];

//     trace(3,"readsp3b: type=%c ns=%d index=%d opt=%d\n",type,ns,index,opt);
//     printf("In sp3 read body\n");

    while (fgets(buff,sizeof(buff),fp)) {

        if (!strncmp(buff,"EOF",3)) break;

        if (buff[0]!='*'||str2time(buff,3,28,&time)) {
//             trace(2,"sp3 invalid epoch %31.31s\n",buff);
            continue;
        }
        if (!strcmp(tsys,"UTC"))
			time=utc2gpst(time); /* utc->gpst */

        for (i=pred_o=pred_c=0;i<n&&fgets(buff,sizeof(buff),fp);i++)
		{

            if (strlen(buff)<4||(buff[0]!='P'&&buff[0]!='V'))
				continue;

            E_Sys sys=buff[1]==' ' ? +E_Sys::GPS : code2sys(buff[1]);
            prn=(int)str2num(buff,2,2);
            if      (sys == +E_Sys::SBS) prn+=100;
            else if (sys == +E_Sys::QZS) prn+=192; /* extension to sp3-c */

			SatSys Sat;
			Sat.sys = sys;
			Sat.prn = prn;
            if (!Sat)
				continue;

			Peph peph 	= {};
			peph.time 	= time;
			peph.index	= index;
			peph.Sat	= Sat;
			int v = 0;

            if (buff[0]=='P')
			{
                pred_c=strlen(buff)>=76&&buff[75]=='P';
                pred_o=strlen(buff)>=80&&buff[79]=='P';
            }
            for (j=0;j<4;j++)
			{
                /* read option for predicted value */
                if (j< 3&&(opt&1)&& pred_o) continue;
                if (j< 3&&(opt&2)&&!pred_o) continue;
                if (j==3&&(opt&1)&& pred_c) continue;
                if (j==3&&(opt&2)&&!pred_c) continue;

                val=str2num(buff, 4+j*14,14);
                std=str2num(buff,61+j* 3,j<3?2:3);

                if (buff[0]=='P')
				{ /* position */
                    if (val!=0.0&&fabs(val-999999.999999)>=1E-6)
					{
                        peph.pos[j]=val*(j<3?1000.0:1E-6);
                        v=1; /* valid epoch */
                    }
                    if ((base=bfact[j<3?0:1])>0.0&&std>0.0)
					{
                        peph.std[j]=(pow(base,std)*(j<3?1E-3:1E-12));
                    }
                }
                else if (v)
				{ /* velocity */
                    if (val!=0.0&&fabs(val-999999.999999)>=1E-6)
					{
                        peph.vel[j]=val*(j<3?0.1:1E-10);
                    }
                    if ((base=bfact[j<3?0:1])>0.0&&std>0.0)
					{
                        peph.vst[j]=(pow(base,std)*(j<3?1E-7:1E-16));
                    }
                }
            }
			if (v)
			{
				if (!addpeph(nav,peph))
					return;
			}
        }
    }
}

/* read sp3 precise ephemeris file ---------------------------------------------
* read sp3 precise ephemeris/clock files and set them to navigation data
* args   : char   *file       I   sp3-c precise ephemeris file
*                                 (wind-card * is expanded)
*          nav_t  *nav        IO  navigation data
*          int    opt         I   options (1: only observed + 2: only predicted +
*                                 4: not combined)
* return : none
* notes  : see ref [1]
*          precise ephemeris is appended and combined
*          nav->peph and nav->ne must by properly initialized before calling the
*          function
*          only files with extensions of .sp3, .SP3, .eph* and .EPH* are read
*-----------------------------------------------------------------------------*/
void readsp3(const char *file, nav_t *nav, int opt)
{
    FILE *fp;
    gtime_t time={0};
    double bfact[2]={0};
    int i,j,n,ns,sats[MAXSAT]={0};
    char *efiles[MAXEXFILE],*ext,type=' ',tsys[4]="";

//     trace(3,"readpephs: file=%s\n",file);
    for (i=0;i<MAXEXFILE;i++) {
        if (!(efiles[i]=(char *)malloc(1024))) {
            for (i--;i>=0;i--) free(efiles[i]);
            return;
        }
    }
    /* expand wild card in file path */
    n=expath(file,efiles,MAXEXFILE);

    for (i=j=0;i<n;i++) {
        if (!(ext=strrchr(efiles[i],'.'))) continue;

        if (!strstr(ext+1,"sp3")&&!strstr(ext+1,".SP3")&&
            !strstr(ext+1,"eph")&&!strstr(ext+1,".EPH")) continue;

        if (!(fp=fopen(efiles[i],"r"))) {
//             trace(2,"sp3 file open error %s\n",efiles[i]);
            continue;
        }
        /* read sp3 header */
        ns=readsp3h(fp,&time,&type,sats,bfact,tsys);

        /* read sp3 body */
        readsp3b(fp,type,sats,ns,bfact,tsys,j++,opt,nav);

        fclose(fp);
    }
    for (i=0;i<MAXEXFILE;i++) free(efiles[i]);

}
/* read satellite antenna parameters -------------------------------------------
* read satellite antenna parameters
* args   : char   *file       I   antenna parameter file
*          gtime_t time       I   time
*          nav_t  *nav        IO  navigation data
* return : status (1:ok,0:error)
* notes  : only support antex format for the antenna parameter file
*-----------------------------------------------------------------------------*/
/*extern int readsap(const char *file, gtime_t time, nav_t *nav)
{
    pcvs_t pcvs={0};
    pcv_t pcv0={0},*pcv;
    int i;

    trace(3,"readsap : file=%s time=%s\n",file,time.to_string(0).c_str());

    if (!readpcv(file,&pcvs)) return 0;

    for (i=0;i<MAXSAT;i++) {
        pcv=searchpcv(i+1,"",time,&pcvs);
        nav->pcvs[i]=pcv?*pcv:pcv0;
    }
    free(pcvs.pcv);
    return 1;
}*/
/* read dcb parameters file --------------------------------------------------*/
int readdcbf(const char *file, nav_t *nav, list<sta_t*>& stations)
{
	//todo aaron, use maps for these rather than arrays,
    FILE *fp;
    double cbias;
    char buff[256],str1[32],str2[32]="";
    int j,type=0;
	SatSys Sat;
//     trace(3,"readdcbf: file=%s\n",file);

    if (!(fp=fopen(file,"r")))
	{
//         trace(2,"dcb parameters file open error: %s\n",file);
        return 0;
    }
    while (fgets(buff,sizeof(buff),fp))
	{
        if      (strstr(buff,"DIFFERENTIAL (P1-P2) CODE BIASES")) type=1;
        else if (strstr(buff,"DIFFERENTIAL (P1-C1) CODE BIASES")) type=2;
        else if (strstr(buff,"DIFFERENTIAL (P2-C2) CODE BIASES")) type=3;

        if (!type||sscanf(buff,"%s %s",str1,str2)<1) continue;

        if ((cbias=str2num(buff,26,9))==0.0) continue;

        if  ( !strcmp(str1,"G")
			||!strcmp(str1,"R"))
		{
			/* receiver dcb */
			for (auto& station : stations)
			{
				if (!strcmp(station->name, str2))
				{
					j = !strcmp(str1,"G") ? 0 : 1;
					station->rbias[j][type-1] = cbias*1E-9*CLIGHT; /* ns -> m */
					break;		//todo aaron, this looks like it had issues to begin with
				}
			}
        }
        else if (Sat=SatSys(str1), Sat)
		{
			/* satellite dcb */
            nav->satNavMap[Sat].cbias[type-1]=cbias*1E-9*CLIGHT; /* ns -> m */
        }
    }
    fclose(fp);

    return 1;
}
/* read dcb parameters ---------------------------------------------------------
* read differential code bias (dcb) parameters
* args   : char   *file       I   dcb parameters file (wild-card * expanded)
*          nav_t  *nav        IO  navigation data
*          sta_t  *sta        I   station info data to import receiver dcb
*                                 (NULL: no use)
* return : status (1:ok,0:error)
* notes  : currently only p1-c1 bias of code *.dcb file
*-----------------------------------------------------------------------------*/
extern int readdcb(const char *file, nav_t *nav, list<sta_t*>& stations)
{
    int i,j,n;
    char *efiles[MAXEXFILE]={0};

//     trace(3,"readdcb : file=%s\n",file);

//     for (i=0;i<MAXSAT;i++)
// 	for (j=0;j<3;j++)
// 	{
//         nav->cbias[i][j]=0.0;
//     }
    for (i=0;i<MAXEXFILE;i++)
	{
        if (!(efiles[i]=(char *) malloc(1024)))
		{
            for (i--;i>=0;i--)
				free(efiles[i]);
			return 0;
        }
    }
    n = expath(file,efiles,MAXEXFILE);

    for (i=0;i<n;i++)
	{
        readdcbf(efiles[i],nav,stations);
    }
    for (i=0;i<MAXEXFILE;i++)
		free(efiles[i]);

    return 1;
}
/* add satellite fcb ---------------------------------------------------------*/
int addfcb(nav_t *nav, gtime_t ts, gtime_t te, int sat,
                  const double *bias, const double *std)	//todo aaron, is this broken?
{
//     fcbd_t *nav_fcb;
//     int i,j;
//
//     if (nav->nf>0&&fabs(timediff(ts,nav->fcb[nav->nf-1].ts))<=1e-3) {
//         for (i=0;i<3;i++) {
//             nav->fcb[nav->nf-1].bias[sat-1][i]=bias[i];
//             nav->fcb[nav->nf-1].std [sat-1][i]=std [i];
//         }
//         return 1;
//     }
//     if (nav->nf>=nav->nfmax) {
//         nav->nfmax=nav->nfmax<=0?2048:nav->nfmax*2;
//         if (!(nav_fcb=(fcbd_t *)realloc(nav->fcb,sizeof(fcbd_t)*nav->nfmax))) {
//             free(nav->fcb); nav->nf=nav->nfmax=0;
//             return 0;
//         }
//         nav->fcb=nav_fcb;
//     }
//     for (i=0;i<MAXSAT;i++) for (j=0;j<3;j++) {
//         nav->fcb[nav->nf].bias[i][j]=nav->fcb[nav->nf].std[i][j]=0.0;
//     }
//     for (i=0;i<3;i++) {
//         nav->fcb[nav->nf].bias[sat-1][i]=bias[i];
//         nav->fcb[nav->nf].std [sat-1][i]=std [i];
//     }
//     nav->fcb[nav->nf  ].ts=ts;
//     nav->fcb[nav->nf++].te=te;
    return 1;
}
/* read satellite fcb file ---------------------------------------------------*/
int readfcbf(const char *file, nav_t *nav)
{
    FILE *fp;
    gtime_t ts,te;
    double ep1[6],ep2[6],bias[3]={0},std[3]={0};
    char buff[1024],str[32],*p;
    int sat;

//     trace(3,"readfcbf: file=%s\n",file);

    if (!(fp=fopen(file,"r"))) {
//         trace(2,"fcb parameters file open error: %s\n",file);
        return 0;
    }
    while (fgets(buff,sizeof(buff),fp)) {
        if ((p=strchr(buff,'#'))) *p='\0';
        if (sscanf(buff,"%lf/%lf/%lf %lf:%lf:%lf %lf/%lf/%lf %lf:%lf:%lf %s"
                   "%lf %lf %lf %lf %lf %lf",ep1,ep1+1,ep1+2,ep1+3,ep1+4,ep1+5,
                   ep2,ep2+1,ep2+2,ep2+3,ep2+4,ep2+5,str,bias,std,bias+1,std+1,
                   bias+2,std+2)<17) continue;
        if (!(sat=SatSys(str))) continue;
        ts=epoch2time(ep1);
        te=epoch2time(ep2);
        if (!addfcb(nav,ts,te,sat,bias,std)) return 0;
    }
    fclose(fp);
    return 1;
}
/* compare satellite fcb -----------------------------------------------------*/
bool cmpfcb(fcbd_t&p1, fcbd_t&p2)
{
    fcbd_t *q1=&p1,*q2=&p2;
    double tt=timediff(q1->ts,q2->ts);
    return tt<-1E-3?-1:(tt>1E-3?1:0);
}
/* read satellite fcb data -----------------------------------------------------
* read satellite fractional cycle bias (dcb) parameters
* args   : char   *file       I   fcb parameters file (wild-card * expanded)
*          nav_t  *nav        IO  navigation data
* return : status (1:ok,0:error)
* notes  : fcb data appended to navigation data
*-----------------------------------------------------------------------------*/
int readfcb(const char *file, nav_t *nav)
{
    char *efiles[MAXEXFILE]={0};
    int i,n;

//     trace(3,"readfcb : file=%s\n",file);

    for (i=0;i<MAXEXFILE;i++) {
        if (!(efiles[i]=(char *)malloc(1024))) {
            for (i--;i>=0;i--) free(efiles[i]);
            return 0;
        }
    }
    n=expath(file,efiles,MAXEXFILE);

    for (i=0;i<n;i++) {
        readfcbf(efiles[i],nav);
    }
    for (i=0;i<MAXEXFILE;i++) free(efiles[i]);

	nav->fcbList.sort(cmpfcb);
    return 1;
}
/* polynomial interpolation by Neville's algorithm ---------------------------*/
double interppol(const double *x, double *y, int n)
{
    for (int j=1; j < n;		j++)
	for (int i=0; i < n - j;	i++)
	{
		y[i] = (x[i+j] * y[i] - x[i] * y[i+1]) / (x[i+j] - x[i]);
	}

    return y[0];
}
/* satellite position by precise ephemeris -----------------------------------*/
int pephpos(
	gtime_t time,
	SatSys Sat,
	nav_t& nav,
	double *rs,
	double *dts,
	double *vare,
	double *varc)
{
    double t[NMAX+1],p[3][NMAX+1],c[2],s[3];

	char id[4];
	Sat.getId(id);
//     trace(4,"pephpos : time=%s sat=%s\n",time.to_string(3).c_str(),id);

    rs[0]	= 0;
	rs[1]	= 0;
	rs[2]	= 0;
	*dts	= 0;

	PephList pephList = nav.pephMap[Sat];

	if	( (pephList.size()							< NMAX + 1)
		||(timediff(time, pephList.begin()->first)	< -MAXDTE)
		||(timediff(time, pephList.rbegin()->first)	> +MAXDTE))
	{
//         trace(3,"no prec ephem %s sat=%s\n",time.to_string(0).c_str(),id);
        return 0;
	}

// 	//search for the ephemeris in the list

	auto peph_it = pephList.lower_bound(time);
	if (peph_it == pephList.end())
	{
		peph_it--;
	}

	auto middle0 = peph_it;

	//go forward a few steps to make sure we're far from the end of the list.
	for (int i = 0; i < NMAX/2; i++)
	{
		peph_it++;
		if (peph_it == pephList.end())
		{
			break;
		}
	}

	//go backward a few steps to make sure we're far from the beginning of the list
	for (int i = 0; i <= NMAX; i++)	//todo aaron, needs +1 to go back an extra step due to end()?
	{
		peph_it--;
		if (peph_it == pephList.begin())
		{
			break;
		}
	}

	auto begin = peph_it;

	//get interpolation parameters and check all ephemerides have values.
	peph_it = begin;
	for (int i = 0; i <= NMAX; i++, peph_it++)
	{
		Peph& peph = peph_it->second;
		if (norm(peph.pos, 3) <= 0)
		{
//             trace(3,"prec ephem outage %s sat=%s\n",time.to_string(0).c_str(), id);
            return 0;
        }

		t[i] = timediff(peph.time, time);
        double* pos = peph.pos;
#if 0
        p[0][i]=pos[0];
        p[1][i]=pos[1];
#else
        /* correciton for earh rotation ver.2.4.0 */
        double sinl = sin(OMGE * t[i]);
        double cosl = cos(OMGE * t[i]);
        p[0][i] = cosl*pos[0] - sinl*pos[1];
        p[1][i] = sinl*pos[0] + cosl*pos[1];
#endif
        p[2][i]=pos[2];
	}

    for (int i = 0; i < 3; i++)
	{
        rs[i] = interppol(t, p[i], NMAX + 1);
    }
    double std = 0;
    if (vare)
	{
        for (int i = 0; i < 3; i++)
			s[i] = middle0->second.std[i];
        std = norm(s, 3);

        /* extrapolation error for orbit */
        if      (t[0   ] > 0) std += EXTERR_EPH * SQR(t[0   ]) / 2;		//todo aaron, needs straigtening as below?
        else if (t[NMAX] < 0) std += EXTERR_EPH * SQR(t[NMAX]) / 2;

        *vare = SQR(std);
    }

    /* linear interpolation for clock */
	auto middle1 = middle0;
	if (middle0 != pephList.begin())
	{
		middle0--;
	}
    t[0] = timediff(time, middle0->second.time);
    t[1] = timediff(time, middle1->second.time);
    c[0] = middle0->second.pos[3];
    c[1] = middle1->second.pos[3];

    if 		(t[0] <= 0)
	{
		*dts = c[0];

        if (*dts != 0)
			std = middle0->second.std[3] * CLIGHT	+ EXTERR_CLK * fabs(t[0]);
    }
    else if (t[1] >= 0)
	{
		*dts = c[1];

        if (*dts != 0)
			std = middle1->second.std[3] * CLIGHT	+ EXTERR_CLK * fabs(t[1]);
    }
    else if ( c[0] != 0
			&&c[1] != 0)
	{
        *dts = (c[1] * t[0] - c[0] * t[1]) / (t[0] - t[1]);

		double inv0 = 1 / middle0->second.std[3] * CLIGHT + EXTERR_CLK * fabs(t[0]);
		double inv1 = 1 / middle1->second.std[3] * CLIGHT + EXTERR_CLK * fabs(t[1]);
		std			= 1 / (inv0 + inv1);
    }
    else
	{
        *dts = 0;
    }
    if (varc)
		*varc=SQR(std);
    return 1;
}

/* satellite clock by precise clock ------------------------------------------*/
int pephclk(gtime_t time, string id, nav_t& nav, double *dtSat, double *varc)
{
    BOOST_LOG_TRIVIAL(debug)
	<< "pephclk : time=" << time.to_string(3)
	<< " id=" << id;

	PclkList pclkList = nav.pclkMap[id];

	if	( (pclkList.size()							< 2)
		||(timediff(time, pclkList.front().	time)	< -MAXDTE)
		||(timediff(time, pclkList.back().	time)	> +MAXDTE))
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "no prec clock " << time.to_string(0)
		<< " id=" << id;

        return -1;	//non zero for pass, negative for no result
    }

	//search for the ephemeris in the list
	auto pclk_it = pclkList.begin();
	while (pclk_it->time < time)
	{
		pclk_it++;
		if (pclk_it == pclkList.end())
		{
			pclk_it--;
			break;
		}
	}
	auto middle1 = pclk_it;
	auto middle0 = middle1;
	if (middle0 != pclkList.begin())
	{
		middle0--;
	}

    /* linear interpolation for clock */
    double t[2];
	double c[2];
    t[0] = timediff(time, middle0->time);
    t[1] = timediff(time, middle1->time);
    c[0] = middle0->clk;
    c[1] = middle1->clk;

	double std = 0;

    if		(t[0] <= 0)
	{
		*dtSat = c[0];

        if (*dtSat == 0)
			return 0;

		std	= middle0->std * CLIGHT	+ EXTERR_CLK * fabs(t[0]);
    }
    else if (t[1] >= 0)
	{
		*dtSat = c[1];

        if (*dtSat == 0)
			return 0;

        std	= middle1->std * CLIGHT	+ EXTERR_CLK * fabs(t[1]);
    }
    else if	( c[0] != 0
			&&c[1] != 0)
	{
        *dtSat = (c[1] * t[0] - c[0] * t[1]) / (t[0] - t[1]);

		double inv0 = 1 / middle0->std * CLIGHT + EXTERR_CLK * fabs(t[0]);
		double inv1 = 1 / middle1->std * CLIGHT + EXTERR_CLK * fabs(t[1]);
		std			= 1 / (inv0 + inv1);
    }
    else
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "prec clock outage " << time.to_string(0)
		<< " sat=" << id;

        return 0;
    }

    if (varc)
		*varc = SQR(std);

    return 1;
}
/* satellite antenna phase center offset ---------------------------------------
* compute satellite antenna phase center offset in ecef
* args   : gtime_t time       I   time (gpst)
*          double *rs         I   satellite position and velocity (ecef)
*                                 {x,y,z,vx,vy,vz} (m|m/s)
*          int    sat         I   satellite number
*          nav_t  *nav        I   navigation data
*          double *dant       I   satellite antenna phase center offset (ecef)
*                                 {dx,dy,dz} (m) (iono-free LC value)
* return : none
*-----------------------------------------------------------------------------*/
void satantoff(
	Trace&		trace,
	gtime_t		time,
	Vector3d&	rs,
	SatSys& 	Sat,
	SatNav*		satNav_ptr,
	Vector3d&	dant,
	PcoMapType*	pcoMap_ptr)
{
    double* lam = satNav_ptr->lam;
    double gmst,erpv[5]={0};
    double gamma,C1,C2,dant1,dant2;
    int i;
    E_FType j = (E_FType)0;
	E_FType k = (E_FType)1;
    tracepde(4,trace, "satantoff: time=%s sat=%2d\n",time.to_string(3).c_str() ,Sat);

    /* sun position in ecef */
	Vector3d rsun;
    sunmoonpos(gpst2utc(time),erpv,rsun.data(),NULL,&gmst);

    /* unit vectors of satellite fixed coordinates */
	Vector3d r = -rs;
	Vector3d ez = r.normalized();
	r = rsun - rs;
	Vector3d es = r.normalized();
	r = ez.cross(es);
	Vector3d ey = r.normalized();
	Vector3d ex = ey.cross(ez);


	int sys = Sat.sys;
    if (NFREQ>=3&&(sys == E_Sys::GAL||sys == E_Sys::SBS)) k=(E_FType)2;

    if (NFREQ<2||lam[j]==0.0||lam[k]==0.0) return;

    gamma=SQR(lam[k])/SQR(lam[j]);		//todo aaron, can use obs2lc?
    C1=gamma/(gamma-1.0);
    C2=-1.0 /(gamma-1.0);

	if (pcoMap_ptr == nullptr)
	{
		return;
	}
	auto& pco = *pcoMap_ptr;
#if (0)
    tracepde(1, trace,"    satpco L1 = %8.4f %8.4f %8.4f L2 = %8.4f %8.4f %8.4f\n",
            pco[j][0],pco[j][1],pco[j][2],pco[k][0],pco[k][1],pco[k][2]);
#endif

    /* iono-free LC */
    for (i=0;i<3;i++) {
#ifndef ANTEXACS
		const pcvacs_t* pcv = &satNav_ptr->pcv;
        dant1=pcv->off[j][0]*ex[i]+pcv->off[j][1]*ey[i]+pcv->off[j][2]*ez[i];
        dant2=pcv->off[k][0]*ex[i]+pcv->off[k][1]*ey[i]+pcv->off[k][2]*ez[i];
#else
        /* ENU to NEU */
        dant1=pco[j][1]*ex(i)+pco[j][0]*ey(i)+pco[j][2]*ez(i);
        dant2=pco[k][1]*ex(i)+pco[k][0]*ey(i)+pco[k][2]*ez(i);
#endif
        dant(i)=C1*dant1+C2*dant2;
    }
}
/* satellite position/clock by precise ephemeris/clock -------------------------
* compute satellite position/clock with precise ephemeris/clock
* args   : gtime_t time       I   time (gpst)
*          int    sat         I   satellite number
*          nav_t  *nav        I   navigation data
*          int    opt         I   sat postion option
*                                 (0: center of mass, 1: antenna phase center)
*          double *rs         O   sat position and velocity (ecef)
*                                 {x,y,z,vx,vy,vz} (m|m/s)
*          double *dts        O   sat clock {bias,drift} (s|s/s)
*          double *var        IO  sat position and clock error variance (m)
*                                 (NULL: no output)
* return : status (1:ok,0:error or data outage)
* notes  : clock includes relativistic correction but does not contain code bias
*          before calling the function, nav->peph, nav->ne, nav->pclk and
*          nav->nc must be set by calling readsp3(), readrnx() or readrnxt()
*          if precise clocks are not set, clocks in sp3 are used instead
*-----------------------------------------------------------------------------*/
int peph2pos(
	Trace&		trace,
	gtime_t		time,
	SatSys&		Sat,
	nav_t& 		nav,
	int			opt,
	Obs&		obs,
	PcoMapType*	pcoMap_ptr)
{
	Vector3d dAnt	= Vector3d::Zero();
	Vector3d rst	= Vector3d::Zero();

    double dtss1 = 0,dtss2 = 0,dtst,vare=0,varc=0,tt=1E-3;

    tracepde(4,trace, "peph2pos: time=%s sat=%2d opt=%d\n", time.to_string(3).c_str(), Sat, opt);

    /* satellite position and clock bias */
    if 	( !pephpos(time, Sat, nav, obs.rSat.data(),	&dtss1,	&vare,	&varc)
		||!pephclk(time, Sat, nav, 					&dtss2,			&varc))
	{
		return 0;
	}

	if (dtss2 != 0)
	{
		double delta = dtss1 - dtss2;
		dtss1 = dtss2;
	}

    time = timeadd(time,tt);

    if 	( !pephpos(time, Sat, nav, rst.data(),		&dtst,	NULL,	NULL)
		||!pephclk(time, Sat, nav, 					&dtst,			NULL))
		return 0;

	/* satellite antenna offset correction */
    if (opt)
	{
        satantoff(trace, time, obs.rSat, Sat, &nav.satNavMap[Sat], dAnt, pcoMap_ptr);
    }

	obs.satVel = (rst - obs.rSat) / tt;
    obs.rSat += dAnt;

    /* relativistic effect correction */
    if (dtss1 != 0)
	{
		double deltaDt = dtst-dtss1;
		double relativisticAdj = 2 * obs.rSat.dot(obs.satVel) / CLIGHT / CLIGHT;
        obs.dtSat[0] = dtss1 - relativisticAdj;
        obs.dtSat[1] = deltaDt / tt;
    }
    else
	{
		/* no precise clock */
        obs.dtSat[0] = 0;
		obs.dtSat[1] = 0;
    }

    obs.var = vare + varc;

// 	obs.svh = 1;
    return 1;
}

/* copy orbit structure to sp3 structure ---------------------------------------
 * args     :       const orbpod_t *orbpod  I       orbit info
 *                  nav_t *nav              O       orbit in sp3 structure
 *
 * return   :       1-successful, 0-failure
 * ---------------------------------------------------------------------------*/
int orb2sp3(nav_t& nav)
{
	for (auto& [Sat,	satOrbit]	: nav.orbpod.satOrbitMap)
	for (auto& [time,	orbitInfo]	: satOrbit.orbitInfoList)
	{
		Peph peph = {};

		peph.index			= 0;
		peph.time			= time;
		peph.Sat			= Sat;

		/* copy itrf coordinates */
		for (int k = 0; k < 3; k++)
		{
			peph.pos[k] = orbitInfo.xtrf[k];
		}

		nav.pephMap[Sat][time] = peph;
	}

    return 1;
}


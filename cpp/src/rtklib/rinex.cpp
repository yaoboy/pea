
#include <boost/log/trivial.hpp>


#include <string>

using std::string;



#include "streamTrace.hpp"
#include "navigation.hpp"
#include "station.hpp"
#include "common.hpp"
#include "gaTime.hpp"

/* constants/macros ----------------------------------------------------------*/

#define NUMSYS      6                   /* number of systems */
#define MAXRNXLEN   (16*MAXOBSTYPE+4)   /* max rinex record length */
#define MAXPOSHEAD  1024                /* max head line position */
#define MINFREQ_GLO -7                  /* min frequency number glonass */
#define MAXFREQ_GLO 13                  /* max frequency number glonass */
#define NINCOBS     262144              /* inclimental number of obs data */

const char syscodes[]="GREJSC";  /* satellite system codes */

const char robscodes[]="CLDS";    /* obs type codes */

const char frqcodes[]="125678";  /* frequency codes */

const double ura_eph[]=
{
	/* ura values (ref [3] 20.3.3.3.1.1) */
    2.4, 3.4, 4.85, 6.85, 9.65, 13.65, 24, 48, 96, 192, 384, 768, 1536, 3072, 6144, 0
};
/* type definition -----------------------------------------------------------*/
struct sigind_t
{                        /* signal index type */
    int 			n;                              /* number of index */
    int 			frq		[MAXOBSTYPE];                /* signal frequency (1:L1,2:L2,...) */
    int 			pos		[MAXOBSTYPE];                /* signal index in obs data (-1:no) */
    unsigned char 	pri 	[MAXOBSTYPE];     /* signal priority (15-0) */
    unsigned char 	type	[MAXOBSTYPE];     /* type (0:C,1:L,2:D,3:S) */
    E_ObsCode 		code	[MAXOBSTYPE];     /* obs code (CODE_L??) */
    double 			shift	[MAXOBSTYPE];           /* phase shift (cycle) */
};

/* set string without tail space ---------------------------------------------*/
void setstr(char *dst, const char *src, int n)
{
    char *p=dst;
    const char *q=src;
    while (*q&&q<src+n) *p++=*q++;
    *p--='\0';
    while (p>=dst&&*p==' ') *p--='\0';
}
/* adjust time considering week handover -------------------------------------*/
gtime_t adjweek(gtime_t t, gtime_t t0)
{
    double tt=timediff(t,t0);
    if (tt<-302400.0) return timeadd(t, 604800.0);
    if (tt> 302400.0) return timeadd(t,-604800.0);
    return t;
}
/* adjust time considering week handover -------------------------------------*/
gtime_t adjday(gtime_t t, gtime_t t0)
{
    double tt=timediff(t,t0);
    if (tt<-43200.0) return timeadd(t, 86400.0);
    if (tt> 43200.0) return timeadd(t,-86400.0);
    return t;
}
#if (0)
/* time string for ver.3 (yyyymmdd hhmmss UTC) -------------------------------*/
void timestr_rnx(char *str)
{
    gtime_t time;
    double ep[6];
    time=timeget();
    time.sec=0.0;
    time2epoch(time,ep);
    sprintf(str,"%04.0f%02.0f%02.0f %02.0f%02.0f%02.0f UTC",ep[0],ep[1],ep[2],
            ep[3],ep[4],ep[5]);
}
/* ura index to ura value (m) ------------------------------------------------*/
double uravalue(int sva, int sys)
{
    double ura=0.0;
    int tmp;
    if (sys==SYS_CMP) {
        if (0<=sva&&sva<6) {
            tmp=(int)floor((pow(2,sva/2+1)*10.0)+0.5);
            ura=tmp/10.0;
        }
        else if (6<=sva&&sva<15) {
            ura=pow(2,sva-2);
        }
        else
            ura=32767.0;
        return ura;
    }
    else
        return 0<=sva&&sva<15?ura_eph[sva]:32767.0;
}
#endif
/* ura value (m) to ura index ------------------------------------------------*/
int uraindex(double value)
{
    int i;
    for (i=0;i<15;i++) if (ura_eph[i]>=value) break;
    return i;
}

/*------------------------------------------------------------------------------
* input rinex functions
*-----------------------------------------------------------------------------*/

/* convert rinex obs type ver.2 -> ver.3 -------------------------------------*/
void convcode(double ver, int sys, const char *str, char *type)
{
    strcpy(type,"   ");

    if      (!strcmp(str,"P1")) { /* ver.2.11 GPS L1PY,GLO L2P */
        if      (sys==+E_Sys::GPS) sprintf(type,"%c1W",'C');
        else if (sys==+E_Sys::GLO) sprintf(type,"%c1P",'C');
    }
    else if (!strcmp(str,"P2")) { /* ver.2.11 GPS L2PY,GLO L2P */
        if      (sys==+E_Sys::GPS) sprintf(type,"%c2W",'C');
        else if (sys==+E_Sys::GLO) sprintf(type,"%c2P",'C');
    }
    else if (!strcmp(str,"C1")) { /* ver.2.11 GPS L1C,GLO L1C/A */
        if      (ver>=2.12) ; /* reject C1 for 2.12 */
        else if (sys==+E_Sys::GPS) sprintf(type,"%c1C",'C');
        else if (sys==+E_Sys::GLO) sprintf(type,"%c1C",'C');
        else if (sys==+E_Sys::GAL) sprintf(type,"%c1X",'C'); /* ver.2.12 */
        else if (sys==+E_Sys::QZS) sprintf(type,"%c1C",'C');
        else if (sys==+E_Sys::SBS) sprintf(type,"%c1C",'C');
    }
    else if (!strcmp(str,"C2")) {
        if (sys==+E_Sys::GPS) {
            if (ver>=2.12) sprintf(type,"%c2W",'C'); /* L2P(Y) */
            else           sprintf(type,"%c2X",'C'); /* L2C */
        }
        else if (sys==+E_Sys::GLO) sprintf(type,"%c2C",'C');
        else if (sys==+E_Sys::QZS) sprintf(type,"%c2X",'C');
        else if (sys==+E_Sys::CMP) sprintf(type,"%c1X",'C'); /* ver.2.12 B1 */
    }
    else if (ver>=2.12&&str[1]=='A') { /* ver.2.12 L1C/A */
        if      (sys==+E_Sys::GPS) sprintf(type,"%c1C",str[0]);
        else if (sys==+E_Sys::GLO) sprintf(type,"%c1C",str[0]);
        else if (sys==+E_Sys::QZS) sprintf(type,"%c1C",str[0]);
        else if (sys==+E_Sys::SBS) sprintf(type,"%c1C",str[0]);
    }
    else if (ver>=2.12&&str[1]=='B') { /* ver.2.12 GPS L1C */
        if      (sys==+E_Sys::GPS) sprintf(type,"%c1X",str[0]);
        else if (sys==+E_Sys::QZS) sprintf(type,"%c1X",str[0]);
    }
    else if (ver>=2.12&&str[1]=='C') { /* ver.2.12 GPS L2C */
        if      (sys==+E_Sys::GPS) sprintf(type,"%c2X",str[0]);
        else if (sys==+E_Sys::QZS) sprintf(type,"%c2X",str[0]);
    }
    else if (ver>=2.12&&str[1]=='D') { /* ver.2.12 GLO L2C/A */
        if      (sys==+E_Sys::GLO) sprintf(type,"%c2C",str[0]);
    }
    else if (ver>=2.12&&str[1]=='1') { /* ver.2.12 GPS L1PY,GLO L1P */
        if      (sys==+E_Sys::GPS) sprintf(type,"%c1W",str[0]);
        else if (sys==+E_Sys::GLO) sprintf(type,"%c1P",str[0]);
        else if (sys==+E_Sys::GAL) sprintf(type,"%c1X",str[0]); /* tentative */
        else if (sys==+E_Sys::CMP) sprintf(type,"%c1X",str[0]); /* extension */
    }
    else if (ver<2.12&&str[1]=='1') {
        if      (sys==+E_Sys::GPS) sprintf(type,"%c1C",str[0]);
        else if (sys==+E_Sys::GLO) sprintf(type,"%c1C",str[0]);
        else if (sys==+E_Sys::GAL) sprintf(type,"%c1X",str[0]); /* tentative */
        else if (sys==+E_Sys::QZS) sprintf(type,"%c1C",str[0]);
        else if (sys==+E_Sys::SBS) sprintf(type,"%c1C",str[0]);
    }
    else if (str[1]=='2') {
        if      (sys==+E_Sys::GPS) sprintf(type,"%c2W",str[0]);
        else if (sys==+E_Sys::GLO) sprintf(type,"%c2P",str[0]);
        else if (sys==+E_Sys::QZS) sprintf(type,"%c2X",str[0]);
        else if (sys==+E_Sys::CMP) sprintf(type,"%c1X",str[0]); /* ver.2.12 B1 */
    }
    else if (str[1]=='5') {
        if      (sys==+E_Sys::GPS) sprintf(type,"%c5X",str[0]);
        else if (sys==+E_Sys::GAL) sprintf(type,"%c5X",str[0]);
        else if (sys==+E_Sys::QZS) sprintf(type,"%c5X",str[0]);
        else if (sys==+E_Sys::SBS) sprintf(type,"%c5X",str[0]);
    }
    else if (str[1]=='6') {
        if      (sys==+E_Sys::GAL) sprintf(type,"%c6X",str[0]);
        else if (sys==+E_Sys::QZS) sprintf(type,"%c6X",str[0]);
        else if (sys==+E_Sys::CMP) sprintf(type,"%c6X",str[0]); /* ver.2.12 B3 */
    }
    else if (str[1]=='7') {
        if      (sys==+E_Sys::GAL) sprintf(type,"%c7X",str[0]);
        else if (sys==+E_Sys::CMP) sprintf(type,"%c7X",str[0]); /* ver.2.12 B2 */
    }
    else if (str[1]=='8') {
        if      (sys==+E_Sys::GAL) sprintf(type,"%c8X",str[0]);
    }
    BOOST_LOG_TRIVIAL(debug)
	<< "convcode: ver=" << ver
	<< " sys="		<< sys
	<< " type= "	<< str
	<< " -> "		<< type;
}
/* decode obs header ---------------------------------------------------------*/
void decode_obsh(
	FILE *fp,
	char *buff,
	double ver,
	int *tsys,
	char tobs[][MAXOBSTYPE][4],
	nav_t *nav,
	sta_t *sta)
{
    /* default codes for unknown code */
    const char *defcodes[] =
    {
        "CWX   ",   /* GPS: L125___ */
        "CC    ",   /* GLO: L12____ */
        "X XXXX",   /* GAL: L1_5678 */
        "CXXX  ",   /* QZS: L1256__ */
        "C X   ",   /* SBS: L1_5___ */
        "X  XX "    /* BDS: L1__67_ */
    };
    double del[3];
    int i,j,k,n,nt,prn,fcn;
    const char *p;
    char *label=buff+60,str[4];

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_obsh: ver=" << ver;

    if      (strstr(label,"MARKER NAME"         ))	{   if (sta)	setstr(sta->name,	buff,60);  	}
	else if (strstr(label,"MARKER NUMBER"       ))	{	if (sta) 	setstr(sta->marker,	buff,20);	}
    else if (strstr(label,"MARKER TYPE"         )) ; /* ver.3 */
    else if (strstr(label,"OBSERVER / AGENCY"   )) ;
    else if (strstr(label,"REC # / TYPE / VERS" ))
	{
        if (sta)
		{
            setstr(sta->recsno, buff,   20);
            setstr(sta->rectype,buff+20,20);
            setstr(sta->recver, buff+40,20);
        }
    }
    else if (strstr(label,"ANT # / TYPE"        ))
	{
        if (sta)
		{
            setstr(sta->antsno,buff   ,20);
            setstr(sta->antdes,buff+20,20);
        }
    }
    else if (strstr(label,"APPROX POSITION XYZ" ))
	{
        if (sta)
		{
            for (i=0,j=0;i<3;i++,j+=14)
				sta->rRec[i]=str2num(buff,j,14);
        }
    }
    else if (strstr(label,"ANTENNA: DELTA H/E/N"))
	{
        if (sta)
		{
            for (i=0,j=0;i<3;i++,j+=14)
				del[i]=str2num(buff,j,14);

            sta->del[2]=del[0]; /* h */
            sta->del[0]=del[1]; /* e */
            sta->del[1]=del[2]; /* n */
        }
    }
    else if (strstr(label,"ANTENNA: DELTA X/Y/Z")) ; /* opt ver.3 */
    else if (strstr(label,"ANTENNA: PHASECENTER")) ; /* opt ver.3 */
    else if (strstr(label,"ANTENNA: B.SIGHT XYZ")) ; /* opt ver.3 */
    else if (strstr(label,"ANTENNA: ZERODIR AZI")) ; /* opt ver.3 */
    else if (strstr(label,"ANTENNA: ZERODIR XYZ")) ; /* opt ver.3 */
    else if (strstr(label,"CENTER OF MASS: XYZ" )) ; /* opt ver.3 */
    else if (strstr(label,"SYS / # / OBS TYPES" ))
	{ /* ver.3 */
        if (!(p=strchr(syscodes,buff[0])))
		{
            BOOST_LOG_TRIVIAL(debug)
			<< "invalid system code: sys=" << buff[0];
            return;
        }
        i=(int)(p-syscodes);
        n=(int)str2num(buff,3,3);
        for (j=nt=0,k=7;j<n;j++,k+=4)
		{
            if (k>58)
			{
                if (!fgets(buff,MAXRNXLEN,fp)) break;
                k=7;
            }
            if (nt<MAXOBSTYPE-1)
				setstr(tobs[i][nt++],buff+k,3);
        }
        *tobs[i][nt]='\0';

        /* change beidou B1 code: 3.02 draft -> 3.02 */
        if (i==5)
		{
            for (j=0;j<nt;j++)
				if (tobs[i][j][1]=='2')
					tobs[i][j][1]='1';
        }
        /* if unknown code in ver.3, set default code */
        for (j=0;j<nt;j++)
		{
            if (tobs[i][j][2])
				continue;

            if (!(p=strchr(frqcodes,tobs[i][j][1])))
				continue;

            tobs[i][j][2]=defcodes[i][(int)(p-frqcodes)];
            BOOST_LOG_TRIVIAL(debug)
			<< "set default for unknown code: sys=" << buff[0]
			<< " code=" << tobs[i][j];
        }
    }
    else if (strstr(label,"WAVELENGTH FACT L1/2")) ; /* opt ver.2 */
    else if (strstr(label,"# / TYPES OF OBSERV" ))
	{ /* ver.2 */
        n=(int)str2num(buff,0,6);
        for (i=nt=0,j=10;i<n;i++,j+=6)
		{
            if (j>58)
			{
                if (!fgets(buff,MAXRNXLEN,fp))
					break;
                j=10;
            }

            if (nt>=MAXOBSTYPE-1)
				continue;

            if (ver<=2.99)
			{
                setstr(str,buff+j,2);
                convcode(ver,E_Sys::GPS,str,tobs[0][nt]);
                convcode(ver,E_Sys::GLO,str,tobs[1][nt]);
                convcode(ver,E_Sys::GAL,str,tobs[2][nt]);
                convcode(ver,E_Sys::QZS,str,tobs[3][nt]);
                convcode(ver,E_Sys::SBS,str,tobs[4][nt]);
                convcode(ver,E_Sys::CMP,str,tobs[5][nt]);
            }
            nt++;
        }
        *tobs[0][nt]='\0';
    }
    else if (strstr(label,"SIGNAL STRENGTH UNIT")) ; /* opt ver.3 */
    else if (strstr(label,"INTERVAL"            )) ; /* opt */
    else if (strstr(label,"TIME OF FIRST OBS"   ))
	{
        if      (!strncmp(buff+48,"GPS",3)) *tsys=TSYS_GPS;
        else if (!strncmp(buff+48,"GLO",3)) *tsys=TSYS_UTC;
        else if (!strncmp(buff+48,"GAL",3)) *tsys=TSYS_GAL;
        else if (!strncmp(buff+48,"QZS",3)) *tsys=TSYS_QZS; /* ver.3.02 */
        else if (!strncmp(buff+48,"BDT",3)) *tsys=TSYS_CMP; /* ver.3.02 */
    }
    else if (strstr(label,"TIME OF LAST OBS"    )) ; /* opt */
    else if (strstr(label,"RCV CLOCK OFFS APPL" )) ; /* opt */
    else if (strstr(label,"SYS / DCBS APPLIED"  )) ; /* opt ver.3 */
    else if (strstr(label,"SYS / PCVS APPLIED"  )) ; /* opt ver.3 */
    else if (strstr(label,"SYS / SCALE FACTOR"  )) ; /* opt ver.3 */
    else if (strstr(label,"SYS / PHASE SHIFTS"  )) ; /* ver.3.01 */
    else if (strstr(label,"GLONASS SLOT / FRQ #")) { /* ver.3.02 */
        if (nav) {
            for (i=0,p=buff+4;i<8;i++,p+=8)
			{
                if (sscanf(p,"R%2d %2d",&prn,&fcn)<2)
					continue;
                if (1<=prn&&prn<=MAXPRNGLO)
					nav->glo_fcn[prn-1]=fcn+8;
            }
        }
    }
    else if (strstr(label,"GLONASS COD/PHS/BIS" ))
	{ /* ver.3.02 */
        if (nav)
		{
            for (i=0,p=buff;i<4;i++,p+=13)
			{
                if      (strncmp(p+1,"C1C",3)) nav->glo_cpbias[0]=str2num(p,5,8);
                else if (strncmp(p+1,"C1P",3)) nav->glo_cpbias[1]=str2num(p,5,8);
                else if (strncmp(p+1,"C2C",3)) nav->glo_cpbias[2]=str2num(p,5,8);
                else if (strncmp(p+1,"C2P",3)) nav->glo_cpbias[3]=str2num(p,5,8);
            }
        }
    }
    else if (strstr(label,"LEAP SECONDS"        ))
	{ /* opt */
        if (nav)
			nav->leaps=(int)str2num(buff,0,6);
    }
    else if (strstr(label,"# OF SALTELLITES"    )) ; /* opt */
    else if (strstr(label,"PRN / # OF OBS"      )) ; /* opt */
}
/* decode nav header ---------------------------------------------------------*/
void decode_navh(char *buff, nav_t *nav)
{
    int i,j;
    char *label=buff+60;

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_navh:";

    if      (strstr(label,"ION ALPHA"           ))	{ /* opt ver.2 */  if (nav) {  for (i=0,j=2;i<4;i++,j+=12) nav->ion_gps[i]		= str2num(buff,j,12);   }  }
    else if (strstr(label,"ION BETA"            ))	{ /* opt ver.2 */  if (nav) {  for (i=0,j=2;i<4;i++,j+=12) nav->ion_gps[i+4]	= str2num(buff,j,12);   }  }
    else if (strstr(label,"DELTA-UTC: A0,A1,T,W"))
	{ /* opt ver.2 */
        if (nav)
		{
            for (i=0,j=3;i<2;i++,j+=19)	nav->utc_gps[i]=str2num(buff,j,19);
            for (;i<4;i++,j+=9)			nav->utc_gps[i]=str2num(buff,j,9);
        }
    }
    else if (strstr(label,"IONOSPHERIC CORR"    ))
	{ /* opt ver.3 */
        if (nav)
		{
            if		(!strncmp(buff,"GPSA",4))		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_gps[i]	= str2num(buff,j,12); }
            else if (!strncmp(buff,"GPSB",4)) 		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_gps[i+4]	= str2num(buff,j,12); }
            else if (!strncmp(buff,"GAL",3))		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_gal[i]	= str2num(buff,j,12); }
            else if (!strncmp(buff,"QZSA",4)) 		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_qzs[i]	= str2num(buff,j,12); }
            else if (!strncmp(buff,"QZSB",4))		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_qzs[i+4]	= str2num(buff,j,12); }
            else if (!strncmp(buff,"BDSA",4)) 		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_cmp[i]	= str2num(buff,j,12); }
            else if (!strncmp(buff,"BDSB",4))		{ for (i=0,j=5;i<4;i++,j+=12) nav->ion_cmp[i+4]	= str2num(buff,j,12); }
        }
    }
    else if (strstr(label,"TIME SYSTEM CORR"    ))
	{ /* opt ver.3 */
        if (nav)
		{
            if (!strncmp(buff,"GPUT",4))
			{
                nav->utc_gps[0]=str2num(buff, 5,17);
                nav->utc_gps[1]=str2num(buff,22,16);
                nav->utc_gps[2]=str2num(buff,38, 7);
                nav->utc_gps[3]=str2num(buff,45, 5);
            }
            else if (!strncmp(buff,"GLUT",4))
			{
                nav->utc_glo[0]=str2num(buff, 5,17);
                nav->utc_glo[1]=str2num(buff,22,16);
            }
            else if (!strncmp(buff,"GAUT",4))
			{ /* v.3.02 */
                nav->utc_gal[0]=str2num(buff, 5,17);
                nav->utc_gal[1]=str2num(buff,22,16);
                nav->utc_gal[2]=str2num(buff,38, 7);
                nav->utc_gal[3]=str2num(buff,45, 5);
            }
            else if (!strncmp(buff,"QZUT",4))
			{ /* v.3.02 */
                nav->utc_qzs[0]=str2num(buff, 5,17);
                nav->utc_qzs[1]=str2num(buff,22,16);
                nav->utc_qzs[2]=str2num(buff,38, 7);
                nav->utc_qzs[3]=str2num(buff,45, 5);
            }
            else if (!strncmp(buff,"BDUT",4))
			{ /* v.3.02 */
                nav->utc_cmp[0]=str2num(buff, 5,17);
                nav->utc_cmp[1]=str2num(buff,22,16);
                nav->utc_cmp[2]=str2num(buff,38, 7);
                nav->utc_cmp[3]=str2num(buff,45, 5);
            }
            else if (!strncmp(buff,"SBUT",4))
			{ /* v.3.02 */
                nav->utc_cmp[0]=str2num(buff, 5,17);
                nav->utc_cmp[1]=str2num(buff,22,16);
                nav->utc_cmp[2]=str2num(buff,38, 7);
                nav->utc_cmp[3]=str2num(buff,45, 5);
            }
        }
    }
    else if (strstr(label,"LEAP SECONDS"        ))
	{ /* opt */
        if (nav)
			nav->leaps=(int)str2num(buff,0,6);
    }
}
/* decode gnav header --------------------------------------------------------*/
void decode_gnavh(char *buff, nav_t *nav)
{
    char *label=buff+60;

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_gnavh:";

    if      (strstr(label,"CORR TO SYTEM TIME"  )) ; /* opt */
    else if (strstr(label,"LEAP SECONDS"        ))
	{ /* opt */
        if (nav)
			nav->leaps=(int)str2num(buff,0,6);
    }
}

/* decode geo nav header -----------------------------------------------------*/
void decode_hnavh(char *buff, nav_t *nav)
{
    char *label=buff+60;

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_hnavh:";

    if      (strstr(label,"CORR TO SYTEM TIME"  )) ; /* opt */
    else if (strstr(label,"D-UTC A0,A1,T,W,S,U" )) ; /* opt */
    else if (strstr(label,"LEAP SECONDS"        ))
	{ /* opt */
        if (nav) nav->leaps=(int)str2num(buff,0,6);
    }
}
/* read rinex header ---------------------------------------------------------*/
int readrnxh(FILE *fp, double *ver, char *type, E_Sys *sys, int *tsys,
                    char tobs[][MAXOBSTYPE][4], nav_t *nav, sta_t *sta)
{
    double bias;
    char buff[MAXRNXLEN],*label=buff+60;
    int i=0,block=0;

	SatSys Sat;

    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxh:";

    *ver    = 2.10;
	*type   = ' ';
    *sys	= E_Sys::GPS;
	*tsys	= TSYS_GPS;

    while (fgets(buff,MAXRNXLEN,fp))
	{
        if (strlen(buff)<=60)
			continue;

        else if (strstr(label,"RINEX VERSION / TYPE"))
		{
            *ver=str2num(buff,0,9);
            *type=*(buff+20);

			char sysChar = buff[40];

            /* satellite system */
            switch (sysChar)
			{
                case ' ':
                case 'G': *sys = E_Sys::GPS;  *tsys = TSYS_GPS; break;
                case 'R': *sys = E_Sys::GLO;  *tsys = TSYS_UTC; break;
                case 'E': *sys = E_Sys::GAL;  *tsys = TSYS_GAL; break; /* v.2.12 */
                case 'S': *sys = E_Sys::SBS;  *tsys = TSYS_GPS; break;
                case 'J': *sys = E_Sys::QZS;  *tsys = TSYS_QZS; break; /* v.3.02 */
                case 'C': *sys = E_Sys::CMP;  *tsys = TSYS_CMP; break; /* v.2.12 */
                case 'M': *sys = E_Sys::NONE; *tsys = TSYS_GPS; break; /* mixed */
                default :
                    BOOST_LOG_TRIVIAL(debug)
					<< "not supported satellite system: " << sysChar;

                    break;
            }
            continue;
        }
        else if (strstr(label,"PGM / RUN BY / DATE"))
			continue;
        else if (strstr(label,"COMMENT"))
		{ /* opt */

            /* read cnes wl satellite fractional bias */
            if	( strstr(buff,"WIDELANE SATELLITE FRACTIONAL BIASES")
				||strstr(buff,"WIDELANE SATELLITE FRACTIONNAL BIASES"))
			{
                block=1;
            }
            else if (block)
			{
                /* cnes/cls grg clock */
                if (!strncmp(buff,"WL",2)&&(Sat=SatSys(buff+3), Sat)&&
                    sscanf(buff+40,"%lf",&bias)==1)
				{
					nav->satNavMap[Sat].wlbias = bias;
                }
                /* cnes ppp-wizard clock */
                else if ((Sat=SatSys(buff+1), Sat)&&sscanf(buff+6,"%lf",&bias)==1)
				{
					nav->satNavMap[Sat].wlbias = bias;
                }
            }
            continue;
        }
        /* file type */
        switch (*type)
		{
            case 'O': decode_obsh(fp,buff,*ver,tsys,tobs,nav,sta); break;
            case 'N': decode_navh (buff,nav); break;
            case 'G': decode_gnavh(buff,nav); break;
            case 'H': decode_hnavh(buff,nav); break;
            case 'J': decode_navh (buff,nav); break; /* extension */
            case 'L': decode_navh (buff,nav); break; /* extension */
        }
        if (strstr(label,"END OF HEADER"))
			return 1;

        if (++i>=MAXPOSHEAD&&*type==' ')
			break; /* no rinex file */
    }
    return 0;
}
/* decode obs epoch ----------------------------------------------------------*/
int decode_obsepoch(
	FILE*		fp,
	char*		buff,
	double		ver,
	gtime_t*	time,
	int&		flag,
	SatSys*		sats)
{
    int n;
    char satid[8]="";

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_obsepoch: ver=" << ver;

    if	(ver <= 2.99)
	{
		/* ver.2 */
        if ((n = (int)str2num(buff, 29, 3)) <= 0)
			return 0;

        /* epoch flag: 3:new site,4:header info,5:external event */
        flag = (int)str2num(buff, 28, 1);

        if	( flag >= 3
			&&flag <= 5)
			return n;

        if (str2time(buff, 0, 26, time))
		{
            BOOST_LOG_TRIVIAL(debug)
			<< "rinex obs invalid epoch: epoch=" << buff;
            return 0;
        }

        int j = 32;
        for (int i=0; i < n; i++, j+=3)
		{
            if (j >= 68)
			{
                if (!fgets(buff, MAXRNXLEN, fp))
					break;
                j = 32;
            }
            if (i < MAXOBS)
			{
                strncpy(satid, buff + j, 3);
                sats[i] = SatSys(satid);
            }
        }
    }
    else
	{
		/* ver.3 */
        if ((n = (int)str2num(buff, 32, 3)) <= 0)
			return 0;

        flag = (int)str2num(buff, 31, 1);

        if	( flag >= 3
			&&flag <= 5)
			return n;

        if	( buff[0] != '>'
			||str2time(buff, 1, 28, time))
		{
            BOOST_LOG_TRIVIAL(debug)
			<< "rinex obs invalid epoch: epoch=" << buff;
            return 0;
        }
    }

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_obsepoch: time=" << time->to_string(3)
	<< " flag=" << flag;

    return n;
}
/* decode obs data -----------------------------------------------------------*/
int decode_obsdata(
	FILE*		fp,
	char*		buff,
	double		ver,
	sigind_t*	index,
	RawObs*		obs)
{
    sigind_t *ind;
    double	vals[MAXOBSTYPE] = {};
    int		llis[MAXOBSTYPE] = {};
    char satid[8]="";
    int stat=1,p[MAXOBSTYPE],k[16],l[16];

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_obsdata: ver=" << ver;

    if (ver > 2.99)
	{
		/* ver.3 */
        strncpy(satid, buff, 3);
        obs->Sat = SatSys(satid);
    }

    if (!obs->Sat)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "decode_obsdata: unsupported sat sat=" << satid;

        stat = 0;
    }

    /* read obs data fields */
    switch (obs->Sat.sys)
	{
        case E_Sys::GLO: 	ind = &index[1]; 	break;
        case E_Sys::GAL: 	ind = &index[2]; 	break;
        case E_Sys::QZS: 	ind = &index[3]; 	break;
        case E_Sys::SBS: 	ind = &index[4]; 	break;
        case E_Sys::CMP: 	ind = &index[5];	break;
        default:      		ind = &index[0];	break;
    }

    int j;
	if (ver <= 2.99)	j = 0;
	else				j = 3;

    for (int i=0; i < ind->n; i++, j+=16)
	{
        if	( ver	<= 2.99
			&&j		>= 80)
		{
			/* ver.2 */
            if (!fgets(buff,MAXRNXLEN,fp))
				break;
            j = 0;
        }

        if (stat)
		{
            vals[i] = str2num(buff,j,14) + ind->shift[i];
            llis[i] = str2num(buff,j+14,1);
			llis[i] = (unsigned char) llis[i] & 0x03;
        }
    }

    if (!stat)
		return 0;

    /* assign position in obs data */		//todo aaron, this might all go away?
	int m = 0;
	int n = 0;
    for (int i = 0; i < ind->n; i++)
	{
		if (ver <= 2.11)	p[i] = ind->frq[i] - 1;
		else				p[i] = ind->pos[i];

        if (ind->type[i] == 0	&&	p[i] == 0)		k[n++] = i; /* C1? index */
        if (ind->type[i] == 0	&&	p[i] == 1)		l[m++] = i; /* C2? index */
    }

    if (ver <= 2.11)
	{
        /* if multiple codes (C1/P1,C2/P2), select higher priority */
        if (n >= 2)
		{
            if		(vals[k[0]] == 0	&&	vals[k[1]] == 0) 	{ 	p[k[0]] = -1;					p[k[1]] = -1;					}
            else if	(vals[k[0]] != 0	&&	vals[k[1]] == 0) 	{ 	p[k[0]] = 0;					p[k[1]] = -1;					}
            else if	(vals[k[0]] == 0	&&	vals[k[1]] != 0) 	{ 	p[k[0]] = -1;					p[k[1]] = 0;					}
            else if (ind->pri[k[1]] > 	ind->pri[k[0]])			{ 	p[k[0]] = NEXOBS<1?-1:NFREQ;	p[k[1]] = 0;					}
            else 												{	p[k[0]] = 0;					p[k[1]] = NEXOBS<1?-1:NFREQ;	}
        }

        if (m >= 2)
		{
            if		(vals[l[0]] == 0	&&	vals[l[1]] == 0) 	{ 	p[l[0]] = -1;					p[l[1]] = -1;          			}
            else if	(vals[l[0]] != 0	&&	vals[l[1]] == 0) 	{ 	p[l[0]] = +1;					p[l[1]] = -1;         			}
            else if	(vals[l[0]] == 0	&&	vals[l[1]] != 0) 	{ 	p[l[0]] = -1;					p[l[1]] = +1;         			}
            else if	(ind->pri[l[1]] >	ind->pri[l[0]])		 	{	p[l[0]] = NEXOBS<2?-1:NFREQ+1;	p[l[1]] = +1;  					}
            else 												{ 	p[l[0]] =1;						p[l[1]] = NEXOBS<2?-1:NFREQ+1;  }
        }
    }

    /* save obs data */
    for (int i = 0; i < ind->n; i++)
	{
		if	( ver	<= 2.99
			&&p[i]	<	0)
		{
			//only testing this because not sure how version 2 works with indices
			continue;
		}

		if	(vals[i] == 0)
		{
			continue;
		}

// 		E_FType ft		= (E_FType) p[i];
		int		code	= ind->code[i];
		E_FType ft		= ftypes[code];
		int		type	= ind->type[i];
		double	Val		= vals[i];

		RawSig* sig_ptr = nullptr;
		for (auto& rawSig : obs->SigsLists[ft])
		{
			//check the list to see if this code exists
			if (rawSig.code == code)
			{
				sig_ptr = &rawSig;
			}
		}

		if (sig_ptr == nullptr)
		{
			//if it doesn't: add it
			RawSig newSig;
			obs->SigsLists[ft].push_back(newSig);
			sig_ptr = &obs->SigsLists[ft].back();
		}

		RawSig& sig = *sig_ptr;

		sig.code	= ind->code[i];

		switch (type)
		{
            case 0: sig.P	= Val; 									break;
            case 1: sig.L	= Val; 		sig.LLI 	= llis[i];  	break;
            case 2: sig.D	= Val;                        			break;
            case 3: sig.snr	= Val * 4 + 0.5;   						break;
        }
    }

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_obsdata: time=" << obs->time.to_string(0)
	<< " sat=" << obs->Sat.id();

    return 1;
}

/* add obs data --------------------------------------------------------------*/
int addobsdata(obs_t *obs, const RawObs *data)
{
#ifndef DEBUGPDE
    obsd_t *obs_data;
#endif
#ifndef DEBUGPDE
    if (obs->nmax<=obs->n)
	{
        if (obs->nmax<=0)
			obs->nmax=NINCOBS;
		else
			obs->nmax*=2;

        if (!(obs_data=(RawObs *)realloc(obs->data,sizeof(RawObs)*obs->nmax)))
		{
            trace(1,"addobsdata: memalloc error n=%dx%d\n",sizeof(RawObs),obs->nmax);
            free(obs->data); obs->data=NULL; obs->n=obs->nmax=0;
            return -1;
        }

        obs->data=obs_data;
    }
#endif
    obs->data[obs->n++]=*data;
    return 1;
}

/* set signal index ----------------------------------------------------------*/
void set_index(
	double ver,
	int sys,
	const char *opt,
	char tobs[MAXOBSTYPE][4],
	sigind_t *ind)
{
    const char *p;
    char str[8];
    double shift;
    int i,j,k,n;
    string optstr;

    for (i = n = 0; *tobs[i]; i++, n++)
	{
        ind->code[i]	= obs2code(tobs[i]+1,ind->frq+i);
        ind->type[i]	= (p=strchr(robscodes,tobs[i][0]))?(int)(p-robscodes):0;
        ind->pri[i]		= getcodepri(sys,ind->code[i],opt);
        ind->pos[i]		= -1;

        /* frequency index for beidou */
        if (sys == +E_Sys::CMP)
		{
            if      (ind->frq[i]==5) ind->frq[i] = 2; /* B2 */
            else if (ind->frq[i]==4) ind->frq[i] = 3; /* B3 */
        }
    }

    /* parse phase shift options */
    switch (sys)
	{
        case E_Sys::GPS: optstr="-GL%2s=%lf"; break;
        case E_Sys::GLO: optstr="-RL%2s=%lf"; break;
        case E_Sys::GAL: optstr="-EL%2s=%lf"; break;
        case E_Sys::QZS: optstr="-JL%2s=%lf"; break;
        case E_Sys::SBS: optstr="-SL%2s=%lf"; break;
        case E_Sys::CMP: optstr="-CL%2s=%lf"; break;
    }

    for (p= opt; p && (p = strchr(p,'-')); p++)
	{
        if (sscanf(p, optstr.c_str(), str, &shift) < 2)
		{
			continue;
		}

        for (i = 0; i < n; i++)
		{
            if (strcmp(code2obs(ind->code[i], NULL), str))
			{
				continue;
			}
            ind->shift[i] = shift;
//             trace(2,"phase shift: sys=%2d tobs=%s shift=%.3f\n", sys, tobs[i], shift);
        }
    }
    /* assign index for highest priority code */
    for (i = 0; i < NFREQ; i++)
	{
        for (j = 0, k = -1; j < n; j++)
		{
            if	( ind->frq[j] == i+1
				&&ind->pri[j]
				&&(k < 0
				  ||ind->pri[j] > ind->pri[k]))
			{
                k = j;
            }
        }
        if (k < 0)
		{
			continue;
		}

        for (j = 0; j < n; j++)
		{
            if (ind->code[j] == ind->code[k])
				ind->pos[j] = i;
        }
    }
    /* assign index of extended obs data */
    for (i = 0; i < NEXOBS; i++)
	{
        for (j = 0; j < n; j++)
		{
            if	( ind->code[j]
				&&ind->pri[j]
				&&ind->pos[j] < 0)
			{
				break;
			}
        }

        if (j >= n)
		{
			break;
		}

        for (k = 0; k < n; k++)
		{
            if (ind->code[k] == ind->code[j])
			{
				ind->pos[k] = NFREQ+i;
			}
        }
    }
    for (i = 0; i < n; i++)
	{
        if	( !ind->code[i]
			||!ind->pri[i]
			|| ind->pos[i] >= 0)
		{
			continue;
		}

//         trace(4,"reject obs type: sys=%2d, obs=%s\n",sys,tobs[i]);
    }
    ind->n = n;

#if 0 /* for debug */
    for (i=0;i<n;i++) {
        trace(2,"set_index: sys=%2d,tobs=%s code=%2d pri=%2d frq=%d pos=%d shift=%5.2f\n",
              sys,tobs[i],ind->code[i],ind->pri[i],ind->frq[i],ind->pos[i],
              ind->shift[i]);
    }
#endif
}

/* read rinex obs data body --------------------------------------------------*/
int readrnxobsb(
	FILE *fp,
	const char *opt,
	double ver,
	char tobs[][MAXOBSTYPE][4],
	int&	flag,
	RawObs*	data)
{
    gtime_t time = {};
    sigind_t index[6]={};
    char buff[MAXRNXLEN];
    int i=0,n=0,nsat=0;
	SatSys sats[MAXOBS]={};

    /* set signal index */
	set_index(ver, E_Sys::GPS, opt, tobs[0], &index[0]);
	set_index(ver, E_Sys::GLO, opt, tobs[1], &index[1]);
	set_index(ver, E_Sys::GAL, opt, tobs[2], &index[2]);
	set_index(ver, E_Sys::QZS, opt, tobs[3], &index[3]);
	set_index(ver, E_Sys::SBS, opt, tobs[4], &index[4]);
	set_index(ver, E_Sys::CMP, opt, tobs[5], &index[5]);

	/* read record */
    while (fgets(buff,MAXRNXLEN,fp))
	{
        /* decode obs epoch */
        if (i == 0)
		{
            if ((nsat = decode_obsepoch(fp, buff, ver, &time, flag, sats)) <= 0)
			{
                continue;
            }
        }
        else if ( flag<=2
				||flag==6)
		{
            data[n].time	= time;
            data[n].Sat		= sats[i-1];

            /* decode obs data */
            if	( decode_obsdata(fp, buff, ver, index, &data[n])
				&&n<MAXOBS)
				n++;
        }
        if (++i > nsat)
			return n;
    }
    return -1;
}

/* read rinex obs ------------------------------------------------------------*/
int readrnxobs(
	FILE *fp,
	gtime_t ts,
	gtime_t te,
	double tint,
	const char *opt,
	int rcv,
	double ver,
	int tsys,
	char tobs[][MAXOBSTYPE][4],
	obs_t *obs)
{
	std::array<RawObs,MAXOBS> data;
    int n,flag=0,stat=0;

    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxobs: rcv=" << rcv << " ver=" << ver << " tsys=" << tsys;

    if (!obs||rcv>NSTATION)
		return 0;

    /* read rinex obs data body */
    if	( (n = readrnxobsb(fp, opt, ver, tobs, flag, &data[0])) >= 0
		&&stat >= 0)
	{
        for (int i = 0; i < n; i++)
		{
            /* utc -> gpst */
            if (tsys == TSYS_UTC)
				data[i].time = utc2gpst(data[i].time);
        }

        /* screen data by time */
        if (n > 0
			&&!screent(data[0].time, ts, te, tint))
			return stat;

        for (int i = 0; i < n; i++)
		{
            /* save obs data */
			obs->obsList.push_back(data[i]);
        }
        stat = 1;
    }
    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxobs: nobs=" << obs->n
	<< " stat=" << stat;

    return stat;
}
/* decode ephemeris ----------------------------------------------------------*/
int decode_eph(double ver, SatSys Sat, gtime_t toc, const double *data,
                      Eph *eph)
{
    Eph eph0 = {};

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_eph: ver=" << ver << " sat=" << Sat.id();

    int sys = Sat.sys;

    if	( sys != +E_Sys::GPS
		&&sys != +E_Sys::GAL
		&&sys != +E_Sys::QZS
		&&sys != +E_Sys::CMP)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "ephemeris error: invalid satellite sat=" << Sat.id();

        return 0;
    }
    *eph=eph0;

    eph->Sat=Sat;
    eph->toc=toc;

    eph->f0=data[0];
    eph->f1=data[1];
    eph->f2=data[2];

    eph->A=SQR(data[10]); eph->e=data[ 8]; eph->i0  =data[15]; eph->OMG0=data[13];
    eph->omg =data[17]; eph->M0 =data[ 6]; eph->deln=data[ 5]; eph->OMGd=data[18];
    eph->idot=data[19]; eph->crc=data[16]; eph->crs =data[ 4]; eph->cuc =data[ 7];
    eph->cus =data[ 9]; eph->cic=data[12]; eph->cis =data[14];

    if (sys==+E_Sys::GPS||sys==+E_Sys::QZS)
	{
        eph->iode=(int)data[ 3];      /* IODE */
        eph->iodc=(int)data[26];      /* IODC */
        eph->toes=     data[11];      /* toe (s) in gps week */
        eph->week=(int)data[21];      /* gps week */
        eph->toe=adjweek(gpst2time(eph->week,data[11]),toc);
        eph->ttr=adjweek(gpst2time(eph->week,data[27]),toc);

        eph->code=(int)data[20];      /* GPS: codes on L2 ch */
        eph->svh =(int)data[24];      /* sv health */
        eph->sva=uraindex(data[23]);  /* ura (m->index) */
        eph->flag=(int)data[22];      /* GPS: L2 P data flag */

        eph->tgd[0]=   data[25];      /* TGD */
        eph->fit   =   data[28];      /* fit interval */
    }
    else if (sys==+E_Sys::GAL)
	{
		/* GAL ver.3 */
        eph->iode=(int)data[ 3];      /* IODnav */
        eph->toes=     data[11];      /* toe (s) in galileo week */
        eph->week=(int)data[21];      /* gal week = gps week */
        eph->toe=adjweek(gpst2time(eph->week,data[11]),toc);
        eph->ttr=adjweek(gpst2time(eph->week,data[27]),toc);

        eph->code=(int)data[20];      /* data sources */
                                      /* bit 0 set: I/NAV E1-B */
                                      /* bit 1 set: F/NAV E5a-I */
                                      /* bit 2 set: F/NAV E5b-I */
                                      /* bit 8 set: af0-af2 toc are for E5a.E1 */
                                      /* bit 9 set: af0-af2 toc are for E5b.E1 */
        eph->svh =(int)data[24];      /* sv health */
                                      /* bit     0: E1B DVS */
                                      /* bit   1-2: E1B HS */
                                      /* bit     3: E5a DVS */
                                      /* bit   4-5: E5a HS */
                                      /* bit     6: E5b DVS */
                                      /* bit   7-8: E5b HS */
        eph->sva =uraindex(data[23]); /* ura (m->index) */

        eph->tgd[0]=   data[25];      /* BGD E5a/E1 */
        eph->tgd[1]=   data[26];      /* BGD E5b/E1 */
    }
    else if (sys==+E_Sys::CMP)
	{
		/* BeiDou v.3.02 */
        eph->toc=bdt2gpst(eph->toc);  /* bdt -> gpst */
        eph->iode=(int)data[ 3];      /* AODE */
        eph->iodc=(int)data[28];      /* AODC */
        eph->toes=     data[11];      /* toe (s) in bdt week */
        eph->week=(int)data[21];      /* bdt week */
        eph->toe=bdt2gpst(bdt2time(eph->week,data[11])); /* bdt -> gpst */
        eph->ttr=bdt2gpst(bdt2time(eph->week,data[27])); /* bdt -> gpst */
        eph->toe=adjweek(eph->toe,toc);
        eph->ttr=adjweek(eph->ttr,toc);

        eph->svh =(int)data[24];      /* satH1 */
        eph->sva=uraindex(data[23]);  /* ura (m->index) */

        eph->tgd[0]=   data[25];      /* TGD1 B1/B3 */
        eph->tgd[1]=   data[26];      /* TGD2 B2/B3 */
    }

    if (eph->iode<0||1023<eph->iode)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "rinex nav invalid: sat=" << Sat.id() << " iode=" << eph->iode;
    }

    if (eph->iodc<0||1023<eph->iodc)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "rinex nav invalid: sat=" << Sat.id() << " iodc=" << eph->iodc;
    }
    return 1;
}
/* decode glonass ephemeris --------------------------------------------------*/
int decode_geph(double ver, SatSys Sat, gtime_t toc, double *data,
                       Geph *geph)
{
    Geph geph0={};
    gtime_t tof;
    double tow,tod;
    int week,dow;

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_geph: ver=" << ver << " sat=" << Sat.id();

    if (Sat.sys!=+E_Sys::GLO)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "glonass ephemeris error: invalid satellite sat=" << Sat.id();

        return 0;
    }
    *geph=geph0;

    geph->Sat=Sat;

    /* toc rounded by 15 min in utc */
    tow=time2gpst(toc,&week);
    toc=gpst2time(week,floor((tow+450.0)/900.0)*900);
    dow=(int)floor(tow/86400.0);

    /* time of frame in utc */
    tod=ver<=2.99?data[2]:fmod(data[2],86400.0); /* tod (v.2), tow (v.3) in utc */
    tof=gpst2time(week,tod+dow*86400.0);
    tof=adjday(tof,toc);

    geph->toe=utc2gpst(toc);   /* toc (gpst) */
    geph->tof=utc2gpst(tof);   /* tof (gpst) */

    /* iode = tb (7bit), tb =index of UTC+3H within current day */
    geph->iode=(int)(fmod(tow+10800.0,86400.0)/900.0+0.5);

    geph->taun=-data[0];       /* -taun */
    geph->gamn= data[1];       /* +gamman */

    geph->pos[0]=data[3]*1E3; geph->pos[1]=data[7]*1E3; geph->pos[2]=data[11]*1E3;
    geph->vel[0]=data[4]*1E3; geph->vel[1]=data[8]*1E3; geph->vel[2]=data[12]*1E3;
    geph->acc[0]=data[5]*1E3; geph->acc[1]=data[9]*1E3; geph->acc[2]=data[13]*1E3;

    geph->svh=(int)data[ 6];
    geph->frq=(int)data[10];
    geph->age=(int)data[14];

    /* some receiver output >128 for minus frequency number */
    if (geph->frq >  128)
		geph->frq -= 256;

    if (geph->frq<MINFREQ_GLO||MAXFREQ_GLO<geph->frq)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "rinex gnav invalid freq: sat=" << Sat << " fn=" << geph->frq;
    }
    return 1;
}
/* decode geo ephemeris ------------------------------------------------------*/
int decode_seph(double ver, SatSys Sat, gtime_t toc, double *data,
                       Seph *seph)
{
    Seph seph0 = {};
    int week;

    BOOST_LOG_TRIVIAL(debug)
	<< "decode_seph: ver=" << ver << " sat=" << Sat.id();

    if (Sat.sys!=+E_Sys::SBS)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "geo ephemeris error: invalid satellite sat=" << Sat.id();

        return 0;
    }
    *seph=seph0;

    seph->Sat=Sat;
    seph->t0 =toc;

    time2gpst(toc,&week);
    seph->tof=adjweek(gpst2time(week,data[2]),toc);

    seph->af0=data[0];
    seph->af1=data[1];

    seph->pos[0]=data[3]*1E3; seph->pos[1]=data[7]*1E3; seph->pos[2]=data[11]*1E3;
    seph->vel[0]=data[4]*1E3; seph->vel[1]=data[8]*1E3; seph->vel[2]=data[12]*1E3;
    seph->acc[0]=data[5]*1E3; seph->acc[1]=data[9]*1E3; seph->acc[2]=data[13]*1E3;

    seph->svh=(int)data[6];
    seph->sva=uraindex(data[10]);

    return 1;
}

/* read rinex navigation data body -------------------------------------------*/
int readrnxnavb(FILE *fp, const char *opt, double ver, E_Sys sys,
                       int *type, Eph *eph, Geph *geph, Seph *seph)
{
    gtime_t toc;
    double data[64];
    int i=0,j,prn,sp=3;

    char buff[MAXRNXLEN],id[8]="",*p;

    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxnavb: ver=" << ver << " sys=" << sys;

	SatSys Sat = {};
    while (fgets(buff,MAXRNXLEN,fp))
	{

        if (i==0)
		{
            /* decode satellite field */
            if (ver>=3.0||sys==+E_Sys::GAL||sys==+E_Sys::QZS)
			{
				/* ver.3 or GAL/QZS */
                strncpy(id,buff,3);
                Sat=SatSys(id);
                sp=4;
                if (ver>=3.0) sys=Sat.sys;
            }
            else
			{
                prn=(int)str2num(buff,0,2);
                Sat.sys = sys;
				Sat.prn = prn;
            }
            /* decode toc field */
            if (str2time(buff+sp,0,19,&toc))
			{
                BOOST_LOG_TRIVIAL(debug)
				<< "rinex nav toc error: " << buff;

                return 0;
            }
            /* decode data fields */
            for (j=0,p=buff+sp+19;j<3;j++,p+=19)
			{
                data[i++]=str2num(p,0,19);
            }
        }
        else
		{
            /* decode data fields */
            for (j=0,p=buff+sp;j<4;j++,p+=19)
			{
                data[i++]=str2num(p,0,19);
            }
            /* decode ephemeris */
            if		(sys==+E_Sys::GLO&&i>=15)	{ *type=1; return decode_geph(ver,Sat,toc,data,geph);  }
            else if	(sys==+E_Sys::SBS&&i>=15) 	{ *type=2; return decode_seph(ver,Sat,toc,data,seph);  }
            else if	(i>=31)						{ *type=0; return decode_eph(ver,Sat,toc,data,eph);    }
        }
    }
    return -1;
}

/* add ephemeris to navigation data ------------------------------------------*/
void add_eph(nav_t *nav, const Eph& eph)
{
	nav->ephMap[eph.Sat].push_back(eph);
}

void add_geph(nav_t *nav, const Geph& geph)
{
	nav->gephMap[geph.Sat].push_back(geph);
}

void add_seph(nav_t *nav, const Seph& seph)
{
	nav->sephMap[seph.Sat].push_back(seph);
}

/* read rinex nav/gnav/geo nav -----------------------------------------------*/
int readrnxnav(FILE *fp, const char *opt, double ver, E_Sys sys, nav_t *nav)
{
    Eph		eph;
    Geph	geph;
    Seph	seph;
    int stat,type;

    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxnav: ver=" << ver << " sys=" << sys;

    if (!nav)
		return 0;

    /* read rinex navigation data body */
    while ((stat=readrnxnavb(fp,opt,ver,sys,&type,&eph,&geph,&seph))>=0)
	{

        /* add ephemeris to navigation data */
        if (stat)
		{
            switch (type)
			{
                case 1 : add_geph(nav,geph); break;
                case 2 : add_seph(nav,seph); break;
                default: add_eph (nav,eph ); break;
            }
        }
    }
    return nav->ephMap.size()>0||nav->gephMap.size()>0||nav->sephMap.size()>0;
}

/* read rinex clock ----------------------------------------------------------*/
int readrnxclk(FILE *fp, const char *opt, int index, nav_t *nav)
{
    char buff[MAXRNXLEN];

//     trace(3,"readrnxclk: index=%d\n", index);

    if (!nav)
		return 0;

    while (fgets(buff, sizeof(buff), fp))
	{
		gtime_t time;
        if (str2time(buff,8,26,&time))
		{
//             trace(2,"rinex clk invalid epoch: %34.34s\n", buff);
            continue;
        }

		string type;
		type.assign(buff,2);

		string idString;
		if		(type == "AS")	{	idString.assign(buff + 3, 3);	}
		else if	(type == "AR")	{	idString.assign(buff + 3, 4);	}
		else 						continue;

		Pclk preciseClock = {};

		preciseClock.clk	= str2num(buff, 40, 19);
		preciseClock.std	= str2num(buff, 60, 19);
		preciseClock.time	= time;
		preciseClock.index	= index;

		nav->pclkMap[idString].push_back(preciseClock);
    }

    return nav->pclkMap.size() > 0;
}

/* read rinex file -----------------------------------------------------------*/
int readrnxfppde(
	FILE *fp,
	gtime_t ts,
	gtime_t te,
	double tint,
	const char *opt,
	int flag,
	int index,
	char *type,
	obs_t *obs,
	nav_t *nav,
	sta_t *sta,
	double *ver,
	E_Sys *sys,
	int *tsys,
	char tobs[][MAXOBSTYPE][4])
{
    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxfp: flag=" << flag << " index=" << index;

    /* read rinex header */
    if (!ftell(fp))
	{
        if (!readrnxh(fp, ver, type, sys, tsys, tobs, nav, sta)) return 0;
        else return 1;
    }

    /* flag=0:except for clock,1:clock */
    if	( (!flag	&&	*type == 'C')
		||( flag	&&	*type != 'C'))
		return 0;

    /* read rinex body */
    switch (*type)
	{
        case 'O': return readrnxobs(fp,ts,te,tint,opt,index,*ver,*tsys,tobs,obs);
        case 'N': return readrnxnav(fp,opt,*ver,*sys      ,nav);
        case 'G': return readrnxnav(fp,opt,*ver,E_Sys::GLO,nav);
        case 'H': return readrnxnav(fp,opt,*ver,E_Sys::SBS,nav);
        case 'J': return readrnxnav(fp,opt,*ver,E_Sys::QZS,nav); /* extension */
        case 'L': return readrnxnav(fp,opt,*ver,E_Sys::GAL,nav); /* extension */
        case 'C': return readrnxclk(fp,opt,index,nav);
    }
    BOOST_LOG_TRIVIAL(debug)
	<< "unsupported rinex type ver=" << *ver << " type=" << *type;

    return 0;
}

/* read rinex file -----------------------------------------------------------*/
int readrnxfp(FILE *fp, gtime_t ts, gtime_t te, double tint,
                     const char *opt, int flag, int index, char *type,
                     obs_t *obs, nav_t *nav, sta_t *sta)
{
    double ver;
    int tsys;
    char tobs[NUMSYS][MAXOBSTYPE][4]={{""}};
	E_Sys sys;

    BOOST_LOG_TRIVIAL(debug)
	<< "readrnxfp: flag=" << flag << " index=" << index;

    /* read rinex header */
    if (!readrnxh(fp,&ver,type,&sys,&tsys,tobs,nav,sta))
		return 0;

    /* flag=0:except for clock,1:clock */
    if ((!flag&&*type=='C')||(flag&&*type!='C'))
		return 0;

    /* read rinex body */
    switch (*type)
	{
        case 'O': return readrnxobs(fp,ts,te,tint,opt,index,ver,tsys,tobs,obs);
        case 'N': return readrnxnav(fp,opt,ver,sys    	 ,nav);
        case 'G': return readrnxnav(fp,opt,ver,E_Sys::GLO,nav);
        case 'H': return readrnxnav(fp,opt,ver,E_Sys::SBS,nav);
        case 'J': return readrnxnav(fp,opt,ver,E_Sys::QZS,nav); /* extension */
        case 'L': return readrnxnav(fp,opt,ver,E_Sys::GAL,nav); /* extension */
        case 'C': return readrnxclk(fp,opt,index,nav);
    }
    BOOST_LOG_TRIVIAL(debug)
	<< "unsupported rinex type ver=" << ver << " type=" << *type;

    return 0;
}

/* read rinex obs and nav files ------------------------------------------------
* read rinex obs and nav files
* args   : char *file    I      file (wild-card * expanded) ("": stdin)
*          int   rcv     I      receiver number for obs data
*         (gtime_t ts)   I      observation time start (ts.time==0: no limit)
*         (gtime_t te)   I      observation time end   (te.time==0: no limit)
*         (double tint)  I      observation time interval (s) (0:all)
*          char  *opt    I      rinex options (see below,"": no option)
*          obs_t *obs    IO     observation data   (NULL: no input)
*          nav_t *nav    IO     navigation data    (NULL: no input)
*          sta_t *sta    IO     station parameters (NULL: no input)
* return : status (1:ok,0:no data,-1:error)
* notes  : read data are appended to obs and nav struct
*          before calling the function, obs and nav should be initialized.
*          observation data and navigation data are not sorted.
*          navigation data may be duplicated.
*
*          rinex options (separated by spaces) :
*
*            -GLss[=shift]: select GPS signal ss (ss: RINEX 3 code, "1C","2W"...)
*            -RLss[=shift]: select GLO signal ss
*            -ELss[=shift]: select GAL signal ss
*            -JLss[=shift]: select QZS signal ss
*            -CLss[=shift]: select BDS signal ss
*            -SLss[=shift]: select SBS signal ss
*
*            shift: carrier phase shift to be added (cycle)
*
*-----------------------------------------------------------------------------*/
// extern int readrnxt(const char *file, int rcv, gtime_t ts, gtime_t te,
//                     double tint, const char *opt, obs_t *obs, nav_t *nav,
//                     sta_t *sta)
// {
//     int i,n,stat=0;
//     const char *p;
//     char type=' ',*files[MAXEXFILE]={0};
//
//     trace(3,"readrnxt: file=%s rcv=%d\n",file,rcv);
//
//     if (!*file) {
//         return readrnxfp(stdin,ts,te,tint,opt,0,1,&type,obs,nav,sta);
//     }
//     for (i=0;i<MAXEXFILE;i++) {
//         if (!(files[i]=(char *)malloc(1024))) {
//             for (i--;i>=0;i--) free(files[i]);
//             return -1;
//         }
//     }
//     /* expand wild-card */
//     if ((n=expath(file,files,MAXEXFILE))<=0) {
//         for (i=0;i<MAXEXFILE;i++) free(files[i]);
//         return 0;
//     }
//     /* read rinex files */
//     for (i=0;i<n&&stat>=0;i++) {
//         stat=readrnxfile(files[i],ts,te,tint,opt,0,rcv,&type,obs,nav,sta);
//     }
//     /* if station name empty, set 4-char name from file head */
//     if (type=='O'&&sta) {
//         if (!(p=strrchr(file,FILEPATHSEP))) p=file-1;
//         if (!*sta->name) setstr(sta->name,p+1,4);
//     }
//     for (i=0;i<MAXEXFILE;i++) free(files[i]);
//
//     return stat;
// }
// extern int readrnx(const char *file, int rcv, const char *opt, obs_t *obs,
//                    nav_t *nav, sta_t *sta)
// {
//     gtime_t t={0};
//
//     trace(3,"readrnx : file=%s rcv=%d\n",file,rcv);
//
//     return readrnxt(file,rcv,t,t,0.0,opt,obs,nav,sta);
// }
/* read rinex clock files ------------------------------------------------------
* read rinex clock files
* args   : char *file    I      file (wild-card * expanded)
*          nav_t *nav    IO     navigation data    (NULL: no input)
* return : number of precise clock
*-----------------------------------------------------------------------------*/
// extern int readrnxc(const char *file, nav_t *nav)
// {
//     gtime_t t={0};
//     int i,n,index=0,stat=1;
//     char *files[MAXEXFILE]={0},type;
//
//     trace(3,"readrnxc: file=%s\n",file);
//
//     for (i=0;i<MAXEXFILE;i++) {
//         if (!(files[i]=(char *)malloc(1024))) {
//             for (i--;i>=0;i--) free(files[i]); return 0;
//         }
//     }
//     /* expand wild-card */
//     n=expath(file,files,MAXEXFILE);
//
//     /* read rinex clock files */
//     for (i=0;i<n;i++) {
//         if (readrnxfile(files[i],t,t,0.0,NULL,1,index++,&type,NULL,nav,NULL)) {
//             continue;
//         }
//         stat=0;
//         break;
//     }
//     for (i=0;i<MAXEXFILE;i++) free(files[i]);
//
//     if (!stat) return 0;
//
//     /* unique and combine ephemeris and precise clock */
//     combpclk(nav);
//
//     return nav->pclkMap.size();
// }

#ifndef __RINEX__HPP__
#define __RINEX__HPP__

#include "gaTime.hpp"
#include "enums.h"

struct sta_t;
struct obs_t;
struct nav_t;

int readrnxfppde(FILE *fp, GTime ts, GTime te, double tint,  const char *opt, int flag, int index, char *type,  obs_t *obs, nav_t *nav, sta_t *sta, double *ver1,  E_Sys *sys1, int *tsys1, char tobs[][MAXOBSTYPE][4]);
int readrnx (const char *file, int rcv, const char *opt, obs_t *obs,  nav_t *nav, sta_t *sta);
int readrnxt(const char *file, int rcv, GTime ts, GTime te, double tint, const char *opt, obs_t *obs, nav_t *nav,  sta_t *sta);
int readrnxc(const char *file, nav_t *nav);

#endif

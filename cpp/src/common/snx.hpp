
#ifndef __SNX_HPP__
#define __SNX_HPP__

#include <vector>
#include <string>

using std::vector;
using std::string;

#define MAXSNXSTR      64

typedef struct                  /* station-wise information */
{
    /* header block */
    char    snxtype[MAXSNXSTR];    /* SINEX file type */
    double  ver;                /* version */
    int     np;                 /* number of estimated parameters */
    char    solcont[MAXSNXSTR];    /* solution types S O E T C A */

    /* ID block */
	string	siteCode;
	string	ptCode;
	string	monuId;

    /* receiver block */
    char    rectype[MAXSNXSTR];    /* receiver type */
    char    recsn[MAXSNXSTR];      /* receiver serial number */
    char    recfirm[MAXSNXSTR];    /* receiver firmware */
    int     recstart[3];        /* receiver start time YY:DOY:SOD */
    int     recend[3];          /* receiver end time YY:DOY:SOD */

    /* anntenna block */
    char    anttype[MAXSNXSTR];    /* antenna type */
    char    antsn[MAXSNXSTR];      /* antenna serial number */
    int     antstart[3];        /* antenna start time YY:DOY:SOD */
    int     antend[3];          /* antenna end time YY:DOY:SOD */

    /* GPS phase_center block (no Galileo yet) */
    double  gpsl1[3];           /* GPS L1 PCO UNE (m) */
    double  gpsl2[3];           /* GPS L2 PCO UNE (m) */

    /* eccentricity block */
    int     eccrs;              /* reference system UNE (0) or XYZ (1) */
    double  ecc[3];             /* eccentricity UNE or XYZ (m) */

    /* solution/estimate block */
    int     solvalid[3];        /* solution valid time */
    char    unit[MAXSNXSTR];       /* parameter units */
    double  pos[3];             /* real position (ecef) (m)*/
    double  pstd[3];            /* position std (m) */

    /* epochs block (not implemented) */

    /* bias/epochs block (not implemented) */

    /* solution/apriori block (not implemented) */

} snx_t;

//===============================================================================
// site/id block structure
/*
*-------------------------------------------------------------------------------
+SITE/ID
*CODE PT __DOMES__ T _STATION DESCRIPTION__ _LONGITUDE_ _LATITUDE__ HEIGHT_
*/
//===============================================================================
typedef struct
{
    char sitecode[5]; // station 4 char id TODO: change to std::string
    char ptcode[3];   // physical monument used at the site TODO: -> std::string
    char domes[10];   // domes number unique monument num TODO: -> std::string
    char typecode[2]; // observation technique {C,D,L,M,P,or R}
    char desc[23];    // site description eg town/city TODO: ->std::string
    char long_deg[4]; // longitude degress (uint16_t) east is positive
    char long_min[3]; // TODO uint8_t
    float long_sec; // TDOO float
    char lat_deg[4];  // int16_t (+/-)
    char lat_min[3];  // uint8_t
    float lat_sec;  // float
    double height;    //
} snx_siteid_t;
//=============================================================================
//+SOLUTION/ESTIMATE
//                                                                                                                 |*INDEX _TYPE_ CODE PT SOLN _REF_EPOCH__ UNIT S ___ESTIMATED_VALUE___ __STD_DEV__
//    1 STAX   ALBH  A    1 10:001:00000 m    2 -2.34133301687257e+06 5.58270e-04
//    2 STAY   ALBH  A    1 10:001:00000 m    2 -3.53904951624333e+06 7.77370e-04
//    3 STAZ   ALBH  A    1 10:001:00000 m    2  4.74579129951391e+06 8.98560e-04
//=============================================================================
/*typedef struct
{
    char type[5];
    char code[5];
    int  soln;
    int  ref_year;
    int  ref_day;
    int  ref_seconds;
    char unit[2];
    double estimate;
    double stdev;
} snx_solution_t;*/
//=============================================================================
/*
+SITE/RECEIVER
*CODE PT SOLN T _DATA START_ __DATA_END__ ___RECEIVER_TYPE____ _S/N_ _FIRMWARE__
 ALBH  A ---- C 93:327:70260 94:016:15540 ROGUE SNR-8C         313   Meenix 7.4
 ALBH  A ---- C 94:016:15540 95:011:80100 ROGUE SNR-8000       168   SFG2 0.0 lk
*/
//=============================================================================
typedef struct
{
    char    sitecode[5];
    char    ptcode[3];   // physical monument used at the site TODO: -> std::string
    int     solnnum;
    char    typecode[2];
    int     recstart[3];        /* receiver start time YY:DOY:SOD */
    int     recend[3];          /* receiver end time YY:DOY:SOD */
    char    rectype[21];    /* receiver type */
    char    recsn[6];      /* receiver serial number */
    char    recfirm[12];    /* receiver firmware */
}snx_receiver_t;

//=============================================================================
/*
+SITE/ANTENNA
*CODE PT SOLN T _DATA START_ __DATA_END__ ____ANTENNA_TYPE____ _S/N_
 ALBH  A ---- C 92:125:00000 94:104:74100 AOAD/M_B        EMRA 91119
 ALBH  A ---- C 94:104:74100 95:011:80100 AOAD/M_T        EMRA 92172
*/
//=============================================================================
typedef struct
{
    char    sitecode[5];
    char    ptcode[3];   // physical monument used at the site TODO: -> std::string
    int     solnnum;
    char    typecode[2];
    int     antstart[3];        /* antenna start time YY:DOY:SOD */
    int     antend[3];          /* antenna end time YY:DOY:SOD */
    char    anttype[21];    /* receiver type */
    char    antsn[6];      /* receiver serial number */
}snx_antenna_t;
//=============================================================================
/*
+SITE/ECCENTRICITY
*CODE PT SOLN T _DATA START_ __DATA_END__ REF __DX_U__ __DX_N__ __DX_E__
 ALBH  A ---- C 92:146:00000 94:104:74100 UNE   0.1260   0.0000   0.0000
*/
//=============================================================================
typedef struct
{
    char    sitecode[5];
    char    ptcode[3];   // physical monument used at the site TODO: -> std::string
    int     solnnum;
    char    typecode[2];
    int     eccstart[3];        /* antenna start time YY:DOY:SOD */
    int     eccend[3];          /* antenna end time YY:DOY:SOD */
    char    eccrs[4];              /* reference system UNE (0) or XYZ (1) */
    double  ecc[3];             /* eccentricity UNE or XYZ (m) */
}snx_ecc_t;
//=============================================================================
/*
+SOLUTION/EPOCHS
*CODE PT SOLN T _DATA_START_ __DATA_END__ _MEAN_EPOCH_
 ALBH  A    1 C 94:002:00000 94:104:00000 94:053:00000
*/
//=============================================================================
typedef struct
{
    char    sitecode[5];
    char    ptcode[3];   // physical monument used at the site TODO: -> std::string
    int     solnnum;
    char    typecode[2];
    int     start[3];
    int     end[3];
    int     mean[3];

}snx_solepoch_t;
//=============================================================================
/*
+SOLUTION/ESTIMATE
*INDEX _TYPE_ CODE PT SOLN _REF_EPOCH__ UNIT S ___ESTIMATED_VALUE___ __STD_DEV__
     1 STAX   ALBH  A    1 10:001:00000 m    2 -2.34133301687257e+06 5.58270e-04
     2 STAY   ALBH  A    1 10:001:00000 m    2 -3.53904951624333e+06 7.77370e-04
     3 STAZ   ALBH  A    1 10:001:00000 m    2  4.74579129951391e+06 8.98560e-04
     4 VELX   ALBH  A    1 10:001:00000 m/y  2 -9.92019926884722e-03 1.67050e-05
     5 VELY   ALBH  A    1 10:001:00000 m/y  2 -8.46787398931193e-04 2.12080e-05
     6 VELZ   ALBH  A    1 10:001:00000 m/y  2 -4.85721729753769e-03 2.39140e-05
*/
//=============================================================================
typedef struct
{
    int     index;
    char    type[7];
    char    sitecode[5];
    char    ptcode[3];   // physical monument used at the site TODO: -> std::string
    int     solnnum;
    int     refepoch[3];
    char    unit[5];
    char    S[2];
    double  estimate;
    double  stdev;

}snx_solution_t;

typedef struct
{
    vector<snx_siteid_t>   vec_siteid;
    vector<snx_receiver_t> vec_receivers;
    vector<snx_antenna_t>  vec_antennas;
    vector<snx_ecc_t>      vec_eccs;
    vector<snx_solepoch_t> vec_solepochs;
    vector<snx_solution_t> vec_sols;
} sinex_t;

int snxGetSites(const char *file, std::vector<snx_siteid_t> *vec_snxid);

/* SINEX functions */
int readsnx(const char *file, char *sta, snx_t *snx);
int readsinex(const char *file, sinex_t *snx);
void snxsiteids(char *buff,std::vector<snx_siteid_t> *vec_snxid);
void snxreceivers(char *buff,std::vector<snx_receiver_t> *vec_receivers);
void snxantennas(char *buff,std::vector<snx_antenna_t> *vec_antennas);
void snxeccs(char *buff,std::vector<snx_ecc_t> *vec_eccs);
void snxsolepochs(char *buff, std::vector<snx_solepoch_t> *vec_solepochs);
void snxsolutions(char *buff,std::vector<snx_solution_t> *vec_solutions);
#endif // SNX_H

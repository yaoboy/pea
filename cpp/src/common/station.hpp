#ifndef __STATION__HPP
#define __STATION__HPP

#include <memory>

#include "constants.h"
#include "station.hpp"
#include "gaTime.hpp"
#include "newsnx.hpp"
#include "snx.hpp"
#include "ppp.hpp"
#include "vmf3.h"

#include "eigenIncluder.hpp"

struct sta_t
{        /* station parameter type */
    char name   [MAXANT]; /* marker name */			//todo aaron, remove these
    char marker [MAXANT]; /* marker number */
    char antdes [MAXANT]; /* antenna descriptor */
    char antsno [MAXANT]; /* antenna serial number */
    char rectype[MAXANT]; /* receiver type descriptor */
    char recver [MAXANT]; /* receiver firmware version */
    char recsno [MAXANT]; /* receiver serial number */
    int antsetup;       /* antenna setup id */
    int itrf;           /* ITRF realization year */
    int deltype;        /* antenna delta type (0:enu,1:xyz) */
    Vector3d rRec;      /* station position (ecef) (m) */
    Vector3d del;      /* antenna position delta (e/n/u or x/y/z) (m) */
    double hgt;         /* antenna height (m) */
    double rbias[2][3]; /* receiver dcb (0:p1-p2, 1:p1-c1, 2:p2-c2) (m) */
};

struct IonoStation
{
	double Rot_pos[3]	= {};
};


struct StationLogs
{
	GTime	firstEpoch	= GTime::noTime();
	GTime	lastEpoch	= GTime::noTime();
	int		epochCount	= 0;
	int		obsCount	= 0;
	int		slipCount	= 0;
	map<E_ObsCode, int>	codeCount;
	map<string, int>	satCount;
};

/** Object to maintain receiver station data
 */
struct Station : IonoStation, StationLogs
{
	sta_t				station;					///< Legacy options to be (re)moved
	rtk_t 				rtk;						///< Legacy rtk filter status
	newsnx_stn_snx_t 	snx;						///< Antenna information
// 	snx_t 				snx;						///< Antenna information
	vmf3_t				vmf3	= {.m = 1};

	ObsList				obsList;					///< Observations available for this station at this epoch
	string				id;							///< Unique name for this station (4 characters)

	double				mjd0[3]			= {}; 		// mjd time for vmf3
	ClockJump			cj				= {};

 	std::unique_ptr<std::ostream>	trace;	///< Trace output for this station
};

using StationList = list<Station*>;			///< List of station pointers


#endif

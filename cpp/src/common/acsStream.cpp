
#include "navigation.hpp"
#include "acsStream.hpp"
#include "enums.h"


// From the RTCM spec...
//  - table 3.5-91 (GPS)
//  - table 3.5-96 (GLONASS)
//  - table 3.5-99 (GALILEO)
//  - table 3.5-105 (QZSS)
//  - table 3.5-108 (BEIDOU)



struct SignalInfo
{
    uint8_t		signal_id;
    E_FType		ftype;
    E_ObsCode	rinex_observation_code;
};


BETTER_ENUM(RtcmMessageType, uint16_t,
        NONE 			= 0,
        MSM7_GPS 		= 1077,
        MSM7_GLONASS 	= 1087,
        MSM7_GALILEO 	= 1097,
        MSM7_QZSS 		= 1117,
        MSM7_BEIDOU 	= 1127,
        GPS_EPHEMERIS	= 1019,
		SSR_ORB_CORR	= 1057,
		SSR_CLK_CORR	= 1058,
		SSR_COMB_CORR	= 1060,
		SSR_URA			= 1061
)


// sys -> rtcm signal enum -> siginfo (sig enum,
map<E_Sys, map<uint8_t, SignalInfo>> signal_id_mapping =
{
    {	E_Sys::GPS,
		{
			{2,		{2,		L1,		E_ObsCode::L1C}},
			{3,		{3,		L1,		E_ObsCode::L1P}},
			{4,		{4,		L1,		E_ObsCode::L1W}},

			{8,		{8,		L2,		E_ObsCode::L2C}},
			{9,		{9,		L2,		E_ObsCode::L2P}},
			{10,	{10,	L2,		E_ObsCode::L2W}},
			{15,	{15,	L2,		E_ObsCode::L2S}},
			{16,	{16,	L2,		E_ObsCode::L2L}},
			{17,	{17,	L2,		E_ObsCode::L2X}},

			{22,	{22,	L5,		E_ObsCode::L5I}},
			{23,	{23,	L5,		E_ObsCode::L5Q}},
			{24,	{24,	L5,		E_ObsCode::L5X}},

			{30,	{2,		L1,		E_ObsCode::L1S}},
			{31,	{2,		L1,		E_ObsCode::L1L}},
			{32,	{2,		L1,		E_ObsCode::L1X}}
		}
	},

    {	E_Sys::GLO,
		{
			{2,		{2,		G1,		E_ObsCode::L1C}},
			{3,		{3,		G1,		E_ObsCode::L1P}},

			{8,		{8,		G2,		E_ObsCode::L2C}},
			{9,		{9,		G2,		E_ObsCode::L2P}}
		}
	},

    {	E_Sys::GAL,
		{
			{2,		{2,		E1,		E_ObsCode::L1C}},
			{3,		{3,		E1,		E_ObsCode::L1A}},
			{4,		{4,		E1,		E_ObsCode::L1B}},
			{5,		{5,		E1,		E_ObsCode::L1X}},
			{6,		{6,		E1,		E_ObsCode::L1Z}},

			{8,		{8,		E6,		E_ObsCode::L6C}},
			{9,		{9,		E6,		E_ObsCode::L6A}},
			{10,	{10,	E6,		E_ObsCode::L6B}},
			{11,	{11,	E6,		E_ObsCode::L6X}},
			{12,	{12,	E6,		E_ObsCode::L6Z}},

			{14,	{14,	E5B,	E_ObsCode::L7I}},
			{15,	{15,	E5B,	E_ObsCode::L7Q}},
			{16,	{16,	E5B,	E_ObsCode::L7X}},

			{18,	{18,	E5AB,	E_ObsCode::L8I}},
			{19,	{19,	E5AB,	E_ObsCode::L8Q}},
			{20,	{20,	E5AB,	E_ObsCode::L8X}},

			{22,	{22,	E5A,	E_ObsCode::L5I}},
			{23,	{23,	E5A,	E_ObsCode::L5Q}},
			{24,	{24,	E5A,	E_ObsCode::L5X}}
		}
	},

    {	E_Sys::QZS,
		{
			{2,		{2,		L1,		E_ObsCode::L1C}},

			{9,		{9,		LEX,	E_ObsCode::L6S}},
			{10,	{10,	LEX,	E_ObsCode::L6L}},
			{11,	{11,	LEX,	E_ObsCode::L6X}},

			{15,	{15,	L2,		E_ObsCode::L2S}},
			{16,	{16,	L2,		E_ObsCode::L2L}},
			{17,	{17,	L2,		E_ObsCode::L2X}},

			{22,	{22,	L5,		E_ObsCode::L5I}},
			{23,	{23,	L5,		E_ObsCode::L5Q}},
			{24,	{24,	L5,		E_ObsCode::L5X}},

			{30,	{30,	L1,		E_ObsCode::L1S}},
			{31,	{31,	L1,		E_ObsCode::L1L}},
			{32,	{32,	L1,		E_ObsCode::L1X}}
		}
	},

    {	E_Sys::CMP,
		{
			{2,		{2,		B1,		E_ObsCode::L2I}},
			{3,		{3,		B1,		E_ObsCode::L2Q}},
			{4,		{4,		B1,		E_ObsCode::L2X}},

			{8,		{8,		B3,		E_ObsCode::L6I}},
			{9,		{9,		B3,		E_ObsCode::L6Q}},
			{10,	{10,	B3,		E_ObsCode::L6X}},

			{14,	{14,	B2,		E_ObsCode::L7I}},
			{15,	{15,	B2,		E_ObsCode::L7Q}},
			{16,	{16,	B2,		E_ObsCode::L7X}}
		}
	}
};

// sys -> rtcm signal enum -> siginfo (sig enum,
map<E_Sys, map<E_ObsCode, E_FType>> codeTypeMap =
{
    {	E_Sys::GPS,
		{
			{E_ObsCode::L1C,	L1	},
			{E_ObsCode::L1P,	L1	},
			{E_ObsCode::L1W,	L1	},

			{E_ObsCode::L2C,	L2	},
			{E_ObsCode::L2P,	L2	},
			{E_ObsCode::L2W,	L2	},
			{E_ObsCode::L2S,	L2	},
			{E_ObsCode::L2L,	L2	},
			{E_ObsCode::L2X,	L2	},

			{E_ObsCode::L5I,	L5	},
			{E_ObsCode::L5Q,	L5	},
			{E_ObsCode::L5X,	L5	},

			{E_ObsCode::L1S,	L1	},
			{E_ObsCode::L1L,	L1	},
			{E_ObsCode::L1X,	L1	}
		}
	},

    {	E_Sys::GLO,
		{
			{E_ObsCode::L1C,	G1	},
			{E_ObsCode::L1P,	G1	},

			{E_ObsCode::L2C,	G2	},
			{E_ObsCode::L2P,	G2	}
		}
	},

    {	E_Sys::GAL,
		{
			{E_ObsCode::L1C,	E1	},
			{E_ObsCode::L1A,	E1	},
			{E_ObsCode::L1B,	E1	},
			{E_ObsCode::L1X,	E1	},
			{E_ObsCode::L1Z,	E1	},

			{E_ObsCode::L6C,	E6	},
			{E_ObsCode::L6A,	E6	},
			{E_ObsCode::L6B,	E6	},
			{E_ObsCode::L6X,	E6	},
			{E_ObsCode::L6Z,	E6	},

			{E_ObsCode::L7I,	E5B	},
			{E_ObsCode::L7Q,	E5B	},
			{E_ObsCode::L7X,	E5B	},

			{E_ObsCode::L8I,	E5AB},
			{E_ObsCode::L8Q,	E5AB},
			{E_ObsCode::L8X,	E5AB},

			{E_ObsCode::L5I,	E5A	},
			{E_ObsCode::L5Q,	E5A	},
			{E_ObsCode::L5X,	E5A	}
		}
	},

    {	E_Sys::QZS,
		{
			{E_ObsCode::L1C,	L1	},

			{E_ObsCode::L6S,	LEX	},
			{E_ObsCode::L6L,	LEX	},
			{E_ObsCode::L6X,	LEX	},

			{E_ObsCode::L2S,	L2	},
			{E_ObsCode::L2L,	L2	},
			{E_ObsCode::L2X,	L2	},

			{E_ObsCode::L5I,	L5	},
			{E_ObsCode::L5Q,	L5	},
			{E_ObsCode::L5X,	L5	},

			{E_ObsCode::L1S,	L1	},
			{E_ObsCode::L1L,	L1	},
			{E_ObsCode::L1X,	L1	}
		}
	},

    {	E_Sys::CMP,
		{
			{E_ObsCode::L2I,	B1	},
			{E_ObsCode::L2Q,	B1	},
			{E_ObsCode::L2X,	B1	},

			{E_ObsCode::L6I,	B3	},
			{E_ObsCode::L6Q,	B3	},
			{E_ObsCode::L6X,	B3	},

			{E_ObsCode::L7I,	B2	},
			{E_ObsCode::L7Q,	B2	},
			{E_ObsCode::L7X,	B2	}
		}
	}
};

// From the RTCM spec table 3.5-73

map<E_Sys, map<E_FType, double>> signal_phase_alignment =
{
    {	E_Sys::GPS,
		{
			{L1,		1.57542E9},
			{L2,		1.22760E9},
			{L5,		1.17645E9}
		}
	},

    // TODO

    //    {SatelliteSystem::GLONASS, {
    //        {FrequencyBand::G1, 1.57542E9},
    //        {FrequencyBand::G2, 1.22760E9},
    //    }},

    {	E_Sys::GAL,
		{
			{E1,		1.57542E9},
			{E5A,		1.17645E9},
			{E5B,		1.20714E9},
			{E5AB,		1.1191795E9},
			{E6,		1.27875E9},
		}
	},

    {	E_Sys::QZS,
		{
			{L1,		1.57542E9},
			{L2,		1.22760E9},
			{L5,		1.17645E9}
		}
	},

    {	E_Sys::CMP,
		{
			{B1,		1.561098E9},
			{B2,		1.207140E9},
			{B3,		1.26852E9}
		}
	}
};

const uint16_t PREAMBLE = 0xD3;


struct RtcmDecoder
{
	static void setTime(GTime& time, double tow)
	{
		auto now = utc2gpst(timeget());

		int week;
		double tow_p = time2gpst(now, &week);		//todo aaron, cant use now all the time

		int sPerWeek = 60*60*24*7;
		if      (tow < tow_p - sPerWeek/2)				tow += sPerWeek;
		else if (tow > tow_p + sPerWeek/2)				tow -= sPerWeek;

		time = gpst2time(week, tow);
	}

    struct Decoder
    {
		uint8_t* 		data			= nullptr;
		unsigned int 	message_length	= 0;
    };



	struct SSRDecoder : Decoder
	{
		constexpr static int updateInterval[16] =
		{
			1, 2, 5, 10, 15, 30, 60, 120, 240, 300, 600, 900, 1800, 3600, 7200, 10800
		};

		void decode()
		{
			int i = 0;
			int message_number		= getbituInc(data, i, 12);
			int	epochTime1s			= getbituInc(data, i, 20);
			int	ssrUpdateIntIndex	= getbituInc(data, i, 4);
			int	multipleMessage		= getbituInc(data, i, 1);

			int ssrUpdateInterval	= updateInterval[ssrUpdateIntIndex];
			double epochTime = epochTime1s + ssrUpdateInterval / 2;

			if 	( message_number == RtcmMessageType::SSR_ORB_CORR
				||message_number == RtcmMessageType::SSR_COMB_CORR)
			{
				int referenceDatum	= getbituInc(data, i, 1);
			}
			int	iod					= getbituInc(data, i, 4);
			int	provider			= getbituInc(data, i, 16);
			int	solution			= getbituInc(data, i, 4);
			int	numSats				= getbituInc(data, i, 6);

			for (int sat = 0; sat < numSats; sat++)
			{
				int	satId			= getbituInc(data, i, 6);

				SatSys Sat(E_Sys::GPS, satId);

				if 	( message_number == RtcmMessageType::SSR_ORB_CORR
					||message_number == RtcmMessageType::SSR_COMB_CORR)
				{
					SSREph ssrEph;

					setTime(ssrEph.t0, epochTime);

					ssrEph.iod 			= iod;
					ssrEph.iode			= getbituInc(data, i, 8);
					ssrEph.deph[0]		= getbitsInc(data, i, 22) * 0.1e-3;
					ssrEph.deph[1]		= getbitsInc(data, i, 20) * 0.4e-3;
					ssrEph.deph[2]		= getbitsInc(data, i, 20) * 0.4e-3;
					ssrEph.ddeph[0]		= getbitsInc(data, i, 21) * 0.001e-3;
					ssrEph.ddeph[1]		= getbitsInc(data, i, 19) * 0.004e-3;
					ssrEph.ddeph[2]		= getbitsInc(data, i, 19) * 0.004e-3;

					if	( ssrEph.iod		!= nav.satNavMap[Sat].ssr.ssrEph.iod
						||ssrEph.t0.time	!= nav.satNavMap[Sat].ssr.ssrEph.t0.time)
					{
						nav.satNavMap[Sat].ssr.ssrEph = ssrEph;
					}
				}

				if 	( message_number == RtcmMessageType::SSR_CLK_CORR
					||message_number == RtcmMessageType::SSR_COMB_CORR)
				{
					SSRClk ssrClk;

					setTime(ssrClk.t0, epochTime);

					ssrClk.iod 			= iod;
					ssrClk.dclk[0]		= getbitsInc(data, i, 22) * 0.1e-3;
					ssrClk.dclk[1]		= getbitsInc(data, i, 21) * 0.001e-3;
					ssrClk.dclk[2]		= getbitsInc(data, i, 27) * 0.00002e-3;

					if	( ssrClk.iod		!= nav.satNavMap[Sat].ssr.ssrClk.iod
						||ssrClk.t0.time	!= nav.satNavMap[Sat].ssr.ssrClk.t0.time)
					{
						nav.satNavMap[Sat].ssr.ssrClk = ssrClk;
					}
				}

				if 	( message_number == RtcmMessageType::SSR_URA)
				{
					SSRUra ssrUra;

					setTime(ssrUra.t0, epochTime);

					ssrUra.iod 			= iod;
					ssrUra.ura			= getbituInc(data, i, 6);

					if	( ssrUra.iod		!= nav.satNavMap[Sat].ssr.ssrUra.iod
						||ssrUra.t0.time	!= nav.satNavMap[Sat].ssr.ssrUra.t0.time)
					{
						nav.satNavMap[Sat].ssr.ssrUra = ssrUra;
					}
				}
			}
		}
	};

	struct EphemerisDecoder : Decoder
	{
		void decode()
		{
			Eph eph = {};
			int i = 12;	//todo aaron, magic numbers

			if (i + 476 > message_length * 8)
			{
				tracepde(2,std::cout, "rtcm3 1019 length error: len=%d\n", message_length);
				return;
			}

			E_Sys sys = E_Sys::GPS;
			int prn			= getbituInc(data, i,  6);
			int week		= getbituInc(data, i, 10);
			eph.sva   		= getbituInc(data, i,  4);
			eph.code  		= getbituInc(data, i,  2);
			eph.idot  		= getbitsInc(data, i, 14)*P2_43*SC2RAD;
			eph.iode  		= getbituInc(data, i,  8);
			double toc     	= getbituInc(data, i, 16)*16.0;
			eph.f2    		= getbitsInc(data, i,  8)*P2_55;
			eph.f1    		= getbitsInc(data, i, 16)*P2_43;
			eph.f0    		= getbitsInc(data, i, 22)*P2_31;
			eph.iodc  		= getbituInc(data, i, 10);
			eph.crs   		= getbitsInc(data, i, 16)*P2_5;
			eph.deln  		= getbitsInc(data, i, 16)*P2_43*SC2RAD;
			eph.M0    		= getbitsInc(data, i, 32)*P2_31*SC2RAD;
			eph.cuc   		= getbitsInc(data, i, 16)*P2_29;
			eph.e     		= getbituInc(data, i, 32)*P2_33;
			eph.cus   		= getbitsInc(data, i, 16)*P2_29;
			double sqrtA    = getbituInc(data, i, 32)*P2_19;
			eph.toes  		= getbituInc(data, i, 16)*16.0;
			eph.cic   		= getbitsInc(data, i, 16)*P2_29;
			eph.OMG0  		= getbitsInc(data, i, 32)*P2_31*SC2RAD;
			eph.cis   		= getbitsInc(data, i, 16)*P2_29;
			eph.i0    		= getbitsInc(data, i, 32)*P2_31*SC2RAD;
			eph.crc   		= getbitsInc(data, i, 16)*P2_5;
			eph.omg   		= getbitsInc(data, i, 32)*P2_31*SC2RAD;
			eph.OMGd  		= getbitsInc(data, i, 24)*P2_43*SC2RAD;
			eph.tgd[0]		= getbitsInc(data, i,  8)*P2_31;
			eph.svh   		= getbituInc(data, i,  6);
			eph.flag  		= getbituInc(data, i,  1);
			eph.fit   		= getbituInc(data, i,  1)?0.0:4.0; /* 0:4hr,1:>4hr */

			if (prn >= 40)
			{
				sys = E_Sys::SBS;
				prn += 80;
			}

			if (1)	//todo aaron, janky?
			{
				int week;
				double tow	= time2gpst(utc2gpst(timeget()), &week);
				eph.ttr		= gpst2time(week, floor(tow));
			}
			eph.Sat		= SatSys(E_Sys::GPS, prn);
			eph.week	= adjgpsweek(week);
			eph.toe		= gpst2time(eph.week,eph.toes);
			eph.toc		= gpst2time(eph.week,toc);
			eph.A		= SQR(sqrtA);

			//check for iode, add if not found.
			if (eph.iode != 0)
			{
				for (auto& eph_ : nav.ephMap[eph.Sat])
				{
					if (eph_.iode == eph.iode)
					{
						return;
					}
				}
			}

			std::cout << "Adding ephemeris for " << eph.Sat.id() << std::endl;
			nav.ephMap[eph.Sat].push_back(eph);
		}
	};

	struct MSM7Decoder : Decoder
	{
        E_FType code_to_ftype(E_Sys sys, E_ObsCode code)
		{
            if	( codeTypeMap		.count(sys)		> 0
				&&codeTypeMap[sys]	.count(code)	> 0)
			{
                return codeTypeMap.at(sys).at(code);
            }

            return FTYPE_NONE;
        }

        boost::optional<SignalInfo> get_signal_info(E_Sys sys, uint8_t signal)
		{
            if	( signal_id_mapping		.count(sys)		> 0
				&&signal_id_mapping[sys].count(signal)	> 0)
			{
                return signal_id_mapping.at(sys).at(signal);
            }

            return boost::optional<SignalInfo>();
        }

        E_ObsCode signal_to_code(E_Sys sys, uint8_t signal)
		{
            boost::optional<SignalInfo> info = get_signal_info(sys, signal);

            if (info)
			{
                return info->rinex_observation_code;
            }

            return E_ObsCode::NONE;
        }


        ObsList decode()
		{
			ObsList obsList;
			int i = 0;

			int message_number				= getbituInc(data, i,	12);
			int reference_station_id		= getbituInc(data, i,	12);
			int epoch_time_					= getbituInc(data, i,	30);
			int multiple_message			= getbituInc(data, i,	1);
			int issue_of_data_station		= getbituInc(data, i,	3);
			int reserved					= getbituInc(data, i,	7);
			int clock_steering_indicator	= getbituInc(data, i,	2);
			int external_clock_indicator	= getbituInc(data, i,	2);
			int smoothing_indicator			= getbituInc(data, i,	1);
			int smoothing_interval			= getbituInc(data, i,	3);

			//create observations for satelllites according to the mask
			for (int sat = 0; sat < 64; sat++)
			{
				bool mask 					= getbituInc(data, i,	1);
				if (mask)
				{
					Obs obs;
					obs.Sat.sys = E_Sys::GPS;
					obs.Sat.prn = sat + 1;
					setTime(obs.time, epoch_time_ * 0.001);
					obsList.push_back(obs);
				}
			}

			//create a temporary list of signals
			list<E_ObsCode> signalMaskList;
			for (int sig = 0; sig < 32; sig++)
			{
				bool mask 					= getbituInc(data, i,	1);
				if (mask)
				{
					int code = signal_to_code(E_Sys::GPS, sig + 1);
					signalMaskList.push_back(E_ObsCode::_from_integral(code));
				}
			}

			//create a temporary list of signal pointers for simpler iteration later
			list<RawSig*> signalPointerList;

			//create signals for observations according to existing observations, the list of signals, and the cell mask
			for (auto& obs		: obsList)
			for (auto& sigNum	: signalMaskList)
			{
				bool mask 					= getbituInc(data, i,	1);
				if (mask)
				{
					RawSig sig;

					sig.code = sigNum;
					E_FType ft = code_to_ftype(E_Sys::GPS, sig.code);

					obs.SigsLists[ft].push_back(sig);

					RawSig* pointer = &obs.SigsLists[ft].back();
					signalPointerList.push_back(pointer);
				}
			}

			//get satellite specific data - needs to be in chunks

			for (auto& obs : obsList)
			{
				int ms_rough_range			= getbituInc(data, i,	8);
				if (ms_rough_range == 255)
					continue;

				for (auto& [ft, sigList]	: obs.SigsLists)
				for (auto& sig				: sigList)
				{
					sig.P = ms_rough_range;
					sig.L = ms_rough_range;
				}
			}

			for (auto& obs : obsList)
			{
				int extended_sat_info		= getbituInc(data, i,	4);
// 				if (fine_doppler == 0x4000)
// 					continue;

				for (auto& [ft, sigList]	: obs.SigsLists)
				for (auto& sig				: sigList)
				{

				}
			}

			for (auto& obs : obsList)
			{
				int rough_range_modulo		= getbituInc(data, i,	10);
// 				if (fine_doppler == 0x4000)
// 					continue;

				for (auto& [ft, sigList]	: obs.SigsLists)
				for (auto& sig				: sigList)
				{
					sig.P += rough_range_modulo * std::pow(2, -10);
					sig.L += rough_range_modulo * std::pow(2, -10);
				}
			}

			for (auto& obs : obsList)
			{
				int rough_doppler			= getbituInc(data, i,	14);
				if (rough_doppler == 0x2000)
					continue;

				for (auto& [ft, sigList]	: obs.SigsLists)
				for (auto& sig				: sigList)
				{
					sig.D = rough_doppler;
				}
			}

			//get signal specific data

			for (auto& signalPointer : signalPointerList)
			{
				int fine_pseudorange		= getbitsInc(data, i,	20);
				if (fine_pseudorange == 0x80000)
					continue;

				RawSig& sig = *signalPointer;
				sig.P += fine_pseudorange			* std::pow(2, -29);
			}

			for (auto& signalPointer : signalPointerList)
			{
				int fine_phase_range		= getbitsInc(data, i,	24);
				if (fine_phase_range == 0x800000)
					continue;

				RawSig& sig = *signalPointer;
				sig.L += fine_phase_range			* std::pow(2, -31);
			}

			for (auto& signalPointer : signalPointerList)
			{
				int lock_time_indicator	= getbituInc(data, i,	10);
// 				if (fine_doppler == 0x4000)
// 					continue;

				RawSig& sig = *signalPointer;
// 				sig.L += fine_phase_range;
			}

			for (auto& signalPointer : signalPointerList)
			{
				int half_cycle_ambiguity	= getbituInc(data, i,	1);
// 				if (fine_doppler == 0x4000)
// 					continue;

				RawSig& sig = *signalPointer;
// 				sig.L += fine_phase_range;
			}

			for (auto& signalPointer : signalPointerList)
			{
				int carrier_noise_ratio		= getbituInc(data, i,	10);
// 				if (fine_doppler == 0x4000)
// 					continue;

				RawSig& sig = *signalPointer;
				sig.snr = carrier_noise_ratio	* std::pow(2, -4);
			}

			for (auto& signalPointer : signalPointerList)
			{
				int fine_doppler		= getbitsInc(data, i,	15);
				if (fine_doppler == 0x4000)
					continue;

				RawSig& sig = *signalPointer;
				sig.D += fine_doppler			* std::pow(2, -15);
			}


			//convert millisecond measurements to meters or cycles
			for (auto& obs				: obsList)
			for (auto& [ft, sigList]	: obs.SigsLists)
			for (auto& sig				: sigList)
			{
				sig.P *= CLIGHT										/ 1000;
				sig.L *= signal_phase_alignment[obs.Sat.sys][ft]	/ 1000;
// 				sig.D *= 1;//* frequency / CLIGHT;
			}

			return obsList;
		}
	};

    static uint16_t message_length(char header[2])
	{
        // Message length is 10 bits starting at bit 6
        return getbitu((uint8_t*)header, 6,	10);
    }

	static RtcmMessageType message_type(const uint8_t message[])
	{
		auto id = getbitu(message, 0,	12);

		if (!RtcmMessageType::_is_valid(id))
		{
			return RtcmMessageType::NONE;
		}

		return RtcmMessageType::_from_integral(id);
	}
};


void RtcmStream::parseRTCM(std::istream& inputStream)
{
	while (inputStream)
	{
		int pos;
		while (1)
		{
			// Skip to the start of the frame - marked by preamble character 0xD3
			pos = inputStream.tellg();
			int c = inputStream.get();
			if (inputStream)
			{
				if (c == PREAMBLE)
				{
					break;
				}
			}
			else
			{
				return;
			}
		}

		// Read the frame length - 2 bytes big endian only want 10 bits
		char buf[2];
		inputStream.read((char*)buf, 2);
		if (inputStream.fail())
		{
			inputStream.clear();
			inputStream.seekg(pos);
			return;
		}

		auto message_length = RtcmDecoder::message_length(buf);

		// Read the frame data (include the header)
		unsigned char data[message_length + 3];
		data[0] = PREAMBLE;
		data[1] = buf[0];
		data[2] = buf[1];
		unsigned char* message = data + 3;
		inputStream.read((char*)message, message_length);
		if (inputStream.fail())
		{
			inputStream.clear();
			inputStream.seekg(pos);
			return;
		}

		// Read the frame CRC
		unsigned int crcRead = 0;
		inputStream.read((char*)&crcRead, 3);
		if (inputStream.fail())
		{
			inputStream.clear();
			inputStream.seekg(pos);
			return;
		}

		unsigned int crcCalc = crc24q(data, sizeof(data));

		if	( (((char*)&crcCalc)[0] != ((char*)&crcRead)[2])
			||(((char*)&crcCalc)[1] != ((char*)&crcRead)[1])
			||(((char*)&crcCalc)[2] != ((char*)&crcRead)[0]))
		{
			continue;
		}

		auto message_type = RtcmDecoder::message_type((unsigned char*) message);

// 		if (message_type != 0)
// 			std::cout << "FOUND " << message_type << std::endl;

		if 		(message_type == +RtcmMessageType::GPS_EPHEMERIS)
		{
			RtcmDecoder::EphemerisDecoder decoder;
			decoder.data = (uint8_t*) message;
			decoder.message_length	= message_length;

			decoder.decode();
		}
		else if ( message_type == +RtcmMessageType::SSR_COMB_CORR
				||message_type == +RtcmMessageType::SSR_ORB_CORR
				||message_type == +RtcmMessageType::SSR_CLK_CORR
				||message_type == +RtcmMessageType::SSR_URA)
		{
			RtcmDecoder::SSRDecoder decoder;
			decoder.data = (uint8_t*) message;
			decoder.message_length	= message_length;

			decoder.decode();
		}
		else if (message_type == +RtcmMessageType::MSM7_GPS)
		{
			RtcmDecoder::MSM7Decoder decoder;
			decoder.data = (uint8_t*) message;
			decoder.message_length	= message_length;

			ObsList obsList = decoder.decode();
			obsListList.push_back(obsList);
		}
	}
}

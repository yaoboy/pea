
#ifndef __ACS_FILESTREAM_HPP
#define __ACS_FILESTREAM_HPP


/** Interface to be used for file streams
 */
struct ACSFileStream
{
	std::ifstream inputStream;
	string path;

	ACSFileStream()
	{

	}

	void setPath(const string& path)
	{
		this->path = path;
	}

	void openFile()
	{
		inputStream.open(path);
	}
};


#endif

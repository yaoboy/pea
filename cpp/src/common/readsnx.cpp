
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
//#include <memory>
#include <vector>
#include "snx.hpp"
//
// TODO return a list of stations that are in the sinex file
//
/* read sinex header block -----------------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *
 * return   :       0 fail, 1 success
 * ---------------------------------------------------------------------------*/
int snxheader(char* buff, snx_t* snx)
{
	char* p = NULL;

	/* first line */
	if (strstr(buff, "SNX") && !strstr(buff, "%ENDSNX"))
	{
		/* file type */
		strcpy(snx->snxtype, "SNX");

		/* version */
		snx->ver = strtod(buff + 6, &p);

		/* number of parameters */
		snx->np = strtol(buff + 60, &p, 10);

		/* solution contents */
	}
	else
	{
		fprintf(stdout, "not a SINEX file?\n");
		return 0;
	}

	return 1;
}
/* read sinex ID block ---------------------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *                  char    *sta            I       station name
 *                  int     *id             O       block index
 *
 * return   :       0-no station found, 1-successful
 * ---------------------------------------------------------------------------*/
int snxid(char* buff, snx_t* snx, char* sta, int* id)
{
	char tmpsite[16] = "";

	strncpy(tmpsite, buff + 1, 4);

	/*match the ID with the input station */
	if (!strcmp(tmpsite, sta))
	{
		snx->siteCode.assign(sta);
		snx->ptCode.assign(buff + 6, 2);
		snx->monuId.assign(buff + 9, 9);
//         strcpy(snx->sitecode,sta);
//         strncpy(snx->ptcode,buff+6,2); // TODO memset? isn't this dangerous? strncpy doesn't add '\0'
//         strncpy(snx->monuid,buff+9,9);
	}

	/* exit the ID block */
	if (strstr(buff, "-SITE/ID"))
	{
		*id = 0;

		if (snx->siteCode == "")
		{
			return 0;
		}
	}

	return 1;
}
//=============================================================================
/* Parse the SITE/ID block from a sinex file, for example:
+SITE/ID
*CODE PT __DOMES__ T _STATION DESCRIPTION__ _LONGITUDE_ _LATITUDE__ HEIGHT_
 ALBH  A 40129M003 C VICTORIA/SIDNEY, CANAD 236 30 45.1  48 23 23.2    31.7
*/
//=============================================================================
int snxSiteId(char* buff, snx_siteid_t* snxid)
{
	char code[5];     memset(code, '\0', sizeof(code));
	char pt[3];       memset(pt, '\0', sizeof(pt));
	char domes[10];   memset(domes, '\0', sizeof(domes));
	char T[2];        memset(T, '\0', sizeof(T));
	char desc[23];    memset(desc, '\0', sizeof(desc));
	char long_deg[4]; memset(long_deg, '\0', sizeof(long_deg));
	char long_min[3]; memset(long_min, '\0', sizeof(long_min));
	char long_sec[5]; memset(long_sec, '\0', sizeof(long_sec));
	char lat_deg[4];  memset(lat_deg, '\0', sizeof(lat_deg));
	char lat_min[3];  memset(lat_min, '\0', sizeof(lat_min));
	char lat_sec[5];  memset(lat_sec, '\0', sizeof(lat_sec));
	//char height[8];   memset(height,'\0',sizeof(height));
	char tmp[8];   memset(tmp, '\0', sizeof(tmp));

	//strncpy(code ,buff+1,sizeof(code)-1);
	strncpy(snxid->sitecode , buff + 1, sizeof(snxid->sitecode) - 1);
	strncpy(snxid->ptcode, buff + 7, sizeof(snxid->ptcode) - 1);
	strncpy(snxid->domes, buff + 9, sizeof(snxid->domes) - 1);
	strncpy(snxid->typecode, buff + 19, sizeof(snxid->typecode) - 1);
	strncpy(snxid->desc, buff + 21, sizeof(snxid->desc) - 1);

	strncpy(snxid->long_deg, buff + 44, sizeof(snxid->long_deg) - 1);
	strncpy(snxid->long_min, buff + 48, sizeof(snxid->long_min) - 1);
	char stmp[5]; memset(stmp, '\0', sizeof(stmp)); strncpy(stmp, buff + 51, sizeof(stmp) - 1);
	snxid->long_sec = atof(stmp);
	//strncpy(snxid->long_sec,buff+51,sizeof(snxid->long_sec)-1);

	strncpy(snxid->lat_deg, buff + 56, sizeof(snxid->lat_deg) - 1);
	strncpy(snxid->lat_min, buff + 60, sizeof(snxid->lat_min) - 1);
	memset(stmp, '\0', sizeof(stmp)); strncpy(stmp, buff + 63, sizeof(stmp) - 1);
	snxid->lat_sec = atof(stmp);

	//strncpy(snxid->height,buff+68,sizeof(snxid->height)-1);
	strncpy(tmp, buff + 68, sizeof(tmp) - 1);
	snxid->height = atof(tmp);
	//printf("code:<%s> pt:<%s> domes:<%s> T:<%s> desc:<%s> ",code,pt,domes,T,desc);
	//printf("longitude:<%s><%s><%s> ",long_deg,long_min,long_sec);
	//printf("lat:<%s><%s><%s> height<%s>\n",lat_deg,lat_min,lat_sec,height);
	return 1;
}
/* read sinex receiver block ---------------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *                  char    *sta            I       station name
 *                  int     *rec            O       block index
 * ---------------------------------------------------------------------------*/
void snxrec(char* buff, snx_t* snx, char* sta, int* rec)
{
	char tmpsite[16] = "", *p = NULL;

	strncpy(tmpsite, buff + 1, 4);

	/*match the ID with the input station */
	if (strstr(tmpsite, sta))
	{
		strncpy(snx->rectype, buff + 42, 20);
		strncpy(snx->recsn, buff + 63, 5);
		strncpy(snx->recfirm, buff + 69, 11);
		snx->recstart[0] = strtol(buff + 16, &p, 10);
		snx->recstart[1] = strtol(buff + 19, &p, 10);
		snx->recstart[2] = strtol(buff + 23, &p, 10);
		snx->recend[0]  = strtol(buff + 29, &p, 10);
		snx->recend[1]  = strtol(buff + 32, &p, 10);
		snx->recend[2]  = strtol(buff + 36, &p, 10);
	}

	/* exit the receiver block */
	if (strstr(buff, "-SITE/RECEIVER"))
		*rec = 0;
}
/* read sinex antenna block ----------------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *                  char    *sta            I       station name
 *                  int     *ant            O       block index
 * ---------------------------------------------------------------------------*/
void snxant(char* buff, snx_t* snx, char* sta, int* ant)
{
	char tmpsite[16] = "", *p = NULL;

	strncpy(tmpsite, buff + 1, 4);

	/*match the ID with the input station */
	if (strstr(tmpsite, sta))
	{
		strncpy(snx->anttype, buff + 42, 20);
		strncpy(snx->antsn, buff + 63, 5);
		snx->antstart[0] = strtol(buff + 16, &p, 10);
		snx->antstart[1] = strtol(buff + 19, &p, 10);
		snx->antstart[2] = strtol(buff + 23, &p, 10);
		snx->antend[0]  = strtol(buff + 29, &p, 10);
		snx->antend[1]  = strtol(buff + 32, &p, 10);
		snx->antend[2]  = strtol(buff + 36, &p, 10);
	}

	/* exit the receiver block */
	if (strstr(buff, "-SITE/ANTENNA"))
		*ant = 0;
}
/* read sinex GPS PCO block ----------------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *                  char    *sta            I       station name
 *                  int     *gpspco         O       block index
 * ---------------------------------------------------------------------------*/
void snxgpspco(char* buff, snx_t* snx, char* sta, int* gpspco)
{
	char* p = NULL;

	/*match the antenna type to get GPS PCV L1&L2 */
	if (strstr(buff, snx->anttype))
	{
		snx->gpsl1[0] = strtod(buff + 28, &p); /* L1 */
		snx->gpsl1[1] = strtod(buff + 35, &p);
		snx->gpsl1[2] = strtod(buff + 42, &p);
		snx->gpsl2[0] = strtod(buff + 49, &p); /* L2 */
		snx->gpsl2[1] = strtod(buff + 56, &p);
		snx->gpsl2[2] = strtod(buff + 63, &p);
	}

	/* exit the receiver block */
	if (strstr(buff, "-SITE/GPS_PHASE_CENTER"))
		*gpspco = 0;

	if (sta != NULL)
	{

	}

	return;
}
/* read sinex eccentricity block -----------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *                  char    *sta            I       station name
 *                  int     *ecc            O       block index
 * ---------------------------------------------------------------------------*/
void snxecc(char* buff, snx_t* snx, char* sta, int* ecc)
{
	char tmpsite[16] = "", *p = NULL;

	strncpy(tmpsite, buff + 1, 4);

	if (strstr(tmpsite, sta))
	{
		if (strstr(buff + 42, "UNE"))
			snx->eccrs = 0; /* UNE reference system */
		else
			snx->eccrs = 1; /* XYZ reference system */

		snx->ecc[0] = strtod(buff + 46, &p); /* up */
		snx->ecc[1] = strtod(buff + 55, &p); /* north */
		snx->ecc[2] = strtod(buff + 64, &p); /* east */

	}

	if (strstr(buff, "-SITE/ECCENTRICITY"))
	{
		*ecc = 0;
	}
}
/* read sinex solution block ---------------------------------------------------
 * args     :       char    *buff           I       buffer
 *                  snx_t   *snx            I/O     sinex strcture
 *                  char    *sta            I       station name
 *                  int     *sol            O       block index
 * ---------------------------------------------------------------------------*/
void snxsol(char* buff, snx_t* snx, char* sta, int* sol)
{
	char tmpsite[16] = "", *p = NULL;

	strncpy(tmpsite, buff + 14, 4);

	if (strstr(tmpsite, sta))
	{
		strncpy(snx->unit, buff + 40, 4);

		if (strstr(buff, "STAX"))               /* X coordiante */
		{
			snx->pos[0] = strtod(buff + 47, &p);
			snx->pstd[0] = strtod(buff + 69, &p);
		}
		else if (strstr(buff, "STAY"))          /* Y coordiante */
		{
			snx->pos[1] = strtod(buff + 47, &p);
			snx->pstd[1] = strtod(buff + 69, &p);
		}
		else if (strstr(buff, "STAZ"))          /* Z coordiante */
		{
			snx->pos[2] = strtod(buff + 47, &p);
			snx->pstd[2] = strtod(buff + 69, &p);
		}
	}

	if (strstr(buff, "-SOLUTION/ESTIMATE"))
	{
		*sol = 0;
	}
}
/* read SINEX solution - --------------------------------------------------------
 * args     :       const char *file        I       SINEX file path
 *                  char *sta               I       station name
 *                  snx_t *snx              O       SINEX solution
 * ---------------------------------------------------------------------------*/
//readsnx           (char const*, char*, snx_t*)
extern int readsnx(const char* file, char* sta, snx_t* snx)
{
	FILE* fp;
	char buff[2048], stat[16] = {0};
	int id = 0, rec = 0, ant = 0, gpspco = 0, ecc = 0, sol = 0, i = 0;

	if (!(fp = fopen(file, "r")))
	{
		fprintf(stdout, "SINEX file open error : %s\n", file);
		return 0;
	}

	/* convert station name to upper case */
	while (sta[i] != '\0')
	{
		stat[i] = toupper(sta[i]);
		i++;
	}

	/* read sinex file */
	while (fgets(buff, sizeof(buff), fp))
	{
		if (buff[0] == '*') continue;

		/* header block */
		if (strstr(buff, "%="))
		{
			/* read sinex header */
			if (!snxheader(buff, snx))
				return 0;
		}

		/* ID block */
		else if (strstr(buff, "+SITE/ID") || id == 1)
		{
			if (id == 0)
			{
				id = 1;
				continue;
			}

			/* read sinex id */
			if (!snxid(buff, snx, stat, &id))
				return 0;
		}
		/* receiver block */
		else if (strstr(buff, "+SITE/RECEIVER") || rec == 1)
		{
			if (rec == 0)
			{
				rec = 1;
				continue;
			}

			/* read receiver information */
			snxrec(buff, snx, stat, &rec);
		}

		/* antenna block */
		else if (strstr(buff, "SITE/ANTENNA") || ant == 1)
		{
			if (ant == 0)
			{
				ant = 1;
				continue;
			}

			/* read antenna information */
			snxant(buff, snx, stat, &ant);
		}
		/* GPS phase center block */
		else if (strstr(buff, "+SITE/GPS_PHASE_CENTER") || gpspco == 1)
		{
			if (gpspco == 0)
			{
				gpspco = 1;
				continue;
			}

			/* read gpspco information */
			snxgpspco(buff, snx, stat, &gpspco);
		}
		/* eccentricity block */
		else if (strstr(buff, "+SITE/ECCENTRICITY") || ecc == 1)
		{
			if (ecc == 0)
			{
				ecc = 1;
				continue;
			}

			/* read eccentricity */
			snxecc(buff, snx, stat, &ecc);
		}
		/* solution block */
		else if (strstr(buff, "+SOLUTION/ESTIMATE") || sol == 1)
		{
			if (sol == 0)
			{
				sol = 1;
				continue;
			}

			/* read solution */
			snxsol(buff, snx, stat, &sol);
		}

		else if (strstr(buff, "%ENDSNX"))
		{
#if (0)
			fprintf(stdout, "Reading SINEX file finished for %s \n", sta);
#endif
		}
	}

	fclose(fp);
	return 1;
}

extern int snxGetSolutions(const char* file, std::vector<snx_siteid_t>* vec_snxid)
{
	FILE* fp;
	char buff[2048];
	int n = 0;

	if (!(fp = fopen(file, "r")))
	{
		fprintf(stdout, "SINEX file open error : %s\n", file);
		return 0;
	}

	/* read sinex file */
	while (fgets(buff, sizeof(buff), fp))
	{
		if (buff[0] == '*')
			continue;
	}

	return 0;
}

// TODO ensure we get a unique set of results back..
// reads the SITE/ID block
extern int snxGetSites(const char* file, std::vector<snx_siteid_t>* vec_snxid)
{
	FILE* fp;
	char buff[2048];
	int id = 0;
	int n = 0;

	if (!(fp = fopen(file, "r")))
	{
		fprintf(stdout, "SINEX file open error : %s\n", file);
		return 0;
	}

	/* read sinex file */
	while (fgets(buff, sizeof(buff), fp))
	{
		if (buff[0] == '*') continue;

		// Get out of site id bloack
		if (strstr(buff, "-SITE/ID"))
		{
			id = 0;
			return n;
		}
		/* ID block */
		else if (strstr(buff, "+SITE/ID"))
		{
			if (id == 0)
			{
				id = 1;
				continue;
			}

			/* read sinex id */
			//snx_siteid_t snxid = {};
			//snxSiteId(buff,snxid[n]->get());
		}
		//    snxSiteId(buff,snxid->at(n)->get());
		else if (id == 1)
		{
			//auto newsnxid = std::make_unique<snx_siteid_t>();
			snx_siteid_t newsnxid = {};
			memset(newsnxid.sitecode, '\0', sizeof(newsnxid.sitecode));
			strncpy(newsnxid.sitecode , buff + 1, sizeof(newsnxid.sitecode) - 1);

			memset(newsnxid.ptcode, '\0', sizeof(newsnxid.ptcode));
			strncpy(newsnxid.ptcode, buff + 7, sizeof(newsnxid.ptcode) - 1);

			memset(newsnxid.domes, '\0', sizeof(newsnxid.domes));
			strncpy(newsnxid.domes, buff + 9, sizeof(newsnxid.domes) - 1);

			memset(newsnxid.typecode, '\0', sizeof(newsnxid.typecode));
			strncpy(newsnxid.typecode, buff + 19, sizeof(newsnxid.typecode) - 1);

			memset(newsnxid.desc, '\0', sizeof(newsnxid.desc));
			strncpy(newsnxid.desc, buff + 21, sizeof(newsnxid.desc) - 1);

			memset(newsnxid.long_deg, '\0', sizeof(newsnxid.long_deg));
			strncpy(newsnxid.long_deg, buff + 44, sizeof(newsnxid.long_deg) - 1);

			memset(newsnxid.long_min, '\0', sizeof(newsnxid.long_min));
			strncpy(newsnxid.long_min, buff + 48, sizeof(newsnxid.long_min) - 1);

			char stmp[5]; memset(stmp, '\0', sizeof(stmp)); strncpy(stmp, buff + 51, sizeof(stmp) - 1);
			newsnxid.long_sec = atof(stmp);

			memset(newsnxid.lat_deg, '\0', sizeof(newsnxid.lat_deg));
			strncpy(newsnxid.lat_deg, buff + 56, sizeof(newsnxid.lat_deg) - 1);

			memset(newsnxid.lat_min, '\0', sizeof(newsnxid.lat_min));
			strncpy(newsnxid.lat_min, buff + 60, sizeof(newsnxid.lat_min) - 1);

			memset(stmp, '\0', sizeof(stmp)); strncpy(stmp, buff + 63, sizeof(stmp) - 1);
			newsnxid.lat_sec = atof(stmp);

			char tmp[8];
			memset(tmp, '\0', sizeof(tmp));
			strncpy(tmp, buff + 68, sizeof(tmp) - 1);
			newsnxid.height = atof(tmp);

			vec_snxid->push_back(newsnxid);
			n++;
		}
	}

	return n;
}

/* read SINEX solution - --------------------------------------------------------
 * args     :       const char *file        I       SINEX file path
 *                  snx_t *snx              O       SINEX solution
 *
 *
 * ---------------------------------------------------------------------------*/
extern int readsinex(const char* file, sinex_t* snxt)
{
	FILE* fp;
	char buff[2048];
	int id = 0, rec = 0, ant = 0, ecc = 0, sol = 0, epoch = 0;

	std::vector<snx_siteid_t> vec_snxsiteids;
	std::vector<snx_receiver_t> vec_receivers;
	std::vector<snx_antenna_t> vec_antennas;
	std::vector<snx_ecc_t> vec_eccs;
	std::vector<snx_solepoch_t> vec_solepochs;
	std::vector<snx_solution_t> vec_sols;

	if (!(fp = fopen(file, "r")))
	{
		fprintf(stdout, "SINEX file open error : %s\n", file);
		return 0;
	}

	while (fgets(buff, sizeof(buff), fp))
	{
		if (buff[0] == '*') continue;

		/*
		    // header block
		    if (strstr(buff,"%=")) {
		        // read sinex header
		        if (!snxheader(buff,snx))
		            return 0;
		    }
		*/
		// Get out of site id bloack
		else if (strstr(buff, "-SITE/ID"))
		{
			id = 0;
			snxt->vec_siteid = vec_snxsiteids;
			//return n;
		}
		// ID block
		else if (strstr(buff, "+SITE/ID") || id == 1)
		{
			if (id == 0)
			{
				id = 1;
				continue;
			}

			snxsiteids(buff, &vec_snxsiteids);
		}
		// receiver block
		else if (strstr(buff, "-SITE/RECEIVER"))
		{
			rec = 0;
			snxt->vec_receivers = vec_receivers;
		}
		else if (strstr(buff, "+SITE/RECEIVER") || rec == 1)
		{
			if (rec == 0)
			{
				rec = 1;
				continue;
			}

			// read receiver information
			snxreceivers(buff, &vec_receivers);
		}
		// antenna block
		else if (strstr(buff, "-SITE/ANTENNA"))
		{
			ant = 0;
			snxt->vec_antennas = vec_antennas;
		}
		else if (strstr(buff, "+SITE/ANTENNA") || ant == 1)
		{
			if (ant == 0)
			{
				ant = 1;
				continue;
			}

			// read antenna information
			snxantennas(buff, &vec_antennas);
		}
		/*
		    // GPS phase center block
		    else if (strstr(buff,"+SITE/GPS_PHASE_CENTER")||gpspco==1) {
		        if (gpspco==0) {
		            gpspco=1;
		            continue;
		        }
		        // read gpspco information
		        snxgpspco(buff,snx,stat,&gpspco);
		    }*/
		else if (strstr(buff, "-SOLUTION/EPOCHS"))
		{
			epoch = 0;
			snxt->vec_solepochs = vec_solepochs;
		}
		else if (strstr(buff, "+SOLUTION/EPOCHS") || epoch == 1)
		{
			if (epoch == 0)
			{
				epoch = 1;
				continue;
			}

			snxsolepochs(buff, &vec_solepochs);
		}
		// eccentricity block
		else if (strstr(buff, "-SITE/ECCENTRICITY"))
		{
			ecc = 0;
			snxt->vec_eccs = vec_eccs;
		}
		else if (strstr(buff, "+SITE/ECCENTRICITY") || ecc == 1)
		{
			if (ecc == 0)
			{
				ecc = 1;
				continue;
			}

			snxeccs(buff, &vec_eccs);
		}
		// solution block
		else if (strstr(buff, "-SOLUTION/ESTIMATE"))
		{
			sol = 0;
			snxt->vec_sols = vec_sols;
		}
		else if (strstr(buff, "+SOLUTION/ESTIMATE") || sol == 1)
		{
			if (sol == 0)
			{
				sol = 1;
				continue;
			}

			// read solution
			snxsolutions(buff, &vec_sols);
		}

		/*
		    else if (strstr(buff,"%ENDSNX")) {
		        fprintf(stdout,"Reading SINEX file finished for %s \n",sta);
		    }
		*/
	}

	fclose(fp);
	return 1;
}
//=============================================================================
// Parse the siteid block a line at a time into a vector of siteid structure
//
/*
+SITE/ID
*CODE PT __DOMES__ T _STATION DESCRIPTION__ _LONGITUDE_ _LATITUDE__ HEIGHT_
 ALBH  A 40129M003 C VICTORIA/SIDNEY, CANAD 236 30 45.1  48 23 23.2    31.7
 ALGO  A 40104M002 C Algonquin, CANADA      281 55 43.0  45 57 20.8   200.9
 BRIB  A 49395M001 C Briones Reservoir, UNI 237 50 50.8  37 55 09.8   262.2
*/
//=============================================================================
void snxsiteids(char* buff, std::vector<snx_siteid_t>* vec_snxid)
{
	snx_siteid_t newsnxid = {};

	memset(newsnxid.sitecode, '\0', sizeof(newsnxid.sitecode));
	strncpy(newsnxid.sitecode , buff + 1, sizeof(newsnxid.sitecode) - 1);

	memset(newsnxid.ptcode, '\0', sizeof(newsnxid.ptcode));
	strncpy(newsnxid.ptcode, buff + 7, sizeof(newsnxid.ptcode) - 1);

	memset(newsnxid.domes, '\0', sizeof(newsnxid.domes));
	strncpy(newsnxid.domes, buff + 9, sizeof(newsnxid.domes) - 1);

	memset(newsnxid.typecode, '\0', sizeof(newsnxid.typecode));
	strncpy(newsnxid.typecode, buff + 19, sizeof(newsnxid.typecode) - 1);

	memset(newsnxid.desc, '\0', sizeof(newsnxid.desc));
	strncpy(newsnxid.desc, buff + 21, sizeof(newsnxid.desc) - 1);

	memset(newsnxid.long_deg, '\0', sizeof(newsnxid.long_deg));
	strncpy(newsnxid.long_deg, buff + 44, sizeof(newsnxid.long_deg) - 1);

	memset(newsnxid.long_min, '\0', sizeof(newsnxid.long_min));
	strncpy(newsnxid.long_min, buff + 48, sizeof(newsnxid.long_min) - 1);

	char stmp[5]; memset(stmp, '\0', sizeof(stmp)); strncpy(stmp, buff + 51, sizeof(stmp) - 1);
	newsnxid.long_sec = atof(stmp);

	memset(newsnxid.lat_deg, '\0', sizeof(newsnxid.lat_deg));
	strncpy(newsnxid.lat_deg, buff + 56, sizeof(newsnxid.lat_deg) - 1);

	memset(newsnxid.lat_min, '\0', sizeof(newsnxid.lat_min));
	strncpy(newsnxid.lat_min, buff + 60, sizeof(newsnxid.lat_min) - 1);

	memset(stmp, '\0', sizeof(stmp)); strncpy(stmp, buff + 63, sizeof(stmp) - 1);
	newsnxid.lat_sec = atof(stmp);

	char tmp[8]; memset(tmp, '\0', sizeof(tmp)); strncpy(tmp, buff + 68, sizeof(tmp) - 1);
	newsnxid.height = atof(tmp);

	vec_snxid->push_back(newsnxid);
}

//=============================================================================
/*
+SITE/RECEIVER
*CODE PT SOLN T _DATA START_ __DATA_END__ ___RECEIVER_TYPE____ _S/N_ _FIRMWARE__
 ALBH  A ---- C 93:327:70260 94:016:15540 ROGUE SNR-8C         313   Meenix 7.4
 ALBH  A ---- C 94:016:15540 95:011:80100 ROGUE SNR-8000       168   SFG2 0.0 lk
*/
//=============================================================================
void snxreceivers(char* buff, std::vector<snx_receiver_t>* vec_receivers)
{
	snx_receiver_t rcvr = {};

	memset(rcvr.sitecode, '\0', sizeof(rcvr.sitecode));
	strncpy(rcvr.sitecode , buff + 1, sizeof(rcvr.sitecode) - 1);

	memset(rcvr.ptcode, '\0', sizeof(rcvr.ptcode));
	strncpy(rcvr.ptcode, buff + 6, sizeof(rcvr.ptcode) - 1);

	char tmp[5]; memset(tmp, '\0', sizeof(tmp)); strncpy(tmp, buff + 9, sizeof(tmp) - 1);

	if (strstr(tmp, "----"))
	{
		rcvr.solnnum = 0;
	}
	else
	{
		rcvr.solnnum = atoi(tmp);
	}

	memset(rcvr.typecode, '\0', sizeof(rcvr.typecode));
	strncpy(rcvr.typecode, buff + 14, sizeof(rcvr.typecode) - 1);

	char yy[3]; memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 16, sizeof(yy) - 1);
	char doy[4]; memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 19, sizeof(doy) - 1);
	char sec[6]; memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 23, sizeof(sec) - 1);
	rcvr.recstart[0] = atoi(yy);
	rcvr.recstart[1] = atoi(doy);
	rcvr.recstart[2] = atoi(sec);

	memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 29, sizeof(yy) - 1);
	memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 32, sizeof(doy) - 1);
	memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 36, sizeof(sec) - 1);
	rcvr.recend[0] = atoi(yy);
	rcvr.recend[1] = atoi(doy);
	rcvr.recend[2] = atoi(sec);

	memset(rcvr.rectype, '\0', sizeof(rcvr.rectype));
	strncpy(rcvr.rectype, buff + 42, sizeof(rcvr.rectype) - 1);

	memset(rcvr.recsn, '\0', sizeof(rcvr.recsn));
	strncpy(rcvr.recsn, buff + 63, sizeof(rcvr.recsn) - 1);

	memset(rcvr.recfirm, '\0', sizeof(rcvr.recfirm));
	strncpy(rcvr.recfirm, buff + 69, sizeof(rcvr.recfirm) - 1);

	vec_receivers->push_back(rcvr);
}
//=============================================================================
/*
+SITE/ANTENNA
*CODE PT SOLN T _DATA START_ __DATA_END__ ____ANTENNA_TYPE____ _S/N_
 ALBH  A ---- C 92:125:00000 94:104:74100 AOAD/M_B        EMRA 91119
 */
//=============================================================================
void snxantennas(char* buff, std::vector<snx_antenna_t>* vec_antennas)
{
	snx_antenna_t ant = {};

	memset(ant.sitecode, '\0', sizeof(ant.sitecode));
	strncpy(ant.sitecode , buff + 1, sizeof(ant.sitecode) - 1);

	memset(ant.ptcode, '\0', sizeof(ant.ptcode));
	strncpy(ant.ptcode, buff + 6, sizeof(ant.ptcode) - 1);

	char tmp[5]; memset(tmp, '\0', sizeof(tmp)); strncpy(tmp, buff + 9, sizeof(tmp) - 1);

	if (strstr(tmp, "----"))
	{
		ant.solnnum = 0;
	}
	else
	{
		ant.solnnum = atoi(tmp);
	}

	memset(ant.typecode, '\0', sizeof(ant.typecode));
	strncpy(ant.typecode, buff + 14, sizeof(ant.typecode) - 1);

	char yy[3]; memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 16, sizeof(yy) - 1);
	char doy[4]; memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 19, sizeof(doy) - 1);
	char sec[6]; memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 23, sizeof(sec) - 1);
	ant.antstart[0] = atoi(yy);
	ant.antstart[1] = atoi(doy);
	ant.antstart[2] = atoi(sec);

	memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 29, sizeof(yy) - 1);
	memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 32, sizeof(doy) - 1);
	memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 36, sizeof(sec) - 1);
	ant.antend[0] = atoi(yy);
	ant.antend[1] = atoi(doy);
	ant.antend[2] = atoi(sec);

	memset(ant.anttype, '\0', sizeof(ant.anttype));
	strncpy(ant.anttype, buff + 42, sizeof(ant.anttype) - 1);

	memset(ant.antsn, '\0', sizeof(ant.antsn));
	strncpy(ant.antsn, buff + 63, sizeof(ant.antsn) - 1);

	vec_antennas->push_back(ant);
}
//=============================================================================
/*
+SITE/ECCENTRICITY
*CODE PT SOLN T _DATA START_ __DATA_END__ REF __DX_U__ __DX_N__ __DX_E__
 ALBH  A ---- C 92:146:00000 94:104:74100 UNE   0.1260   0.0000   0.0000
*/
//=============================================================================
void snxeccs(char* buff, std::vector<snx_ecc_t>* vec_eccs)
{
	snx_ecc_t ecc = {};

	memset(ecc.sitecode, '\0', sizeof(ecc.sitecode));
	strncpy(ecc.sitecode , buff + 1, sizeof(ecc.sitecode) - 1);

	memset(ecc.ptcode, '\0', sizeof(ecc.ptcode));
	strncpy(ecc.ptcode, buff + 6, sizeof(ecc.ptcode) - 1);

	char tmp[5]; memset(tmp, '\0', sizeof(tmp)); strncpy(tmp, buff + 9, sizeof(tmp) - 1);

	if (strstr(tmp, "----"))
	{
		ecc.solnnum = 0;
	}
	else
	{
		ecc.solnnum = atoi(tmp);
	}

	memset(ecc.typecode, '\0', sizeof(ecc.typecode));
	strncpy(ecc.typecode, buff + 14, sizeof(ecc.typecode) - 1);

	char yy[3]; memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 16, sizeof(yy) - 1);
	char doy[4]; memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 19, sizeof(doy) - 1);
	char sec[6]; memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 23, sizeof(sec) - 1);
	ecc.eccstart[0] = atoi(yy);
	ecc.eccstart[1] = atoi(doy);
	ecc.eccstart[2] = atoi(sec);

	memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 29, sizeof(yy) - 1);
	memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 32, sizeof(doy) - 1);
	memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 36, sizeof(sec) - 1);
	ecc.eccend[0] = atoi(yy);
	ecc.eccend[1] = atoi(doy);
	ecc.eccend[2] = atoi(sec);

	char eccrs[4]; memset(eccrs, '\0', sizeof(eccrs)); strncpy(ecc.eccrs, buff + 42, sizeof(eccrs) - 1);

	char dx[8]; memset(dx, '\0', sizeof(dx)); strncpy(dx, buff + 46, sizeof(dx) - 1); ecc.ecc[0] = atof(dx);
	memset(dx, '\0', sizeof(dx)); strncpy(dx, buff + 55, sizeof(dx) - 1); ecc.ecc[1] = atof(dx);
	memset(dx, '\0', sizeof(dx)); strncpy(dx, buff + 64, sizeof(dx) - 1); ecc.ecc[2] = atof(dx);
	vec_eccs->push_back(ecc);
}
//=============================================================================
/*
+SOLUTION/EPOCHS
*CODE PT SOLN T _DATA_START_ __DATA_END__ _MEAN_EPOCH_
 ALBH  A    1 C 94:002:00000 94:104:00000 94:053:00000
*/
//=============================================================================
void snxsolepochs(char* buff, std::vector<snx_solepoch_t>* vec_solepochs)
{

	snx_solepoch_t epoch = {};

	memset(epoch.sitecode, '\0', sizeof(epoch.sitecode));
	strncpy(epoch.sitecode , buff + 1, sizeof(epoch.sitecode) - 1);

	memset(epoch.ptcode, '\0', sizeof(epoch.ptcode));
	strncpy(epoch.ptcode, buff + 6, sizeof(epoch.ptcode) - 1);

	char tmp[5]; memset(tmp, '\0', sizeof(tmp)); strncpy(tmp, buff + 9, sizeof(tmp) - 1);

	if (strstr(tmp, "----"))
	{
		epoch.solnnum = 0;
	}
	else
	{
		epoch.solnnum = atoi(tmp);
	}

	memset(epoch.typecode, '\0', sizeof(epoch.typecode));
	strncpy(epoch.typecode, buff + 14, sizeof(epoch.typecode) - 1);

	char yy[3];  memset(yy, '\0', sizeof(yy));   strncpy(yy, buff + 16, sizeof(yy) - 1);
	char doy[4]; memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 19, sizeof(doy) - 1);
	char sec[6]; memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 23, sizeof(sec) - 1);
	epoch.start[0] = atoi(yy);
	epoch.start[1] = atoi(doy);
	epoch.start[2] = atoi(sec);

	memset(yy, '\0', sizeof(yy));   strncpy(yy, buff + 29, sizeof(yy) - 1);
	memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 32, sizeof(doy) - 1);
	memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 36, sizeof(sec) - 1);
	epoch.end[0] = atoi(yy);
	epoch.end[1] = atoi(doy);
	epoch.end[2] = atoi(sec);

	memset(yy, '\0', sizeof(yy));   strncpy(yy, buff + 42, sizeof(yy) - 1);
	memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 45, sizeof(doy) - 1);
	memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 49, sizeof(sec) - 1);
	epoch.mean[0] = atoi(yy);
	epoch.mean[1] = atoi(doy);
	epoch.mean[2] = atoi(sec);

	vec_solepochs->push_back(epoch);
}

//=============================================================================
/*
+SOLUTION/ESTIMATE
*INDEX _TYPE_ CODE PT SOLN _REF_EPOCH__ UNIT S ___ESTIMATED_VALUE___ __STD_DEV__
     1 STAX   ALBH  A    1 10:001:00000 m    2 -2.34133301687257e+06 5.58270e-04
     2 STAY   ALBH  A    1 10:001:00000 m    2 -3.53904951624333e+06 7.77370e-04
     3 STAZ   ALBH  A    1 10:001:00000 m    2  4.74579129951391e+06 8.98560e-04
     4 VELX   ALBH  A    1 10:001:00000 m/y  2 -9.92019926884722e-03 1.67050e-05
     5 VELY   ALBH  A    1 10:001:00000 m/y  2 -8.46787398931193e-04 2.12080e-05
     6 VELZ   ALBH  A    1 10:001:00000 m/y  2 -4.85721729753769e-03 2.39140e-05
*/
//=============================================================================
void snxsolutions(char* buff, std::vector<snx_solution_t>* vec_solutions)
{
	snx_solution_t sol = {};

	char idx[6]; memset(idx, '\0', sizeof(idx)); strncpy(idx, buff + 1, sizeof(idx) - 1);
	sol.index = atoi(idx);

	memset(sol.type, '\0', sizeof(sol.type)); strncpy(sol.type, buff + 7, sizeof(sol.type) - 1);

	memset(sol.sitecode, '\0', sizeof(sol.sitecode));
	strncpy(sol.sitecode , buff + 14, sizeof(sol.sitecode) - 1);

	memset(sol.ptcode, '\0', sizeof(sol.ptcode));
	strncpy(sol.ptcode, buff + 19, sizeof(sol.ptcode) - 1);

	char tmp[5]; memset(tmp, '\0', sizeof(tmp)); strncpy(tmp, buff + 22, sizeof(tmp) - 1);

	if (strstr(tmp, "----"))
	{
		sol.solnnum = 0;
	}
	else
	{
		sol.solnnum = atoi(tmp);
	}

	char yy[3]; memset(yy, '\0', sizeof(yy)); strncpy(yy, buff + 27, sizeof(yy) - 1);
	char doy[4]; memset(doy, '\0', sizeof(doy)); strncpy(doy, buff + 30, sizeof(doy) - 1);
	char sec[6]; memset(sec, '\0', sizeof(sec)); strncpy(sec, buff + 34, sizeof(sec) - 1);
	sol.refepoch[0] = atoi(yy);
	sol.refepoch[1] = atoi(doy);
	sol.refepoch[2] = atoi(sec);

	memset(sol.unit, '\0', sizeof(sol.unit)); strncpy(sol.unit, buff + 40, sizeof(sol.unit) - 1);
	memset(sol.S, '\0', sizeof(sol.S)); strncpy(sol.S, buff + 45, sizeof(sol.S) - 1);

	char est[22]; memset(est, '\0', sizeof(est)); strncpy(est, buff + 47, sizeof(est) - 1);
	char std[12]; memset(std, '\0', sizeof(std)); strncpy(std, buff + 69, sizeof(std) - 1);
	vec_solutions->push_back(sol);
}

//=============================================================================
//#ifdef UTIL
//=============================================================================
//#ifdef UTIL
//int main(int argc, char *argv[])
//{
//    char[] snxfile = {"/mnt/c/Users/mikem/code/acs/pea/example/EX03/general/igs14.atx"};
//    extern int readsnx(const char *file, char *sta, snx_t *snx);

//    std::vector<std::unique_ptr<snx_t>> snx;
//    readsnx("" , sta, snx[index].get());

//    return 0;
//}
//#endif






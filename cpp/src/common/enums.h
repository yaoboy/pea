

#ifndef __ENUMS_H__
#define __ENUMS_H__

#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(Enum) \
  public:                                      \
    Enum() = default;

#include "enum.h"	//BETTER_ENUM

typedef enum
{
	F1,	L1 = F1,
	F2,	L2 = F2,
	F5,	L5 = F5,
	NUM_FTYPES,
	FTYPE_NONE,
	FTYPE_IF12,
	FTYPE_IF15,
	F6 = FTYPE_NONE,
	F8 = FTYPE_NONE,
	F3 = FTYPE_NONE,
	F4 = FTYPE_NONE,
	F7 = FTYPE_NONE,
	NONE,
// 	L1,
// 	L2,
// 	L5,
	G1,
	G2,
	E1,
	E5A,
	E5B,
	E5AB,
	E6,
	LEX,
	B1,
	B2,
	B3
} E_FType;

typedef enum
{
	CODE,
	PHAS,
	NUM_MEAS
} E_Meas;

// typedef enum
// {
// 	SYS_NONE,
// 	SYS_GPS,
// 	SYS_SBS,
// 	SYS_GLO,
// 	SYS_GAL,
// 	SYS_QZS,
// 	SYS_CMP,	SYS_BDS = SYS_CMP,
// 	SYS_LEO,
// 	SYS_IRN,
// 	NUM_SYS
// } E_Sysa;

BETTER_ENUM(E_Sys,			short int,
			NONE,
			GPS,
			SBS,
			GLO,
			GAL,
			QZS,
			CMP,
			LEO,
			IRN,
			NUM_SYS
   		)

BETTER_ENUM(BiasGroup,		short int,
			GPS,
			GLO,
			GAL,
			BDS,
			NUM)

BETTER_ENUM(E_Ephemeris,		short int,
			BROADCAST,
			PRECISE,
			SBAS,
			SSR_APC,
			SSR_COM)

BETTER_ENUM(KF,				short int,
	NONE,
	ONE,
	REC_POS,
	REC_POS_RATE,
	REF_SYS_BIAS,
	REC_SYS_BIAS,
	REC_SYS_BIAS_RATE,
	TROP,
	SAT_CLOCK,
	SAT_CLOCK_RATE,
	ORBIT_PTS,
	AMBIGUITY,
	IONOSPHERIC,
	DCB,
	EOP,

	XFORM_XLATE,
	XFORM_RTATE,
	XFORM_SCALE,


	PHASE_BIAS
)



//config file enums

BETTER_ENUM(E_TropModel,		int,
			VMF3,
			GPT2)

BETTER_ENUM(E_NoiseModel,		int,
			UNIFORM,
			ELEVATION_DEPENDENT,
			ELEVATION_DEPENDENT2)

BETTER_ENUM(E_LogLevel,			int,
			DEBUG,
			WARN,
			ERROR)

BETTER_ENUM(E_ProcNoiseModel,	int,
			GAUSSIAN,
			RANDOMWALK)

BETTER_ENUM(E_IonoModel,		int,
			NONE,
			BSPLINE,
			SPHERICAL_CAPS,
			SPHERICAL_HARMONICS)

BETTER_ENUM(E_IonoMode,			int,
			OFF,                        /* ionosphere option: correction off */
			BROADCAST,                  /* ionosphere option: broadcast model */
			SBAS,                       /* ionosphere option: SBAS model */
			IONO_FREE_LINEAR_COMBO,		/* ionosphere option: L1/L2 or L1/L5 iono-free LC */
			ESTIMATE,                   /* ionosphere option: estimation */
			TOTAL_ELECTRON_CONTENT,     /* ionosphere option: IONEX TEC model */
			QZS,                        /* ionosphere option: QZSS broadcast model */
			LEX,                        /* ionosphere option: QZSS LEX ionospehre */
			STEC)                       /* ionosphere option: SLANT TEC model */

BETTER_ENUM(E_LinearCombo,		int,
			ANY,
			L1L2_ONLY,
			L1L5_ONLY)

BETTER_ENUM(E_Period,			int,
			SECOND	= 1, 			SECONDS = SECOND,
			MINUTE	= 60, 			MINUTES = MINUTE,
			HOUR	= 60 * 60,		HOURS	= HOUR,
			DAY		= 60 * 60 * 24, DAYS	= DAY)

BETTER_ENUM(E_PosFrame,			int,
			NONE,
			XYZ,
			NED,
			RTN)

BETTER_ENUM(E_FilterMode,		int,
			LSQ,
			KALMAN)

BETTER_ENUM(E_Inverter,			int,
			LLT,
			LDLT,
			INV)


BETTER_ENUM(E_ObsCode, int,
	NONE  = 0 ,     		          /* obs code: none or unknown */
	L1C   = 1 ,						  /* obs code: L1C/A,G1C/A,E1C (GPS,GLO,GAL,QZS,SBS) */
	L1P   = 2 ,        		          /* obs code: L1P,G1P    (GPS,GLO) */
	L1W   = 3 ,        		          /* obs code: L1 Z-track (GPS) */
	L1Y   = 4 ,        		          /* obs code: L1Y        (GPS) */
	L1M   = 5 ,        		          /* obs code: L1M        (GPS) */
	L1N   = 6 ,        		          /* obs code: L1codeless (GPS) */
	L1S   = 7 ,        		          /* obs code: L1C(D)     (GPS,QZS) */
	L1L   = 8 ,        		          /* obs code: L1C(P)     (GPS,QZS) */
	L1E   = 9 ,        		          /* obs code: L1-SAIF    (QZS) */
	L1A   = 10,        		          /* obs code: E1A        (GAL) */
	L1B   = 11,        		          /* obs code: E1B        (GAL) */
	L1X   = 12,        		          /* obs code: E1B+C,L1C(D+P) (GAL,QZS) */
	L1Z   = 13,        		          /* obs code: E1A+B+C,L1SAIF (GAL,QZS) */
	L2C   = 14,        		          /* obs code: L2C/A,G1C/A (GPS,GLO) */
	L2D   = 15,        		          /* obs code: L2 L1C/A-(P2-P1) (GPS) */
	L2S   = 16,        		          /* obs code: L2C(M)     (GPS,QZS) */
	L2L   = 17,        		          /* obs code: L2C(L)     (GPS,QZS) */
	L2X   = 18,        		          /* obs code: L2C(M+L),B1I+Q (GPS,QZS,CMP) */
	L2P   = 19,        		          /* obs code: L2P,G2P    (GPS,GLO) */
	L2W   = 20,        		          /* obs code: L2 Z-track (GPS) */
	L2Y   = 21,        		          /* obs code: L2Y        (GPS) */
	L2M   = 22,        		          /* obs code: L2M        (GPS) */
	L2N   = 23,        		          /* obs code: L2codeless (GPS) */
	L5I   = 24,        		          /* obs code: L5/E5aI    (GPS,GAL,QZS,SBS) */
	L5Q   = 25,        		          /* obs code: L5/E5aQ    (GPS,GAL,QZS,SBS) */
	L5X   = 26,        		          /* obs code: L5/E5aI+Q  (GPS,GAL,QZS,SBS) */
	L7I   = 27,        		          /* obs code: E5bI,B2I   (GAL,CMP) */
	L7Q   = 28,        		          /* obs code: E5bQ,B2Q   (GAL,CMP) */
	L7X   = 29,        		          /* obs code: E5bI+Q,B2I+Q (GAL,CMP) */
	L6A   = 30,        		          /* obs code: E6A        (GAL) */
	L6B   = 31,        		          /* obs code: E6B        (GAL) */
	L6C   = 32,        		          /* obs code: E6C        (GAL) */
	L6X   = 33,        		          /* obs code: E6B+C,LEXS+L,B3I+Q (GAL,QZS,CMP) */
	L6Z   = 34,        		          /* obs code: E6A+B+C    (GAL) */
	L6S   = 35,        		          /* obs code: LEXS       (QZS) */
	L6L   = 36,        		          /* obs code: LEXL       (QZS) */
	L8I   = 37,        		          /* obs code: E5(a+b)I   (GAL) */
	L8Q   = 38,        		          /* obs code: E5(a+b)Q   (GAL) */
	L8X   = 39,        		          /* obs code: E5(a+b)I+Q (GAL) */
	L2I   = 40,        		          /* obs code: B1I        (CMP) */
	L2Q   = 41,        		          /* obs code: B1Q        (CMP) */
	L6I   = 42,        		          /* obs code: B3I        (CMP) */
	L6Q   = 43,        		          /* obs code: B3Q        (CMP) */
	L3I   = 44,        		          /* obs code: G3I        (GLO) */
	L3Q   = 45,        		          /* obs code: G3Q        (GLO) */
	L3X   = 46,        		          /* obs code: G3I+Q      (GLO) */
	L1I   = 47,        		          /* obs code: B1I        (BDS) */
	L1Q   = 48,         		          /* obs code: B1Q        (BDS) */
	MAXCODE = 48,
	NUM_CODES
)


BETTER_ENUM(E_Estimate, int,
	STAX,
	STAY,
	STAZ,
	VELX,
	VELY,
	VELZ,
	XGC,
	YGC,
	ZGC,
	RS_RAR,
	RS_DER,
	RS_RA,
	RS_DE,
	RS_PL,
	LOD,
	UT,
	XPOR,
	YPOR,
	XPO,
	YPO,
	NUT_LN,
	NUT_OB,
	NUTRLN,
	NUTROB,
	NUT_X,
	NUT_Y,
	NUTR_X,
	NUTR_Y,
	SAT__X,
	SAT__Y,
	SAT__Z,
	SAT_VX,
	SAT_VY,
	SAT_VZ,
	SAT_RP,
	SAT_GX,
	SAT_GZ,
	SATYBI,
	TROTOT,
	TRODRY,
	TROWET,
	TGNTOT,
	TGNDRY,
	TGNWET,
	TGETOT,
	TGEDRY,
	TGEWET,
	RBIAS,
	TBIAS,
	SBIAS,
	ZBIAS,
	AXI_OF,
	SATA_X,
	SATA_Y,
	SATA_Z,
	CN,
	SN)

#endif

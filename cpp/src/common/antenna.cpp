/*
* TODO function RADOME to NONE (for searching for alternative IGS antenna models)
*               anttype = radome2none(anttype) "BLAHBLAHBLAH DOME"-> "BLAHBLAHBLAH NONE"
*                                              JPLA->NONE ,etc
*-----------------------------------------------------------------------------*/
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <iostream>

#include <boost/log/trivial.hpp>


#include "common.hpp"
#include "antenna.h"
#include "enums.h"

#include "eigenIncluder.hpp"

const char galfreq[]="15678";

/* compare two time tags (t2-t1) -----------------------------------------------
 *
 * args     :       double t1[6]           I       time tag in [YMDHMS]
 *                  double t2[6]           I       time tag in [YMDHMS]
 *
 * return   :       julian day difference
 *----------------------------------------------------------------------------*/
double timecomp(const double t1[6], const double t2[6])
{
    /* convert two time tags to julian day */
    double jd1 = ymdhms2jd(t1);
    double jd2 = ymdhms2jd(t2);

	BOOST_LOG_TRIVIAL(debug) << "time difference in julian day is "<< jd2-jd1;

    return jd2-jd1;
}

/* add antenna to data structure -----------------------------------------------
 *
 * args     :       const pcvacs_t *pcv    I       antenna pcv for one
 *                  pcvacss_t *pcvs        O       antenna pcv for whole file
 *
 *----------------------------------------------------------------------------*/
void addpcv(const pcvacs_t *pcv, PcvList& pcvList)
{
	int nf	= pcv->nf;
	int nz	= pcv->nz;
	int naz	= pcv->naz;

	pcvList.push_back(*pcv);

	if (pcv->naz > 0)
		pcvList.back().recPcv3Mat.resize(nf, nz, naz);

	pcvList.back().satPcvMat.resize(nf, nz);		//todo aaron, change to a map of vectors for frequencies

}

/* decode antenna field */
int decodef(char *p, int n, double *v)
{
    int i;
    for (i = 0; i < n; i++)
		v[i] = 0;

    for (i = 0, p = strtok(p," "); p && i < n; p = strtok(NULL, " "))
	{
        v[i] = atof(p) * 1E-3;
		i++;
    }
    return i;
}
/* find sat or rec antenna information -----------------------------------------
 *
 * args     :       const char *type       I       antenna type
 *                  const char *code       I       satellite code
 *                  const double tc[6]     I       current time [YMDHMS]
 *                  const pcvacss_t *pcvs  I       antenna info from ANTEX
 *                  const int id           I       0-rec,1-sat
 *----------------------------------------------------------------------------*/
pcvacs_t* findantenna(
	string type,
	string code,
	double tc[6],
	PcvList& pcvList,
	int id)
{
	BOOST_LOG_TRIVIAL(debug)
	<< "Searching for " << type << ", " << code;

	for (auto& pcv : pcvList)
	{
        /* searching for rec antenna based on IGS antenna type */
        if (id == 0)
		{
            if (pcv.type == type)
			{
                BOOST_LOG_TRIVIAL(debug) << "Rec Antenna " << type << "matches " << pcv.type;

                return &pcv;
            }
            /* for JPL network processing */
	    // called from main instead
	    // radome2none, then call findantenna()
            //else if (strstr(type,"JPLA")||strstr(type,"AUST")) {
	    //	char * antennaType = radome2none(pcv->type);
            //    if (strstr(pcv->type,"AOAD/M_T        NONE"))
            //        return pcv;
            //}
        }
        else
		{
			// searching for sat antenna based on satellite code "sNN"

            // check BLOCK first
			if (0&&pcv.type.find(type)	== string::npos)
				continue;

			// check PRN
			if (pcv.code.find(code)		== string::npos)
				continue;

			// if no start time
			if (!ymdhms2jd(pcv.tf))
				return &pcv;

            /* check the validity */
			if	(   (timecomp(pcv.tf, tc) >= 0)
				&&( (timecomp(pcv.tu, tc) <= 0)
					||!ymdhms2jd(pcv.tu)))
			{
				BOOST_LOG_TRIVIAL(debug) << "Sat Antenna " << code << "matches " << pcv.code;

				return &pcv;
			}
        }
    }

    return nullptr;
}
/* linear interpolate pcv ------------------------------------------------------
 *
 * args     :       double x1              I       x1 lower bound (degree)
 *                  double x2              I       x2 upper bound (degree)
 *                  double y1              I       y1 lower bound (m)
 *                  double y2              I       y2 upper bound (m)
 *                  double x               O       x current point (degree)
 *
 * return   :       interpolated pcv (m)
 *----------------------------------------------------------------------------*/
double interp(double x1, double x2, double y1, double y2, double x)
{
#if (0)
    return (y2-y1)*(x-x1)/(x2-x1)+y1;
#endif
    return y2-(y2-y1)*(x2-x)/(x2-x1);
}

/* fetch satellite pcv ---------------------------------------------------------
 *
 * args     :       const pcvacs_t pc      I       antenna info
 *                  const double nadir     I       satellite nadir (degree)
 *                  double *pcv            O       sat pcv (m)
 *----------------------------------------------------------------------------*/
void satpcv(pcvacs_t *pc, double nadir, double* pcv)
{
    double x1,x2,y1,y2,zen1,dzen;

    int nf = pc->nf;
    int nz = pc->nz;
    zen1 = pc->zenStart;
    dzen = pc->zenDelta;

    /* linear interpolate satellite pcv */
    for (int i = 0; i < nf; i++)
	{
        /* initialize pcv */
        pcv[i] = 0;
        for (int j = 0;j < nz; j++)
		{
            if ((zen1+dzen*j)>nadir)
			{
                x1 = zen1+dzen*(j-1);		y1 = pc->satPcvMat(i,j-1);	//lower bound
                x2 = zen1+dzen*j;			y2 = pc->satPcvMat(i,j);	//upper bound

                /* interpolate */
                pcv[i] = interp(x1, x2, y1, y2, nadir);
                break;
            }
        }
    }

    return;
}

/* fetch rec pco ---------------------------------------------------------------
 *
 * args     :       const pcvacs_t *pc     I       antenna info
 *                  const chat sys         I       satellite system
 *                  const int freq         I       frequency 1 or 2
 *                  double pco[3]          O       rec pco (m)
 *
 * return   :       none
 *
 * note     :       frequencies are sorted as GPS, GLONASS, Galileo and BeiDou
 *----------------------------------------------------------------------------*/
void recpco(pcvacs_t *pc, char sys, int freq, Vector3d& pco)
{
    int f = -1;

    /* pco value */		//todo aaron, this looks sketchy, do the pcv structs always have the freqencies in the same order, with all present? NO! needs fixing!!!
    if 		(sys == pc->sys[0])	{	/* GPS 		*/	if (freq < pc->ngps)	{	f = freq + 0;								}	}
    else if (sys == pc->sys[1])	{	/* GLONASS 	*/	if (freq < pc->nglo)	{	f = freq + pc->ngps;						}	}
    else if (sys == pc->sys[2])	{   /* GALILEO 	*/	if (freq < pc->ngal)	{	f = freq + pc->ngps + pc->nglo;				}	}
    else if (sys == pc->sys[3])	{   /* BeiDou 	*/	if (freq < pc->nbds)	{	f = freq + pc->ngps + pc->nglo + pc->ngal;	}	}

	if (f < 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "no rec pco value for system " << sys << " frequency " << freq;
		return;
	}
	else
	{
		/* assign rec/sat pco */
		pco = pc->pcoMap[(E_FType) f];
	}
}

/* fetch rec pcv ---------------------------------------------------------------
 *
 * args     :       const pcvacs_t pc      I       antenna info
 *                  const chat sys         I       satellite system
 *                  const int freq         I       frequency 1, 2
 *                  const double el        I       satellite elevation (degree)
 *                  const double azi       I       azimuth (degree)
 *                  double *pcv            O       rec pcv (m)
 *
 * return   :       none
 *
 * note     :       frequencies are sorted as GPS, GLONASS, Galileo and BeiDou
 *----------------------------------------------------------------------------*/
void recpcv(
	pcvacs_t*	pc,
	char		sys,
	int			freq,
	double		el,
	double		azi,
	double&		pcv)
{
    int		nz		= pc->nz;
    int		naz		= pc->naz;
    double	zen1	= pc->zenStart;
    double	dzen	= pc->zenDelta;
    double	dazi	= pc->aziDelta;
    double	zen		= 90 - el;

	int f = -1;

    /* pco value */		//todo aaron, this looks sketchy, do the pcv structs always have the freqencies in the same order, with all present? NO! needs fixing!!!
    if		(sys == pc->sys[0])	{	/* GPS		*/	if	(freq < pc->ngps)		{	f = 0;								}	}
    else if (sys == pc->sys[1])	{	/* GLONASS	*/	if	(freq < pc->nglo)		{	f = pc->ngps;						}	}
    else if (sys == pc->sys[2])	{	/* GALILEO	*/	if	(freq < pc->ngal)		{	f = pc->ngps + pc->nglo;			}	}
    else if (sys == pc->sys[3])	{	/* BeiDou	*/	if	(freq < pc->nbds)		{	f = pc->ngps + pc->nglo + pc->ngal;	}	}

	if (f < 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "No rec pcv value for system " << sys << " frequency " << freq;
		return;
	}

    double x1,x2,x3,x4,y1,y2,y3,y4,t1,t2;

    /* select zenith angle range */
	int i;
    for (i = 1; i < nz; i++)
	{
        if ((zen1+dzen*i) >= zen)
		{
			x1 = zen1 + dzen * (i-1);	y1 = pc->satPcvMat(freq, i-1);		// lower bound
			x2 = zen1 + dzen * (i);		y2 = pc->satPcvMat(freq, i);		// upper bound
            break;
        }
    }

    if (naz == 0)
	{
		/* linear interpolate receiver pcv - non azimuth-dependent */
        /* interpolate */
        pcv = interp(x1,x2,y1,y2,zen);
    }
    else
	{
		/* bilinear interpolate receiver pcv - azimuth-dependent */
        /* select azimuth angle range */
		int j;
        for (j = 0; j < naz; j++)
		{
            if ((dazi * j) >= azi)
			{
                x3 = dazi*(j-1);	y3 = pc->recPcv3Mat(freq, i-1, j-1);
				x4 = dazi*j;		y4 = pc->recPcv3Mat(freq, i-1, j);
                break;
            }
        }

        y1 = pc->recPcv3Mat(freq, i, j-1);
        y2 = pc->recPcv3Mat(freq, i, j);

        /* linear interpolation along zenith angle */
        t1		= interp(x1, x2, y3, y1, zen);
        t2 		= interp(x1, x2, y4, y2, zen);

        /* linear interpolation along azimuth angle */
        pcv	= interp(x3, x4, t1, t2, azi);
    }

    return;
}
//=============================================================================
// radomeNoneAntennaType = radome2none(antennaType)
//
//       e,g, "AOAD/M_T        JPLA" => "AOAD/M_T        NONE"
//
// Change the last four characters of antenna type to NONE
// This function is useful for when searching for an antenna model in ANTEX
//
// The IGS convention is to default to NONE for the radome if the calibration
// value is not available
//=============================================================================
//void radome2none(char *restrict antenna_type)
void radome2none(string& antenna_type)
{
    size_t length = antenna_type.size();
    if (length != 20)
	{
        printf("\n*** ERROR radome2none(): string length is less then 20 characters received %ld characters\n",length);
        return;
    }
    antenna_type.replace(length - 4, 4, "NONE");
}

/* read antex file -------------------------------------------------------------
 *
 * args     :       const char *file       I       antex file path
 *                  pcvacss_t *ds_pcvs     O       antenna info
 *---------------------------------------------------------------------------*/
int readantexf(const char *file, PcvList& pcvList)
{
    FILE *fp;

    const pcvacs_t pcv0 = {0};
    pcvacs_t ds_pcv;

    //int i,k,j,offset,noazi_flag = 0,num_azi_rd = 0,num_calibrated=0;
    int i,k,j,offset,noazi_flag = 0,num_azi_rd = 0,num_calibrated;
    int new_antenna=0,num_antennas=0,start_freq=0;
    int irms=0,ind=0;

    char buff[512],tmp[10],ant_serialnum[20],ant_code[10],cospar_id[10];
    char cal_method[20],cal_agency[20],cal_date[10];
    char valid_from[43],valid_until[43],*p;
    const char *p1,*p2;

    double pcv_val;

    if (!(fp=fopen(file,"r")))
	{
		BOOST_LOG_TRIVIAL(warning) << "Warning: ANTEX file opening error";
        return 0;
    }

    while (fgets(buff,sizeof(buff),fp))
	{
        if (irms) continue;
        /* Read in the ANTEX header information */
        if		(strlen(buff)<60||strstr(buff+60,"ANTEX VERSION / SYST"))	{	continue;	}
        else if (strlen(buff)<60||strstr(buff+60,"PCV TYPE / REFANT")) 		{	continue;	}
        else if (strlen(buff)<60||strstr(buff+60,"COMMENT")) 				{	continue;	}
        else if (strlen(buff)<60||strstr(buff+60,"END OF HEADER"))			{	continue;	}
        /* Read in specific Antenna information now */
        else if (strstr(buff+60,"START OF ANTENNA"))
		{
            num_antennas++;
            ds_pcv = pcv0;
            new_antenna = 1;        /* flag for new antenna */
        }
        else if (!new_antenna)
		{
            continue;
        }
        else if (strstr(buff+60,"METH / BY / # / DATE"))
		{
            strncpy(cal_method,buff,20);
            /* Should be CHAMBER or FIELD or ROBOT or COPIED ot CONVERTED */

            strncpy(cal_agency,buff+20,20);

            strncpy(tmp,buff+40,10);
            num_calibrated = atoi(tmp);

            strncpy(cal_date,buff+50,10);
        }
        else if (strstr(buff+60,"DAZI"))
		{
            strncpy(tmp,buff   ,8);tmp[8] = '\0';
            ds_pcv.aziDelta = atof(tmp);

            if (ds_pcv.aziDelta < 0.0001)	ds_pcv.naz = 0;
            else                     		ds_pcv.naz = (360.0 / ds_pcv.aziDelta) + 1;
        }
        else if (strstr(buff+60,"END OF ANTENNA"))
		{
            /* reset the flags for the next antenna */
            new_antenna=0;
            start_freq=0;       /* flag for new frequency */
            ind=0;

            /* stack antenna pco and pcv */
            addpcv( &ds_pcv, pcvList);
        }
        else if (strstr(buff+60,"TYPE / SERIAL NO"))
		{
			ds_pcv.type.assign(buff,20);
			ds_pcv.type[20]='\0';
            /* pcv.type = &ant_type; */
            strncpy(ant_serialnum,buff+20,20);
			ds_pcv.code.assign(ant_serialnum);
            strncpy(ant_code,buff+40,10);
            strncpy(cospar_id,buff+50,10);

            if (strstr(buff,"AOAD/M_T"))
                k=0;
        }
        else if (strstr(buff+60,"ZEN1 / ZEN2 / DZEN"))
		{
            strncpy(tmp, buff,		8);	tmp[8] = '\0'; 	ds_pcv.zenStart	= atof(tmp);
            strncpy(tmp, buff+8,	7);	tmp[8] = '\0'; 	ds_pcv.zenStop	= atof(tmp);
            strncpy(tmp, buff+16,	7);	tmp[8] = '\0'; 	ds_pcv.zenDelta	= atof(tmp);

            ds_pcv.nz = (ds_pcv.zenStop - ds_pcv.zenStart) / ds_pcv.zenDelta + 1 ;
        }
        else if (strstr(buff+60,"# OF FREQUENCIES"))
		{
            strncpy(tmp, buff,		8);	tmp[8] = '\0'; 	ds_pcv.nf		= atoi(tmp);
        }
        else if (strstr(buff+60,"VALID FROM"))
		{
            j=0;
            /* if (!str2time(buff,0,43,&pcv.ts)) continue;*/
            strncpy(valid_from,buff   ,43);
            p = strtok(valid_from," ");
            while (p != NULL)
			{
                ds_pcv.tf[j]=(double) atoi(p);
                j++;
                p = strtok(NULL," ");
            }
        }
        else if (strstr(buff+60,"VALID UNTIL"))
		{
            j=0;
            /* if (!str2time(buff,0,43,&pcv.te)) continue;*/
            strncpy(valid_until,buff   ,43);
            p = strtok(valid_until," ");
            while(p!=NULL)
			{
                ds_pcv.tu[j]=(double) atoi(p);
                j++;
                p = strtok(NULL," ");
            }
        }
        else if (strstr(buff+60,"NORTH / EAST / UP"))
		{
			double neu[3];
            if (decodef(buff, 3, neu) < 3)
			{
                 continue;
            }

            /* assign pco value in ENU */
			Vector3d& enu = ds_pcv.pcoMap[(E_FType)(start_freq-1)];//todo aaron, seg faults here, more than once in odd times
            enu[0] = neu[1];
            enu[1] = neu[0];
            enu[2] = neu[2];
        }
        else if (strstr(buff+60,"START OF FREQUENCY"))
		{
            start_freq++;
            ind++;
            if (buff[3]=='E')
			{
                p1=galfreq;
                p2=strchr(galfreq,buff[5]);
                start_freq=p2-p1+1;
            }

            num_azi_rd = 0;
            noazi_flag = 0;

            /* sort the frequencies by systems */
            if		(ds_pcv.sys[0] == '\0') 	{                						ds_pcv.sys[0] = buff[3]; }
            else if (ds_pcv.sys[1] == '\0') 	{	if (buff[3] != ds_pcv.sys[0]) 		ds_pcv.sys[1] = buff[3]; }
            else if (ds_pcv.sys[2] == '\0')		{	if (buff[3] != ds_pcv.sys[1]) 		ds_pcv.sys[2] = buff[3]; }
            else if (ds_pcv.sys[3] == '\0') 	{	if (buff[3] != ds_pcv.sys[2]) 		ds_pcv.sys[3] = buff[3]; }

            if		(buff[3]=='G')		ds_pcv.ngps++;
            else if (buff[3]=='R')		ds_pcv.nglo++;
            else if (buff[3]=='E')		ds_pcv.ngal++;
            else if (buff[3]=='C')		ds_pcv.nbds++;
            /**************************************************************/
            /* Initialise the variable data arrays for PCO, NOAZI and PCV */
            /**************************************************************/
            if (ind == 1)
			{
				ds_pcv.satPcvMat.resize(ds_pcv.nf, ds_pcv.nz);
				ds_pcv.satPcvMat.setZero();

                if (ds_pcv.naz > 0)
				{
					ds_pcv.recPcv3Mat.resize(ds_pcv.nf, ds_pcv.nz, ds_pcv.naz);
					ds_pcv.recPcv3Mat.setZero();
                }
            }
        }
        else if (strstr(buff+60,"END OF FREQUENCY"))	{	noazi_flag	= 0;	continue;	}
        else if (strstr(buff+60,"START OF FREQ RMS"))	{	irms		= 1;	continue;	}
        else if (strstr(buff+60,"END OF FREQ RMS"))		{	irms		= 0;	continue;	}
        else if (!irms&&strstr(buff,"NOAZI"))
		{
            for (i = 0;i<ds_pcv.nz;i++)
			{
                offset = i*8 + 8;
                strncpy(tmp,buff+offset,8); tmp[8]='\0';
                pcv_val = atof(tmp);
                ds_pcv.satPcvMat(start_freq-1,i) = pcv_val*1e-3;
            }
            noazi_flag = 1;
        }
        else if(!irms&&noazi_flag==1)
		{
            strncpy(tmp,buff,8); tmp[8]='\0';

            for (i = 0;i<ds_pcv.nz;i++)
			{
                offset = i*8 + 8;
                strncpy(tmp,buff+offset,8); tmp[8]='\0';
                pcv_val = atof(tmp);
                ds_pcv.recPcv3Mat(start_freq-1,i,num_azi_rd) = pcv_val*1e-3;
            }
            num_azi_rd++;
        }
    }
    fclose(fp);

    return 1;
}

/* satellite antenna model ------------------------------------------------------
* compute satellite antenna phase center parameters
* args   : pcv_t *pcv       I   antenna phase center parameters
*          double nadir     I   nadir angle for satellite (rad)
*          double *dant     O   range offsets for each frequency (m)
* return : none
*-----------------------------------------------------------------------------*/
// inplace of antmodel_
// this will not work
// for galileo models
void interp_satantmodel(pcvacs_t *pcv, double nadir, double *dant)
{
    for (int j = 0; j < pcv->satPcvMat.rows(); j++)
	{
        double	nadirDeg		= nadir * R2D;				// ang=0-90
        int		numSections		= pcv->satPcvMat.cols();
		double	startAngle		= pcv->zenStart;
		double	sectionWidth	= pcv->zenDelta;
		double	realSection		= ((nadirDeg - startAngle) / sectionWidth);
		double	intSection		= (int) realSection;
		double	fraction		= realSection - intSection;

		if		(intSection < 0)				{	dant[j] = pcv->satPcvMat(j, 0);					}
		else if	(intSection >= numSections)		{	dant[j] = pcv->satPcvMat(j, numSections-1);		}
		else
		{
			double a = pcv->satPcvMat.coeff(j, intSection);
			double b = pcv->satPcvMat.coeff(j, intSection + 1);

			dant[j] = a + fraction * (b - a);
		}
    }
}

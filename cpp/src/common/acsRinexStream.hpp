
#ifndef __RINEX_STREAM__HPP
#define __RINEX_STREAM__HPP

/** Interface for rinex streams
 */
struct RinexStream : ObsStream, NavStream
{
    char	ctype;
    double	version;
    E_Sys	navigation_system;
    int 	time_system;
    char	observation_types[100][MAXOBSTYPE][4];
	obs_t 	obsnet;


    FILE*	fp;

	RinexStream()
	{

	}

	void parseRINEX(std::istream& inputStream)
	{
		//read some of the input,(up to next epoch header?)
		//save outputs to member variables.
		//eg. header metadata
		//eg. list of (ObsLists) with multiple sats, signals combined for each epoch.
		//dont parse all, just some.

		//this structure to match real-time architecture.
		int stat = 0;
		// account for rinex comment in the middle of the file
		while   ( stat<=0
				&&!feof(fp))
		{
			stat = readrnxfppde(fp, {}, {}, 0, "", 0, 1, &ctype, &obsnet, nullptr, nullptr,	&version, &navigation_system, &time_system, observation_types);
		}

		obsListList.push_back(std::move(obsnet.obsList));
	}

	int readRinexHeader()
	{
        if  ( !feof(fp)
			&&!readrnxfppde(fp, {}, {}, 0, nullptr, 0, 0, &ctype, nullptr, nullptr, &station, &version, &navigation_system, &time_system, observation_types))
		{
			BOOST_LOG_TRIVIAL(error)
			<< "ERROR: Failed to read header from RINEX file ";

            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
	}
};

#endif

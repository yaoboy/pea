#ifndef __CORRECTIONS__HPP__
#define __CORRECTIONS__HPP__

#include "observations.hpp"
#include "gaTime.hpp"

/* atmosphere models ---------------------------------------------------------*/
double	ionmodel(GTime t, const double *ion, const double *pos, const double *azel);
double	ionmapf(Vector3d&rr, const double *azel);
double	ionppp(const double *pos, const double *azel, double re, double hion, double *pppos);
int		iontec(GTime time, const nav_t *nav, const double *pos, const double *azel, int opt, double&delay, double&var);
void	readtec(const char *file, nav_t *nav, int opt,FILE *fpout);


void obsVariances(
	ObsList& obsList);

#endif



#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <map>

using std::vector;
using std::string;
using std::list;
using std::map;

#include "gnuplot_i.hpp"


#include "acsConfig.hpp"

void outputPlots()
{
	extern map<string, vector<double>> plotVectors;

	map<string, Gnuplot> plots;

	for (auto& a : list<list<string>>{{"Trop", "rtsTrop"}})
	{
		string firstStr = a.front();

		Gnuplot& gp = plots[firstStr];

		gp.reset_plot();
		gp.set_grid();
		gp.set_style("lines");
		gp.set_title("Value");

		for (auto& name : a)
		{
			gp.prepare_plot_x(plotVectors[name], name);
		}

		if (acsConfig.plot_filename.empty())
		{
			gp.cmd("set terminal x11");
		}
		else
		{
			gp.cmd("set terminal png");
			gp.cmd("set output \""  + acsConfig.plot_filename + firstStr + ".png\"");
		}

		gp.plot_prepared();
	}

	if (acsConfig.plot_filename.empty())
	{
		std::cin.clear();
		std::cin.ignore(std::cin.rdbuf()->in_avail());
		std::cin.get();
	}
}




#include "testUtils.hpp"

list<string>							TestStack::TestStackList;
list<string>							TestStack::RecordStackList;
unordered_map<string, vector<double>>	TestStack::TestDoubleData;
unordered_map<string, string>			TestStack::TestStringData;
unordered_map<string, int>				TestStack::TestStatus;
unordered_map<string, string>			TestStack::TestRedirect;
std::ofstream							TestStack::TestOutputStream;
std::ofstream							TestStack::TestNameStream;
bool									TestStack::DontTest			= false;

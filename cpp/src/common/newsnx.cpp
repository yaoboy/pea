#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/utsname.h>

#include <boost/log/trivial.hpp>

#include "eigenIncluder.hpp"
#include "acsConfig.hpp"
#include "algebra.hpp"
#include "gaTime.hpp"
#include "newsnx.hpp"
#include "station.hpp"

using std::endl;
using std::getline;
using std::ifstream;
using std::ofstream;
using std::cout;			//todo aaron, should be using boost log or other



// Sinex 2.02 documentation indicates 2 digit years. >50 means 1900+N. <=50 means 2000+N
// To achieve this, when we read years, if >50 add 1900 else add 2000. This source will
// cease to work safely around 2045!
// when we write years, write out modulo 100
// This only applies to site data, for satellites it is using 4 digit years

void nearestYear(int& year)
{
	if (year > 50)	year += 1900;
	else			year += 2000;
}

newsinex_t theSinex(false); // the one and only sinex object.


// Do we need any others?
satellite_map 	prnmap;
station_map		sitemap;

string trim(const string& ref)
{
	int start = 0, stop = ref.length() - 1, len;

	while (start != stop && isspace(ref[start]))
		start++;

	while (stop != start && isspace(ref[stop]))
		stop--;

	len = stop - start + 1;
	return ref.substr(start, len);
}

// return seconds diff bewteen left and right. If left < right the value is negative
// each argument is given as year/doy/sod
int time_compare(int left[3], int right[3])
{
	int leftfull	= (left[0]	* 365 + left[1])	* 86400 + left[2];
	int rightfull	= (right[0] * 365 + right[1])	* 86400 + right[2];

	return leftfull - rightfull;
}

bool compare(newsnx_ref_t& one, 	newsnx_ref_t& two)
{
	if (one.refline.compare(two.refline) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_input_file_t& one, 	newsnx_input_file_t& two)
{
	if 	(  one.epoch[0]	== two.epoch[0]
        && one.epoch[1]	== two.epoch[1]
        && one.epoch[2]	== two.epoch[2]
        && one.agency		.compare(two.agency)		== 0
		&& one.file			.compare(two.file)			== 0
        && one.description	.compare(two.description)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_solstatistic_t& one, 	newsnx_solstatistic_t& two)
{
	if (one.name.compare(two.name) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satpc_t& one, 	newsnx_satpc_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.freq	== two.freq
		&&one.freq2	== two.freq2)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satecc_t& one, 	newsnx_satecc_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.type == two.type)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satmass_t& one, 	newsnx_satmass_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.start[0]	== two.start[0]
		&&one.start[1]	== two.start[1]
		&&one.start[2]	== two.start[2]
		&&one.stop[0]	== two.stop[0]
		&&one.stop[1]	== two.stop[1]
		&&one.stop[2]	== two.stop[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satfreqchn_t& one, 	newsnx_satfreqchn_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.start[0]	== two.start[0]
        &&one.start[1]	== two.start[1]
        &&one.start[2]	== two.start[2]
        &&one.stop[0]	== two.stop[0]
        &&one.stop[1]	== two.stop[1]
        &&one.stop[2]	== two.stop[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satid_t& one, 	newsnx_satid_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.prn.compare(two.prn) == 0
		&&one.timeSinceLaunch[0]	== two.timeSinceLaunch[0]
		&&one.timeSinceLaunch[1]	== two.timeSinceLaunch[1]
		&&one.timeSinceLaunch[2]	== two.timeSinceLaunch[2]
		&&one.timeUntilDecom[0]		== two.timeUntilDecom[0]
		&&one.timeUntilDecom[1]		== two.timeUntilDecom[1]
		&&one.timeUntilDecom[2]		== two.timeUntilDecom[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_precode_t& one, 	newsnx_precode_t& two)
{
	if 	(one.precesscode.compare(two.precesscode) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_source_id_t& one, 	newsnx_source_id_t& two)
{
	if 	(one.source.compare(two.source) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_nutcode_t& one, 	newsnx_nutcode_t& two)
{
	if 	(one.nutcode.compare(two.nutcode) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satident_t& one, 	newsnx_satident_t& two)
{
	if 	(one.svn.compare(two.svn) == 0)
	{
		return true;
	}
	return false;
}
bool compare(newsnx_comment_t& one, 	newsnx_comment_t& two)
{
	if 	(one.cmtline.compare(two.cmtline) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satprn_t& one, 	newsnx_satprn_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.prn.compare(two.prn) == 0
        &&one.start[0] == two.start[0]
        &&one.start[1] == two.start[1]
        &&one.start[2] == two.start[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satpower_t& one, 	newsnx_satpower_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.start[0]	== two.start[0]
        &&one.start[1]	== two.start[1]
        &&one.start[2]	== two.start[2]
        &&one.stop[0]	== two.stop[0]
        &&one.stop[1]	== two.stop[1]
        &&one.stop[2]	== two.stop[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_satcom_t& one, 	newsnx_satcom_t& two)
{
	if 	( one.svn.compare(two.svn) == 0
		&&one.start[0]	== two.start[0]
        &&one.start[1]	== two.start[1]
        &&one.start[2]	== two.start[2]
        &&one.stop[0]	== two.stop[0]
        &&one.stop[1]	== two.stop[1]
        &&one.stop[2]	== two.stop[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_ack_t& one, 	newsnx_ack_t& two)
{
	if 	( one.agency		.compare(two.agency)		== 0
		&&one.description	.compare(two.description)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_input_history_t& one, 	newsnx_input_history_t& two)
{
	if 	(  one.code				== two.code
		&& one.fmt				== two.fmt
	    && one.create_time[0]	== two.create_time[0]
	    && one.create_time[1]	== two.create_time[1]
	    && one.create_time[2]	== two.create_time[2]
	    && one.start[0]			== two.start[0]
	    && one.start[1]			== two.start[1]
	    && one.start[2]			== two.start[2]
	    && one.stop[0]			== two.stop[0]
	    && one.stop[1]			== two.stop[2]
	    && one.stop[2]			== two.stop[2]
	    && one.obs_tech			== two.obs_tech
	    && one.num_estimates	== two.num_estimates
	    && one.constraint		== two.constraint
	    && one.contents		.compare(two.contents) 		== 0
	    && one.data_agency	.compare(two.data_agency) 	== 0
		&& one.create_agency.compare(two.create_agency) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_siteid_t& one, 	newsnx_siteid_t& two)
{
	if 	(one.sitecode.compare(two.sitecode) == 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_sitedata_t& one, 	newsnx_sitedata_t& two)
{
	if 	( one.site.		compare(two.site)		== 0
		&&one.sitecode.	compare(two.sitecode)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_receiver_t& one, 	newsnx_receiver_t& two)
{
	if 	( one.sitecode.compare(two.sitecode) == 0
		&&one.recstart[0]	== two.recstart[0]
		&&one.recstart[1]	== two.recstart[2]
		&&one.recstart[2]	== two.recstart[2]
		&&one.recend[0]		== two.recend[0]
		&&one.recend[1]		== two.recend[1]
		&&one.recend[2]		== two.recend[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_antenna_t& one, 	newsnx_antenna_t& two)
{
	if 	( one.sitecode.compare(two.sitecode) == 0
		&&one.antstart[0]	== two.antstart[0]
		&&one.antstart[1]	== two.antstart[2]
		&&one.antstart[2]	== two.antstart[2]
		&&one.antend[0]		== two.antend[0]
		&&one.antend[1]		== two.antend[1]
		&&one.antend[2]		== two.antend[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_gps_phase_center_t& one, 	newsnx_gps_phase_center_t& two)
{
	if 	( one.antname	.compare(two.antname)	== 0
		&&one.serialno	.compare(two.serialno)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_gal_phase_center_t& one, 	newsnx_gal_phase_center_t& two)
{
	if 	( one.antname	.compare(two.antname)	== 0
		&&one.serialno	.compare(two.serialno)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_site_ecc_t& one, 	newsnx_site_ecc_t& two)
{
	if 	( one.sitecode.compare(two.sitecode) == 0
	 	&&one.eccstart[0]	== two.eccstart[0]
		&&one.eccstart[1]	== two.eccstart[2]
		&&one.eccstart[2]	== two.eccstart[2]
		&&one.eccend[0]		== two.eccend[0]
		&&one.eccend[1]		== two.eccend[1]
		&&one.eccend[2]		== two.eccend[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_solepoch_t& one, 	newsnx_solepoch_t& two)
{
	if 	( one.sitecode.compare(two.sitecode) == 0
		&&one.start[0]	== two.start[0]
		&&one.start[1]	== two.start[2]
		&&one.start[2]	== two.start[2]
		&&one.end[0]	== two.end[0]
		&&one.end[1]	== two.end[1]
		&&one.end[2]	== two.end[2])
	{
		return true;
	}
	return false;
}

bool compare(newsnx_solestimate_t& one, 	newsnx_solestimate_t& two)
{
	if 	( one.sitecode.compare(two.sitecode)		== 0
		&&one.type.compare(two.type)				== 0
		&&time_compare(one.refepoch, two.refepoch)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_solapriori_t& one, 	newsnx_solapriori_t& two)
{
	if 	( one.sitecode.compare(two.sitecode)		== 0
		&&one.param_type.compare(two.param_type)	== 0
		&&time_compare(one.epoch, two.epoch)		== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_solneq_t& one, 	newsnx_solneq_t& two)
{
	if 	( one.site.compare(two.site)			== 0
		&&one.ptype.compare(two.ptype)			== 0
		&&time_compare(one.epoch, two.epoch)	== 0)
	{
		return true;
	}
	return false;
}

bool compare(newsnx_solmatrix_t& one, 	newsnx_solmatrix_t& two)
{
	if 	( one.row == two.row
		&&one.col == two.col)
	{
		return true;
	}
	return false;
}


template<typename TYPE>
void dedupe(list<TYPE>& source)
{
	list<TYPE> copy;

	for (auto it = source.begin(); it != source.end();   )
	{
		bool found = false;

		for (auto it2 = copy.begin(); it2 != copy.end(); it2++)
		{
			if (compare(*it, *it2))
			{
				found = true;
				break;
			}
		}

		if (found)
		{
			it = source.erase(it);
		}
		else
		{
			copy.push_back(*it);
			it++;
		}
	}
}

template<typename TYPE>
void dedupeB(list<TYPE>& source)
{
	TYPE previous;
	bool first = true;

	for (auto it = source.begin(); it != source.end();   )
	{
		bool found = false;

		if (!first)
		{
			if (compare(*it, previous))
			{
				found = true;
			}
		}

		if (found)
		{
			it = source.erase(it);
		}
		else
		{
			previous = *it;
			it++;
			first = false;
		}
	}
}

// each of the lists is parsed for duplicates. When a dup is found it is erased. At the end of each loop the _copy list should contain the same stuff
// as the original
void dedupe_sinex()
{
	// do the lists which are not sorted first

	// general stuff
	dedupe(theSinex.refstrings);
	dedupe(theSinex.commentstrings);
	dedupe(theSinex.inputHistory);
	dedupe(theSinex.inputFiles);
	dedupe(theSinex.acknowledgements);
	dedupe(theSinex.list_nutcodes);
	dedupe(theSinex.list_precessions);
	dedupe(theSinex.list_source_ids);
	dedupe(theSinex.list_satids);
	dedupe(theSinex.list_satidents);
	dedupe(theSinex.list_satprns);
	dedupe(theSinex.list_satfreqchns);
	dedupe(theSinex.list_satmasses);
	dedupe(theSinex.list_satcoms);
	dedupe(theSinex.list_satpowers);
	dedupe(theSinex.list_sateccs);
	dedupe(theSinex.list_satpcs);
	dedupe(theSinex.list_statistics);

	// 	// TODO: need to make sure sitecode & type match on index
	// site stuff
	// all data is sorted before coming in here, so it suffices to just check against the previous value
	dedupeB(theSinex.list_siteids);
	dedupeB(theSinex.list_sitedata);
	dedupeB(theSinex.list_receivers);
	dedupeB(theSinex.list_antennas);
	dedupeB(theSinex.list_gps_pcs);
	dedupeB(theSinex.list_gal_pcs);
	dedupeB(theSinex.list_site_eccs);
	dedupeB(theSinex.list_solepochs);
	dedupeB(theSinex.list_normal_eqns);
	newsnx_solmatrix_t me_copy;

	for (matrix_type	t = ESTIMATE;		t < MAX_MATRIX_TYPE;	t = static_cast<matrix_type>	(static_cast<int>(t) + 1))
	for (matrix_value	v = CORRELATION;	v < MAX_MATRIX_VALUE;	v = static_cast<matrix_value>	(static_cast<int>(v) + 1))
	{
		if (theSinex.matrix_map[t][v].empty())
			continue;

		dedupeB(theSinex.matrix_map[t][v]);
	}

	return;
}

// TODO; What if we are reading a second file. What wins?
int read_snx_header(std::ifstream& in)
{
	string s;

	std::getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL(error) << "empty file" << endl;
		return 1;
	}

	// verify line contents
	if 	(  s[0] != '%'
		|| s[1] != '='
		|| s[2] != 'S'
		|| s[3] != 'N'
		|| s[4] != 'X')
	{
		// error. not a sinex file
		BOOST_LOG_TRIVIAL(error) << "Not a sinex file" << endl;
		return 2;
	}

	// remaining characters indiciate properties of the file
	if (s.length() > 5)
	{
		const char* p = s.c_str();
		char create_agc[4];
		char data_agc[4];
		char solcontents[7];

		int  readcount = sscanf(p + 6, "%4lf %3s %2d:%3d:%5d %3s %2d:%3d:%5d %2d:%3d:%5d %c %5d %c %c %c %c %c %c %c",
		                   &theSinex.ver,
		                   create_agc,
		                   &theSinex.filedate[0],
		                   &theSinex.filedate[1],
		                   &theSinex.filedate[2],
		                   data_agc,
		                   &theSinex.solution_start_date[0],
		                   &theSinex.solution_start_date[1],
		                   &theSinex.solution_start_date[2],
		                   &theSinex.solution_end_date[0],
		                   &theSinex.solution_end_date[1],
		                   &theSinex.solution_end_date[2],
		                   &theSinex.ObsCode,
		                   &theSinex.numparam,
		                   &theSinex.ConstCode,
		                   &solcontents[0],
		                   &solcontents[1],
		                   &solcontents[2],
		                   &solcontents[3],
		                   &solcontents[4],
		                   &solcontents[5]);

		if (readcount < 15)
		{
			// error, not enough parameters
			BOOST_LOG_TRIVIAL(error) << "Not enough parameters on header line (expected min 15), got " << readcount << endl;
			return 3;
		}

		while (readcount < 21)
		{
			solcontents[readcount - 15] = ' ';
			readcount++;
		}

		solcontents[6] = '\0';

		theSinex.create_agc	= create_agc;
		theSinex.data_agc	= data_agc;
		theSinex.solcont	= solcontents;

		nearestYear(theSinex.filedate[0]);
		nearestYear(theSinex.solution_start_date[0]);
		nearestYear(theSinex.solution_end_date[0]);
	}

	// Only one line for header - no sort required
	return 0;
}

void sinex_update_header(
	const char* create_agc,
	int			create_date[3],
	const char*	data_agc,
	int			soln_start[3],
	int			soln_end[3],
	const char	obsCode,
	const char	constCode,
	const char	contents[7])
{
	newsnx_input_history_t siht;

	siht.code			= '+';
	siht.fmt			= theSinex.ver;
	siht.create_agency	= theSinex.create_agc;
	siht.data_agency	= theSinex.data_agc;
	siht.obs_tech		= theSinex.ObsCode;
	siht.constraint		= theSinex.ConstCode;
	siht.num_estimates	= theSinex.numparam;
	siht.contents		= theSinex.solcont;

	memcpy(siht.create_time,	theSinex.filedate,				sizeof(siht.create_time));
	memcpy(siht.start,			theSinex.solution_start_date,	sizeof(siht.start));
	memcpy(siht.stop,			theSinex.solution_end_date,		sizeof(siht.stop));

	theSinex.inputHistory.push_back(siht);

	theSinex.ver = 2.02;	// Fix this if the sinex format gets updated!

	if (strlen(data_agc) > 0)
		theSinex.data_agc = data_agc;
	else
		theSinex.data_agc = theSinex.create_agc;

	theSinex.create_agc	= create_agc;
	theSinex.solcont	= contents;


	memcpy(theSinex.filedate,				create_date,	sizeof(theSinex.filedate));
	memcpy(theSinex.solution_start_date,	soln_start,		sizeof(theSinex.solution_start_date));
	memcpy(theSinex.solution_end_date,		soln_end,		sizeof(theSinex.solution_end_date));

	if (obsCode		!= ' ')
		theSinex.ObsCode = obsCode;

	if (constCode 	== ' ')
		theSinex.ConstCode = constCode;

	theSinex.numparam = theSinex.kfState.x.rows()-1;
}

void write_snx_header(std::ofstream& out)
{
	char line[80];
	char c;
	int  i;

	memset(line, 0, 80);

	sprintf(line, "%%=SNX %4.2lf %3s %2.2d:%3.3d:%5.5d %3s %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %c %5d %c",
			theSinex.ver,
			theSinex.create_agc.c_str(),
			theSinex.filedate[0] % 100,
			theSinex.filedate[1],
			theSinex.filedate[2],
			theSinex.data_agc.c_str(),
			theSinex.solution_start_date[0] % 100,
			theSinex.solution_start_date[1],
			theSinex.solution_start_date[2],
			theSinex.solution_end_date[0] % 100,
			theSinex.solution_end_date[1],
			theSinex.solution_end_date[2],
			theSinex.ObsCode,
			theSinex.numparam,
			theSinex.ConstCode);

	i = 0;
	c = theSinex.solcont[0];

	while (c != ' ')
	{
		char s[3];

		sprintf(s, " %c", c);
		strcat(line, s);

		++i;

		if (i <= static_cast<int>(theSinex.solcont.length()))
			c = theSinex.solcont[i];
		else
			c = ' ';
	}

	out << line << endl;
}

int read_snx_reference(std::ifstream& in)
{
	const string closure("-FILE/REFERENCE");
	string s;
	newsnx_ref_t srt;

	std::getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error)
		<< "end of file reached reading " << closure.substr(1) << " block" << endl;

		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		srt.refline = s;
		theSinex.refstrings.push_back(srt);

		std::getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error)
			<< "end of file reached reading " << closure.substr(1) << " block" << endl;

			return 1;
		}
	}

	// no sort required for references
	return 0;
}


void write_as_comments(ofstream& out, list<string>& comments)
{
	for (auto& comment : comments)
	{
		string s = comment;

		// just make sure it starts with * as required by format
		s[0] = '*';

		out << s << endl;
	}
}

int write_snx_reference(ofstream& out)
{
	out << "+FILE/REFERENCE" << endl;

	for (auto& refString : theSinex.refstrings)
	{
		out << refString.refline << endl;
	}

	out << "-FILE/REFERENCE" << endl;

	return 0;
}

int read_snx_comment(ifstream& in)
{
	const string closure("-FILE/COMMENT");
	string s;
	newsnx_comment_t sct;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error)
		<< "end of file reached reading " << closure.substr(1) << " block" << endl;

		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		sct.cmtline = s;
		theSinex.commentstrings.push_back(sct);

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error)
			<< "end of file reached reading " << closure.substr(1) << " block" << endl;

			return 1;
		}
	}

	// no sort required for comment lines
	return 0;
}

int write_snx_comments(ofstream& out)
{
	out << "+FILE/COMMENT" << endl;

	for (auto& commentstring : theSinex.commentstrings)
	{
		out << commentstring.cmtline << endl;
	}

	out << "-FILE/COMMENT" << endl;

	return 0;
}

int read_snx_input_history(ifstream& in)
{
	const string closure("-INPUT/HISTORY");
	string s;
	newsnx_input_history_t siht;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error)
		<< "end of file reached reading " << closure.substr(1) << " block" << endl;

		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] != ' ')
		{
			// column 1 required to be blank by format
			// so treat as a comment for input history
			theSinex.historyComments.push_back(s);
		}
		else
		{
			// remaining characters indiciate properties of the history
			if (s.length() > 5)
			{
				const char* p = s.c_str();
				char create_agc[4];
				char data_agc[4];
				char solcontents[7];
				int  readcount;

				siht.code = s[1];

				readcount = sscanf(p + 6, "%4lf %3s %2d:%3d:%5d %3s %2d:%3d:%5d %2d:%3d:%5d %c %5d %c %c %c %c %c %c %c",
									&siht.fmt,
									create_agc,
									&siht.create_time[0],
									&siht.create_time[1],
									&siht.create_time[2],
									data_agc, &siht.start[0],
									&siht.start[1],
									&siht.start[2],
									&siht.stop[0],
									&siht.stop[1],
									&siht.stop[2],
									&siht.obs_tech,
									&siht.num_estimates,
									&siht.constraint,
									&solcontents[0],
									&solcontents[1],
									&solcontents[2],
									&solcontents[3],
									&solcontents[4],
									&solcontents[5]);

				if (readcount >= 15)
				{
					while (readcount < 21)
					{
						solcontents[readcount - 15] = ' ';
						readcount++;
					}

					solcontents[6] = '\0';

					siht.create_agency = create_agc;
					siht.data_agency = data_agc;
					siht.contents = solcontents;

					nearestYear(siht.create_time[0]);
					nearestYear(siht.start[0]);
					nearestYear(siht.stop[0]);

					theSinex.inputHistory.push_back(siht);
				}
				else
				{
					// add to comment line, user will spot it and fix the error
					theSinex.historyComments.push_back(s);
				}
			}
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error)
			<< "end of file reached reading " << closure.substr(1) << " block" << endl;

			return 1;
		}
	}

	// no sort required for history?
	return 0;
}

int write_snx_input_history(ofstream& out)
{
	write_as_comments(out, theSinex.historyComments);

	out << "+INPUT/HISTORY" << endl;

	for (auto it = theSinex.inputHistory.begin(); it != theSinex.inputHistory.end(); it++)
	{
		char line[80];
		newsnx_input_history_t siht = *it;
		int i = 0;
		char c;

		memset (line, 0, 80);

		sprintf(line, " %cSNX %4.2lf %3s %2.2d:%3.3d:%5.5d %3s %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %c %5d %c",
				siht.code,
				siht.fmt,
				siht.create_agency.c_str(),
				siht.create_time[0] % 100,
				siht.create_time[1],
				siht.create_time[2],
				siht.data_agency.c_str(),
				siht.start[0] % 100,
				siht.start[1],
				siht.start[2],
				siht.stop[0] % 100,
				siht.stop[1],
				siht.stop[2],
				siht.obs_tech,
				siht.num_estimates,
				siht.constraint);

		c = siht.contents[i];

		while (c != ' ')
		{
			char s[3];

			s[0] = ' ';
			s[1] = c;
			s[2] = '\0';

			strcat(line, s);
			i++;

			if (static_cast<int>(siht.contents.length()) >= i)
				c = siht.contents[i];
			else
				c = ' ';
		}

		out << line << endl;
	}

	out << "-INPUT/HISTORY" << endl;

	return 0;
}

int read_snx_input_files(ifstream& in)
{
	const string closure("-INPUT/FILES");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error)
		<< "end of file reached reading " << closure.substr(1) << " block" << endl;

		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] != ' ')
		{
			// column 1 required to be blank by format
			// so treat as a comment for input history
			theSinex.filesComments.push_back(s);
		}
		else
		{
			newsnx_input_file_t sif;
			char agency[4];
			const char* p	= s.c_str();
			sif.file		= s.substr(18, 29);
			sif.description	= s.substr(48, 32);

			int  readcount = sscanf(p + 1, "%3s %2d:%3d:%5d",
			                   agency, sif.epoch, sif.epoch + 1, sif.epoch + 2);

			if (readcount == 4)
			{
				sif.agency = agency;

				nearestYear(sif.epoch[0]);

				theSinex.inputFiles.push_back(sif);
			}
			else
			{
				// error on line. Add to comments? user can fix it if they write it out again?
				theSinex.filesComments.push_back(s);
			}

		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error)
			<< "end of file reached reading " << closure.substr(1) << " block" << endl;

			return 1;
		}
	}

	// no sort required for input files
	return 0;
}

int write_snx_input_files(ofstream& out)
{
	write_as_comments(out, theSinex.filesComments);

	out << "+INPUT/FILES" << endl;

	for (auto& inputFile : theSinex.inputFiles)
	{
		newsnx_input_file_t& sif = inputFile;

		char line[81];
		sprintf(line, " %3s %2.2d:%3.3d:%5.5d %-29s %s",
				sif.agency.c_str(),
				sif.epoch[0] % 100,
				sif.epoch[1],
				sif.epoch[2],
				sif.file.c_str(),
				sif.description.c_str());

		out << line << endl;
	}

	out << "-INPUT/FILES" << endl;

	return 0;
}

int read_snx_acknowledgements(ifstream& in)
{
	const string closure("-INPUT/ACKNOWLEDGEMENTS");
	const string closure2("-INPUT/ACKNOWLEDGMENTS");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error)
		<< "end of file reached reading " << closure.substr(1) << " block" << endl;

		return 1;
	}

	while 	(  strncmp(s.c_str(), closure.c_str(),	closure.length())
			&& strncmp(s.c_str(), closure2.c_str(),	closure2.length()))
	{
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_ack_t sat;

			sat.description	= s.substr(5);
			sat.agency		= s.substr(1, 3);

			theSinex.acknowledgements.push_back(sat);
		}
		else
		{
			// non blank in column 1. add to comment block
			theSinex.ackComments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error)
			<< "end of file reached reading " << closure.substr(1) << " block" << endl;

			return 1;
		}
	}

	// no sort required for acknowledgement lines
	return 0;
}

int write_snx_acknowledgements(ofstream& out)
{
	write_as_comments(out, theSinex.ackComments);

	out << "+INPUT/ACKNOWLEDGEMENTS" << endl;

	for (auto& acknowledgement : theSinex.acknowledgements)
	{
		newsnx_ack_t& ack = acknowledgement;

		char line[81];
		sprintf(line, " %3s %s", ack.agency.c_str(), ack.description.c_str());

		out << line << endl;
	}

	out << "-INPUT/ACKNOWLEDGEMENTS" << endl;

	return 0;
}

// compare by sitecode only. If left < right return value is true
static bool compare_siteids(const newsnx_siteid_t& left, const newsnx_siteid_t& right)
{
	return left.sitecode.compare(right.sitecode) < 0;
}

int read_snx_siteIds(ifstream& in)
{
	const string closure("-SITE/ID");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_siteid_t sst;

			sst.sitecode	= s.substr(1, 4);
			sst.ptcode		= s.substr(6, 2);
			sst.domes		= s.substr(9, 9);
			sst.typecode 	= s[19];
			sst.desc		= s.substr(21, 22);


			int    readcount = sscanf(p + 44, "%3d %2d %4lf %3d %2d %4lf %7lf",
								&sst.long_deg,
								&sst.long_min,
								&sst.long_sec,
								&sst.lat_deg,
								&sst.lat_min,
								&sst.lat_sec,
								&sst.height);

			if (readcount == 7)
			{
				theSinex.list_siteids.push_back(sst);
			}
			else
			{
				theSinex.siteIdcomments.push_back(s);
			}
		}
		else
		{
			theSinex.siteIdcomments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_siteids.sort(compare_siteids);
	return  0;
}

int write_snx_siteids(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.siteIdcomments);

	out << "+SITE/ID" << endl;

	for (auto& ssi : theSinex.list_siteids)
	{
		bool doit = false;

		char line[81];
		sprintf(line, " %4s %2s %9s %c %22s %3d %2d %4.1lf %3d %2d %4.1lf %7.1lf",
		        ssi.sitecode.c_str(),
		        ssi.ptcode.c_str(),
		        ssi.domes.c_str(),
		        ssi.typecode,
		        ssi.desc.c_str(),
		        ssi.long_deg,
		        ssi.long_min,
		        ssi.long_sec,
		        ssi.lat_deg,
		        ssi.lat_min,
		        ssi.lat_sec,
		        ssi.height);

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (ssi.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
			out << line << endl;
	}

	out << "-SITE/ID" << endl;

	return 0;
}

// compare by the 2 station ids only. if left < right, return value is true.
static bool compare_sitedata(const newsnx_sitedata_t& left, const newsnx_sitedata_t& right)
{
	int sitec = left.site.compare(right.site);

	if (sitec == 0)
		sitec = left.sitecode.compare(right.sitecode);

	return (sitec < 0);
}

int read_snx_siteData(ifstream& in)
{
	string closure("-SITE/DATA");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_sitedata_t sst;

			sst.site		= s.substr(1, 4);
			sst.station_pt	= s.substr(6, 2);
			sst.soln_id		= s.substr(9, 4);
			sst.sitecode	= s.substr(14, 4);
			sst.site_pt		= s.substr(18, 2);
			sst.sitesoln	= s.substr(20, 4);

			sst.obscode		= s[24];
			int    start[3]; // yr:doy:sod
			int    end[3]; //yr:doy:sod
			int    create[3]; //yr:doy:sod
			char   agency[3];

			int    readcount;

			readcount = sscanf(p + 28, "%2d:%3d:%5d %2d:%3d:%5d %3s %2d:%3d:%5d",
			                   start, start + 1, start + 2, end, end + 1, end + 2, agency,
			                   create, create + 1, create + 2);

			if (readcount == 10)
			{
				sst.agency = agency;

				for (int i = 0; i < 3; i++)
				{
					sst.start[i]	= start[i];
					sst.stop[i]		= end[i];
					sst.create[i]	= create[i];
				}

				// see comment at top of file
				if 	(  sst.start[0] != 0
					|| sst.start[1] != 0
					|| sst.start[2] != 0)
				{
					nearestYear(sst.start[0]);
				}

				if 	(  sst.stop[0] != 0
					|| sst.stop[1] != 0
					|| sst.stop[2] != 0)
				{
					nearestYear(sst.stop[0]);
				}

				nearestYear(sst.create[0]);

				theSinex.list_sitedata.push_back(sst);
			}
			else
			{
				// treat line as a comment. User can fix it if they write it out
				theSinex.siteDatacomments.push_back(s);
			}
		}
		else
		{
			// treat as a comment
			theSinex.siteDatacomments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_sitedata.sort(compare_sitedata);
	return 0;
}

int write_snx_sitedata(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.siteDatacomments);

	out << "+SITE/DATA" << endl;

	for (auto& sitedata : theSinex.list_sitedata)
	{
		newsnx_sitedata_t& ssd = sitedata;
		bool doit = false;

		char line[81];
		sprintf(line, " %4s %2s %4s %4s %2s %4s %c %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %3s %2.2d:%3.3d:%5.5d",
		        ssd.site.c_str(),
		        ssd.station_pt.c_str(),
		        ssd.soln_id.c_str(),
		        ssd.sitecode.c_str(),
		        ssd.site_pt.c_str(),
		        ssd.sitesoln.c_str(),
		        ssd.obscode,
		        ssd.start[0] % 100,
		        ssd.start[1],
		        ssd.start[2],
		        ssd.stop[0] % 100,
		        ssd.stop[1],
		        ssd.stop[2],
		        ssd.agency.c_str(),
		        ssd.create[0] % 100,
		        ssd.create[1],
		        ssd.create[2]);

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (ssd.site.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
			out << line << endl;
	}

	out << "-SITE/DATA" << endl;
	return 0;
}

// compare by site code and start time. if left < right the return value is true
static bool compare_receivers(const newsnx_receiver_t& left, const newsnx_receiver_t& right)
{
	// Always upper case so no need to case compare
	int comp = left.sitecode.compare(right.sitecode);

	int i = 0;

	while (comp == 0 && i < 3)
	{
		comp = left.recstart[i] - right.recstart[i];
		i++;
	}

	return (comp < 0);
}

int read_snx_receivers(ifstream& in)
{
	string closure("-SITE/RECEIVER");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_receiver_t srt;

			srt.sitecode	= s.substr(1, 4);
			srt.ptcode		= s.substr(6, 2);
			srt.solnid		= s.substr(9, 4);
			srt.typecode	= s[14];
			srt.rectype		= s.substr(42, 20);
			srt.recsn		= s.substr(63, 5);
			srt.recfirm 	= trim(s.substr(69, 11));
			int start[3];
			int stop[3];
			int readcount;

			readcount = sscanf(p + 16, "%2d:%3d:%5d %2d:%3d:%5d",
			                   &srt.recstart[0],
			                   &srt.recstart[1],
			                   &srt.recstart[2],
			                   &srt.recend[0],
			                   &srt.recend[1],
			                   &srt.recend[2]);

			if (readcount == 6)
			{
				// see comment at top of file
				if 	(  srt.recstart[0] != 0
					|| srt.recstart[1] != 0
					|| srt.recstart[2] != 0)
				{
					nearestYear(srt.recstart[0]);
				}

				if (srt.recend[0] != 0 || srt.recend[1] != 0 || srt.recend[2] != 0)
				{
					nearestYear(srt.recend[0]);
				}

				theSinex.list_receivers.push_back(srt);
			}
			else
			{
				// did not read properly. Treat as a comment
				theSinex.receivercomments.push_back(s);
			}
		}
		else
		{
			// treat as comment
			theSinex.receivercomments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_receivers.sort(compare_receivers);
	return 0;
}

int write_snx_receivers(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.receivercomments);

	out << "+SITE/RECEIVER" << endl;

	for (auto& receiver : theSinex.list_receivers)
	{
		newsnx_receiver_t& srt = receiver;
		bool doit = false;

		char line[81];
		sprintf(line, " %4s %2s %4s %c %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %20s %5s %s",
					srt.sitecode.c_str(),
					srt.ptcode.c_str(),
					srt.solnid.c_str(),
					srt.typecode,
					srt.recstart[0] % 100,
					srt.recstart[1],
					srt.recstart[2],
					srt.recend[0] % 100,
					srt.recend[1],
					srt.recend[2],
					srt.rectype.c_str(),
					srt.recsn.c_str(),
					srt.recfirm.c_str());

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (srt.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
			out << line << endl;
	}

	out << "-SITE/RECEIVER" << endl;
	return 0;
}

// compare by sitecode and start time. if left < right the return value is true
static bool compare_antennas(const newsnx_antenna_t& left, newsnx_antenna_t& right)
{
	int comp = left.sitecode.compare(right.sitecode);

	int i = 0;

	while (!comp && i < 3)
	{
		comp = left.antstart[i] - right.antstart[i];
		i++;
	}

	return (comp < 0);
}

int read_snx_antennas(ifstream& in)
{
	string closure("-SITE/ANTENNA");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_antenna_t sat;

			sat.sitecode	= s.substr(1, 4);
			sat.ptcode		= s.substr(6, 2);
			sat.solnnum		= s.substr(9, 4);
			sat.typecode	= s[14];
			sat.anttype		= s.substr(42, 20);
			sat.antsn		= trim(s.substr(63, 5));

			int    readcount = sscanf(p + 16, "%2d:%3d:%5d %2d:%3d:%5d",
					                   &sat.antstart[0],
					                   &sat.antstart[1],
					                   &sat.antstart[2],
					                   &sat.antend[0],
					                   &sat.antend[1],
					                   &sat.antend[2]);

			if (readcount == 6)
			{
				// see comment at top of file
				if 	(  sat.antstart[0] != 0
					|| sat.antstart[1] != 0
					|| sat.antstart[2] != 0)
				{
					nearestYear(sat.antstart[0]);
				}

				if 	(  sat.antend[0] != 0
					|| sat.antend[1] != 0
					|| sat.antend[2] != 0)
				{
					nearestYear(sat.antend[0]);
				}

				theSinex.list_antennas.push_back(sat);
			}
			else
			{
				// treat as comment.
				theSinex.antennacomments.push_back(s);
			}
		}
		else
		{
			theSinex.antennacomments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_antennas.sort(compare_antennas);
	return 0;
}

int write_snx_antennas(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.antennacomments);

	out << "+SITE/ANTENNA" << endl;

	for (auto& antenna : theSinex.list_antennas)
	{
		newsnx_antenna_t& sat = antenna;
		bool doit = false;

		char line[81];
		sprintf(line, " %4s %2s %4s %c %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %20s %s",
					sat.sitecode.c_str(),
					sat.ptcode.c_str(),
					sat.solnnum.c_str(),
					sat.typecode,
					sat.antstart[0] % 100,
					sat.antstart[1],
					sat.antstart[2],
					sat.antend[0] % 100,
					sat.antend[1],
					sat.antend[2],
					sat.anttype.c_str(),
					sat.antsn.c_str());

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (sat.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
			out << line << endl;
	}

	out << "-SITE/ANTENNA" << endl;
	return 0;
}

// compare by antenna type and serial number. return true if left < right
static bool compare_gps_pc(newsnx_gps_phase_center_t& left, newsnx_gps_phase_center_t& right)
{
	int comp = left.antname.compare(right.antname);

	if (!comp)
		comp = left.serialno.compare(right.serialno);

	return (comp < 0);
}

int read_gps_phase_center(ifstream& in)
{
	string closure("-SITE/GPS_PHASE_CENTER");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_gps_phase_center_t sgpct;

			sgpct.antname	= s.substr(1, 20);
			sgpct.serialno	= s.substr(22, 5);
			sgpct.calib		= s.substr(70, 10);

			int readcount = sscanf(p + 28, "%6lf %6lf %6lf %6lf %6lf %6lf",
			                   			&sgpct.L1[0],
			                   			&sgpct.L1[1],
			                   			&sgpct.L1[2],
			                   			&sgpct.L2[0],
			                   			&sgpct.L2[1],
			                   			&sgpct.L2[2]);

			if (readcount == 6)
			{
				theSinex.list_gps_pcs.push_back(sgpct);
				comment = false;
			}
		}

		if (comment)
		{
			// treat as comment line
			theSinex.gps_pc_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_gps_pcs.sort(compare_gps_pc);
	return 0;
}

void truncateSomething(char* buf)
{
	if 	(  strlen(buf) == 7
		&& buf[1] == '0'
		&& buf[0] == '-')
	{
		for (int j = 2; j < 8; j++)
		{
			buf[j - 1] = buf[j];
		}
	}
}


int write_snx_gps_pcs(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.gps_pc_comments);

	out << "+SITE/GPS_PHASE_CENTER" << endl;

	for (auto& gps_pc : theSinex.list_gps_pcs)
	{
		newsnx_gps_phase_center_t& sgt = gps_pc;
		char buf[8];
		bool doit = false;

		char line[81];

		sprintf(line, " %20s %5s ",
		        sgt.antname.c_str(), sgt.serialno.c_str());

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L1[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L2[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		strcat(line, sgt.calib.c_str());

		if (pstns == NULL)
		{
			doit = true;
		}
		else
		{
			for (auto& stn : *pstns)
			{
				if (sgt.antname.compare(stn.anttype) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
		{
			out << line << endl;
		}
	}

	out << "-SITE/GPS_PHASE_CENTER" << endl;
	return 0;
}

// compare by antenna type and serial number. return true0 if left < right
static bool compare_gal_pc(newsnx_gal_phase_center_t& left, newsnx_gal_phase_center_t& right)
{
	int comp = left.antname.compare(right.antname);

	if (!comp)
		comp = left.serialno.compare(right.serialno);

	return (comp < 0);
}

// Gallileo phase centers take three line each!
int read_gal_phase_center(ifstream& in)
{
	string closure("-SITE/GAL_PHASE_CENTER");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_gal_phase_center_t sgpct;

			sgpct.antname	= s.substr(1, 20);
			sgpct.serialno	= s.substr(22, 5);
			sgpct.calib		= s.substr(69, 10);


			int readcount = sscanf(p + 28, "%6lf %6lf %6lf %6lf %6lf %6lf",
			                   &sgpct.L1[0],
			                   &sgpct.L1[1],
			                   &sgpct.L1[2],
			                   &sgpct.L5[0],
			                   &sgpct.L5[1],
			                   &sgpct.L5[2]);

			if (readcount == 6)
			{
				string t, u;
				int readcount2;

				getline(in, t);

				if (in.eof())
				{
					BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
					return 1;
				}

				// test both for closure line
				if (!strncmp(t.c_str(), closure.c_str(), closure.length()))
				{
					// put s into comment block ...
					theSinex.gal_pc_comments.push_back(s);
					goto dosort;
				}

				getline(in, u);

				if (in.eof())
				{
					BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
					return 1;
				}

				if (!strncmp(u.c_str(), closure.c_str(), closure.length()))
				{
					// put s AND t into comment block ...
					theSinex.gal_pc_comments.push_back(s);
					theSinex.gal_pc_comments.push_back(t);
					goto dosort;
				}

				// Do we need to check the antenna name and serial each time? I am going to assume not
				readcount = sscanf(t.c_str() + 28, "%6lf %6lf %6lf %6lf %6lf %6lf",
									&sgpct.L6[0],
									&sgpct.L6[1],
									&sgpct.L6[2],
									&sgpct.L7[0],
									&sgpct.L7[1],
									&sgpct.L7[2]);
				readcount2 = sscanf(u.c_str() + 28, "%6lf %6lf %6lf",
									&sgpct.L8[0],
									&sgpct.L8[1],
									&sgpct.L8[2]);

				if (readcount == 6 && readcount2 == 3)
				{
					theSinex.list_gal_pcs.push_back(sgpct);
				}
				else
				{
					// push all 3 lines to the comment block
					theSinex.gal_pc_comments.push_back(s);
					theSinex.gal_pc_comments.push_back(t);
					theSinex.gal_pc_comments.push_back(u);
				}
			}
			else
			{
				theSinex.gal_pc_comments.push_back(s);
				// read another two lines to sync back up, after testing they are not the closure line
				getline(in, s);

				if (in.eof())
				{
					BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
					return 1;
				}

				if (!strncmp(s.c_str(), closure.c_str(), closure.length()))
					goto dosort;

				theSinex.gal_pc_comments.push_back(s);
				getline(in, s);

				if (in.eof())
				{
					BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
					return 1;
				}

				if (!strncmp(s.c_str(), closure.c_str(), closure.length()))
					goto dosort;

				theSinex.gal_pc_comments.push_back(s);
			}
		}
		else
		{
			theSinex.gal_pc_comments.push_back(s);
			// read another two lines to sync back up, after testing they are not the closure line
			getline(in, s);

			if (in.eof())
			{
				BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
				return 1;
			}

			if (!strncmp(s.c_str(), closure.c_str(), closure.length()))
				goto dosort;

			theSinex.gal_pc_comments.push_back(s);
			getline(in, s);

			if (in.eof())
			{
				BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
				return 1;
			}

			if (!strncmp(s.c_str(), closure.c_str(), closure.length()))
				goto dosort;

			theSinex.gal_pc_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

dosort:
	theSinex.list_gal_pcs.sort(compare_gal_pc);
	return 0;
}

int write_snx_gal_pcs(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.gal_pc_comments);

	out << "+SITE/GAL_PHASE_CENTER" << endl;

	for (auto& gal_pc : theSinex.list_gal_pcs)
	{
		newsnx_gal_phase_center_t& sgt = gal_pc;
		char buf[8];
		bool doit = false;

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (sgt.antname.compare(stn.anttype) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (!doit)
			continue;

		char line[81];

		sprintf(line, " %20s %5s ",
		        sgt.antname.c_str(), sgt.serialno.c_str());

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L1[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L5[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		strcat(line, sgt.calib.c_str());
		out << line << endl;

		sprintf(line, " %20s %5s ",
		        sgt.antname.c_str(),
		        sgt.serialno.c_str());

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L6[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L7[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		strcat(line, sgt.calib.c_str());
		out << line << endl;

		sprintf(line, " %20s %5s ",
		        sgt.antname.c_str(), sgt.serialno.c_str());

		for (int i = 0; i < 3; i++)
		{
			sprintf(buf, "%6.4lf", sgt.L8[i]);
			truncateSomething(buf);
			strcat(line, buf);
			strcat(line, " ");
		}

		strcat(line, "                    ");
		strcat(line, sgt.calib.c_str());
		out << line << endl;
	}

	out << "-SITE/GAL_PHASE_CENTER" << endl;
	return 0;
}

// return true if left < right
static bool compare_eccentricity(newsnx_site_ecc_t& left, newsnx_site_ecc_t& right)
{
	int comp = left.sitecode.compare(right.sitecode);
	int i = 0;

	while (!comp && i < 3)
	{
		comp = left.eccstart[i] - right.eccstart[i];
		i++;
	}

	return (comp < 0);
}

int read_snx_site_eccentricity(ifstream& in)
{
	string closure("-SITE/ECCENTRICITY");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_site_ecc_t sset;

			sset.sitecode 	= s.substr(1, 4);
			sset.ptcode		= s.substr(6, 2);
			sset.solnnum	= s.substr(9, 4);
			sset.typecode	= s[14];
			sset.eccrs		= s.substr(42, 3);
			char   junk[3];

			int readcount = sscanf(p + 16, "%2d:%3d:%5d %2d:%3d:%5d %3s %8lf %8lf %8lf",
			                   &sset.eccstart[0],
			                   &sset.eccstart[1],
			                   &sset.eccstart[2],
			                   &sset.eccend[0],
			                   &sset.eccend[1],
			                   &sset.eccend[2],
			                   junk,
			                   &sset.ecc[0],
			                   &sset.ecc[1],
			                   &sset.ecc[2]);

			if (readcount == 10)
			{
				// see comment at top of file
				if (sset.eccstart[0] != 0 || sset.eccstart[1] != 0 || sset.eccstart[2] != 0)
				{
					nearestYear(sset.eccstart[0]);
				}

				if (sset.eccend[0] != 0 || sset.eccend[1] != 0 || sset.eccend[2] != 0)
				{
					nearestYear(sset.eccend[0]);
				}

				theSinex.list_site_eccs.push_back(sset);
				comment = false;
			}
		}

		if (comment)
		{
			theSinex.site_ecc_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_site_eccs.sort(compare_eccentricity);
	return 0;
}

int write_snx_site_eccs(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.site_ecc_comments);

	out << "+SITE/ECCENTRICITY" << endl;

	for (auto& set : theSinex.list_site_eccs)
	{
		bool doit = false;

		char line[81];
		sprintf(line, " %4s %2s %4s %c %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %3s %8.4lf %8.4lf %8.4lf",
					set.sitecode.c_str(),
					set.ptcode.c_str(),
					set.solnnum.c_str(),
					set.typecode,
					set.eccstart[0] % 100,
					set.eccstart[1],
					set.eccstart[2],
					set.eccend[0] % 100,
					set.eccend[1],
					set.eccend[2],
					set.eccrs.c_str(),
					set.ecc[0],
					set.ecc[1],
					set.ecc[2]);

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (set.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
			out << line << endl;
	}

	out << "-SITE/ECCENTRICITY" << endl;
	return 0;
}

// return true if left < right
static bool compare_site_epochs(newsnx_solepoch_t& left, newsnx_solepoch_t& right)
{
	int comp = left.sitecode.compare(right.sitecode);
	int i = 0;

	while (!comp && i < 3)
	{
		comp = left.start[i] - right.start[i];
		i++;
	}

	return (comp < 0);
}

int read_snx_epochs(ifstream& in, bool hasbias)
{
	string closure("-SOLUTION/EPOCHS");
	string s;

	if (hasbias)
		closure = "-BIAS/EPOCHS";

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		const char* p = s.c_str();

		if (p[0] == ' ')
		{
			newsnx_solepoch_t sst;

			sst.sitecode	= s.substr(1, 4);
			sst.ptcode		= s.substr(6, 2);
			sst.solnnum		= s.substr(9, 4);
			sst.typecode	= s[14];

			int readcount = sscanf(p + 16, "%2d:%3d:%5d %2d:%3d:%5d %2d:%3d:%5d",
					                   &sst.start[0],
					                   &sst.start[1],
					                   &sst.start[2],
					                   &sst.end[0],
					                   &sst.end[1],
					                   &sst.end[2],
					                   &sst.mean[0],
					                   &sst.mean[1],
					                   &sst.mean[2]);

			if (readcount == 9)
			{
				// see comment at top of file
				if 	(  sst.start[0] != 0
					|| sst.start[1] != 0
					|| sst.start[2] != 0)
				{
					nearestYear(sst.start[0]);
				}

				if 	(  sst.end[0] != 0
					|| sst.end[1] != 0
					|| sst.end[2] != 0)
				{
					nearestYear(sst.end[0]);
				}

				if 	(  sst.mean[0] != 0
					|| sst.mean[1] != 0
					|| sst.mean[2] != 0)
				{
					nearestYear(sst.mean[0]);
				}

				comment = false;
			}
		}

		if (comment)
		{
			theSinex.epochcomments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_solepochs.sort(compare_site_epochs);
	return 0;
}

int write_snx_epochs(ofstream& out, std::list<newsnx_stn_snx_t>* pstns)
{
	write_as_comments(out, theSinex.epochcomments);

	if (theSinex.epochs_have_bias)
		out << "+BIAS/EPOCHS" << endl;
	else
		out << "+SOLUTION/EPOCHS" << endl;

	for (auto& solepoch : theSinex.list_solepochs)
	{
		newsnx_solepoch_t& sst = solepoch;
		bool doit = false;
		char line[81];


		sprintf(line, " %4s %2s %4s %c %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d",
					sst.sitecode.c_str(),
					sst.ptcode.c_str(),
					sst.solnnum.c_str(),
					sst.typecode,
					sst.start[0] % 100,
					sst.start[1],
					sst.start[2],
					sst.end[0] % 100,
					sst.end[1],
					sst.end[2],
					sst.mean[0] % 100,
					sst.mean[1],
					sst.mean[2]);

		if (pstns == NULL)
			doit = true;
		else
		{
			for (auto& stn : *pstns)
			{
				if (sst.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (doit)
			out << line << endl;
	}

	if (theSinex.epochs_have_bias)
		out << "-BIAS/EPOCHS" << endl;
	else
		out << "-SOLUTION/EPOCHS" << endl;

	return 0;
}

int read_snx_statistics(ifstream& in)
{
	string closure("-SOLUTION/STATISTICS");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		if (s[0] == ' ')
		{
			string  stat = s.substr(1, 30);
			double  dval;
			int		ival;
			short	etype;

			if (s.substr(33).find(".") != string::npos)
			{
				dval = (double)atof(s.c_str() + 33);
				etype = 1;
			}
			else
			{
				ival = atoi(s.c_str() + 33);
				etype = 0;
			}

			newsnx_solstatistic_t sst;
			sst.name = trim(stat);
			sst.etype = etype;

			if (etype == 0)
				sst.value.ival = ival;

			if (etype == 1)
				sst.value.dval = dval;

			theSinex.list_statistics.push_back(sst);
			comment = false;
		}

		if (comment)
		{
			theSinex.statistics_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	// no sort required for statistics
	return 0;
}

int write_snx_statistics(ofstream& out)
{
	write_as_comments(out, theSinex.statistics_comments);

	out << "+SOLUTION/STATISTICS" << endl;

	for (auto& statistic : theSinex.list_statistics)
	{
		char line[81];

		if (statistic.etype == 0) // int
			sprintf(line, " %-30s %22d", statistic.name.c_str(), statistic.value.ival);

		if (statistic.etype == 1) // double
			sprintf(line, " %-30s %22.15lf", statistic.name.c_str(), statistic.value.dval);

		out << line << endl;
	}

	out << "-SOLUTION/STATISTICS" << endl;

	return 0;
}

// return true if left is less than right
static bool compare_estimates(newsnx_solestimate_t& left, newsnx_solestimate_t& right)
{
	int comp = left.sitecode.compare(right.sitecode);

	if (!comp)
	{
		// compare first on type, then on epoch
		if (left.type.compare(right.type) == 0)
		{
			comp = time_compare(left.refepoch, right.refepoch);
		}
		else
		{
			int ltype = 0;
			int rtype = 0;
			string s = trim(left.type);

			try
			{
				ltype = E_Estimate::_from_string(s.c_str());
			}
			catch (...)			{			}

			s = trim(right.type);

			try
			{
				rtype = E_Estimate::_from_string(s.c_str());
			}
			catch (...)			{			}

			comp = ltype - rtype;
		}
	}

	if (!comp)
		comp = left.index - right.index;

	return (comp < 0);
}

int read_snx_estimates(ifstream& in)
{
	string closure("-SOLUTION/ESTIMATE");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		if (s[0] == ' ')
		{
			newsnx_solestimate_t sst;

			sst.type 		= s.substr(7,	6);
			sst.sitecode	= s.substr(14,	4);
			sst.ptcode 		= s.substr(19,	2);
			sst.solnnum 	= s.substr(22,	4);

			sst.index		= atoi(s.substr(1, 5).c_str());

			char   unit[5];

			int    readcount = sscanf(s.c_str() + 27, "%2d:%3d:%5d %4s %c %21lf %11lf",
			                   &sst.refepoch[0],
			                   &sst.refepoch[1],
			                   &sst.refepoch[2],
			                   unit,
			                   &sst.constraint,
			                   &sst.estimate,
			                   &sst.stddev);

			if (readcount == 7)
			{
				sst.unit 		= unit;

				// see comment at top of file
				if 	( sst.refepoch[0] != 0
					||sst.refepoch[1] != 0
					||sst.refepoch[2] != 0)
				{
					nearestYear(sst.refepoch[0]);
				}

				theSinex.estimates_map[sst.index] = sst;
				comment = false;
			}
		}

		if (comment)
		{
			theSinex.estimate_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	return 0;
}

int write_snx_estimates_from_filter(
	ofstream& out)
{
	out << "+SOLUTION/ESTIMATE" << endl;
	write_as_comments(out, theSinex.estimate_comments);

	for (auto& [key, index] : theSinex.kfState.kfIndexMap)
	{
		if (key.type != KF::REC_POS)
		{
			continue;
		}

		string type;
		if		(key.num == 0) type = "STAX";
		else if	(key.num == 1) type = "STAY";
		else if	(key.num == 2) type = "STAZ";

		string ptcode;
		for (auto& siteid : theSinex.list_siteids)
		{
			if (siteid.sitecode == key.str)
			{
				ptcode = siteid.ptcode;
			}
		}

		char line[81];
		sprintf(line, " %5d %6s %4s %2s %4d %2.2d:%3.3d:%5.5d %-4s %c %21.14le %11.5le",
		        index,
		        type.c_str(),
		        key.str.c_str(),
		        ptcode.c_str(),
		        1,
		        theSinex.solution_end_date[0] % 100,
		        theSinex.solution_end_date[1],
		        theSinex.solution_end_date[2],
		        "m",
		        ' ',
						theSinex.kfState.x(index),
		        sqrt(	theSinex.kfState.P(index,index)));

		out << line << endl;
	}

	out << "-SOLUTION/ESTIMATE" << endl;

	return 0;
}

int write_snx_estimates(
	ofstream& out,
	std::list<newsnx_stn_snx_t>* pstns = NULL)
{
	write_as_comments(out, theSinex.estimate_comments);

	out << "+SOLUTION/ESTIMATE" << endl;

	for (auto& [index, sst] : theSinex.estimates_map)
	{
		bool doit = (pstns == NULL);

		if (pstns != NULL)
		{
			for (auto& stn : *pstns)
			{
				if (sst.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (!doit)
			continue;

		char line[81];

		sprintf(line, " %5d %6s %4s %2s %4s %2.2d:%3.3d:%5.5d %-4s %c %21.14le %11.5le",
		        sst.index,
		        sst.type.c_str(),
		        sst.sitecode.c_str(),
		        sst.ptcode.c_str(),
		        sst.solnnum.c_str(),
		        sst.refepoch[0] % 100,
		        sst.refepoch[1],
		        sst.refepoch[2],
		        sst.unit.c_str(),
		        sst.constraint,
		        sst.estimate,
		        sst.stddev);

		out << line << endl;
	}

	out << "-SOLUTION/ESTIMATE" << endl;

	return 0;
}

// return true if left is less than right
static bool compare_apriori(newsnx_solapriori_t& left, newsnx_solapriori_t& right)
{
	int comp = left.sitecode.compare(right.sitecode);

	if (!comp)
	{
		// compare first on type, then on epoch
		if (left.param_type.compare(right.param_type) == 0)
		{
			comp = time_compare(left.epoch, right.epoch);
		}
		else
		{
			int ltype = 0;
			int rtype = 0;
			string s = trim(left.param_type);

			try
			{
				ltype = E_Estimate::_from_string(s.c_str());
			}
			catch (...)			{			}

			s = trim(right.param_type);

			try
			{
				rtype = E_Estimate::_from_string(s.c_str());
			}
			catch (...)			{			}

			comp = ltype - rtype;
		}
	}

	if (!comp)
		comp = left.idx - right.idx;

	return (comp < 0);
}

int read_snx_apriori(ifstream& in)
{
	string closure("-SOLUTION/APRIORI");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		bool comment = true;
		if (s[0] == ' ')
		{
			newsnx_solapriori_t sst = {};

			string index	= s.substr(1, 5);
			sst.idx			= atoi(index.c_str());
			sst.param_type	= s.substr(7, 6);
			sst.sitecode	= s.substr(14, 4);
			sst.ptcode		= s.substr(19, 2);
			sst.solnnum		= s.substr(22, 4);

			char   unit[5];

			unit[4] = '\0';

			int    readcount = sscanf(s.c_str() + 27, "%2d:%3d:%5d %4s %c %21lf %11lf",
					                   &sst.epoch[0],
					                   &sst.epoch[1],
					                   &sst.epoch[2],
					                   unit,
					                   &sst.constraint,
					                   &sst.param,
					                   &sst.stddev);

			if (readcount == 7)
			{
				sst.unit		= unit;

				// see comment at top of file
				if 	( sst.epoch[0] != 0
					||sst.epoch[1] != 0
					||sst.epoch[2] != 0)
				{
					nearestYear(sst.epoch[0]);
				}

				theSinex.apriori_map[sst.idx] = sst;
				comment = false;
			}
		}

		if (comment)
		{
			theSinex.apriori_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	return 0;
}

int write_snx_apriori(ofstream& out, std::list<newsnx_stn_snx_t>* pstns = NULL)
{
	write_as_comments(out, theSinex.apriori_comments);

	out << "+SOLUTION/APRIORI" << endl;

	for (auto& [index, apriori] : theSinex.apriori_map)
	{
		newsnx_solapriori_t& sst = apriori;
		bool doit = (pstns == NULL);

		if (pstns != NULL)
		{
			for (auto& stn : *pstns)
			{
				if (sst.sitecode.compare(stn.sitecode) == 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (!doit)
			continue;

		char line[81];

		sprintf(line, " %5d %6s %4s %2s %4s %2.2d:%3.3d:%5.5d %-4s %c %21.14le %11.5le",
		        sst.idx,
		        sst.param_type.c_str(),
		        sst.sitecode.c_str(),
		        sst.ptcode.c_str(),
		        sst.solnnum.c_str(),
		        sst.epoch[0] % 100,
		        sst.epoch[1],
		        sst.epoch[2],
		        sst.unit.c_str(),
		        sst.constraint,
		        sst.param,
		        sst.stddev);

		out << line << endl;
	}

	out << "-SOLUTION/APRIORI" << endl;

	return 0;
}

// return true if left is less than right
static bool compare_normals(newsnx_solneq_t& left, newsnx_solneq_t& right)
{
	int comp = left.site.compare(right.site);

	if (!comp)
	{
		// compare first on type, then on epoch
		if (left.ptype.compare(right.ptype) == 0)
		{
			comp = time_compare(left.epoch, right.epoch);
		}
		else
		{
			int ltype = 0;
			int rtype = 0;
			string s = trim(left.ptype);

			try
			{
				ltype = E_Estimate::_from_string(s.c_str());
			}
			catch (...)			{			}

			s = trim(right.ptype);

			try
			{
				rtype = E_Estimate::_from_string(s.c_str());
			}
			catch (...)			{			}


			comp = ltype - rtype;
		}
	}

	if (!comp)
		comp = left.param - right.param;

	return (comp < 0);
}

int read_snx_normals(ifstream& in)
{
	string closure("-SOLUTION/NORMAL_EQUATION_VECTOR");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_solneq_t sst;

			string parmnum	= s.substr(2, 5);
			sst.param		= atoi(parmnum.c_str());
			sst.ptype		= s.substr(7, 6);
			sst.site		= s.substr(14, 4);
			sst.pt			= s.substr(19, 2);
			sst.solnnum		= s.substr(22, 4);
			char   unit[5];

			unit[4] = '\0';

			int    readcount = sscanf(s.c_str() + 27, "%2d:%3d:%5d %4s %c %21lf",
			                   	&sst.epoch[0],
								&sst.epoch[1],
								&sst.epoch[2],
								unit,
								&sst.constraint,
								&sst.normal);

			if (readcount == 6)
			{
				sst.unit = unit;

				// see comment at top of file
				if (sst.epoch[0] != 0 || sst.epoch[1] != 0 || sst.epoch[2] != 0)
				{
					nearestYear(sst.epoch[0]);
				}

				theSinex.list_normal_eqns.push_back(sst);
			}
			else
			{
				theSinex.normal_eqns_comments.push_back(s);
			}
		}
		else
		{
			theSinex.normal_eqns_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_normal_eqns.sort(compare_normals);
	return 0;
}

int write_snx_normal(ofstream& out, std::list<newsnx_stn_snx_t>* pstns = NULL)
{
	write_as_comments(out, theSinex.normal_eqns_comments);

	out << "+SOLUTION/NORMAL_EQUATION_VECTOR" << endl;

	for (auto& sst : theSinex.list_normal_eqns)
	{
		bool doit = (pstns == NULL);

		if (pstns != NULL)
		{
			for (auto& stn : *pstns)
			{
				if (sst.site.compare(stn.sitecode) != 0)
				{
					doit = true;
					break;
				}
			}
		}

		if (!doit)
			continue;

		char line[81];

		sprintf(line, " %5d %6s %4s %2s %4s %2.2d:%3.3d:%5.5d %-4s %c %21.15lf",
				sst.param,
				sst.ptype.c_str(),
				sst.site.c_str(),
				sst.pt.c_str(),
				sst.solnnum.c_str(),
				sst.epoch[0] % 100,
				sst.epoch[1],
				sst.epoch[2],
				sst.unit.c_str(),
				sst.constraint,
				sst.normal);

		out << line << endl;
	}

	out << "-SOLUTION/NORMAL_EQUATION_VECTOR" << endl;

	return 0;
}

// return true if left is less than right. Just use indices of row and col
// for the comparison
int compare_matrix_entries(newsnx_solmatrix_t& left, newsnx_solmatrix_t& right)
{
	int comp;

	if (left.row == right.row)
		comp = left.col - right.col;
	else
		comp = left.row - right.row;

	return (comp < 0);
}

int read_snx_matrix(ifstream& in, matrix_type type, matrix_value value, char L_or_U)
{
	string 	closure;
	string 	s;
	int    	maxrow = 0, maxcol = 0;
	bool	doit = true;

	// we cannot merge the different triangles .. too hard
	if (theSinex.tri[type][value] != ' ' && theSinex.tri[type][value] != L_or_U)
	{
		BOOST_LOG_TRIVIAL (info) << "Matrix already read from another file, but other triangle. Cannot merge" << endl;
		doit = false;
	}

	theSinex.tri[type][value] = L_or_U;

	switch (type)
	{
		case ESTIMATE:
			closure = "-SOLUTION/MATRIX_ESTIMATE";
			break;

		case APRIORI:
			closure = "-SOLUTION/MATRIX_APRIORI";
			break;

		case NORMAL_EQN:

			// normal equations matrix is only ever INFO ...
			if (value != INFORMATION)
			{
				BOOST_LOG_TRIVIAL (error) << "incorrect argument for NORMAL_EQUATION_MATRIX" << endl;
				return 2;
			}

			closure = "-SOLUTION/NORMAL_EQUATION_MATRIX";
			break;

		case MAX_MATRIX_TYPE:
			// error! do nothing
			return 3;
			break;
	}

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()) != 0)
	{
		if (doit)
		{
			if (s[0] == ' ')
			{
				newsnx_solmatrix_t smt;

				int readcount = sscanf(s.c_str(), " %5d %5d %21lf %21lf %21lf",
				                   &smt.row,
				                   &smt.col,
				                   &smt.value[0],
				                   &smt.value[1],
				                   &smt.value[2]);

				if (readcount > 2)
				{
					for (int i = readcount - 2; i < 3; i++)
						smt.value[i] = -1;

					smt.numvals = readcount - 2;

					if (smt.row > maxrow) maxrow = smt.row;
					if (smt.col > maxcol) maxcol = smt.col;

					theSinex.matrix_map[type][value].push_back(smt);
				}
				else
				{
					theSinex.matrix_comments.push_back(s);
				}
			}
			else
			{
				theSinex.matrix_comments.push_back(s);
			}
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.matrix_map[type][value].sort(compare_matrix_entries);

	return 0;
}

void write_snx_matrices_from_filter(
	ofstream& out)
{
	char line[81];

	const char* type_strings	[MAX_MATRIX_TYPE];
	const char* value_strings	[MAX_MATRIX_VALUE];
	const char* fmt = " %5d %5d %21.14le";

	type_strings[ESTIMATE]		= "SOLUTION/MATRIX_ESTIMATE";
	type_strings[APRIORI]		= "SOLUTION/MATRIX_APRIORI";
	type_strings[NORMAL_EQN]	= "SOLUTION/NORMAL_EQUATION_MATRIX";

	value_strings[CORRELATION]	= "CORR";
	value_strings[COVARIANCE]	= "COVA";
	value_strings[INFORMATION]	= "INFO";

	for (auto& mt : {ESTIMATE})
	for (auto& mv : {COVARIANCE})
	{
		//print header
		sprintf(line, "+%s %c %s",
			type_strings[mt],
			theSinex.tri[mt][mv],
			mt == NORMAL_EQN ? "" : value_strings[mv]);

		out <<  line << endl;

		write_as_comments(out, theSinex.matrix_comments);

		MatrixXd& P = theSinex.kfState.P;

		char* fmt1 = " %21.14le";

		for (int i = 1; i < theSinex.kfState.x.rows();	i++)
		for (int j = 1; j <= i;		)
		{
			if (P(i,j) == 0)
			{
				j++;
				continue;
			}

			//start printing a line
			sprintf(line, fmt,
					i,
					j,
					P(i,j));
			j++;

			for (int k = 0; k < 2; k++)
			{
				if	( (P(i,j) == 0)
					||(j > i))
				{
					break;
				}

				char buf[23];
				sprintf(buf, fmt1, P(i,j));
				strcat(line, buf);
				j++;
			}

			out << line << endl;

		}

		//print footer
		sprintf(line, "-%s %c %s",
			type_strings[mt],
			theSinex.tri[mt][mv],
			mt == NORMAL_EQN ? "" : value_strings[mv]);

		out << line << endl;
	}
}

void write_snx_matrices(ofstream& out, list<newsnx_stn_snx_t>* pstns)
{
	const char* type_strings[MAX_MATRIX_TYPE];
	const char* value_strings[MAX_MATRIX_VALUE];
	const char* fmt = " %5d %5d %21.14le";
	int			idx = -1;
	list<int>	indices;

	type_strings[ESTIMATE]		= "SOLUTION/MATRIX_ESTIMATE";
	type_strings[APRIORI]		= "SOLUTION/MATRIX_APRIORI";
	type_strings[NORMAL_EQN]	= "SOLUTION/NORMAL_EQUATION_MATRIX";

	value_strings[CORRELATION]	= "CORR";
	value_strings[COVARIANCE]	= "COVA";
	value_strings[INFORMATION]	= "INFO";

	if (pstns)
	{
		for (auto& stn : *pstns)
		{
			idx = -1;
			string sitecode = stn.sitecode;

			// find the starting index from the estimates for that site
			for (auto& [index, estimate] : theSinex.estimates_map)
			{
				if 	(  estimate.sitecode.compare(sitecode)			== 0
					&& estimate.type.substr(0, 3).compare("STA")	== 0)
				{
					idx = index;
					break;
				}
			}

			if (idx != -1)
				indices.push_back(idx);
		}
	}

	write_as_comments(out, theSinex.matrix_comments);

	for (matrix_type 	mt = ESTIMATE;		mt < MAX_MATRIX_TYPE;	mt = static_cast<matrix_type>	(static_cast<int>(mt) + 1))
	for (matrix_value	mv = CORRELATION;	mv < MAX_MATRIX_VALUE;	mv = static_cast<matrix_value>	(static_cast<int>(mv) + 1))
	{
		if 	(  mt == NORMAL_EQN
			&& mv != INFORMATION)
			continue;

		if (theSinex.matrix_map[mt][mv].empty())
			continue;

		char line[81];
		sprintf(line, "+%s %c %s",
			type_strings[mt],
			theSinex.tri[mt][mv],
			mt == NORMAL_EQN ? "" : value_strings[mv]);

		out <<  line << endl;

		for (auto& matrix : theSinex.matrix_map[mt][mv])
		{
			newsnx_solmatrix_t& 	smt = matrix;
			char* fmt1 = " %21.14le";
			char buf[23];

			sprintf(line, fmt,
					smt.row,
					smt.col,
					smt.value[0]);

			for (int i = 1; i < smt.numvals; i++)
			{
				sprintf(buf, fmt1, smt.value[i]);
				strcat(line, buf);
			}

			// for station restricted row & col must be idx, idx+1 or idx+2 for some
			// station in the list ...
			bool doit = (pstns == NULL);

			if (pstns != NULL)
			{
				// FIXME: only need to consider row > col for L and row < col for U
				for (auto& idx1 : indices)
				{
					for (auto& idx2 : indices)
					{
						if 	( (smt.row - idx1) >= 0
							&&(smt.row - idx1) <  3
							&&(smt.col - idx2) >= 0
							&&(smt.col - idx2) <  3)
						{
							doit = true;
							break;
						}
					}

					if (doit)
						break;
				}
			}

			if (doit)
				out << line << endl;
		}

		sprintf(line, "-%s %c", type_strings[mt], theSinex.tri[mt][mv]);

		if (mt != NORMAL_EQN)
		{
			strcat(line, " ");
			strcat(line, value_strings[mv]);
		}

		out << line << endl;
	}
}

int read_snx_precode(ifstream& in)
{
	string closure("-PRECESSION/DATA");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()) != 0)
	{
		if (s[0] == ' ')
		{
			newsnx_precode_t snt;

			snt.precesscode	= s.substr(1, 8);
			snt.comment		= s.substr(10);

			theSinex.list_precessions.push_back(snt);
		}
		else
		{
			theSinex.precession_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	// no sort required for  precession codes
	return 0;
}

int write_snx_precodes(ofstream& out)
{
	write_as_comments(out, theSinex.precession_comments);

	out << "+PRECESSION/DATA" << endl;

	for (auto& spt : theSinex.list_precessions)
	{
		char line[81];

		sprintf(line, " %8s %s", spt.precesscode.c_str(), spt.comment.c_str());

		out << line << endl;
	}

	out << "-PRECESSION/DATA" << endl;

	return 0;
}

int read_snx_nutcode(ifstream& in)
{
	string closure("-NUTATION/DATA");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_nutcode_t snt;

			snt.nutcode = s.substr(1, 8);
			snt.comment = s.substr(10);

			theSinex.list_nutcodes.push_back(snt);
		}
		else
		{
			theSinex.nutation_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	// no sort required for nutcodes
	return 0;
}

int write_snx_nutcodes(ofstream& out)
{
	write_as_comments(out, theSinex.nutation_comments);

	out << "+NUTATION/DATA" << endl;

	for (auto& nutcode : theSinex.list_nutcodes)
	{
		newsnx_nutcode_t& snt = nutcode;

		char line[81];

		sprintf(line, " %8s %s", snt.nutcode.c_str(), snt.comment.c_str());

		out << line << endl;
	}

	out << "-NUTATION/DATA" << endl;

	return 0;
}

int read_snx_sourceids(ifstream& in)
{
	string closure("-SOURCE/ID");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_source_id_t ssi;

			ssi.source		= s.substr(1, 4);
			ssi.iers		= s.substr(6, 8);
			ssi.icrf		= s.substr(15, 16);
			ssi.comments	= s.substr(32);

			theSinex.list_source_ids.push_back(ssi);
		}
		else
		{
			theSinex.sourceid_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	// no sort required for source ids
	return 0;
}

int write_snx_sourceids(ofstream& out)
{
	write_as_comments(out, theSinex.sourceid_comments);

	out << "+SOURCE/ID" << endl;

	for (auto& source_id : theSinex.list_source_ids)
	{
		newsnx_source_id_t& ssi = source_id;

		char line[101];

		sprintf(line, " %4s %8s %16s %s", ssi.source.c_str(), ssi.iers.c_str(), ssi.icrf.c_str(), ssi.comments.c_str());

		out << line << endl;
	}

	out << "-SOURCE/ID" << endl;

	return 0;
}

// return true if left < right
static bool compare_satids(newsnx_satid_t& left, newsnx_satid_t& right)
{
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft	= atoi(left.svn.substr(1).c_str());
	int     nright	= atoi(right.svn.substr(1).c_str());
	int		comp;

	if (constleft == constright)
		comp = nleft - nright;
	else
		comp = constleft - constright;

	return (comp < 0);
}

int read_snx_satellite_ids(ifstream& in)
{
	string closure("-SATELLITE/ID");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satid_t sst;

			sst.svn			= s.substr(1, 4);
			sst.prn			= sst.svn[0] + s.substr(6, 2);
			sst.cospar		= s.substr(9, 9);;
			sst.obsCode		= s[18];
			sst.antRcvType	= s.substr(47);

			const char* p = s.c_str() + 21;

			int 	readcount = sscanf(p, "%2d:%3d:%5d %2d:%3d:%5d",
			                   &sst.timeSinceLaunch[0],
			                   &sst.timeSinceLaunch[1],
			                   &sst.timeSinceLaunch[2],
			                   &sst.timeUntilDecom[0],
			                   &sst.timeUntilDecom[1],
			                   &sst.timeUntilDecom[2]);

			if (readcount == 6)
			{
				// TODO: make the following adjustements
				// TSL if 0 is Sinex file start date
				// TUD if 0 is Sinex file end date

				theSinex.list_satids.push_back(sst);
			}
			else
			{
				theSinex.satid_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satid_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	// sort by SVN
	theSinex.list_satids.sort(compare_satids);
	return 0;
}

int write_snx_satids(ofstream& out)
{
	write_as_comments(out, theSinex.satid_comments);

	out << "+SATELLITE/ID" << endl;

	for (auto& ssi : theSinex.list_satids)
	{
		char line[101];

		sprintf(line, " %4s %2s %9s %c %2.2d:%3.3d:%5.5d %2.2d:%3.3d:%5.5d %20s",
		        ssi.svn.c_str(),
		        ssi.prn.c_str() + 1,
		        ssi.cospar.c_str(),
		        ssi.obsCode,
		        ssi.timeSinceLaunch[0],
		        ssi.timeSinceLaunch[1],
		        ssi.timeSinceLaunch[2],
		        ssi.timeUntilDecom[0],
		        ssi.timeUntilDecom[1],
		        ssi.timeUntilDecom[2],
		        ssi.antRcvType.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/ID" << endl;

	return 0;
}

// return true if left < right
static bool compare_satidents(newsnx_satident_t& left, newsnx_satident_t& right)
{
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft		= atoi(left.svn.substr(1).c_str());
	int     nright		= atoi(right.svn.substr(1).c_str());
	int		comp;

	if (constleft == constright)
		comp = nleft - nright;
	else
		comp = constleft - constright;

	return (comp <= 0);
}

int read_snx_satellite_identifiers(ifstream& in)
{
	string closure("-SATELLITE/IDENTIFIER");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satident_t sst;

			sst.svn			= s.substr(1, 4);
			sst.cospar		= s.substr(6, 9);
			sst.category	= atoi(s.substr(16, 6).c_str());
			sst.blocktype	= s.substr(23, 15);
			sst.comment		= s.substr(39);

			theSinex.list_satidents.push_back(sst);
		}
		else
		{
			theSinex.satident_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error)
			<< "end of file reached reading " << closure.substr(1)
			<< " block" << endl;
			return 1;
		}
	}

	// sort by SVN
	theSinex.list_satidents.sort(compare_satidents);

	return 0;
}

int write_snx_satidents(ofstream& out)
{
	write_as_comments(out, theSinex.satident_comments);

	out << "+SATELLITE/IDENTIFIER" << endl;

	for (auto& ssi : theSinex.list_satidents)
	{
		char line[101];

		sprintf(line, " %4s %9s %6d %-15s %s",
		        ssi.svn.c_str(),
		        ssi.cospar.c_str(),
		        ssi.category,
		        ssi.blocktype.c_str(),
		        ssi.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/IDENTIFIER" << endl;

	return 0;
}

// NB this DOES not compare by PRN!!
// return true if left < right
static bool compare_satprns(newsnx_satprn_t& left, newsnx_satprn_t& right)
{
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft		= atoi(left.svn.substr(1).c_str());
	int     nright		= atoi(right.svn.substr(1).c_str());
	int		comp;

	if (constleft == constright)
		comp = nleft - nright;
	else
		comp = constleft - constright;

	return (comp < 0);
}

int read_snx_satprns(ifstream& in)
{
	string closure("-SATELLITE/PRN");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()) != 0)
	{
		if (s[0] == ' ')
		{
			newsnx_satprn_t spt;

			spt.svn			= s.substr(1, 4);
			spt.prn			= s.substr(36, 3);
			spt.comment		= s.substr(40);

			int readcount = sscanf(s.c_str() + 6, "%4d:%3d:%5d %4d:%3d:%5d",
			                   &spt.start[0],
			                   &spt.start[1],
			                   &spt.start[2],
			                   &spt.stop[0],
			                   &spt.stop[1],
			                   &spt.stop[2]);

			if (readcount == 6)
			{
				// No need to adjust years since for satellites the year is 4 digits ...
				theSinex.list_satprns.push_back(spt);
			}
			else
			{
				theSinex.satprn_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satprn_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	// sort by SVN
	theSinex.list_satprns.sort(compare_satprns);
	return 0;
}

int write_snx_satprns(ofstream& out)
{
	write_as_comments(out, theSinex.satprn_comments);

	out << "+SATELLITE/PRN" << endl;

	char line[101];

	for (auto& spt : theSinex.list_satprns)
	{
		sprintf(line, " %4s %4.4d:%3.3d:%5.5d %4.4d:%3.3d:%5.5d %3s %s",
		        spt.svn.c_str(),
		        spt.start[0],
		        spt.start[1],
		        spt.start[2],
		        spt.stop[0],
		        spt.stop[1],
		        spt.stop[2],
		        spt.prn.c_str(),
		        spt.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/PRN" << endl;

	return 0;
}

// return true if left < right
static bool compare_freq_channels(newsnx_satfreqchn_t& left, newsnx_satfreqchn_t& right)
{
	// start by comparing SVN...
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft		= atoi(left.svn.substr(1).c_str());
	int     nright		= atoi(right.svn.substr(1).c_str());
	int		result;

	if (constleft == constright)
		result = nleft - nright;
	else
		result = constleft - constright;

	// then by start time if the same space vehicle
	for (int i = 0; i < 3; i++)
		if (result == 0)
			result = left.start[i] - right.start[i];

	return (result < 0);
}

int read_snx_satfreqchn(ifstream& in)
{
	string closure("-SATELLITE/FREQUENCY_CHANNEL");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satfreqchn_t	sfc;

			sfc.svn		= s.substr(1, 4);
			sfc.comment	= s.substr(40);

			int readcount = sscanf(s.c_str() + 6, "%4d:%3d:%5d %4d:%3d:%5d %3d",
			                   &sfc.start[0],
			                   &sfc.start[1],
			                   &sfc.start[2],
			                   &sfc.stop[0],
			                   &sfc.stop[1],
			                   &sfc.stop[2],
			                   &sfc.channel);

			if (readcount == 7)
			{
				// No need to adjust years since for satellites the year is 4 digits ...
				theSinex.list_satfreqchns.push_back(sfc);
			}
			else
			{
				theSinex.satfreqchn_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satfreqchn_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_satfreqchns.sort(compare_freq_channels);
	return 0;
}

int write_snx_satfreqchn(ofstream& out)
{
	write_as_comments(out, theSinex.satfreqchn_comments);

	out << "+SATELLITE/FREQUENCY_CHANNEL" << endl;

	for (auto& sfc : theSinex.list_satfreqchns)
	{
		char line[101];

		sprintf(line, " %4s %4.4d:%3.3d:%5.5d %4.4d:%3.3d:%5.5d %3d %s",
		        sfc.svn.c_str(),
		        sfc.start[0],
		        sfc.start[1],
		        sfc.start[2],
		        sfc.stop[0],
		        sfc.stop[1],
		        sfc.stop[2],
		        sfc.channel,
		        sfc.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/FREQUENCY_CHANNEL" << endl;

	return 0;
}

// return true if left < right
static bool compare_satmass(newsnx_satmass_t& left, newsnx_satmass_t& right)
{
	// start by comparing SVN...
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft		= atoi(left.svn.substr(1).c_str());
	int     nright		= atoi(right.svn.substr(1).c_str());
	int		result;

	if (constleft == constright)
		result = nleft - nright;
	else
		result = constleft - constright;

	// then by start time if the same space vehicle
	for (int i = 0; i < 3; i++)
		if (result == 0)
			result = left.start[i] - right.start[i];

	return (result < 0);
}

int read_snx_satmass(ifstream& in)
{
	string closure("-SATELLITE/MASS");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satmass_t	ssm;

			ssm.svn		= s.substr(1, 4);
			ssm.comment	= s.substr(46);

			int readcount = sscanf(s.c_str() + 6, "%4d:%3d:%5d %4d:%3d:%5d %9lf",
			                   &ssm.start[0],
			                   &ssm.start[1],
			                   &ssm.start[2],
			                   &ssm.stop[0],
			                   &ssm.stop[1],
			                   &ssm.stop[2],
			                   &ssm.mass);

			if (readcount == 7)
			{
				// No need to adjust years since for satellites the year is 4 digits ...
				theSinex.list_satmasses.push_back(ssm);
			}
			else
			{
				theSinex.satmass_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satmass_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_satmasses.sort(compare_satmass);
	return 0;
}

int write_snx_satmass(ofstream& out)
{
	write_as_comments(out, theSinex.satmass_comments);

	out << "+SATELLITE/MASS" << endl;

	for (auto& ssm : theSinex.list_satmasses)
	{
		char line[101];

		sprintf(line, " %4s %4.4d:%3.3d:%5.5d %4.4d:%3.3d:%5.5d %9.3lf %s",
		        ssm.svn.c_str(),
		        ssm.start[0],
		        ssm.start[1],
		        ssm.start[2],
		        ssm.stop[0],
		        ssm.stop[1],
		        ssm.stop[2],
		        ssm.mass,
		        ssm.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/MASS" << endl;

	return 0;
}

// return true if left < right
static bool compare_satcom(newsnx_satcom_t& left, newsnx_satcom_t& right)
{
	// start by comparing SVN...
	char	constleft		= left.svn[0];
	char    constright		= right.svn[0];
	int     nleft			= atoi(left.svn.substr(1).c_str());
	int     nright			= atoi(right.svn.substr(1).c_str());
	int		result;

	if (constleft == constright)
		result = nleft - nright;
	else
		result = constleft - constright;

	// then by start time if the same space vehicle
	for (int i = 0; i < 3; i++)
		if (result == 0)
			result = left.start[i] - right.start[i];

	return (result < 0);
}

int read_snx_satcom(ifstream& in)
{
	string closure("-SATELLITE/COM");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()) != 0)
	{
		if (s[0] == ' ')
		{
			newsnx_satcom_t	sct;

			sct.svn		= s.substr(1, 4);
			sct.comment	= s.substr(66);

			int readcount = sscanf(s.c_str() + 6, "%4d:%3d:%5d %4d:%3d:%5d %9lf %9lf %9lf",
			                   &sct.start[0],
			                   &sct.start[1],
			                   &sct.start[2],
			                   &sct.stop[0],
			                   &sct.stop[1],
			                   &sct.stop[2],
			                   &sct.com[0],
			                   &sct.com[1],
			                   &sct.com[2]);

			if (readcount == 9)
			{
				// No need to adjust years since for satellites the year is 4 digits ...
				theSinex.list_satcoms.push_back(sct);
			}
			else
			{
				theSinex.satcom_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satcom_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_satcoms.sort(compare_satcom);
	return 0;
}

int write_snx_satcom(ofstream& out)
{
	write_as_comments(out, theSinex.satcom_comments);

	out << "+SATELLITE/COM" << endl;

	for (auto& sct : theSinex.list_satcoms)
	{
		char line[101];

		sprintf(line, " %4s %4.4d:%3.3d:%5.5d %4.4d:%3.3d:%5.5d %9.4lf %9.4lf %9.4lf %s",
		        sct.svn.c_str(),
		        sct.start[0],
		        sct.start[1],
		        sct.start[2],
		        sct.stop[0],
		        sct.stop[1],
		        sct.stop[2],
		        sct.com[0],
		        sct.com[1],
		        sct.com[2],
		        sct.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/COM" << endl;

	return 0;
}

// return true if left < right
static bool compare_satecc(newsnx_satecc_t& left, newsnx_satecc_t& right)
{
	// start by comparing SVN...
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft	= atoi(left.svn.substr(1).c_str());
	int     nright	= atoi(right.svn.substr(1).c_str());
	int		result;

	if (constleft == constright)
		result = nleft - nright;
	else
		result = constleft - constright;

	// then by type (P or L)
	if (result == 0)
		result = static_cast<int>(left.type) - static_cast<int>(right.type);

	return (result < 0);
}

int read_snx_satecc(ifstream& in)
{
	string closure("-SATELLITE/ECCENTRICITY");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satecc_t	set;

			set.svn		= s.substr(1, 4);
			set.equip	= s.substr(6, 20);
			set.type	= s[27];
			set.comment	= s.substr(59);

			int readcount = sscanf(s.c_str() + 29, "%9lf %9lf %9lf",
			                   &set.ecc[0],
			                   &set.ecc[1],
			                   &set.ecc[2]);

			if (readcount == 3)
			{
				theSinex.list_sateccs.push_back(set);
			}
			else
			{
				theSinex.satecc_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satecc_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_sateccs.sort(compare_satecc);
	return 0;
}

int write_snx_satecc(ofstream& out)
{
	write_as_comments(out, theSinex.satecc_comments);

	out << "+SATELLITE/ECCENTRICITY" << endl;

	for (auto& set : theSinex.list_sateccs)
	{
		char line[101];

		sprintf(line, " %4s %-20s %c %9.4lf %9.4lf %9.4lf %s",
		        set.svn.c_str(),
		        set.equip.c_str(),
		        set.type,
		        set.ecc[0],
		        set.ecc[1],
		        set.ecc[2],
		        set.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/ECCENTRICITY" << endl;

	return 0;
}

// return true if left < right
static bool compare_satpower(newsnx_satpower_t& left, newsnx_satpower_t& right)
{
	// start by comparing SVN...
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft		= atoi(left.svn.substr(1).c_str());
	int     nright		= atoi(right.svn.substr(1).c_str());
	int		result;

	if (constleft == constright)
		result = nleft - nright;
	else
		result = constleft - constright;

	// then by start time if the same space vehicle
	for (int i = 0; i < 3; i++)
		if (result == 0)
			result = left.start[i] - right.start[i];

	return (result < 0);
}

int read_snx_satpower(ifstream& in)
{
	string closure("-SATELLITE/TX_POWER");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satpower_t	spt;

			spt.svn		= s.substr(1, 4);
			spt.comment	= s.substr(41);

			int readcount = sscanf(s.c_str() + 6, "%4d:%3d:%5d %4d:%3d:%5d %4d",
			                   &spt.start[0],
			                   &spt.start[1],
			                   &spt.start[2],
			                   &spt.stop[0],
			                   &spt.stop[1],
			                   &spt.stop[2],
			                   &spt.power);

			if (readcount == 7)
			{
				// No need to adjust years since for satellites the year is 4 digits ...
				theSinex.list_satpowers.push_back(spt);
			}
			else
			{
				theSinex.satpower_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satpower_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_satpowers.sort(compare_satpower);
	return 0;
}

int write_snx_satpower(ofstream& out)
{
	write_as_comments(out, theSinex.satpower_comments);

	out << "+SATELLITE/TX_POWER" << endl;

	for (auto& spt : theSinex.list_satpowers)
	{
		char line[101];

		sprintf(line, " %4s %4.4d:%3.3d:%5.5d %4.4d:%3.3d:%5.5d %4d %s",
		        spt.svn.c_str(),
		        spt.start[0],
		        spt.start[1],
		        spt.start[2],
		        spt.stop[0],
		        spt.stop[1],
		        spt.stop[2],
		        spt.power,
		        spt.comment.c_str());

		out << line << endl;
	}

	out << "-SATELLITE/TX_POWER" << endl;

	return 0;
}

// return true if left < right
static bool compare_satpc(newsnx_satpc_t& left, newsnx_satpc_t& right)
{
	// start by comparing SVN...
	char	constleft	= left.svn[0];
	char    constright	= right.svn[0];
	int     nleft	= atoi(left.svn.substr(1).c_str());
	int     nright	= atoi(right.svn.substr(1).c_str());
	int		result;

	if (constleft == constright)
		result = nleft - nright;
	else
		result = constleft - constright;

	// then by the first freq number
	if (result == 0)
		result = static_cast<int>(left.freq) - static_cast<int>(right.freq);

	return (result < 0);
}

int read_snx_satpc(ifstream& in)
{
	string closure("-SATELLITE/PHASE_CENTER");
	string s;

	getline(in, s);

	if (in.eof())
	{
		BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
		return 1;
	}

	while (strncmp(s.c_str(), closure.c_str(), closure.length()))
	{
		if (s[0] == ' ')
		{
			newsnx_satpc_t		spt;

			int				readcount2;

			spt.svn		= s.substr(1, 4);
			spt.freq	= s[6];
			spt.freq2	= s[29];
			spt.antenna	= s.substr(52, 10);
			spt.type	= s[63];
			spt.model	= s[65];

			int readcount = sscanf(s.c_str() + 6, "%6lf %6lf %6lf",
			                   &spt.zxy[0],
			                   &spt.zxy[1],
			                   &spt.zxy[2]);

			if (spt.freq2 != ' ')
			{
				readcount2 = sscanf(s.c_str() + 31, "%6lf %6lf %6lf",
			                   &spt.zxy2[0],
			                   &spt.zxy2[1],
			                   &spt.zxy2[2]);
			}

			if 	(   readcount	== 3
				&&( spt.freq2	== ' '
				  ||readcount2	== 3))
			{
				theSinex.list_satpcs.push_back(spt);
			}
			else
			{
				theSinex.satpc_comments.push_back(s);
			}
		}
		else
		{
			theSinex.satpc_comments.push_back(s);
		}

		getline(in, s);

		if (in.eof())
		{
			BOOST_LOG_TRIVIAL (error) << "end of file reached reading " << closure.substr(1) << " block" << endl;
			return 1;
		}
	}

	theSinex.list_satpcs.sort(compare_satpc);
	return 0;
}

int write_snx_satpc(ofstream& out)
{
	write_as_comments(out, theSinex.satpc_comments);

	out << "+SATELLITE/PHASE_CENTER" << endl;

	for (auto& spt : theSinex.list_satpcs)
	{
		char line[101];
		char freq2line[23];

		memset(freq2line, ' ', sizeof(freq2line));
		freq2line[22] = '\0';

		if (spt.freq2 != ' ')
			sprintf(freq2line, "%c %6.4lf %6.4lf %6.4lf",
			        spt.freq2,
			        spt.zxy2[0],
			        spt.zxy2[1],
			        spt.zxy2[2]);

		sprintf(line, " %4s %c %6.4lf %6.4lf %6.4lf %22s %-10s %c %c",
		        spt.svn.c_str(),
		        spt.freq,
		        spt.zxy[0],
		        spt.zxy[1],
		        spt.zxy[2],
		        freq2line,
		        spt.antenna.c_str(),
		        spt.type,
		        spt.model);

		out << line << endl;
	}

	out << "-SATELLITE/PHASE_CENTER" << endl;

	return 0;
}

/* ordering function for sat_snx_t */
bool sat_svn_time_compare(newsnx_sat_snx_t& left, newsnx_sat_snx_t& right)
{
	int svncmp = left.svn.compare(right.svn);

	if (svncmp == 0)
	{
		int timecmp = time_compare(left.start, right.start);

		if (timecmp <= 0)
			return true;
		else
			return false;
	}
	else if (svncmp < 0)
		return true;
	else
		return false;
}

int build_maps()
{
	list<newsnx_stn_snx_t>		sitelist;
	int						result = 0;

	newsnx_stn_snx_t			stn;
	memset (&stn, 0, sizeof(stn));

	if (!theSinex.list_satidents.empty())
	{
		list<newsnx_sat_snx_t> 	satlist;
		newsnx_sat_snx_t			sat;
		memset (&sat, 0, sizeof(sat));

		// create satellites by going through the satellite identifiers list
		for (auto& sat_id : theSinex.list_satidents)
		{
			sat.svn			= sat_id.svn;
			sat.category	= sat_id.category;
			sat.cospar		= trim(sat_id.cospar);
			sat.blocktype	= trim(sat_id.blocktype);

			satlist.push_back(sat);
		}

		// now go through the lists of satellite ids, eccentricities and phase centers as these do not change over time
		// on read we have sorted all lists by SVN so we only need to do a merge here, no sorting required.
		auto i = satlist.begin();

		// eccecntricities first
		for (auto it = theSinex.list_sateccs.begin(); it != theSinex.list_sateccs.end(); )
		{
			int compare = it->svn.compare(i->svn);

			if (compare < 0)
			{
				// satecc svn 'less' than the sat svn. No match possible. Move on to the next eccentricity
				it++;
			}
			else if (compare == 0)
			{
				// add the eccentricity to the current record. Error if we encounter a third one!
				if (i->numeccs < 2)
				{
					int idx = i->numeccs;

					i->ecctype[idx]		= it->type;
					i->eccequip[idx]	= it->equip;

					for (int j = 0; j < 3; j++)
					{
						i->eccentricity[idx][j] = it->ecc[j];
					}

					i->numeccs++;
				}
				else
				{
					// full already. Write error, then ignore and move on
					BOOST_LOG_TRIVIAL(error) << "two eccentricities already defined for svn " << it->svn << endl;
					result++;
				}

				it++;
			}
			else
			{
				// move the sat iterator on and check again
				i++;

				// if we have reached the end of the satellites we are done with this loop
				if (i == satlist.end())
					it = theSinex.list_sateccs.end();
			}
		}

		// now phase centers
		i = satlist.begin();

		for (auto it = theSinex.list_satpcs.begin(); it != theSinex.list_satpcs.end(); )
		{
			int compare = it->svn.compare(i->svn);

			if (compare < 0)
			{
				// satpc svn 'less' than the sat svn. No match possible. Move on to the next phase center record
				it++;
			}
			else if (compare == 0)
			{
				// add the phase centers to the current record. Error if we encounter a sixth one!
				if (i->numfreqs < 5)
				{
					int idx = i->numfreqs;

					i->freq[idx] = it->freq;

					for (int j = 0; j < 3; j++)
					{
						i->zxy[idx][j] = it->zxy[j];
					}

					i->numfreqs++;

					if (it->freq2 != ' ')
					{
						if (i->numfreqs == 5)
						{
							BOOST_LOG_TRIVIAL(error) << "attempt to add 6th phase center for SVN " << i->svn << endl;
							result++;
						}
						else
						{
							idx++;
							i->freq[idx] = it->freq2;

							for (int j = 0; j < 3; j++)
							{
								i->zxy[idx][j] = it->zxy2[j];
							}
						}
					}

					// now add antenna, pctype and model
					i->antenna = it->antenna;
					i->pctype = it->type;
					i->pcmodel = it->model;
				}
				else
				{
					BOOST_LOG_TRIVIAL(error)
					<< "5 frequencies for phase centers already for SVN " << i->svn << endl;
					result++;
				}

				it++;

			}
			else
			{
				// move the sat iterator on and check again
				i++;

				// if we have reached the end of the satellites we are done with this loop
				if (i == satlist.end())
					it = theSinex.list_satpcs.end();
			}
		}

		BOOST_LOG_TRIVIAL(info)
		<< "After eccentricities and phase centers " << satlist.size()
		<< " satellites in list." << endl;

		// now add the first timed loop - prns
		i = satlist.begin();

		for (auto it = theSinex.list_satprns.begin(); it != theSinex.list_satprns.end(); )
		{
			int compare = it->svn.compare(i->svn);

			if (compare < 0)
			{
				// prn list SVN < current satlist SVN. No match possible. Move on to the next prn record
				it++;
			}
			else if (compare == 0)
			{
				// since there will only be one record per SVN currently, we update the first one we find and
				// create new entries for later ones
				if (i->prn.empty())
				{
					i->prn = it->prn;

					for (int j = 0; j < 3; j++)
					{
						i->start[j] = it->start[j];
						i->stop[j] = it->stop[j];
					}
				}
				else
				{
					newsnx_sat_snx_t sat = *i;

					sat.prn = it->prn;

					for (int j = 0; j < 3; j++)
					{
						sat.start[j] = it->start[j];
						sat.stop[j] = it->stop[j];
					}

					i++;
					satlist.insert(i, sat);
					i--;
				}

				it++;
			}
			else
			{
				if (i != satlist.end())
					i++;
			}
		}

		BOOST_LOG_TRIVIAL(info)
		<< "After PRNs added, " << satlist.size()
		<< " satellites in list." << endl;

		i = satlist.begin();
		list<newsnx_sat_snx_t> additions;

		// now add mass records
		for (auto it = theSinex.list_satmasses.begin(); it != theSinex.list_satmasses.end(); it++ )
		{
			for (; i != satlist.end(); i++)
			{
				if (it->svn.compare(i->svn) == 0)
					break;
			}

			if (i == satlist.end())
			{
				// we did not find the SVN in the list. Ignore record.
				i = satlist.begin();
				continue;
			}
			else if (i->prn.empty())
			{
				// no prn for this satellite. Insert mass record and move on
				i->mass = it->mass;

				for (int j = 0; j < 3; j++)
				{
					i->start[j] = it->start[j];
					i->stop[j] = it->stop[j];
				}

				i++;
				continue;
			}

			if (time_compare(i->start, it->stop) >= 0)
			{
				// first prn record for sat starts after the mass record ends
				newsnx_sat_snx_t sat = *i;
				sat.prn.clear();
				sat.mass = it->mass;

				for (int j = 0; j < 3; j++)
				{
					sat.start[j]	= it->start[j];
					sat.stop[j]		= it->stop[j];
				}

				additions.push_back(sat);
				i++;
				continue;
			}

			auto k = i;

			for (; k != satlist.end() && k->svn.compare(it->svn) == 0 &&
			        time_compare(k->stop, it->start) <= 0; k++)
			{
				//do nothing...
			}

			if (k == satlist.end() || k->svn.compare(it->svn) != 0)
			{
				// last prn period for this satellite finishes before start
				// of mass record. Add new record with no prn to the addtions list
				newsnx_sat_snx_t sat = *i;
				sat.prn.clear();

				for (int j = 0; j < 3; j++)
				{
					sat.start[j] = it->start[j];
					sat.stop[j] = it->stop[j];
				}

				sat.mass = it->mass;
				additions.push_back(sat);
				i++;
				continue;
			}

			int tscompare = time_compare(it->start, i->start);  // start comparison
			int tecompare = time_compare(it->stop, i->stop);	// stop comparison

			if (tscompare == 0)
			{
				i->mass = it->mass;

				if (tecompare < 0)
				{
					// mass record end first. End current record at mass end,
					// and add new record with no mass to additions list
					newsnx_sat_snx_t sat = *i;

					for (int j = 0; j < 3; j++)
						sat.start[j] = (i->stop[j] = it->stop[j]);

					sat.mass = 0;
					additions.push_back(sat);
				}
				else if (tecompare > 0)
				{
					// existing record ends first. Possible next existing record(s) also overlaps?
					newsnx_sat_snx_t sat = *i;

					for (int j = 0; j < 3; j++)
					{
						sat.start[j] = i->stop[j];
						sat.stop[j] = it->stop[j];
					}

					sat.prn.clear();
					i++;

					while (i->svn.compare(it->svn) == 0 &&
					        time_compare(i->stop, it->stop) <= 0)
					{
						// fill in any gap record
						if (time_compare(sat.start, i->start) < 0)
						{
							for (int j = 0; j < 3; j++)
								sat.stop[j] = i->start[j];

							additions.push_back(sat);

							for (int j = 0; j < 3; j++)
							{
								sat.start[j] = i->stop[j];
								sat.stop[j] = it->stop[j];
							}
						}

						i->mass = it->mass;
						i++;
					}

					if (i->svn.compare(it->svn) == 0 &&
					        time_compare(i->start, it->start) < 0)
					{
						// ended because current sat period is after mass record.
						sat = *i;
						i->mass = it->mass;

						for (int j = 0; j < 3; j++)
							sat.start[j] = (i->stop[j] = it->stop[j]);

						sat.mass = 0;
						additions.push_back(sat);
					}
				}

				// otherwise the time periods match. nothing more to do.
				i++;
				continue;
			}

			// now the time periods overlap
			if (tscompare < 0)
			{
				// mass record starts first.
				newsnx_sat_snx_t sat = *i;
				sat.prn.clear();
				sat.mass = it->mass;

				for (int j = 0; j < 3; j++)
				{
					sat.start[j] = it->start[j];
					sat.stop[j] = i->start[j];
				}

				additions.push_back(sat);

				while (i->svn.compare(it->svn) == 0 &&
				        time_compare(i->stop, it->stop) <= 0)
				{
					i->mass = it->mass;

					for (int j = 0; j < 3; j++)
						sat.start[j] = i->stop[j];

					i++;

					if (i->svn.compare(it->svn) == 0)
						for (int j = 0; j < 3; j++)
							sat.stop[j] = i->start[j];
					else
						for (int j = 0; j < 3; j++)
							sat.stop[j] = it->stop[j];

					if (time_compare(sat.start, sat.stop) < 0)
						additions.push_back(sat);
				}

				// if above while ends because current record ends after mass record,
				// we need to update the current record to start at mass record end
				// and push a mass/prn record to the additions list
				if (i->svn.compare(it->svn) == 0)
				{
					for (int j = 0; j < 3; j++)
					{
						sat.start[j] = i->start[j];
						sat.stop[j] = it->stop[j];
						i->start[j] = it->stop[j];
					}

					sat.prn = i->prn;
					additions.push_back(sat);
				}
			}
			else if (tscompare > 0)
			{
				// current sat record starts first
				newsnx_sat_snx_t sat = *i;

				for (int j = 0; j < 3; j++)
				{
					sat.start[j] = i->start[j];
					sat.stop[j] = it->start[j];
					i->start[j] = it->start[j];
				}

				additions.push_back(sat);

				// check if sat record starts completely covers current mass record
				if (time_compare(i->stop, it->stop) > 0)
				{
					for (int j = 0; j < 3; j++)
					{
						sat.start[j] = i->start[j];
						i->start[j] = (sat.stop[j] = it->stop[j]);
					}

					sat.mass = it->mass;
					additions.push_back(sat);
					i++;
				}
				else
				{
					// mass record ends after current sat record
					i->mass = it->mass;

					for (int j = 0; j < 3; j++)
					{
						sat.start[j] = i->stop[j];
					}

					sat.mass = it->mass;
					sat.prn.clear();
					i++;

					while (i->svn.compare(it->svn) == 0)
					{
						// add any gap periods
						tecompare = time_compare(i->start, it->stop);

						if (tecompare > 0)
							break;

						for (int j = 0; j < 3; j++)
							sat.stop[j] = i->start[j];

						additions.push_back(sat);
						i->mass = it->mass;
						tecompare = time_compare(i->stop, it->stop);

						for (int j = 0; j < 3; j++)
						{
							sat.start[j] = (tecompare < 0) ? i->stop[j] : it->stop[j];

							if (tecompare > 0)
								i->start[j] = it->stop[j];
						}

						i++;
					}
				}
			}

			satlist.merge(additions, sat_svn_time_compare);
		}

		// print out current list?
		BOOST_LOG_TRIVIAL(info)
		<< "number of sata after mass insertions = "
		<< satlist.size() << endl;
	}

	return result;
}

string		headers[] = {"FILE/REFERENCE",   										// 0
                         "FILE/COMMENT",											// 1
                         "INPUT/HISTORY",											// 2
                         "INPUT/FILES",												// 3
                         "INPUT/ACKNOWLEDGEMENTS",									// 4
                         "NUTATION/DATA",											// 5
                         "PRECESSION/DATA",											// 6
                         "SOURCE/ID",												// 7
                         "SITE/ID",													// 8
                         "SITE/DATA",												// 9
                         "SITE/RECEIVER",											// 10
                         "SITE/ANTENNA",											// 11
                         "SITE/GPS_PHASE_CENTER",									// 12
                         "SITE/GAL_PHASE_CENTER",									// 13
                         "SITE/ECCENTRICITY",										// 14
                         "BIAS/EPOCHS",												// 15
                         "SOLUTION/EPOCHS",											// 16
                         "SOLUTION/STATISTICS",										// 17
                         "SOLUTION/ESTIMATE",										// 18
                         "SOLUTION/APRIORI",										// 19
                         "SOLUTION/NORMAL_EQUATION_VECTOR",							// 20
                         "SOLUTION/MATRIX_ESTIMATE", /* {p} {type} */				// 21
                         "SOLUTION/MATRIX_APRIORI", /* {p} {type} */				// 22
                         "SOLUTION/NORMAL_EQUATION_MATRIX", /* {p} */				// 23
                         "SATELLITE/IDENTIFIER", /* must precede the ID entry */	// 24
                         "SATELLITE/PRN",											// 25
                         "SATELLITE/MASS",											// 26
                         "SATELLITE/FREQUENCY_CHANNEL",								// 27
                         "SATELLITE/TX_POWER",										// 28
                         "SATELLITE/COM",											// 29
                         "SATELLITE/ECCENTRICITY",									// 30
                         "SATELLITE/PHASE_CENTER",									// 31
                         "SATELLITE/ID",											// 32
                         "INPUT/ACKNOWLEDGMENTS",									// 33 - misspelt in some files, co need to cater for it
                         "ENDSNX"
                    };												// 33

int read_sinex(string filepath)
{
	ifstream 		filestream(filepath);
	int				failure = 0;
	string			line;
	list<string>	comments;

	BOOST_LOG_TRIVIAL(info)
	<< "reading " << filepath << std::endl;

	if (!filestream.fail())
	{
		failure = read_snx_header(filestream);
		if (failure)
		{
			filestream.close();
			BOOST_LOG_TRIVIAL(error)
			<< "Error reading header line." << endl;

			return failure;
		}
	}

	while (!filestream.eof())
	{
		getline(filestream, line);

		if (filestream.eof())
		{
			// error - did not find closure line. Report and clean up.
			BOOST_LOG_TRIVIAL(error)
			<< "Closure line not found before end." << endl;

			failure = 1;
			break;
		}
		else if (line[0] == '*')
		{
			comments.push_back(line);
		}
		else if (line[0] == '+')
		{
			int 	i;
			char	c;
			matrix_value mv;
			string	mvs;

			for (i = 0; i < 33; i++)
			{
				int compare = line.compare(1, strlen(headers[i].c_str()), headers[i]);

				if (compare == 0)
					break;
			}

			switch (i)
			{
				// no external comments for reference block
				case 0:																																				failure = read_snx_reference		(filestream);	break;
				// no external comments for comment block (strange that, huh?)
				case 1:																																				failure = read_snx_comment			(filestream);	break;
				// move all comments read into historyComments;
				case 2:		theSinex.historyComments		.insert(theSinex.historyComments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_input_history	(filestream);	break;
				case 3:		theSinex.filesComments			.insert(theSinex.filesComments			.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_input_files		(filestream);	break;
				case 4:		//fallthrough to 33
				case 33:	theSinex.ackComments			.insert(theSinex.ackComments			.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_acknowledgements	(filestream);	break;
				case 5:		theSinex.nutation_comments		.insert(theSinex.nutation_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_nutcode			(filestream);	break;
				case 6:		theSinex.precession_comments	.insert(theSinex.precession_comments	.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_precode			(filestream);	break;
				case 7:		theSinex.sourceid_comments		.insert(theSinex.sourceid_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_sourceids			(filestream);	break;
				case 8:		theSinex.siteIdcomments			.insert(theSinex.siteIdcomments			.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_siteIds			(filestream);	break;
				case 9:		theSinex.siteDatacomments		.insert(theSinex.siteDatacomments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_siteData			(filestream);	break;
				case 10:	theSinex.receivercomments		.insert(theSinex.receivercomments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_receivers			(filestream);	break;
				case 11:	theSinex.antennacomments		.insert(theSinex.antennacomments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_antennas			(filestream);	break;
				case 12:	theSinex.gps_pc_comments		.insert(theSinex.gps_pc_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_gps_phase_center		(filestream);	break;
				case 13:	theSinex.gal_pc_comments		.insert(theSinex.gal_pc_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_gal_phase_center		(filestream);	break;
				case 14:	theSinex.site_ecc_comments		.insert(theSinex.site_ecc_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_site_eccentricity	(filestream);	break;
				case 17:	theSinex.statistics_comments	.insert(theSinex.statistics_comments	.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_statistics		(filestream);	break;
				case 18:	theSinex.estimate_comments		.insert(theSinex.estimate_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_estimates			(filestream);	break;
				case 19:	theSinex.apriori_comments		.insert(theSinex.apriori_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_apriori			(filestream);	break;
				case 20:	theSinex.normal_eqns_comments	.insert(theSinex.normal_eqns_comments	.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_normals			(filestream);	break;
				case 24:	theSinex.satident_comments		.insert(theSinex.satident_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satellite_identifiers(filestream);break;
				case 25:	theSinex.satprn_comments		.insert(theSinex.satprn_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satprns			(filestream);	break;
				case 26:	theSinex.satmass_comments		.insert(theSinex.satmass_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satmass			(filestream);	break;
				case 27:	theSinex.satfreqchn_comments	.insert(theSinex.satfreqchn_comments	.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satfreqchn		(filestream);	break;
				case 28:	theSinex.satpower_comments		.insert(theSinex.satpower_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satpower			(filestream);	break;
				case 29:	theSinex.satcom_comments		.insert(theSinex.satcom_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satcom			(filestream);	break;
				case 30:	theSinex.satecc_comments		.insert(theSinex.satecc_comments		.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satecc			(filestream);	break;
				case 31:	theSinex.satpc_comments			.insert(theSinex.satpc_comments			.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satpc			(filestream);	break;
				case 32:	theSinex.satid_comments			.insert(theSinex.satid_comments			.end(), comments.begin(), comments.end());	comments.clear();	failure = read_snx_satellite_ids	(filestream);	break;
				case 23:	theSinex.matrix_comments		.insert(theSinex.matrix_comments		.end(), comments.begin(), comments.end());	comments.clear();	c = line[headers[i].length() + 2];
																																									failure = read_snx_matrix			(filestream, NORMAL_EQN, INFORMATION, c);			break;
				case 15:
					if (!theSinex.epochs_have_bias && !theSinex.list_solepochs.empty())
					{
						BOOST_LOG_TRIVIAL(error)
						<< "cannot combine BIAS/EPOCHS and SOLUTION/EPOCHS blocks." << endl;

						failure = 1;
						break;
					}

					theSinex.epochs_have_bias = true;
					theSinex.epochcomments.insert(theSinex.epochcomments.end(), comments.begin(), comments.end());
					comments.clear();
					failure = read_snx_epochs(filestream, true);
					break;

				case 16:
					if (theSinex.epochs_have_bias && !theSinex.list_solepochs.empty())
					{
						BOOST_LOG_TRIVIAL(error)
						<< "cannot combine BIAS/EPOCHS and SOLUTION/EPOCHS blocks." << endl;

						failure = 1;
						break;
					}

					theSinex.epochs_have_bias = false;
					theSinex.epochcomments			.insert(theSinex.epochcomments.end(), comments.begin(), comments.end());
					comments.clear();

					failure = read_snx_epochs(filestream, false);
					break;

				case 21:
					theSinex.matrix_comments.insert(theSinex.matrix_comments.end(), comments.begin(), comments.end());
					comments.clear();
					c = line[headers[i].length() + 2];
					mvs = line.substr(headers[i].length() + 4, 4);

					if 		(!mvs.compare("CORR"))	mv = CORRELATION;
					else if (!mvs.compare("COVA"))	mv = COVARIANCE;
					else if (!mvs.compare("INFO"))	mv = INFORMATION;

					failure = read_snx_matrix(filestream, ESTIMATE, mv, c);
					break;

				case 22:
					theSinex.matrix_comments.insert(theSinex.matrix_comments.end(), comments.begin(), comments.end());
					comments.clear();
					c = line[headers[i].length() + 2];
					mvs = line.substr(headers[i].length() + 4, 4);

					if 		(!mvs.compare("CORR"))	mv = CORRELATION;
					else if (!mvs.compare("COVA"))	mv = COVARIANCE;
					else if (!mvs.compare("INFO"))	mv = INFORMATION;

					failure = read_snx_matrix(filestream, APRIORI, mv, c);
					break;

				default:
					BOOST_LOG_TRIVIAL(error)
					<< "error unknown header line: " << line << endl;

					failure = 1;
					break;
			}
		}
		else if (line[0] == '%')
		{
			if (line.compare(1, strlen(headers[33].c_str()), headers[34]))
			{
				// error in file. report it.
				BOOST_LOG_TRIVIAL(error)
				<< "line starting '%' met not final line" << endl << line << endl;

				failure = 1;
			}

			break;
		}

		if (failure)
			break;
	}

	dedupe_sinex();

	return failure;
}

int  write_sinex(
	string filepath,
	list<newsnx_stn_snx_t>*		stationListPointer,
	newsnx_sat_snx_t*			psat)
{
	ofstream 	filestream(filepath);

	if (!filestream)
	{
		return 1;
	}
	int 		failure = 0;
	bool		domatrices = false;

	write_snx_header(filestream);

	if (!theSinex.refstrings.		empty())		failure += write_snx_reference		(filestream);
	if (!theSinex.commentstrings.	empty())		failure += write_snx_comments		(filestream);
	if (!theSinex.inputHistory.		empty())		failure += write_snx_input_history	(filestream);
	if (!theSinex.inputFiles.		empty())		failure += write_snx_input_files	(filestream);
	if (!theSinex.acknowledgements.	empty())		failure += write_snx_acknowledgements(filestream);

	if (psat == NULL)
	{
		if (!theSinex.list_siteids.		empty())		failure += write_snx_siteids	(filestream, stationListPointer);
		if (!theSinex.list_sitedata.	empty())		failure += write_snx_sitedata	(filestream, stationListPointer);
		if (!theSinex.list_receivers.	empty())		failure += write_snx_receivers	(filestream, stationListPointer);
		if (!theSinex.list_antennas.	empty())		failure += write_snx_antennas	(filestream, stationListPointer);
		if (!theSinex.list_gps_pcs.		empty())		failure += write_snx_gps_pcs	(filestream, stationListPointer);
		if (!theSinex.list_gal_pcs.		empty())		failure += write_snx_gal_pcs	(filestream, stationListPointer);
		if (!theSinex.list_site_eccs.	empty())		failure += write_snx_site_eccs	(filestream, stationListPointer);
		if (!theSinex.list_solepochs.	empty())		failure += write_snx_epochs		(filestream, stationListPointer);
		if (!theSinex.list_statistics.	empty())		failure += write_snx_statistics	(filestream);
// 		if (!theSinex.estimates_map.	empty())		failure += write_snx_estimates	(filestream, stationListPointer);
																	write_snx_estimates_from_filter	(filestream);
		if (!theSinex.apriori_map.		empty())		failure += write_snx_apriori	(filestream, stationListPointer);
		if (!theSinex.list_normal_eqns.	empty())		failure += write_snx_normal		(filestream, stationListPointer);

		for (int i = 0; i < 3; i++)
			domatrices |= !theSinex.matrix_map[i].empty();

		if (domatrices)
// 			write_snx_matrices(filestream, stationListPointer);
			write_snx_matrices_from_filter(filestream);
	}

	if	(  stationListPointer	== NULL
		&& psat		== NULL)
	{
		if (!theSinex.list_source_ids.	empty())		failure += write_snx_sourceids	(filestream);
		if (!theSinex.list_nutcodes.	empty())		failure += write_snx_nutcodes	(filestream);
		if (!theSinex.list_precessions.	empty())		failure += write_snx_precodes	(filestream);
	}

	if (stationListPointer == NULL)
	{
		if (!theSinex.list_satids.		empty())		failure += write_snx_satids		(filestream);
		if (!theSinex.list_satidents.	empty())		failure += write_snx_satidents	(filestream);
		if (!theSinex.list_satprns.		empty())		failure += write_snx_satprns	(filestream);
		if (!theSinex.list_satfreqchns.	empty())		failure += write_snx_satfreqchn	(filestream);
		if (!theSinex.list_satmasses.	empty())		failure += write_snx_satmass	(filestream);
		if (!theSinex.list_satcoms.		empty())		failure += write_snx_satcom		(filestream);
		if (!theSinex.list_sateccs.		empty())		failure += write_snx_satecc		(filestream);
		if (!theSinex.list_satpowers.	empty())		failure += write_snx_satpower	(filestream);
		if (!theSinex.list_satpcs.		empty())		failure += write_snx_satpc		(filestream);
	}

	filestream << "%%ENDSNX" << endl;

	return failure;
}


void sinex_add_statistic(
	const string& what,
	const int val)
{
	newsnx_solstatistic_t sst;

	sst.name		= what;
	sst.etype		= 0;
	sst.value.ival	= val;

	theSinex.list_statistics.push_back(sst);
}

void sinex_add_statistic(
	const string& what,
	const double val)
{
	newsnx_solstatistic_t sst;

	sst.name		= what;
	sst.etype		= 1;
	sst.value.dval	= val;

	theSinex.list_statistics.push_back(sst);
}

int sinex_check_add_ga_reference()
{
	// step 1: check it is not already there
	for (auto it = theSinex.refstrings.begin(); it != theSinex.refstrings.end(); it++)
	{
		if (it->refline.find("Geoscience Australia") != string::npos)
		{
			return 1;
		}
	}

	// step 2: remove any other provider's details
	// NB we do not increment the iterator in the loop because the erase if found will do it for us
	for (auto it = theSinex.refstrings.begin(); it != theSinex.refstrings.end(); )
	{
		string	s = it->refline;

		if 	( s.find("DESCRIPTION") != string::npos
			||s.find("OUTPUT") 		!= string::npos
	        ||s.find("CONTACT") 	!= string::npos
	        ||s.find("SOFTWARE") 	!= string::npos
	        ||s.find("HARDWARE") 	!= string::npos
	        ||s.find("INPUT") 		!= string::npos)
		{
			it = theSinex.refstrings.erase(it);
		}
		else
		{
			it++;
		}
	}

	// step 3: put in the Geoscience reference
	struct utsname	buf;
	newsnx_ref_t srt;
	char 	line[81];

	sprintf(line, " %-18s %s", "DESCRIPTION", "Geoscience Australia");
	srt.refline = line;
	theSinex.refstrings.push_back(srt);

	// FIXME: network solution could be different?
	sprintf(line, " %-18s %s", "OUTPUT", "PPP Solution");
	srt.refline = line;
	theSinex.refstrings.push_back(srt);

	sprintf(line, " %-18s %s", "CONTACT", "npi@ga.gov.au");
	srt.refline = line;
	theSinex.refstrings.push_back(srt);

	// TODO: replace 0.1 with some auto generated variable or config file entry, should be current version in bitbucket
	sprintf(line, " %-18s %s", "SOFTWARE", "Ginan PEA Version 0.1");
	srt.refline = line;
	theSinex.refstrings.push_back(srt);

	int result = uname(&buf);

	if (result == 0)
	{
		int len;
		sprintf(line, " %-18s ", "HARDWARE");
		len = strlen(line);

		if ((len + strlen(buf.sysname)) < 80)
		{
			strcat(line, buf.sysname);
			strcat(line, " ");
			len = strlen(line);
		}

		if ((len + strlen(buf.release)) < 80)
		{
			strcat(line, buf.release);
			strcat(line, " ");
			len = strlen(line);
		}

		if ((len + strlen(buf.version)) < 80)
		{
			strcat(line, buf.version);
			strcat(line, " ");
		}

		srt.refline = line;
		theSinex.refstrings.push_back(srt);
	}

	sprintf(line, " %-18s %s", "INPUT", "RINEX");
	srt.refline = line;
	theSinex.refstrings.push_back(srt);

	return 0;
}

void sinex_add_acknowledgement(const string& who, const string& description)
{
	newsnx_ack_t 	sat;

	sat.agency = who.substr(0, 3);
	sat.description = description;

	theSinex.acknowledgements.push_back(sat);
}

void sinex_add_comment(const string& what)
{
	newsnx_comment_t sct;

	sct.cmtline = what;

	theSinex.commentstrings.push_back(sct);
}

void sinex_add_file(const string& who, const GTime& when, const string& filename, const string& description)
{
	double 				ep[6];
	int					yds[3];
	newsnx_input_file_t	sif;

	time2epoch(when, ep);
	epoch2yds(ep, yds);


	for (int i = 0; i < 3; i++)
		sif.epoch[i] = yds[i];

	sif.agency			= who;
	sif.file			= filename;
	sif.description		= description;

	theSinex.inputFiles.push_back(sif);
}

void sinex_report()
{
	cout << "count of refstrings = " 					<< theSinex.refstrings			.size() << endl;
	cout << "count of comments = " 						<< theSinex.commentstrings		.size() << endl;
	cout << "count of history comments = " 				<< theSinex.historyComments		.size() << endl;
	cout << "count of input histories = " 				<< theSinex.inputHistory		.size() << endl;
	cout << "count of file comments = " 				<< theSinex.filesComments		.size() << endl;
	cout << "count of input files = " 					<< theSinex.inputFiles			.size() << endl;
	cout << "count of acknowledgement comments = " 		<< theSinex.ackComments			.size() << endl;
	cout << "count of acknowledgements = " 				<< theSinex.acknowledgements	.size() << endl;

	cout << "count of site id comments = " 				<< theSinex.siteIdcomments		.size() << endl;
	cout << "count of site ids = " 						<< theSinex.list_siteids		.size() << endl;
	cout << "count of site data comments = " 			<< theSinex.siteDatacomments	.size() << endl;
	cout << "count of site datas = " 					<< theSinex.list_sitedata		.size() << endl;
	cout << "count of receiver comments = " 			<< theSinex.receivercomments	.size() << endl;
	cout << "count of receivers = " 					<< theSinex.list_receivers		.size() << endl;
	cout << "count of antenna comments = " 				<< theSinex.antennacomments		.size() << endl;
	cout << "count of antennas = " 						<< theSinex.list_antennas		.size() << endl;
	cout << "count of site eccentricity comments = " 	<< theSinex.site_ecc_comments	.size() << endl;
	cout << "count of site eccentricities = " 			<< theSinex.list_site_eccs		.size() << endl;
	cout << "count of GPS phase centre comments = " 	<< theSinex.gps_pc_comments		.size() << endl;
	cout << "count of GPS phase centres = " 			<< theSinex.list_gps_pcs		.size() << endl;
	cout << "count of GAL phase cnetre comments = " 	<< theSinex.gal_pc_comments		.size() << endl;
	cout << "count of GAL phase centres = " 			<< theSinex.list_gal_pcs		.size() << endl;

	cout << "solutions have bias = " 					<< theSinex.epochs_have_bias << endl;
	cout << "count of epoch comments = " 				<< theSinex.epochcomments		.size() << endl;
	cout << "count of solution epochs = " 				<< theSinex.list_solepochs		.size() << endl;
	cout << "count of statistics comments = " 			<< theSinex.statistics_comments	.size() << endl;
	cout << "count of statistics = " 					<< theSinex.list_statistics		.size() << endl;
	cout << "count of estimate comments = " 			<< theSinex.estimate_comments	.size() << endl;
	cout << "count of estimates = " 					<< theSinex.estimates_map		.size() << endl;
	cout << "count of apriori comments = " 				<< theSinex.apriori_comments	.size() << endl;
	cout << "count of aprioris = " 						<< theSinex.apriori_map			.size() << endl;
	cout << "count of normal equation comments = " 		<< theSinex.normal_eqns_comments.size() << endl;
	cout << "count of normal equations = " 				<< theSinex.list_normal_eqns	.size() << endl;
	cout << "count of matrix comments = " 				<< theSinex.matrix_comments		.size() << endl;
	int mcount = 0;

	if (theSinex.tri[ESTIMATE][CORRELATION] 	!= ' ')		mcount++;
	if (theSinex.tri[ESTIMATE][COVARIANCE] 		!= ' ')		mcount++;
	if (theSinex.tri[ESTIMATE][INFORMATION] 	!= ' ')		mcount++;

	cout << "count of matrix estimates = " << mcount << endl;
	mcount = 0;

	if (theSinex.tri[APRIORI][CORRELATION] 		!= ' ')		mcount++;
	if (theSinex.tri[APRIORI][COVARIANCE] 		!= ' ')		mcount++;
	if (theSinex.tri[APRIORI][INFORMATION] 		!= ' ')		mcount++;

	cout << "count of matrix apriori = " << mcount << endl;
	mcount = 0;

	if (theSinex.tri[NORMAL_EQN][CORRELATION] 	!= ' ')		mcount++;
	if (theSinex.tri[NORMAL_EQN][COVARIANCE] 	!= ' ')		mcount++;
	if (theSinex.tri[NORMAL_EQN][INFORMATION] 	!= ' ')		mcount++;

	cout << "count of matrix normals = " << mcount << endl;

	cout << "count of satid comments = " 			<< theSinex.satid_comments		.size() << endl;
	cout << "count of sat IDs = " 					<< theSinex.list_satids			.size() << endl;
	cout << "count of satident comments = " 		<< theSinex.satident_comments	.size() << endl;
	cout << "count of sat idents = " 				<< theSinex.list_satidents		.size() << endl;
	cout << "count of prn comments = " 				<< theSinex.satprn_comments		.size() << endl;
	cout << "count of prns = " 						<< theSinex.list_satprns		.size() << endl;
	cout << "count of freq channel comments = " 	<< theSinex.satfreqchn_comments	.size() << endl;
	cout << "count of freq channels = " 			<< theSinex.list_satfreqchns	.size() << endl;
	cout << "count of mass comments = " 			<< theSinex.satmass_comments	.size() << endl;
	cout << "count of satmasses = " 				<< theSinex.list_satmasses		.size() << endl;
	cout << "count of COM comments = " 				<< theSinex.satcom_comments		.size() << endl;
	cout << "count of sat COMs = "			 		<< theSinex.list_satcoms		.size() << endl;
	cout << "count of sat ecc comments = " 			<< theSinex.satecc_comments		.size() << endl;
	cout << "count of sat eccentricities = " 		<< theSinex.list_sateccs		.size() << endl;
	cout << "count of sat power comments = " 		<< theSinex.satpower_comments	.size() << endl;
	cout << "count of sat powers = " 				<< theSinex.list_satpowers		.size() << endl;
	cout << "count of sat phase centre comments = " << theSinex.satpc_comments		.size() << endl;
	cout << "count of sat phase centres = " 		<< theSinex.list_satpcs			.size() << endl;
	cout << "count of source ID comments = " 		<< theSinex.sourceid_comments	.size() << endl;
	cout << "count of source IDs = " 				<< theSinex.list_source_ids		.size() << endl;
	cout << "count of nutation comments = " 		<< theSinex.nutation_comments	.size() << endl;
	cout << "count of nutations = " 				<< theSinex.list_nutcodes		.size() << endl;
	cout << "count of precession_comments = "	 	<< theSinex.precession_comments	.size() << endl;
	cout << "count of precessions = " 				<< theSinex.list_precessions	.size() << endl;
}

bool compare_stn_estimates(
	newsnx_stn_soln_t& left,
	newsnx_stn_soln_t& right)
{
	// compare first on type, then on epoch
	if (left.type.compare(right.type) == 0)
	{
		return (time_compare(left.yds, right.yds) < 0);
	}
	else
	{
		int ltype = 0;
		int rtype = 0;
		string s = trim(left.type);

		try
		{
			ltype = E_Estimate::_from_string(s.c_str());
		}
		catch (...)		{		}

		s = trim(right.type);

		try
		{
			rtype = E_Estimate::_from_string(s.c_str());
		}
		catch (...)		{		}

		return (ltype < rtype);
	}
}

int sinex_site_count()
{
	return theSinex.list_siteids.size();
}

int sinex_sat_count()
{
	int result = theSinex.list_satidents.size();

	if (result == 0)
		result = theSinex.list_satids.size();

	return result;
}

// return value is 0 for success. Otherwise the value indicates the section where data was not found.
// 1 = siteid
// 2 = recevier
// 3 = antenna
// 4 = eccentricity
// 5 = gps phase center
// 6 = estimate
int getstnsnx(string station, int yds[3], newsnx_stn_snx_t& stn_snx)
{
	// while the maps are not yet built, just search the lists individually for what we need..
	// TODO: rewrite this function to do a single map search
	bool	found 		= false;
	int		zeros[3] 	= {};
	int		retval 		= 0;
	stn_snx.has_estimates	= false;
	stn_snx.has_ecc 		= false;
	stn_snx.has_gps_pc 		= false;
	stn_snx.has_gal_pc 		= false;
	stn_snx.has_antenna 	= false;
	stn_snx.has_receiver 	= false;

	for (int i = 0; i < 3; i++)
	{
		stn_snx.start[i]	= yds[i];
		stn_snx.stop[i]		= yds[i];
	}

	// search siteids for station (not time dependent)
	for (auto& siteId : theSinex.list_siteids)
	{
		if (station.compare(siteId.sitecode) == 0)
		{
			stn_snx.sitecode	= station;
			stn_snx.ptcode		= siteId.ptcode;
			stn_snx.monuid		= siteId.domes;
			stn_snx.typecode	= siteId.typecode;
			stn_snx.desc		= siteId.desc;
			stn_snx.long_deg	= siteId.long_deg;
			stn_snx.long_min	= siteId.long_min;
			stn_snx.long_sec	= siteId.long_sec;
			stn_snx.lat_deg		= siteId.lat_deg;
			stn_snx.lat_min		= siteId.lat_min;
			stn_snx.lat_sec		= siteId.lat_sec;
			stn_snx.height		= siteId.height;
			found = true;
			break;
		}
	}

	if (!found)
		return 1;

	found = false;

	for (auto it = theSinex.list_receivers.begin(); it != theSinex.list_receivers.end(); it++)
	{
		if 	( ( station.compare(it->sitecode)		== 0)
			&&( time_compare(it->recstart, yds)		<= 0
			  ||time_compare(it->recstart, zeros)	== 0)
			&&( time_compare(yds, it->recend)		<= 0
			  ||time_compare(it->recend, zeros)		== 0))
		{
			stn_snx.rectype			= it->rectype;
			stn_snx.recsn			= it->recsn;
			stn_snx.recfirm			= it->recfirm;
			stn_snx.has_receiver	= true;
			found = true;
			bool dostart = true;
			bool dostop = true;

			for (int i = 0; i < 3; i++)
			{
				if (dostart)
					stn_snx.start[i] = it->recstart[i];

				if (dostop)
					stn_snx.stop[i] = it->recend[i];
			}

			break;
		}
	}

	if (!found)
		retval = 2;

	found = false;

	for (auto it = theSinex.list_antennas.begin(); it != theSinex.list_antennas.end(); it++)
	{
		if 	( (station.compare(it->sitecode)		== 0)
			&&( time_compare(it->antstart, yds)		<= 0
			  ||time_compare(it->antstart, zeros)	== 0)
			&&( time_compare(yds, it->antend)		<= 0
			  ||time_compare(it->antend, zeros)		== 0))
		{
			stn_snx.anttype = it->anttype;
			stn_snx.antsn	= it->antsn;

			if 	( time_compare(stn_snx.start, zeros)		== 0
				||time_compare(stn_snx.start, it->antstart)	< 0)
			{
				for (int i = 0; i < 3; i++)
				{
					stn_snx.start[i] = it->antstart[i];
				}
			}

			if 	( time_compare(stn_snx.stop, zeros)			== 0
				||time_compare(stn_snx.stop, it->antend)	> 0)
			{
				for (int i = 0; i < 3; i++)
				{
					stn_snx.stop[i] = it->antend[i];
				}
			}

			stn_snx.has_antenna	= true;
			found				= true;
			break;
		}
	}

	if (!found)
		retval = 3;

	found = false;

	for (auto it = theSinex.list_site_eccs.begin(); it != theSinex.list_site_eccs.end(); it++)
	{
		if 	( ( station.compare(it->sitecode)		== 0)
			&&( time_compare(it->eccstart, yds)		<= 0
			  ||time_compare(it->eccstart, zeros)	== 0)
			&&( time_compare(yds, it->eccend)		<= 0
			  ||time_compare(it->eccend, zeros)		== 0))
		{
			stn_snx.eccrs = it->eccrs;

			for (int i = 0; i < 3; i++)
				stn_snx.ecc[i] = it->ecc[i];

			if 	( time_compare(stn_snx.start, zeros)		== 0
				||time_compare(stn_snx.start, it->eccstart)	< 0)
			{
				for (int i = 0; i < 3; i++)
				{
					stn_snx.start[i] = it->eccstart[i];
				}
			}

			if 	( time_compare(stn_snx.stop, zeros)			== 0
				||time_compare(stn_snx.stop, it->eccend)	> 0)
			{
				for (int i = 0; i < 3; i++)
				{
					stn_snx.stop[i] = it->eccend[i];
				}
			}

			stn_snx.has_ecc	= true;
			found			= true;

			break;
		}
	}

	if (!found)
		retval = 4;

	found = false;

	for (auto it = theSinex.list_gps_pcs.begin(); it != theSinex.list_gps_pcs.end(); it++)
	{
		if 	( (stn_snx.anttype.compare(it->antname) == 0)
			&&(stn_snx.antsn.compare(it->serialno)	== 0))
		{
			for (int i = 0; i < 3; i++)
			{
				stn_snx.gpsl1[i] = it->L1[i];
				stn_snx.gpsl2[i] = it->L2[i];
			}

			stn_snx.has_gps_pc	= true;
			found				= true;
			break;
		}
	}

	if (!found)
		retval = 5;

	found = false;

	for (auto& [index, estimate] : theSinex.estimates_map)
	{
		if 	( (station.compare(estimate.sitecode)	== 0)
			&&(time_compare(estimate.refepoch, yds)	<= 0))
		{
			newsnx_stn_soln_t newestimate;

			newestimate.type	= estimate.type;
			newestimate.unit	= estimate.unit;
			newestimate.pos		= estimate.estimate;
			newestimate.pstd	= estimate.stddev;

			for (int i = 0; i < 3; i++)
			{
				newestimate.yds[i] = estimate.refepoch[i];
			}

			stn_snx.estimates.push_back(newestimate);
			stn_snx.has_estimates	= true;
			found					= true;
			/* no break because there could be multiple types */
		}
	}

	if (found)
	{
		// sort the list based on the type, then time. Ensure only one entry of each type - we need the one with the latest time
		stn_snx.estimates.sort(compare_stn_estimates);
		auto cur = stn_snx.estimates.begin();

		for (auto it = stn_snx.estimates.begin(); it != stn_snx.estimates.end(); it++)
		{
			if (it == stn_snx.estimates.begin())
			{
				cur = it;
			}
			else
			{
				if (cur->type.compare(it->type) == 0)
				{
					stn_snx.estimates.erase(cur);
					cur = it;
				}
				else
					cur = it;
			}
		}

		// After this check we have STAX, STAY, & STAZ entries. If one is missing update has_estimates to false
		// NB must use 6 char length strings because this is the size of the field ...
		cur = stn_snx.estimates.begin();

		if (cur->type.compare("STAX  ") == 0)
		{
			cur++;

			if (cur->type.compare("STAY  ") == 0)
			{
				cur++;

				if (cur->type.compare("STAZ  ") != 0)
				{
					stn_snx.has_estimates	= false;
					found					= false;
				}
			}
			else
			{
				stn_snx.has_estimates	= false;
				found					= false;
			}
		}
		else
		{
			stn_snx.has_estimates	= false;
			found					= false;
		}

		if (!found)
		{
			retval = 6;
		}
	}
	else
	{
		retval = 6;
	}

	return retval;
}

void newsnx_stn_snx_t::getPosEstimates(Vector3d& val, Vector3d& std)
{
	for (auto& estimate : estimates)
	{
		if (estimate.type.compare("STAX  ") == 0)
		{
			val(0) = estimate.pos;
			std(0) = estimate.pstd;
		}
		if (estimate.type.compare("STAY  ") == 0)
		{
			val(1) = estimate.pos;
			std(1) = estimate.pstd;
		}
		if (estimate.type.compare("STAZ  ") == 0)
		{
			val(2) = estimate.pos;
			std(2) = estimate.pstd;
		}
	}
}

// void sinex_update_estimate(
// 	const newsnx_stn_snx_t& station,
// 	E_Estimate e,
// 	double val,
// 	double std,
// 	int yds[3],
// 	int newidx,
// 	double defstddev)
// {
// 	char buf[16];
// 	bool done = false;
// 	newsnx_solestimate_t sst;
//
// 	sprintf(buf, "%s", e._to_string());
// 	auto saved = theSinex.list_estimates.end();
//
// 	for (auto [index, estimate] : theSinex.estimates_map)
// 	{
// 		if 	(  estimate.sitecode.compare(station.sitecode) == 0
// 			&& strncmp(estimate.type.c_str(), buf, strlen(buf)) == 0)
// 		{
// 			//exists
//
//
// 			estimate.index = newidx;
// 			sst = estimate;
// 			int diff = time_compare(it->refepoch, yds);
//
// 			// time matches. update estimate and stddev, then exit
// 			if (diff == 0)
// 			{
// 				it->estimate	= val;
// 				it->stddev		= std;
// 				done = true;
// 				break;
// 			}
//
// 			if (diff < 0)
// 			{
// 				saved = it;
// 				continue;
// 			}
//
// 			// won't get here unless station is equal, type is equal and refepoch is after given time
// 			sst.estimate	= val;
// 			sst.stddev		= std;
//
// 			for (int i = 0; i < 3; i++)
// 				sst.refepoch[i] = yds[i];
//
// 			theSinex.estimates_map[sst.index] = sst;
// 			done = true;
// 			break;
// 		}
// 	}
//
// 	// possible we did not insert any value because no eposh for that site/type is later than yds ...
// 	if (!done)
// 	{
// 		if (sst.sitecode.empty())
// 		{
// 			sst.index		= newidx;
// 			sst.type		= buf;
// 			sst.sitecode	= station.sitecode;
// 			sst.ptcode		= station.ptcode;
// 			sst.solnnum		= 1;
// 			sst.unit		= "m";
// 			sst.constraint	= ' ';
// 		}
//
// 		sst.estimate	= val;
// 		sst.stddev		= std;
//
// 		for (int i = 0; i < 3; i++)
// 		{
// 			sst.refepoch[i] = yds[i];
// 		}
//
// 		theSinex.estimates_map[sst.index] = sst;
// 	}
//
// 	// if saved not the end iterator, move that value out of the estimates list and insert into the apriori list
// 	// use the config parm as the stddev for the apriori value
// 	if (saved != theSinex.list_estimates.end())
// 	{
// 		sst = *saved;
// 		theSinex.list_estimates.erase(saved);
//
// 		for (auto it = theSinex.list_apriori.begin(); it != theSinex.list_apriori.end(); )
// 		{
// 			if 	( it->sitecode.compare(sst.sitecode) == 0
// 				&&strncmp(it->param_type.c_str(), buf, strlen(buf)) == 0
// 				&&time_compare(it->epoch, sst.refepoch) <= 0)
// 			{
// 				it = theSinex.list_apriori.erase(it);
// 			}
// 			else
// 			{
// 				it++;
// 			}
// 		}
//
// 		if (sst.stddev < defstddev)
// 			sst.stddev = defstddev;
//
// 		newsnx_solapriori_t sat;
// 		sat.idx			= newidx;
// 		sat.param_type	= sst.type;
// 		sat.sitecode	= sst.sitecode;
// 		sat.ptcode		= sst.ptcode;
// 		sat.solnnum		= sst.solnnum;
//
// 		for (int i = 0; i < 3; i++)
// 			sat.epoch[i] = sst.refepoch[i];
//
// 		sat.unit		= sst.unit;
// 		sat.constraint	= sst.constraint;
// 		sat.param		= sst.estimate;
// 		sat.stddev		= sst.stddev;
//
// 		theSinex.list_apriori.push_back(sat);
// 	}
// }

// necessary for the caller to know if lower or upper triangle!
// void sinex_update_matrix(matrix_type mt, matrix_value mv, int row, int col, int nvals, double val[3])
// {
// 	// TODO: do not allow updates to apriori matrices. When the estimate matrices are updated, move the old values to the apriori matrix
// 	for (auto& mapEntry : theSinex.matrix_map[mt][mv])
// 	{
// 		if 	(  mapEntry.row == row
// 			&& mapEntry.col == col)
// 		{
// 			for (int i = 0; i < nvals; i++)
// 			{
// 				mapEntry.value[col - mapEntry.col + i] = val[i];
// 			}
//
// 			mapEntry.numvals = nvals;
//
// 			return;
// 		}
// 	}
//
// 	// we did not find the entry. create a new one and add it to the list
// 	newsnx_solmatrix_t sst;
//
// 	sst.row		= row;
// 	sst.col		= col;
// 	sst.numvals	= nvals;
//
// 	for (int i = 0; i < nvals; i++)
// 	{
// 		sst.value[i] = val[i];
// 	}
//
// 	theSinex.matrix_map[mt][mv].push_back(sst);
// }

// return 1-up index if found, otherwise -1 to indicate error
int get_snx_stn_stax_idx(string& station)
{
	for (auto& [index, estimate] : theSinex.estimates_map)
	{
		if 	( estimate.sitecode.compare(station) == 0
			&&estimate.type.compare("STAX  ")	== 0)
		{
			return estimate.index;
		}
	}

	return -1;
}

char get_snx_matrix_tri(matrix_type mt, matrix_value mv)
{
	return theSinex.tri[mt][mv];
}

void sinex_update_matrix_tri(matrix_type mt, matrix_value mv, char c)
{
	theSinex.tri[mt][mv] = c;
}



void sinexPostProcessing(
	GTime&						tsync,
	map<string, Station>&		stationMap,
	KFState&					netKFState)
{
	theSinex.inputFiles.		clear();
	theSinex.acknowledgements.	clear();
	theSinex.inputHistory.		clear();

	sinex_check_add_ga_reference();

	{
		list<KFState*> kfStatePointers;

		if (acsConfig.process_user)
		for (auto& [key, rec] : stationMap)
		{
			kfStatePointers.push_back(&rec.rtk.pppState);
		}

		if (acsConfig.process_network)
		{
			kfStatePointers.push_back(&netKFState);
		}

		// push the covar matrix to the sinex object
		theSinex.kfState = mergeFilters(kfStatePointers);
	}

	char tri = get_snx_matrix_tri(ESTIMATE, COVARIANCE);

	if 	( tri != 'L'
		&&tri != 'U')
	{
		sinex_update_matrix_tri(ESTIMATE, COVARIANCE, 'L');
	}

	// add in the files used to create the solution
	for (auto& rnxfile : acsConfig.rinexFiles)
	{
		sinex_add_file(acsConfig.analysis_agency, tsync, rnxfile, "RINEX v3.x");
	}

	for (auto& sp3file : acsConfig.sp3files)
	{
		sinex_add_file(acsConfig.analysis_agency, tsync, sp3file, "SP3");
	}

	for (auto& snxfile : acsConfig.snxfiles)
	{
		sinex_add_file(acsConfig.analysis_agency, tsync, snxfile, "SINEX");
	}

	// Add other statistics as they become available...
	sinex_add_statistic("SAMPLING INTERVAL (SECONDS)", acsConfig.epoch_interval);

	// FIXME here ...
	char obsCode	= ' ';
	char constCode	= ' ';

	string solcont = "ST";
	// uncomment next bit once integrated
	// if (acsConfig.orbit_output) solcont += 'O';


	string data_agc = "";

	// default this to config. If not set, get it from the first station read.
	boost::posix_time::ptime start_epoch = acsConfig.start_epoch;
	GTime start_time;
	start_time.time = static_cast<int>(boost::posix_time::to_time_t(start_epoch));
	start_time.sec	= 0;
	double	ep[6];
	int		start[3];
	int		end[3];
	time2epoch(start_time, ep);
	epoch2yds(ep, start);
	time2epoch(tsync, ep);
	epoch2yds(ep, end);
	struct timeval tv;
	struct tm* tmbuf;
	gettimeofday(&tv, NULL);
	tmbuf = gmtime(&tv.tv_sec);
	int create_yds[3]; // create time for Sinex header
	create_yds[0] = tmbuf->tm_year + 1900;
	create_yds[1] = tmbuf->tm_yday;
	create_yds[2] = tmbuf->tm_sec + 60 * tmbuf->tm_min + 3600 * tmbuf->tm_hour;


	sinex_update_header(acsConfig.analysis_agency.c_str(), create_yds, data_agc.c_str(), start, end, obsCode, constCode, solcont.c_str());

	list<newsnx_stn_snx_t>	stnlist;
	for (auto& [key, rec] : stationMap)
	{
		stnlist.push_back(rec.snx);
	}

	write_sinex(acsConfig.sinex_filename, &stnlist);
}

void sinexPerEpochPerStation(GTime&		tsync,
	Station&	rec)
{
	// check the station data for currency. If later that the end time, refresh Sinex data
	double ep[6];
	int yds[3];
	int defaultStop[3] = {-1,-1,-1};

	time2epoch(tsync, ep);
	epoch2yds(ep, yds);

	if 	(  time_compare(rec.snx.stop, yds)			<  0
		|| time_compare(rec.snx.stop, defaultStop)	== 0)
	{
// 		BOOST_LOG_TRIVIAL(debug)
// 		<< "station "			<< rec.id
// 		<< " out of date - "	<< rec.snx.stop[0] << ":" << rec.snx.stop[1] << ":" << rec.snx.stop[2]
// 		<< " is less than "		<< yds[0] << ":" << yds[1] << ":" << yds[2]
// 		<< ", rebuilding"		<< endl;

		int result = getstnsnx(rec.id, yds, rec.snx);

		if (result == 6)
			return; // No current station position estimate!

		rec.rtk.opt.anttype	= rec.snx.anttype;
		rec.rtk.opt.antdel	= rec.station.del;		//todo aaron, not nice, only place its set, make more generic


		Vector3d dummy;
		rec.snx.getPosEstimates(rec.station.rRec, dummy);
		rec.snx.pos = rec.station.rRec;


		// Initialise the receiver antenna information
		for (bool once : {1})
		{
			string nullstring	= "";
			string tmpant		= rec.rtk.opt.anttype;
			rec.rtk.pcvrec = findantenna(tmpant, nullstring, ep, nav.pcvList, 0);

			if (rec.rtk.pcvrec)
			{
				//all good, carry on
				break;
			}

			// Try searching under the antenna type with DOME => NONE
			radome2none(tmpant);

			rec.rtk.pcvrec = findantenna(tmpant, nullstring, ep, nav.pcvList, 0);

			if (rec.rtk.pcvrec)
			{
				BOOST_LOG_TRIVIAL(warning)
				<< "Using \"" << tmpant
				<< "\" instead of: \"" << rec.rtk.opt.anttype
				<< "\" for radome of " << rec.id;

				break;
			}
			else
			{
				BOOST_LOG_TRIVIAL(error)
				<< "No information for antenna " << rec.rtk.opt.anttype;

				break;
			}
		}
	}
}

# Precise Point Positioning Examples (PPP)

## Using the Ionosphere free observable to process a static data set

In this example we will process 24 hours of data from a permanent reference frame station. The algorithm that will be use an L1+L2 and L1+L5 ionosphere free combination.
The log files and processing results can be found in `/data/acs/pea/output/exs/EX01_IF/`.

    $ ./pea --config ../../config/Ex02.yaml


Table 1.2062.4   GPS week: 2062   Day: 4   MJD: 58682.000

CENT STA|   DX    DY    DZ    RX    RY    RZ   SCL  RMS  WRMS|    TOFT   TDRFT   SDEV    RMS
        |  [mm]  [mm]  [mm] [uas] [uas] [uas] [ppb] [mm] [mm]|    [ps]  [ps/d]   [ps]   [ps]
--------+----------------------------------------------------+------------------------------
brd  n/a|   28     3   134  -673   131 -5867-16.35  832  832 |     -13     -73   1000   2264
cod  121|    0     0     0    20   -31    11  -.08    8    7 |    -797    -168      9    289
emr  111|    0     0     3    27    91   -45  -.08   11   11 |   -1668    1127     19    571
esa  110|   -1     0    -4   -20   -27   -60  -.06   10    9 |     -26   -1763     10    431
gfz  129|   -2     0     3   -43    14     8   .10   23   10 |   -1085     442     18    347
igc  n/a|    1     0     2     0    30  -134  -.14   15   15 |    -573     449     73    357
jpl   80|    1     2     0    55    -3    20   .19    9    9 |  211408     177     12    288
ngs  193|    0    -1    -1   -24   -16     4   .02    8    8 |      -4     -73    977   2120
sio   93|   -2    -1     5   -10    -3    33   .09   17   17 |     ---     ---    ---    ---
pea  147|    1     0     5    -6     4   -34  -.11   14   14 |  215952    -485    192   2304
whu  126|    0    -1     0   -18    -8    63  -.11   13   12 |    1500     313     15   1076


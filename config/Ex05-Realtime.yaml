
# Ex05 - Realtime Example

input_files:

    root_input_directory: /data/acs/pea/proc/exs/products/

    atxfiles:   [ igs14_2045_plus.atx                                ]    # required
    snxfiles:   [ igs19P2062.snx                                     ]    # required
    blqfiles:   [ OLOAD_GO.BLQ                                       ]    # required if ocean loading is applied
    navfiles:   [ brdm1990.19p                                       ]    # required but shouldn't!
    sp3files:   [ igs20624.sp3                                       ]
    erpfiles:   [ igs19P2062.erp                                     ]    #config parser doesn't accept weekly files yet.
    dcbfiles:   [ CAS0MGXRAP_20191990000_01D_01D_DCB.BSX             ]    # monthly DCB file
    #clkfiles:   [     jpl20624.clk                                   ]   # Clk file
    orbfiles:   [ orb_partials/gag20624_orbits_partials_new.out      ]    # only need this if we are estimating orbits

station_data:

    stream_root: "http://cmarshall:MQiSPoEj2e@auscors.ga.gov.au:2101/"

    streams:
        - ALBY00AUS0
        - SYDN00AUS0
        - IGS01


output_files:

    root_output_directory:          /data/acs/pea/output/exs/<CONFIG>/

    log_level:                      warn                             # debug, info, warn, error as defined in boost::log
    log_directory:                  ./
    log_filename:                   <CONFIG>-<YYYY><DDD><HH>.LOG

    output_trace:                   true
    trace_level:                    2
    trace_directory:                ./
    trace_filename:                 <CONFIG>-<STATION><YYYY><DDD><HH>.TRACE

    output_residuals:               true
    residuals_directory:            ./
    residuals_filename:             <CONFIG>-<YYYY><DDD><HH>.RES

    output_summary:                 true
    summary_directory:              ./
    summary_filename:               <CONFIG>-<YYYY><DDD><HH>.SUM

    output_ionex:                   false
    ionex_directory:                ./
    ionex_filename:                 IONEX.ionex
    iondsb_filename:                IONEX.iondcb

    output_plots:                   true
    plot_directory:                 ./
    plot_filename:                  plot.png

    output_clocks:                  false
    clocks_directory:               ./
    clocks_filename:                <CONFIG>.clk

    output_persistance:             true
    persistance_directory:          ./
    persistance_filename:           <CONFIG>.persist


output_options:

    config_description:             Ex05
    analysis_agency:                GAA
    analysis_center:                Geoscience Australia
    analysis_program:               AUSACS
    rinex_comment:                  AUSNETWORK1


processing_options:

    #start_epoch:               2019-07-18 23:39:00
    #end_epoch:                 2017-03-29 23:59:30
    #max_epochs:                 500        #0 is infinite
    epoch_interval:             10          #seconds
    wait_next_epoch:            11
    wait_all_stations:          2

    ppp_ephemeris:              ssr_apc     # broadcast  ssr_apc  ssr_com  precise

    process_modes:
        user:                   true
        network:                false
        minimum_constraints:    false
        rts:                    false
        ionosphere:             false
        unit_tests:             false

    process_sys:
        gps:            true
        glo:            true
        #gal:           true
        #bds:           true

    elevation_mask:     10   #degrees

    tide_solid:         true
    tide_pole:          true
    tide_otl:           true

    phase_windup:       true
    reject_eclipse:     true            #  reject observation during satellite eclipse periods
    raim:               true
    antexacs:           true

    cycle_slip:
        thres_slip: 0.05

    max_inno:   0
    max_gdop:   30

    troposphere:
        model:      vmf3    #gpt2
        vmf3dir:    grid5/
        orography:  orography_ell_5x5
        # gpt2grid: EX03/general/gpt_25.grd

    ionosphere:
        corr_mode:      iono_free_linear_combo
        iflc_freqs:     l1l2_only   #any l1l2_only l1l5_only

    pivot_station:        "USN7"
    pivot_satellite:      "G01"


user_filter_parameters:

    max_filter_iterations:      2
    max_filter_removals:        2

    rts_lag:                    20      #-ve for full reverse, +ve for limited epochs
    rts_directory:              ./
    rts_filename:               PPP-<CONFIG>-<STATION>.rts


default_filter_parameters:

    stations:

        error_model:        elevation_dependent         #uniform elevation_dependent
        code_sigmas:        [0.15]
        phase_sigmas:       [0.0015]

        pos:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0]
            #apriori:                                   # taken from other source, rinex file etc.
            #frame:              xyz #ned
            #proc_noise_model:   Gaussian

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [1.8257418583505538]
            #proc_noise_model:   Gaussian

        clk_rate:
            estimated:          false
            sigma:              [0]
            proc_noise:         [1]

        amb:
            estimated:          true
            sigma:              [60]
            proc_noise:         [0]
            #proc_noise_dt:      day
            #proc_noise_model:   RandomWalk

        trop:
            estimated:          true
            sigma:              [0.1]
            proc_noise:         [0.3]
            proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

        trop_grads:
            estimated:          true
            sigma:              [1]
            proc_noise:         [0]
            #proc_noise_dt:      hour
            #proc_noise_model:   RandomWalk

    satellites:

        clk:
            estimated:          true
            sigma:              [0]
            proc_noise:         [0.03651483716701108]
            #proc_noise_dt:      min
            #proc_noise_model:   RandomWalk

        clk_rate:
            estimated:          false
            sigma:              [0]
            proc_noise:         [1]

        orb:
            estimated:          false


override_filter_parameters:

    stations:

        ALIC:
            pos:
                sigma:              [60]
                proc_noise:         [0]
                ##proc_noise_model:   Gaussian
            #clk:
                #sigma:              [0.01]

        #AGGO:
            #exclude: true
        #ALIC:
            #exclude: true
        #ANKR:
            #exclude: true
                #network_estimated:  false

    satellites:

        ### Constellation Overrides
        #SYS_GPS:
            #srp:
                #sigma:              [0.01]
                #proc_noise:         [0.001]
                ##proc_noise_model:   RandomWalk

        #SYS_GAL:
            #clk:
                #sigma:              [1.0]
                #proc_noise:         [0.01]
                ##proc_noise_model:   RandomWalk

        ### Block Type Overrides
        #GPS-IIR-A:
            #pos: { sigma: [10.0] }
            #vel: { sigma: [0.1] }

        ### PRN Overrides
        #PRN_G10:
            #pos: { sigma: [10.0]    }
            #vel: { sigma: [0.1]     }
            #clk: { sigma: [1.0]     }

        #PRN_G15:
            #exclude: true

        ### SVN Overrides
        #SVN_G265:
            #pos: {sigma: [10.0] }
            #vel: {sigma: [0.1]  }

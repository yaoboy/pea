sudo apt update
sudo apt -y install git gobjc gobjc++ gfortran openssl curl net-tools openssh-server cmake make gzip vim jq python3-cartopy python3-scipy libopenblas-dev python3-matplotlib python3-mpltoolkits.basemap

#yaml
git clone https://github.com/jbeder/yaml-cpp.git
cd yaml-cpp
mkdir cmake-build
cd cmake-build
cmake .. -DCMAKE\_INSTALL\_PREFIX=/usr/local/ -DYAML\_CPP\_BUILD\_TESTS=OFF
sudo make install yaml-cpp
cd ../..
sudo rm -fr yaml-cpp

#boost
wget -c https://dl.bintray.com/boostorg/release/1.73.0/source/boost_1_73_0.tar.gz
gunzip boost_1_73_0.tar.gz
tar xvf boost_1_73_0.tar
cd boost_1_73_0/
./bootstrap.sh
sudo ./b2 install
cd ..
sudo rm -fr boost_1_73_0/ boost_1_73_0.tar

#eigen
git clone https://gitlab.com/libeigen/eigen.git
cd eigen
mkdir cmake-build
cd cmake-build
cmake ..
sudo make install
cd ../..
sudo rm -rf eigen

# Run the single frequency PPP processing for each file in the iontest example
# three stations fail at the moment
# 1) BMAN
# 2) GGTN 
# 3) KARR
# Takes about 9-10 to complete ..
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ALBY00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ANDA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ARD200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ARMC00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ARUB00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx AUCK00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BALA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BARR00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BBOO00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BDLE00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BDST00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BDVL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BEE200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BING00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BKNL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BMAN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BNDY00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BRLA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BRO100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BROC00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BULA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BUR200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx BURA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx CBLA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx CBLT00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx CEDU00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx CHTI00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx CLEV00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx CNBN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx COEN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx COOB00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx COOL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx DALB00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx DARW00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx DAV100ATA_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx DODA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx DUND00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ECOR00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx EDSV00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ERMG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ESPA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx EXMT00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx FLND00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx GABO00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx GASC00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx GGTN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx GLB200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx GNGN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx GROT00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx HERN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx HIL100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx HOB200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx HUGH00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx HYDN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx IHOE00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx JAB200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx JLCK00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KAIK00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KALG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KARR00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KAT100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KAT200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KELN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KGIS00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KILK00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KIRR00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx KUNU00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LAMB00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LARR00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LAUT00FJI_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LILY00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LKYA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LONA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LORD00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx LURA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MAC100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MAJU00MHL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MAW100ATA_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MCHL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MEDO00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MNGO00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MOBS00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MQZG00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MRBA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MRO100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MTCV00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MTDN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MTEM00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MTIS00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MTMA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx MULG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NBRK00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NCLF00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NEBO00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NHIL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NIUM00NIU_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NORF00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NORS00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NSTA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NTJN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx NULA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PARK00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PERT00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PNGM00PNG_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PRCE00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PTHL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PTKL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PTLD00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PTSV00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx PTVL00VUT_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx RAVN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx RHPT00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx RKLD00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx RNSP00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx ROBI00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx RSBY00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx SA4500AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx SAMO00WSM_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx SOLO00SLB_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx SPBY00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx STHG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx STNY00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx STR100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx STR200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx SYDN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx SYM100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TAUP00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TBOB00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TID100AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TITG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TOMP00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TOOG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TOOW00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TOW200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx TURO00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx UCLA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx VGMT00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WAGN00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WALH00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WARA00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WARK00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WARW00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WGTN00NZL_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WILU00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WLAL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx WWLG00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx XMIS00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx YAR200AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx YAR300AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx YARR00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx YEEL00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx YNKI00AUS_S_20201150000_01D_30S_MO.rnx
../cpp/build/pea --config ../config/longtest-ex01-sf-ppp.yaml --rnx YULA00AUS_S_20201150000_01D_30S_MO.rnx

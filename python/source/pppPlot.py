#!/usr/bin/python3
import matplotlib as plb
plb.use('Agg')

# python3 compareClock.py --standard /data/acs/pea/example/EX03/standard/igs20624.clk --test /data/acs/pea/example/EX03/standard/aus20624.clk
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
import os
import math
from numpy import loadtxt
from matplotlib.ticker import MultipleLocator

import argparse
import datetime as dt
import re

#==============================================================================
def parsePPPOutputFile(pppfile): #, ds, satels):

    recPOSRGX   = re.compile('REC_POS\s+(\w+)\s+(\d+)\s+(-?[\d\.]*)\s+(-?[\d\.]*)\s+(-?[\d\.]*)')
    
    output = {}

    count = 0
    with open(pppfile) as fstandard:
        for line in fstandard:
            line = line.rstrip()

            if( recPOSRGX.search(line) ):
                match = recPOSRGX.search(line)
                #print("RECpos match",match[1],match[2],match[3],match[4],match[5])

                if ( 'recPos' not in output) :
                    output['recPos'] = {}
                    output['recPos']['stns'] = []
                
                if ( match[1] not in output['recPos']['stns'] ): 
                    output['recPos']['stns'].append(match[1])
                    output['recPos'][match[1]] = {}
                    output['recPos'][match[1]]['X'] = [] 
                    output['recPos'][match[1]]['Y'] = [] 
                    output['recPos'][match[1]]['Z'] = [] 

                if ( int(match[2]) == 0):  
                    output['recPos'][match[1]]['X'].append(np.float(match[3])) # = [] 
                elif ( int(match[2]) == 1): 
                    output['recPos'][match[1]]['Y'].append(np.float(match[3])) # = [] 
                elif ( int(match[2]) == 2): 
                    output['recPos'][match[1]]['Z'].append(np.float(match[3]))
                
    return output 


#==============================================================================
parser = argparse.ArgumentParser(description='Compare clock solutions from processing runs of the pea',
        epilog='''\

    pppPlot.py:\nthis script takes the output from the pea and plots the differences between the median of the estimated coordinates, and then those provided in the IGS SINEX solution.

    To run, first grep the reported receiver positions reported in the station specific trace files to a seperate file

    > grep "REC_POS" /data/acs/pea/output/exs/EX01_IF/EX01_IF-ALIC201919900.TRACE > ALIC_201919900.PPP

    > pppPlot.py --ppp output/EX01_IF_PPP/ALIC_201919900.PPP

    you should be able to view the plots ALIC

#==============================================================================

    ''')

parser.add_argument('--ppp',dest='pppfile',default='',help=" ")

args = parser.parse_args()
#==============================================================================

if not args.pppfile:
    print("Usage: pppPlot.py --ppp <output/EX01_IF_PPP/ALIC_201919900.PPP>")
    print("Outputs: <station>_pos.png, <station>_snx_pos.png")
    exit(0)

results = parsePPPOutputFile(args.pppfile)#, results, satels)

#==============================================================================
## Plot the results up
#==============================================================================

for stn in (results['recPos']['stns']):
    print("Plotting station:",stn)

    fig1,ax1 = plt.subplots(1,1,figsize=(13,8))
    plt.title(stn+" Estimated pos - median position")
    X = results['recPos'][stn]['X'][:] - np.median(results['recPos'][stn]['X'][:])
    Y = results['recPos'][stn]['Y'][:] - np.median(results['recPos'][stn]['Y'][:])
    Z = results['recPos'][stn]['Z'][:] - np.median(results['recPos'][stn]['Z'][:])
    medZ =np.median(results['recPos'][stn]['Z'][:])
    ax1.plot(X,label='X') #,results['std1'],'o',alpha=ALPHA,label='East sigma')
    ax1.plot(Y,label='Y') #,results['std2'],'o',alpha=ALPHA,label='North sigma')
    ax1.plot(Z,label='Z') #,results['std3'],'o',alpha=ALPHA,label='Up sigma')

    figsavename = stn+'_pos.png'
    fig1.savefig(figsavename)
    print("Saved the plot:", figsavename)

    # Plot the difference with respect to the SINEX solution
    fig2,ax2 = plt.subplots(1,1,figsize=(13,8))
    plt.title(stn+" Estimated pos - sinex position")
    snx = [-4052052.71694,4212835.98092,-2545104.60797]
    snxX = np.array(results['recPos'][stn]['X'][:]) - snx[0]
    snxY = np.array(results['recPos'][stn]['Y'][:]) - snx[1]
    snxZ = np.array(results['recPos'][stn]['Z'][:]) - snx[2]

    ax2.plot(snxX,label='X') 
    ax2.plot(snxY,label='Y') 
    ax2.plot(snxZ,label='Z')
    plt.yscale('symlog', linthreshy=0.1)
    ax2.grid(True)
    #plt.grid('true')

    figsavename = stn+'_snx_pos.png'
    fig2.savefig(figsavename)
    print("Saved the plot:", figsavename)

exit(0)

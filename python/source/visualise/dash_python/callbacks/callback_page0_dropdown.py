
import operator
from app import app
from data.get_data_sol import get_sol_data
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import sol
from data.get_data_sol import get_sol_data
import numpy as np
from callbacks.callback_page2_dropdown import *
import pandas as pd
from callbacks.sort_df import sort_df


## CALLBACK FOR DROP DOWN SELECTION  :pathname == "/" (page_epochs)
##Callback for Graph update through drop down selection
@app.callback(
    Output('epoch_graph', 'figure'),
    [Input('dd1', 'value'),
     Input('dd2', 'value')])

def update_graph1(epochField_1, epochField_2):

        ##Create Empty trace:
        traceList = []


        epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"])   
        # pdb.set_trace()
        if(epochField_1 is not None):
   
                sorted_df = sort_df(epoch,sol.meta["ALL_EPOCH_VALUES"][epochField_1])
                trace1 = go.Scatter(x= sorted_df['x'], y = sorted_df['y'],
                mode="lines+markers",
                name=str(epochField_1))
                traceList.append(trace1)
         
        

        if(epochField_2 is not None):
                sorted_df = sort_df(epoch,sol.meta["ALL_EPOCH_VALUES"][epochField_2])
                trace2=go.Scatter(x= sorted_df['x'], y = sorted_df['y'],
                mode='lines+markers',
                name=str(epochField_2))
                traceList.append(trace2)



        data = traceList
        fig = go.Figure(data = data)

        fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))


        fig.layout.autosize = True
        return fig




import operator

from data.get_data_sol import get_sol_data,sat_fields
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout , sat_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import app, sol,res ,sat_visible_to_rec

import numpy as np
import pandas as pd


from callbacks.helperfunc import *


"""
 * @brief Returns the data trace for a given rec-satellite-attribute pair accross the given epochs.
 * @param graph_type: Desired graph type e.g Line, Scatter etc. 
 * @param rec:        Reciever ID e.g "AGGO".
 * @param sat:        Satellite Id e.g "G01_SVN#"
 * @param groupby:    The field to whihch the attribute for a given rec-satellite-attribute is to group by on.  e.g: EPOCHS, ELEV_ANGLE etc.
 * @param attribute:  Attribute for the rec-sat pair, e.g : "FLOAT_AMB"
"""

def get_trace(graph_type,rec,sat,groupby,attribute):

        
    traceList = []
    mode = ""

    if(graph_type == "LINE" or graph_type == "SCATTER"):

        if (graph_type == "LINE"):
            mode = "lines"
        else:
            mode = "markers"

        
        if(groupby=="EPOCHS"):
            groupby = pd.DatetimeIndex(res[sat][rec][groupby]) 
            #Sort the data frame.
            sortd_df = sort_df(groupby, res[sat][rec][attribute])
            trace =go.Scatter(
            x= sortd_df['x'], 
            y=sortd_df['y'] ,
            mode=mode,
            name=str(sat))
        

        else:
            """Don't do anything. Line graph won't be plotted for anything other then groupby =='EPOCHS'. This has been taken 
            care of, somewhere else in the code """
            groupby = res[sat][rec][groupby]
            trace =go.Scatter(x= groupby, y=res[sat][rec][attribute] ,
            mode=mode,
            name=str(sat))


        

    elif(graph_type =="POLAR"):
        groupby = res[sat][rec][groupby]
        trace = go.Scatterpolar(r=res[sat][rec][attribute], theta=groupby,
                         mode = "markers" , name = sat )

    elif(graph_type =="HISTOGRAM"):
        trace = go.Histogram(x= res[sat][rec][attribute])
        return trace




    traceList.append(trace)


    return traceList





"""
@ app.callback()
    ************************************************************************************************************************************  
    * @brief CALLBACK :pathname == "/page-4".                                                                                          *
    *      This callback defines the input and return params for the function to be executed when this callback is invoked.            *                                                 *
    * @param Output (Equivalent to Return statement):                                                                                  *
    *        residuals_pg_dropdown_3: This ID, which represents a DropDown Menu, will be updated with the list of Satellites.          *
    *        options: A required placeholder, not used though.                                                                         *
    * @param Input:                                                                                                                    *   
    *        residuals_pg_dropdown_2: Reference to the 2nd DropDown Menu at the residuals pg.                                          *
    *        value: Value contained by that DropDown Menu.                                                                             *   
    *                                                                                                                                  *   
    *                                                                                                                                  *
*   ************************************************************************************************************************************   
@ update_residuals_pg_dropdown_3()
    **************************************************************************************************************
    * @brief Updates the DropDown on the page(pathname == "/page-4"),based on the values of DropDown attributes. *
    * @rec: Alias for the the `value` in  residuals_pg_dropdown_2                                                *
    * @return Dropdown: Updated DropDown Menu based on above params.                                             *
    ************************************************************************************************************ *
"""

@app.callback(
    Output('residuals_pg_dropdown_3', 'options'),
    [Input('residuals_pg_dropdown_2', 'value')]
)
def update_residuals_pg_dropdown_3(rec):

    if(rec):
        return [{'label': i, 'value': i} for i in sat_visible_to_rec[rec]]

    return []





"""
@ app.callback()
    ************************************************************************************************************************************  
    * @brief CALLBACK :pathname == "/page-4".                                                                                          *
    *      This callback defines the input and return params for the function to be executed when this callback is invoked.            *                                                 *
    * @param Output (Equivalent to Return statement):                                                                                  *
    *        residuals_pg_dropdown_4: This ID, which represents a DropDown Menu, will be updated with the list of available group by   * 
    *                                   options.                                                                                       *
    *        options: A required placeholder, not used though.                                                                         *
    * @param Input:                                                                                                                    *   
    *        residuals_pg_dropdown_1: Reference to the 1ST DropDown Menu at the Residuals pg.                                          *
    *        value: Value contained by that DropDown Menu.                                                                             *   
    *                                                                                                                                  *   
    *                                                                                                                                  *
*   ************************************************************************************************************************************   
@ update_residuals_pg_dropdown_4()
    **************************************************************************************************************
    * @brief Updates the DropDown on the page(pathname == "/page-4"),based on the values of DropDown attributes. *
    * @graph_type: Alias for the the `value` in  residuals_pg_dropdown_1                                         *
    * @return Dropdown: Updated DropDown Menue based on above params.                                            *
    **************************************************************************************************************
"""
@app.callback(
    Output('residuals_pg_dropdown_4', 'options'),
    [Input('residuals_pg_dropdown_1', 'value')]
)
def update_residuals_pg_dropdown_4(graph_type):
    
    
    if(graph_type == "POLAR"):
        temp = ["AZIM", "ELEV_ANG"]
        return [{'label': i, 'value': i} for i in temp]

    else:
        temp = ["EPOCHS","ELEV_ANG","NADIR_ANG"]
       

        return [{'label': i, 'value': i} for i in temp]


"""
@ app.callback()
    ************************************************************************************************************************************  
    * @brief CALLBACK :pathname == "/page-4".                                                                                          *
    *      This callback defines the input and return params for the function to be executed when this callback is invoked.            *                                                 *
    * @param Output (Equivalent to Return statement):                                                                                  *
    *        res_graph: This ID, which represents a Graph, will be updated with based on the Input params                              *
    *        figure: A required placeholder, not used though.                                                                          *
    * @param Input:                                                                                                                    *   
    *        residuals_pg_dropdown_1: Reference to the 1st DropDown Menu at the residuals pg.                                          *
    *        value: Value contained by that DropDown Menu. Type of the graph to be plotted. e.g SCATTER                                * 
    *                                                                                                                                  *
    *        residuals_pg_dropdown_2: Reference to the 2nd DropDown Menu at the residuals pg.                                          *
    *        value: Value contained by that DropDown Menu. Reciever selected e.g ALIC                                                  * 
    *                                                                                                                                  *
    *        residuals_pg_dropdown_3: Reference to the 3rd DropDown Menu at the residuals pg.                                          *
    *        value: Value contained by that DropDown Menu. Satellite selected e.g G01_SVN#                                             *
    *                                                                                                                                  * 
    *        residuals_pg_dropdown_4: Reference to the 4th DropDown Menu at the residuals pg.                                          *
    *        value: Value contained by that DropDown Menu. Field to group by e.g EPOCH, ELEV_ANG                                       * 
    *                                                                                                                                  *   
    *                                                                                                                                  *
*   ************************************************************************************************************************************   
@ update_residuals_pg_graph()
    **************************************************************************************************************
    * @brief Updates the DropDown on the page(pathname == "/page-4"),based on the values of DropDown attributes. *
    * @graph_type: Alias for the the `value` in  residuals_pg_dropdown_1.                                        *
    * @rec:        Alias for the the `value` in  residuals_pg_dropdown_2.                                        *
    * @sat:        Alias for the the `value` in  residuals_pg_dropdown_3.                                        *
    * @groupby:    Alias for the the `value` in  residuals_pg_dropdown_4.                                        *
    *                                                                                                            *
    * @return Graph: Alias for 'figure'     in  res_grag                                                         *
    ************************************************************************************************************ *
"""


@app.callback(
    Output('res_graph', 'figure'),
    [
     Input('residuals_pg_dropdown_1', 'value')  , 
     Input('residuals_pg_dropdown_2', 'value'),
     Input('residuals_pg_dropdown_3', 'value'),
     Input('residuals_pg_dropdown_4', 'value'),
     Input('residuals_pg_dropdown_5', 'value'),

     ])   
def update_residuals_pg_graph(graph_type,rec,sat, groupby,attribute):

    fig = go.Figure()
    
    #If one of the Drop-down menu is Not select, return the following
    # pdb.set_trace()
    if(graph_type is None or rec is None or sat is None or groupby is None or attribute is None  ):
         return get_empty_graph("Make sure values for all the Dropdown Menus are selected")
   
    elif(graph_type=='LINE' and groupby != 'EPOCHS'):
        return get_empty_graph("Line graph is not available for this selection. Try grouping by Epochs or select a non-line graph type.")


    else:

        if(graph_type =="LINE" or graph_type=="SCATTER"):
            mode = ""
            if (graph_type == "LINE"):
                mode = "lines"
            else :
                mode = "markers"
         
            """
            Check if the current value appearing the "Satellite" dropdown is visible to the satellite selected. 
            If not, prompt, the user to select  a satellite from the updated dropdown.
            """

            if (sat in sat_visible_to_rec[rec]):
                if(sat == "ALL"):
                    tempListTrace = []
                    for satellite in sat_visible_to_rec[rec]:
                        if satellite == "ALL":
                            continue
                        trace = get_trace(graph_type,rec,satellite,groupby,attribute)
                        tempListTrace = tempListTrace + trace



                    return get_figure(tempListTrace,get_units(groupby),get_units(attribute))

                else:
                    #Get the trace
                    trace = get_trace(graph_type,rec,sat,groupby,attribute)
                    return get_figure(trace,get_units(groupby),get_units(attribute))


            return get_empty_graph("Select  Satellite from the Dropdown")



        elif (graph_type =="HISTOGRAM"):

            if(groupby == "EPOCHS"):
                if (sat in sat_visible_to_rec[rec]):
                # pdb.set_trace()
                
                    if(sat == "ALL"):
                        return get_empty_graph(" ALL- FUNCTIONALITY for histograms has not been implemented yet because of performance issues.")

                    else:
                        trace = get_trace(graph_type,rec,sat,groupby,attribute)
                        #TODO :  Implement in future
                        # sliders = [dict(
                        #                 active = 4,
                        #                 currentvalue = {"prefix": "bin size: "},
                        #                 pad = {"t": 20},
                        #                 steps = [dict(label = i, method = 'restyle',  args = ['xbins.size', i]) for i in np.arange(0.25,15)]
                        #             )]
                        fig = go.Figure()
                        fig.add_trace(trace)
                        fig.update_layout(
                            # sliders=sliders
                        )
                                                
                        return fig
                

                else :
                    return get_empty_graph("Select  Satellite from the Dropdown")

            else : 
                ##Create another drop down to add  the bin value of NADIR_ANG / ELEV_ANG:
                return get_empty_graph("Histogram can only be grouped by EPOCHS.")




        elif (graph_type =="POLAR"):
        
            if (sat in sat_visible_to_rec[rec]):
            # pdb.set_trace()
            
                if(sat == "ALL"):
                    # return get_empty_graph(" ALL- FUNCTIONALITY for POLAR PLOTS have not been implemented yet because of performance issues.")
                    tempListTrace = []
                    for satellite in sat_visible_to_rec[rec]:
                        if satellite == "ALL":
                            continue
                        trace = get_trace(graph_type,rec,satellite,groupby,attribute)
                        tempListTrace = tempListTrace + trace

                

                    return get_figure(tempListTrace,get_units(groupby),get_units(attribute))

                else:
    
                    trace = get_trace(graph_type,rec,sat,groupby,attribute)                    
                    return get_figure(trace,get_units(groupby),get_units(attribute))
                

            else :
                return get_empty_graph("Select  Satellite from the Dropdown")



        else:
            return get_empty_graph("Select the GRAPH Type from the Dropdown")









            
        




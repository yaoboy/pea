import operator

from data.get_data_sol import get_sol_data,sat_fields
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout , sat_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import app, sol,res ,sat_visible_to_rec

import numpy as np
import pandas as pd

from callbacks.empty_graph import get_empty_graph
from callbacks.sort_df import sort_df



"""
    Helper function 
"""

def get_trace(graph_type,site,sat,groupby,attribute):

        
    traceList = []
    mode = ""

    if(graph_type == "LINE" or graph_type == "SCATTER"):

        if (graph_type == "LINE"):
            mode = "lines"
        else:
            mode = "markers"

        
        if(groupby=="EPOCHS"):
            groupby = pd.DatetimeIndex(res[sat][site][groupby]) 
            #Sort the data frame.
            sortd_df = sort_df(groupby, res[sat][site][attribute])
            trace =go.Scatter(
            x= sortd_df['x'], 
            y=sortd_df['y'] ,
            mode=mode,
            name=str(site))
        

        else:
            """Don't do anything. Line graph won't be plotted for anything other then groupby =='EPOCHS'. This has been taken 
            care of, somewhere else in the code """
            groupby = res[sat][site][groupby]
            trace =go.Scatter(x= groupby, y=res[sat][site][attribute] ,
            mode=mode,
            name=str(site))


        

    elif(graph_type =="POLAR"):
        groupby = res[sat][site][groupby]
        trace = go.Scatterpolar(r=res[sat][site][attribute], theta=groupby,
                         mode = "markers" , name = sat )

    elif(graph_type =="HISTOGRAM"):
        trace = go.Histogram(x= res[sat][site][attribute])
        return trace




    traceList.append(trace)


    return traceList




## Helper function ends

"""
vvvvvvvvvvvvv  CALLBACKS FOR DROPDOWN vvvvvvvvvvvvvvvvvvv
"""

@app.callback(
    Output('dd6', 'options'),
    [Input('dd5', 'value')]
)
def update_date_dropdown(dd5_val):

    if(dd5_val):
        return [{'label': i, 'value': i} for i in sat_visible_to_rec[dd5_val]]

    return []



@app.callback(
    Output('dd7', 'options'),
    [Input('dd5_flat', 'value')]
)
def update_date_dropdown2(graph_type):
    
    
    if(graph_type == "POLAR"):
        temp = ["AZIM", "ELEV_ANG"]
        return [{'label': i, 'value': i} for i in temp]

    else:
        temp = ["EPOCHS","ELEV_ANG","NADIR_ANG"]
       

        return [{'label': i, 'value': i} for i in temp]

    

"""
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
^^^^^^^^^^ENDS CALLBACKS FOR DROPDOWN^^^^^^^^^^^^^^
"""


@app.callback(
    Output('res_graph', 'figure'),
    [
     Input('dd5_flat', 'value')  , 
     Input('dd5', 'value'),
     Input('dd6', 'value'),
     Input('dd7', 'value'),
     Input('dd8', 'value'),

     ])   
# def update_graph2(dd4, dd4):
#graph_type,site,sat,groupby,attribute
def update_graph4(graph_type,site,sat, groupby,attribute):

    fig = go.Figure()

    
    #If one of the Drop-down menu is Not select, return the following
    # pdb.set_trace()
    if(graph_type is None or site is None or sat is None or groupby is None or attribute is None  ):
         return get_empty_graph("Make sure a value for all the Dropdown Menu is selected")
   
    elif(graph_type=='LINE' and groupby != 'EPOCHS'):
        return get_empty_graph("SELECT Graph Type anything  other then LINE for this selection of  Groupby DrowDown")


    else:

        if(graph_type =="LINE" or graph_type=="SCATTER"):
            mode = ""
            if (graph_type == "LINE"):
                mode = "lines"
            else :
                mode = "markers"
         
            """
            Check if the current value appearing the "Satellite" dropdown is visible to the satellite selected. 
            If not, prompt, the user to select  a satellite from the updated dropdown.
            """

            if (sat in sat_visible_to_rec[site]):
                if(sat == "ALL"):
                    tempListTrace = []
                    for satellite in sat_visible_to_rec[site]:
                        if satellite == "ALL":
                            continue
                        trace = get_trace(graph_type,site,satellite,groupby,attribute)
                        tempListTrace = tempListTrace + trace

                    fig = go.Figure(data=tempListTrace)
                    fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                    fig.layout.autosize = True

                    return fig    

                else:
                    #Get the trace
                    trace = get_trace(graph_type,site,sat,groupby,attribute)
                    fig = go.Figure(data = trace)
                    fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                    fig.layout.autosize = True
                    return fig


            return get_empty_graph("Select  Satellite from the Dropdown")



        elif (graph_type =="HISTOGRAM"):

            if(groupby == "EPOCHS"):
                if (sat in sat_visible_to_rec[site]):
                # pdb.set_trace()
                
                    if(sat == "ALL"):
                        return get_empty_graph(" ALL- FUNCTIONALITY for histograms has not been implemented yet because of performance issues.")

                    else:
                        trace = get_trace(graph_type,site,sat,groupby,attribute)
                        sliders = [dict(
                                        active = 4,
                                        currentvalue = {"prefix": "bin size: "},
                                        pad = {"t": 20},
                                        steps = [dict(label = i, method = 'restyle',  args = ['xbins.size', i]) for i in np.arange(0.25,15)]
                                    )]
                        fig = go.Figure()
                        fig.add_trace(trace)
                        fig.update_layout(
                            sliders=sliders
                        )
                                                
                        return fig
                

                else :
                    return get_empty_graph("Select  Satellite from the Dropdown")

            else : 
                ##Create another drop down to add  the bin value of NADIR_ANG / ELEV_ANG:
                return get_empty_graph("Not implmented yet")




        elif (graph_type =="POLAR"):
        
            if (sat in sat_visible_to_rec[site]):
            # pdb.set_trace()
            
                if(sat == "ALL"):
                    # return get_empty_graph(" ALL- FUNCTIONALITY for POLAR PLOTS have not been implemented yet because of performance issues.")
                    tempListTrace = []
                    for satellite in sat_visible_to_rec[site]:
                        if satellite == "ALL":
                            continue
                        trace = get_trace(graph_type,site,satellite,groupby,attribute)
                        tempListTrace = tempListTrace + trace

                    fig = go.Figure(data=tempListTrace)
                    fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                    fig.layout.autosize = True

                    return fig

                else:
                    trace = get_trace(graph_type,site,sat,groupby,attribute)
                    fig = go.Figure(data = trace)
                                        
                    return fig
                

            else :
                return get_empty_graph("Select  Satellite from the Dropdown")



        else:
            return get_empty_graph("Select the GRAPH Type from the Dropdown")









            
        




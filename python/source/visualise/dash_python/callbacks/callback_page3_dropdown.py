import operator
from app import app
from data.get_data_sol import get_sol_data,sat_fields
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout , sat_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import sol
from data.get_data_sol import get_sol_data
import numpy as np
import pandas as pd

from callbacks.empty_graph import get_empty_graph
from callbacks.sort_df import sort_df



"""
Helper function , Note this is fairly identical to get_trace of callback_pagw2_dropdown.py.
TODO: Make them 1. 
"""

def get_trace(sat,attribute,epoch):
    traceList = []
    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"]) 


    if(sat is not None):

        sat1 = sat
        sat1_attribute = attribute

        if(sat1_attribute  in sat_fields ):

            if(sat1_attribute+"_SIG" in sat_fields):
        
                temp1 = [float(i) for i in sol.sat[sat1][sat1_attribute]]
                temp2 = [float(i) for i in sol.sat[sat1][sat1_attribute+"_SIG"]]

                y_upper = list(np.array(temp1) + np.array(temp2))
                y_lower = list(np.array(temp1) - np.array(temp2))

                sorted_df1 = sort_df(epoch,y_upper)
                
                upper_bound = go.Scatter(
                    name='Upper Bound',
                    x=sorted_df1['x'],
                    y= sorted_df1['y'],
                    mode='lines',
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df2 = sort_df(epoch,temp1)
                trace = go.Scatter(
                    name='Measurement',
                    x=sorted_df2['x'],
                    y= sorted_df2['y'],
                    mode='lines',
                    line=dict(color='rgb(31, 119, 180)'),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')


                sorted_df3 = sort_df(epoch,y_lower)
                lower_bound = go.Scatter(
                    name='Lower Bound',
                    x=sorted_df3['x'],
                    y=sorted_df3['y'],
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    mode='lines')

                traceList.append(lower_bound)
                traceList.append(trace)
                traceList.append(upper_bound)


                return traceList
                
          
            else :
                sorted_df4 = sort_df(epoch,sol.sat[sat1][sat1_attribute])
                
                trace =go.Scatter(
                x=sorted_df4['x'],
                y=sorted_df4['y'],
                mode='lines+markers',
                name=str(sat1))


                traceList.append(trace)

                return traceList




## CALLBACK FOR DROP DOWN SELECTION  :pathname == "/page-3" 
##Callback for Graph update through drop down selection
@app.callback(
    Output('sat_graph', 'figure'),
    [Input('dd4', 'value'),
    Input('dd4a', 'value')
     ])   

def update_graph2(sat,attribute):

    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"])  

    if(sat is None or attribute is None ):
        return get_empty_graph("Select value for  SATELLITE and/or  ATTRIBUTE")


    elif(sat !="ALL"):
        trace = get_trace(sat,attribute,epoch)
        #If trace is empty, do the following:
        if  trace:
            fig = go.Figure(data=trace)
            fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))


            fig.layout.autosize = True
            return fig

    else:
        tempListTrace = []
        for sat in sol.sat.keys():
              trace = get_trace(sat,attribute,epoch)

              #Concatenate the traceList: 
              tempListTrace = tempListTrace + trace
              
        fig = go.Figure(data=tempListTrace)
        fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))


        fig.layout.autosize = True
        return fig


import operator
from app import app
from data.get_data_sol import get_sol_data
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import sol
from data.get_data_sol import get_sol_data ,rec_fields
import numpy as np
import pandas as pd

from callbacks.empty_graph import get_empty_graph
from callbacks.sort_df import sort_df

"""
Helper function for generating graphs
"""

def get_trace(site,attribute,epoch):
    traceList = []
    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"]) 


    if(site is not None):

        site1 = site
        site1_attribute = attribute

        if(site1_attribute  in rec_fields and site1_attribute != "SITE_ID"):

            if(site1_attribute+"_SIG" in rec_fields):
        
                temp1 = [float(i) for i in sol.rec[site1][site1_attribute]]
                temp2 = [float(i) for i in sol.rec[site1][site1_attribute+"_SIG"]]

                y_upper = list(np.array(temp1) + np.array(temp2))
                y_lower = list(np.array(temp1) - np.array(temp2))

                sorted_df1 = sort_df(epoch,y_upper)

                upper_bound = go.Scatter(
                    name='Upper Bound',
                    x=sorted_df1['x'],
                    y= sorted_df1['y'],
                    mode='lines',
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df2 = sort_df(epoch,temp1)    
                trace = go.Scatter(
                    name='Measurement',
                    x=sorted_df2['x'],
                    y= sorted_df2['y'],
                    mode='lines',
                    line=dict(color='rgb(31, 119, 180)'),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df3 = sort_df(epoch,y_lower)       

                lower_bound = go.Scatter(
                    name='Lower Bound',
                    x=sorted_df3['x'],
                    y= sorted_df3['y'],
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    mode='lines')

                traceList.append(lower_bound)
                traceList.append(trace)
                traceList.append(upper_bound)


                return traceList
                
          
            else :
                sorted_df4 = sort_df(epoch,sol.rec[site1][site1_attribute])       
                trace =go.Scatter(
                x=sorted_df4['x'],
                y= sorted_df4['y'],
                mode='lines+markers',
                name=str(site1))


                traceList.append(trace)

                return traceList

    

                

## CALLBACK FOR DROP DOWN SELECTION  :pathname == "/page-2" (page_sites)
##Callback for Graph update through drop down selection
@app.callback(
    Output('sites_graph', 'figure'),
    [Input('dd3', 'value'),
    Input('dd3a', 'value')
     ])   
# def update_graph2(dd3, dd4):
def update_graph2(dd3_value,dd3a_value):


    # pdb.set_trace()
    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"])  

    if(dd3_value is None or dd3a_value is None ):
        return get_empty_graph("Select value for  RECIEVER and/or  ATTRIBUTE")


    elif(dd3_value !="ALL"):
        trace = get_trace(dd3_value,dd3a_value,epoch)
        #If trace is empty, do the following:
        if  trace:
            fig = go.Figure(data=trace)
            fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))


            fig.layout.autosize = True
            return fig

    else:
        tempListTrace = []
        for site in sol.rec.keys():
              trace = get_trace(site,dd3a_value,epoch)

              #Concatenate the traceList: 
              tempListTrace = tempListTrace + trace
              
        fig = go.Figure(data=tempListTrace)
        fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))


        fig.layout.autosize = True
        return fig



       
        

    
        
            
        

  
    
      
            
    



import operator

from data.get_data_sol import get_sol_data,sat_fields
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout , sat_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import app, sol,res ,sat_visible_to_rec

import numpy as np
import pandas as pd



## CALLBACK FOR DROP DOWN SELECTION  :pathname == "/page-3" 
##Callback for Graph update through drop down selection


#THis is to update the options in the sat_names drop down based on the rec_names drop downs: 

# sat_visible_to_rec = {
#     "AGGO" : ["G02_SVN#","G06_SVN#","G12_SVN#","G19_SVN#","G24_SVN#","G25_SVN#","G29_SVN#","G32_SVN#"],
#     "ALIC" : ["G01_SVN#","G03_SVN#","G09_SVN#","G11_SVN#","G14_SVN#","G18_SVN#","G22_SVN#","G23_SVN#","G26_SVN#","G31_SVN#"]
# }


@app.callback(
    Output('dd6', 'options'),
    [Input('dd5', 'value')]
)
def update_date_dropdown(dd5_val):
    return [{'label': i, 'value': i} for i in sat_visible_to_rec[dd5_val]]


#####################  ENDS HERE #############################################


@app.callback(
    Output('res_graph', 'figure'),
    [Input('dd5', 'value'),
     Input('dd6', 'value'),
     Input('dd7', 'value'),
     Input('dd8', 'value'),
     Input('dd5_flat', 'value')
     ])   
# def update_graph2(dd4, dd4):
def update_graph4(dd5_value,dd6_value, dd7_value,dd8_value,dd5_flat_value):

    
    #For now get fixed value for x axis.
    #TODO : Query the x axis param as well.
    # pdb.set_trace()


    fig = go.Figure()


    "Check which of the graph is selected":
    if(dd5_flat_value ==)


          
    # pdb.set_trace()
    """
    Check if the current value appearing the "Satellite" dropdown is visible to the satellite selected. 
    If not, prompt, the user to select  a satellite from the updated dropdown.
    """
    if (dd6_value in sat_visible_to_rec[dd5_value]):
        # pdb.set_trace()
        
        if(dd6_value == "ALL"):

            if(dd7_value == "EPOCHS"):
            
                for satellite in sat_visible_to_rec[dd5_value]:
                    if satellite == "ALL":
                        continue
                    # pdb.set_trace()
                
                    qt = pd.DatetimeIndex(res[satellite][dd5_value]["EPOCHS"])     
                    fig.add_trace(go.Scatter(x= qt, y=list(map(float, res[satellite][dd5_value][dd8_value])),
                    mode='lines',
                    name= satellite))
                    fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                return fig
                        
            else:
                for satellite in sat_visible_to_rec[dd5_value]:
                    if satellite == "ALL":
                        continue
                    # pdb.set_trace()
                
                    fig.add_trace(go.Scatter(x=res[satellite][dd5_value][dd7_value], y=list(map(float, res[satellite][dd5_value][dd8_value])),
                    mode="markers",
                    name=satellite))

                    fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                return fig



        else:
            # pdb.set_trace()
            if(dd7_value == "EPOCHS"):
                # pdb.set_trace()
                qt = pd.DatetimeIndex(res[dd6_value][dd5_value]["EPOCHS"])      
                fig.add_trace(go.Scatter(x= qt, y=list(map(float, res[dd6_value][dd5_value][dd8_value])),
                mode='lines',
                name=dd6_value))
                fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                return fig

            else:
                # pdb.set_trace()
                fig.add_trace(go.Scatter(x=res[dd6_value][dd5_value][dd7_value], y=list(map(float, res[dd6_value][dd5_value][dd8_value])),
                mode="markers",
                name="x"))

                fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)))
                return fig




    return {
    "layout": {
        "xaxis": {
            "visible": False
        },
        "yaxis": {
            "visible": False
        },
        "annotations": [
            {
                "text": "Select the value of a satellite",
                "xref": "paper",
                "yref": "paper",
                "showarrow": False,
                "font": {
                    "size": 28
                }
            }
        ]
    }
}





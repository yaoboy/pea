import operator
from app import app
from data.get_data_sol import get_sol_data
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import sol
from data.get_data_sol import get_sol_data ,rec_fields
import numpy as np
import pandas as pd



from callbacks.helperfunc import *

"""
 * @brief Returns the data trace for a given satellite - attribute pair accross the given epochs.
 * @param rec: Reciever ID, e.g AGGO.
 * @param attribute:  Reciever attribute , e.g SITE_RANGE_RESID_RMS.
 * @return tracelist: Python List containing the values for the Attribute, for the given Reciever accross all Epochs. 
"""

def get_trace(rec,attribute,epoch):
    traceList = []
    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"]) 


    if(rec is not None):

        site1 = rec
        site1_attribute = attribute

        if(site1_attribute  in rec_fields and site1_attribute != "SITE_ID"):

            if(site1_attribute+"_SIG" in rec_fields):
        
                temp1 = [float(i) for i in sol.rec[site1][site1_attribute]]
                temp2 = [float(i) for i in sol.rec[site1][site1_attribute+"_SIG"]]

                

                y_upper = list(np.array(temp1) + np.array(temp2))
                y_lower = list(np.array(temp1) - np.array(temp2))

                sorted_df1 = sort_df(epoch,y_upper)
                

                upper_bound = go.Scatter(
                    name='Upper Bound',
                    x=sorted_df1['x'],
                    y= sorted_df1['y'],
                    mode='lines',
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df2 = sort_df(epoch,temp1)    
                
                trace = go.Scatter(
                    name='Measurement',
                    x=sorted_df2['x'],
                    y= sorted_df2['y'],
                    mode='lines',
                    line=dict(color='rgb(31, 119, 180)'),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df3 = sort_df(epoch,y_lower)       

                lower_bound = go.Scatter(
                    name='Lower Bound',
                    x=sorted_df3['x'],
                    y= sorted_df3['y'],
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    mode='lines')
            
                traceList.append(lower_bound)
                traceList.append(trace)
                traceList.append(upper_bound)


                return traceList

            elif(site1_attribute == "SITE_DX" or site1_attribute == "SITE_DY" or site1_attribute == "SITE_DZ"):
                
                temp1 = [float(i) for i in sol.rec[site1][site1_attribute]]

                if(site1_attribute == "SITE_DX"):
                    temp2 = [float(i) for i in sol.rec[site1]["SITE_X_SIG"]]
                elif (site1_attribute == "SITE_DY"):
                    temp2 = [float(i) for i in sol.rec[site1]["SITE_Y_SIG"]]

                else:
                    temp2 = [float(i) for i in sol.rec[site1]["SITE_Z_SIG"]]


                y_upper = list(np.array(temp1) + np.array(temp2))
                y_lower = list(np.array(temp1) - np.array(temp2))

                sorted_df1 = sort_df(epoch,y_upper)
                

                upper_bound = go.Scatter(
                    name='Upper Bound',
                    x=sorted_df1['x'],
                    y= sorted_df1['y'],
                    mode='lines',
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df2 = sort_df(epoch,temp1)    
                
                trace = go.Scatter(
                    name='Measurement',
                    x=sorted_df2['x'],
                    y= sorted_df2['y'],
                    mode='lines',
                    line=dict(color='rgb(31, 119, 180)'),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df3 = sort_df(epoch,y_lower)       

                lower_bound = go.Scatter(
                    name='Lower Bound',
                    x=sorted_df3['x'],
                    y= sorted_df3['y'],
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    mode='lines')
            
                traceList.append(lower_bound)
                traceList.append(trace)
                traceList.append(upper_bound)


                return traceList


          
            else :
                sorted_df4 = sort_df(epoch,sol.rec[site1][site1_attribute])       
                trace =go.Scatter(
                x=sorted_df4['x'],
                y= sorted_df4['y'],
                mode='lines+markers',
                name=str(site1))


                traceList.append(trace)

                return traceList
                

"""
@ app.callback()
    ************************************************************************************************************************************  
    * @brief CALLBACK :pathname == "/page-2".                                                                                          *
    *      This callback defines the input and return params for the function to be executed when this callback is invoked.              *                                                 *
    * @param Output (Equivalent to Return statement):                                                                                  *
    *        sites_graph: This ID, which represents a Graph created using the Dash Core Components, will be updated                    *
    *        figure: A required placeholder, not used though                                                                           *
    * @param Input:                                                                                                                    *   
    *        reciever_p_dropdown_1: Reference to the 1st DropDown Menu at the reciever pg.                                             *
    *        value: Value contained by that DropDown Menu.                                                                             *   
    *                                                                                                                                  *   
    *        reciever_pg_dropdown_2: Reference to the 2nd DropDown Menu at the reciever pg.                                            *
    *        value: Value contained by that DropDown Menu.                                                                             *
    *                                                                                                                                  *
*   ************************************************************************************************************************************   
@ update_reciever_pg_graph()
    ************************************************************************************************************
    * @brief Updates the graph on the page(pathname == "/page-2"), based on the values of DropDown attributes. *
    * @ rec: Alias for the the `value` in  reciever_pg_dropdown_1                                              *
    * @ attribute: Alias for the `value` in reciever_pg_dropdown_2      
    * @return Graph: Updated graphs based on above params.                                                     *
    ************************************************************************************************************
"""
@app.callback(
    Output('sites_graph', 'figure'),
    [Input('reciever_pg_dropdown_1', 'value'),
    Input('reciever_pg_dropdown_2', 'value')
     ])   
def update_reciever_pg_graph(rec,attribute):


    # pdb.set_trace()
    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"])  

    if(rec is None or attribute is None ):
        return get_empty_graph("Select value for  RECIEVER and/or  ATTRIBUTE")


    elif(rec !="ALL"):
        trace = get_trace(rec,attribute,epoch)
        #If trace is empty, do the following:
        if  trace:
            # pdb.set_trace()
            return get_figure(trace,"EPOCHS",get_units(attribute))

    else:
        tempListTrace = []
        for rec in sol.rec.keys():
              trace = get_trace(rec,attribute,epoch)

              #Concatenate the traceList: 
              tempListTrace = tempListTrace + trace

        return get_figure(tempListTrace,"EPOCHS",get_units(attribute))



       
        

    
        
            
        

  
    
      
            
    



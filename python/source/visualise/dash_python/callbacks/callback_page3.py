import operator
from app import app
from data.get_data_sol import get_sol_data,sat_fields
from layouts.different_graphs import epoch_graphs_layout , sites_graph_layout , sat_graph_layout
from dash.dependencies import Input, Output

import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import pdb
from app import sol
from data.get_data_sol import get_sol_data
import numpy as np
import pandas as pd


from callbacks.helperfunc import *



"""
 * @brief Returns the data trace for a given satellite - attribute pair accross the given epochs.
 * @param sat: Satiellite ID, e.g G01_SVN#
 * @param attribute: Satellite Attribute, e.g SV_RANGE_RESID_RMS
 * @return tracelist: Python List containing the values for the Attribute, for the given Satellite accross all Epochs. 
"""

def get_trace(sat,attribute,epoch):
    traceList = []
    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"]) 


    if(sat is not None):

        sat1 = sat
        sat1_attribute = attribute

        if(sat1_attribute  in sat_fields ):

            if(sat1_attribute+"_SIG" in sat_fields):
        
                temp1 = [float(i) for i in sol.sat[sat1][sat1_attribute]]
                temp2 = [float(i) for i in sol.sat[sat1][sat1_attribute+"_SIG"]]

                y_upper = list(np.array(temp1) + np.array(temp2))
                y_lower = list(np.array(temp1) - np.array(temp2))

                sorted_df1 = sort_df(epoch,y_upper)
                
                upper_bound = go.Scatter(
                    name='Upper Bound',
                    x=sorted_df1['x'],
                    y= sorted_df1['y'],
                    mode='lines',
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')

                sorted_df2 = sort_df(epoch,temp1)
                trace = go.Scatter(
                    name='Measurement',
                    x=sorted_df2['x'],
                    y= sorted_df2['y'],
                    mode='lines',
                    line=dict(color='rgb(31, 119, 180)'),
                    fillcolor='rgba(68, 68, 68, 0.3)',
                    fill='tonexty')


                sorted_df3 = sort_df(epoch,y_lower)
                lower_bound = go.Scatter(
                    name='Lower Bound',
                    x=sorted_df3['x'],
                    y=sorted_df3['y'],
                    marker=dict(color="#444"),
                    line=dict(width=0),
                    mode='lines')

                traceList.append(lower_bound)
                traceList.append(trace)
                traceList.append(upper_bound)


                return traceList
                
          
            else :
                sorted_df4 = sort_df(epoch,sol.sat[sat1][sat1_attribute])
                
                trace =go.Scatter(
                x=sorted_df4['x'],
                y=sorted_df4['y'],
                mode='lines+markers',
                name=str(sat1))


                traceList.append(trace)

                return traceList



"""
@ app.callback()
    ************************************************************************************************************************************  
    * @brief CALLBACK :pathname == "/page-3".                                                                                          *
    *    This callback updates the graph on /page-3 based on the values selected in the dropdowns:satellite_pg_dropdown_1              *
    *    and satellite_pg_dropdown_2                                                                                                   *
    * @param Output:                                                                                                                   *
    *        sat_graph: This ID, which represents a Graph created using the Dash Core Components, will be updated.                     *
    *        figure: A required placeholder, not used though                                                                           *
    * @param Input:                                                                                                                    *   
    *        satellite_pg_dropdown_1: Reference to the 1st DropDown Menu at the satellite pg.                                          *
    *        value: Value contained by that DropDown Menu.                                                                             *   
    *                                                                                                                                  *   
    *        satellite_pg_dropdown_2: Reference to the 2nd DropDown Menu at the satellite pg.                                          *
    *        value: Value contained by that DropDown Menu.                                                                             *
    *                                                                                                                                  *
    * @return Graph: Updated graphs based on above params.                                                                             *
   ************************************************************************************************************************************   
@ update_satellite_pg_graph()
    ************************************************************************************************************
    * @brief Updates the graph on the page(pathname == "/page-3"), based on the values of DropDown attributes. *
    * @ sat: Alias for the the `value` in  satellite_pg_dropdown_1                                             *
    * @ attribute: Alias for the `value` in satellite_pg_dropdown_2                                            *
    * @return Graph: Updated graphs based on above params.                                                     *
    ************************************************************************************************************
"""
@app.callback(
    Output('sat_graph', 'figure'),
    [Input('satellite_pg_dropdown_1', 'value'),
    Input('satellite_pg_dropdown_2', 'value')
     ])   
def update_satellite_pg_graph(sat,attribute):

    epoch = pd.DatetimeIndex(sol.meta["ALL_EPOCH_VALUES"]["EPOCH"])  

    if(sat is None or attribute is None):
        return get_empty_graph("Select value for  SATELLITE and/or  ATTRIBUTE")

    elif (attribute =="SV_NRAD"):
        return get_empty_graph("There are no values for the attribute "+attribute+" in the Database")
    elif(sat !="ALL"):
        trace = get_trace(sat,attribute,epoch)
        #If trace is empty, do the following:
        if  trace:
     
            return get_figure(trace,"EPOCHS",get_units(attribute))

    else:
        tempListTrace = []
        for sat in sol.sat.keys():
              trace = get_trace(sat,attribute,epoch)

              #Concatenate the traceList: 
              tempListTrace = tempListTrace + trace
              

        return get_figure(tempListTrace,"EPOCHS",get_units(attribute))


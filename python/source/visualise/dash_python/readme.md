#Instructions:

#Run the following commands(Detailed explaination below): 
`pip3 install -r requirements.txt`


#A. To see the  `Real Time Application`
Change the path `input_dir` in the file `data/get_sol_data.py`  line 19. This has to be the same path where pea outputs the data. 
(Eventually , this application would be change to automatically read the output path from the pea config files(e.g Ex02-yaml)).

1.)`Run` the pea  and then do the following 
     Continue to section `#C`.


#B. To see the Application running on a static data(No Real Time)
     Continue to section `#C`.


#C. Common to both #A and #B
2.)This application needs a data source to generate graphs. So you need to provide the path to the relevant data files where ever they are on your system.
A self explantory example looks like 
          `python3 index.py -data /data/acs/pea/python/source/visualise/dash_python/raw_data/` 
          
     --After running this command in the terminal you would see something like.



|    Dash is running on http://127.0.0.1:8050/ 
|                                                                                                                             |
|    Warning: This is a development server. Do not use app.run_server                                                         |
|    in production, use a production WSGI server like gunicorn instead.                                                       |
|                                                                                                                             |
| * Serving Flask app "app" (lazy loading)                                                                                    |
| * Environment: production                                                                                                   |
|   WARNING: This is a development server. Do not use it in a production deployment.                                          |
|   Use a production WSGI server instead.                                                                                     |
| * Debug mode: on                                                                                                            | 
|_____________________________________________________________________________________________________________________________|

Simply copy + Paste+ Enter the link (e.g http://127.0.0.1:8050/ ) in your browser and it will open the dashboard.



#About the Application: 
1.) Created on Ubuntu 18.04 with python3 using plotly-dash API.
2.) If you are using a mac, turn on print sharing: System Preferences->Sharing->Enable 'Printer Sharing'


import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html


import pandas as pd
import pdb


from layouts.drowdown_comp import *


epoch_graphs_layout = html.Div([
    html.Div([

        summary_pg_dropdown_1,
        summary_pg_dropdown_2
     
    ]),

    dcc.Graph(id='epoch_graph',style ={'height':'80vh'}),


])

sites_graph_layout = html.Div([
    html.Div([

        reciever_pg_dropdown_1,
        reciever_pg_dropdown_2
    ]),

    dcc.Graph(id='sites_graph',style ={'height':'80vh'}),

])


sat_graph_layout = html.Div([
    html.Div([

        satellite_pg_dropdown_1,
        satellite_pg_dropdown_2
    ]),

    dcc.Graph(id='sat_graph',style ={'height':'80vh'}),

])


res_graph_layout = html.Div([
    html.Div([
        residuals_pg_dropdown_1,
        residuals_pg_dropdown_2,
        residuals_pg_dropdown_3,
        residuals_pg_dropdown_4,
        residuals_pg_dropdown_5
    ]),

    dcc.Graph(id='res_graph',style ={'height':'80vh'}),

])

res_histogram_layout = html.Div([
    html.Div([
        residuals_pg_dropdown_1,
        residuals_pg_dropdown_2,
        residuals_pg_dropdown_3,
        residuals_pg_dropdown_4,
        residuals_pg_dropdown_5
    ]),

    dcc.Graph(id='res_graph',style ={'height':'80vh'}),

])





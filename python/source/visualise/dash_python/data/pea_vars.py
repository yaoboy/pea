from collections import defaultdict



#******************************************************************************************************************************************
#******************************************************************************************************************************************

def getmeta_dict():
    sol_meta_t = {  
    "EPOCH"                       : [],
    "EPOCH_NUM"                   : [],
    "EPOCH_NUM_SITES"             : [],   # Number of sites used at the current epoch (filter step)
    "EPOCH_NUM_SVS"               : [],   # Number of satellites used at the current epoch (filter step)
    "EPOCH_NUM_OBS"               : [],   # Number of LC phase observations used at the current epoch
    "EPOCH_NUM_PARAMS"            : [],   # Number of parameters estimated at the current epoch
    "EPOCH_PREFIT_CHISQR_INC"     : [],   # Pre-fit chi square increment per degree of freedom
    "EPOCH_POSTFIT_CHISQR_DOF"    : [],   # Post-fit chi square per degree of freedom at the current epoch
    "EPOCH_MEAN_PHASE_RESID"      : [],   # Mean LC phase residual at the current epoch (filter step)
    "EPOCH_PHASE_RESID_RMS"       : [],   # LC phase RMS residual at the current epoch (filter step)
    "EPOCH_MEAN_RANGE_RESID"      : [],   # Mean LC range residual at the current epoch (filter step)
    "EPOCH_RANGE_RESID_RMS"       : [],   # LC range RMS residual at the current epoch (filter step)
    "EPOCH_NUM_AMB"               : [],   # Number of ambiguity parameters in the solution at th e current epoch
    "EPOCH_NUM_NL_AMB_FIXED"      : [],   # Number of fixed Narrow Lane (NL) ambiguity parameters in the solution
    "EPOCH_NUM_WL_AMB_FIXED"      : [],   # Number of fixed Wide Lane (WL) ambiguity parameters in the solution
    
    #SOLN_SUM // #tag for solution summary information
    "SOL_MEAN_EPOCH"              : [],  # Mean epoch of solution
    "SOL_NUM_SITES"               : [],   # Total number of sites in the solution
    "SOL_NUM_SVS"                 : [],   # Total number of satellites in the solution
    "SOL_NUM_OBS"                 : [],   # Total number of LC phase observations used in the solution
    "SOL_NUM_PARAMS"              : [],   # Total number of parameters estimated in the solution
    "SOL_POSTFIT_CHISQR_DOF"      : [],   # Solution post-fit chi square per degree of freedom
    "SOL_MEAN_PHASE_RESID"        : [],   # Mean phase residual of solution
    "SOL_PHASE_RESID_RMS"         : [],   # LC phase residual RMS for solution
    "SOL_MEAN_RANGE_RESID"        : [],   # Mean range residual of solution
    "SOL_RANGE_RESID_RMS"         : [],   # LC phase residual RMS for solution
    "SOL_NUM_AMB"                 : [],   # Total number of ambiguity parameter in the solution
    "SOL_NUM_WL_AMB_FIXED"        : [],   # Total number of fixed Wide Lane (WL) ambiguity parameters in the solution
    "SOL_NUM_AMB_FIXED"           : [],   # Total number of fixed ambiguity parameter in the solution
    "NUM_CODE_OBS"                : []   # Number of code observations (for internal calculations)

    }
    return sol_meta_t

def getrec_dict():
    sol_rec_t = {
    "SITE_ID"                     : "",
    "REC_TYP"                     : "",# Site receiver type
    "ANT_TYP"                     : "",  # Site antenna type
    "SITE_X_APR"                  : [],   # X site position component a priori value
    "SITE_Y_APR"                  : [],   # Y site position component a priori value
    "SITE_Z_APR"                  : [],   # Z site position component a priori value
    "PHASE_ME"                    : [],   # Phase Measurement error sigma, sqrt(variance)
    "RANGE_ME"                    : [],   # Range Measurement error sigma, sqrt(variance)
    "TROP_ZWD_APR"                : [],   # Tropospheric Zenith Wet Delay (ZWD) parameter a priori value
    "TROP_GRAD_NS_APR"            : [],  # North-South (NS) Tropospheric gradient parameter a priori value
    "TROP_GRAD_EW_APR"            : [],   # East-West (EW) Tropospheric gradient parameter a priori value
    "REC_CLK_APR"                 : [],   # Site receiver clock offset a priori value
    "ELEV_WGT_MODEL"              : "",  # The elevation angle dependent weighting model used to weight observations
    "ELEV_CUTOFF"                 : [],  # The elevation angle cut off angle for observations collected at this site
        # #SITE_CON # #tag for site parameter constraints
    "SITE_X_CON"                  : [],   # X site position component parameter constraint
    "SITE_Y_CON"                  : [],   # Y site position component parameter constraint
    "SITE_Z_CON"                  : [],   # Z site position component parameter constraint
    "TROP_ZWD_CON"                : [],  # Tropospheric Zenith Wet Delay (ZWD) parameter a priori constraint
    "TROP_GRAD_NS_CON"            : [],   # North-South (NS) Tropospheric gradient parameter a priori constraint
    "TROP_GRAD_EW_CON"            : [],  # North-South (EW) Tropospheric gradient parameter a priori constraint
    "REC_CLK_CON"                 : [],   # Site receiver clock offset a priori constraint (sigma)
    "FLOAT_AMB_CON"               : [],   # A priori constraint applied to ambiguity parameters
# #SITE_PN # #tag for site parameter process noise
    "SITE_X_PN"                   : [],   # X site position component parameter process noise
    "SITE_Y_PN"                   : [],   # Y site position component parameter process noise
    "SITE_Z_PN"                   : [],   # Z site position component parameter process noise
    "TROP_ZWD_PN"                 : [],   # Tropospheric Zenith Wet Delay (ZWD) parameter process noise
    "TROP_GRAD_NS_PN"             : [],   # North-South (NS) Tropospheric gradient parameter process noise
    "TROP_GRAD_EW_PN"             : [],   # North-South (EW) Tropospheric gradient parameter process noise
    "REC_CLK_PN"                  : [],   # Site receiver clock offset process noise (variance/time)
    # #SITE_EST # #tag for site parameer estimates
    "SITE_NUM_SVS"                : [],   # Number of satellites observed by site at the current epoch (filter step)
    "REC_CLK_EST"                 : [],   # Site receiver clock offset estimate at this epoch (filter step)
    "REC_CLK_EST_SIG"             : [],    # Site receiver clock offset estimate sigma at this epoch (filter stp)
    "SITE_LAT"                    : [],    # Estimated latitude component of the site position
    "SITE_LON"                    : [],    # Estimated longitude component of the site position
    "SITE_HT"                     : [],    # Estimated height component of the site position
    "SITE_LAT_SIG"                : [],    # Estimated latitude component of the site position
    "SITE_LON_SIG"                : [],    # Estimated longitude component of the site position
    "SITE_HT_SIG"                 : [],    # X site position component estimate (sinex value + KF estimate)
    "SITE_X"                      : [],
    "SITE_Y"                      : [],    # Y site position component estimate
    "SITE_Z"                      : [],    # Z site position component estimate
    "SITE_DX"                     : [],    # Change in X site position component estimate (estimated by KF)
    "SITE_DY"                     : [],    # Change in Y site position component estimate 
    "SITE_DZ"                     : [],    # Change in Z site position component estimat
    "SITE_X_SIG"                  : [],    # X site position component estimate sigma
    "SITE_Y_SIG"                  : [],    # Y site position component estimate sigma
    "SITE_Z_SIG"                  : [],    # Z site position component estimate sigma
    "SITE_CXY"                    : [],    # Covariance between X and Y site position component estimates
    "SITE_CXZ"                    : [],    # Covariance between X and Z site position component estimates 
    "SITE_CYZ"                    : [],    # Covariance between Y and Z site position component estimate
    "SITE_DN"                     : [],    # Adjustment to the North (N) component of positon wrt the a priori site position
    "SITE_DE"                     : [],    # Adjustment to the East (E) component of positon wrt the a priori site position
    "SITE_DU"                     : [],    # Adjustment to the Up (U) component of positon wrt the a priori site position
    "SITE_DN_SIG"                 : [],    # Sigma of the adjustment to the North (N) component of positon
    "SITE_DE_SIG"                 : [],    # Sigma of the adjustment to the East (E) component of positon
    "SITE_DU_SIG"                 : [],    # Sigma of the adjustment to the Up (U) component of positon
    "SITE_CNE"                    : [],    # Covariance btw North (N) and East (E) site position component estimates
    "SITE_CNH"                    : [],    # Covariance btw North (N) and Height (H) site position component estimates
    "SITE_CEH"                    : [],    # Covariance btw East (E) and Height (N) site position component estimates
    "TROP_ZWD"                    : [],    # Tropospheric Zenith Wet Delay (ZWD) parameter estimate
    "TROP_DZWD"                   : [],   # Adjustment to prior (pre-processor) Tropospheric Zenith Wet Delay (ZWD)
    "TROP_ZWD_SIG"                : [],    # Tropospheric Zenith Wet Delay (ZWD) parameter estimate sigma
    "TROP_GRAD_NS"                : [],    # North-South (NS) Tropospheric gradient parameter estimate
    "TROP_GRAD_EW"                : [],    # East-West (EW) Tropospheric gradient parameter estimate
    "TROP_GRAD_NS_SIG"            : [],    # North-South (NS) Tropospheric gradient parameter estimate sigma
    "TROP_GRAD_EW_SIG"            : [],    # East-West (EW) Tropospheric gradient parameter estimate sigma
    "SITE_MEAN_PHASE_RESID"       : [],    # Mean LC phase residual for this site to observed satellites at current epoch 
    "SITE_PHASE_RESID_RMS"        : [],    # one-way LC phase RMS residual for this site to observed satellites at current
    "SITE_MEAN_RANGE_RESID"       : [],    # Mean LC phase residual for this site to observed satellites at current epoch  
    "SITE_RANGE_RESID_RMS"        : [],    # Number of ambiguity parameters for site at current epoch (filter step)
    "NUM_WL_AMB_FIXED"            : [],    # Number of Wide Lane (WL) ambiguity parameters for site fixed at the current epoch 
    "EPOCH_NUM_SITES"             : [],   
    "NUM_NL_AMB_FIXED"            : []   # Number of Narrow Lane (NL) ambiguity parameters for site fixed at the
    }

    return sol_rec_t


def getsat_dict():
    sol_sat_t={
    'SVN_ID'                      : "",   # Name of SV (PRN#_SVN#). Ie G01_035
    'SV_BLOCK_TYPE'               : "",   # Satellite Block type
    'SV_PCO_X'                    : [],    # Satellite X antenna Phase Centre Offset (PCO) wrt satellite centre of mass (CoM)
    'SV_PCO_Y'                    : [],    # Satellite Y antenna Phase Centre Offset (PCO) wrt satellite centre of mass (CoM)
    'SV_PCO_Z'                    : [],    # Satellite Z antenna Phase Centre Offset (PCO) wrt satellite centre of mass (CoM)
    'SRP_MODEL'                   : "",   # Satellite Solar Radiation Pressure (SRP) model used for this satellite
    'SRP_NUM'                     : [],    # Number of Solar Radiation Pressure (SRP) parameters used in SRP model
    'SV_X_APR'                    : [],    # Satellite X position parameter a priori value
    'SV_Y_APR'                    : [],    # Satellite Y position parameter a priori value
    'SV_Z_APR'                    : [],    # Satellite Z position parameter a priori value
    'SV_VX_APR'                   : [],    # Satellite X velocity parameter a priori value
    'SV_VY_APR'                   : [],    # Satellite Y velocity parameter a priori value
    'SV_VZ_APR'                   : [],    # Satellite Z velocity parameter a priori value
    'SV_DRAD_APR'                 : [],    # Satellite Direct (D) SRP parameter a priori value
    'SV_YRAD_APR'                 : [],    # Satellite Y-Bias (Y) SRP parameter a priori value
    'SV_BRAD_APR'                 : [],    # Satellite B-Bias (B) SRP parameter a priori value
    'SV_CLK_APR'                  : [],    # Satellite clock offset parameter a priori value
    

    
    # #SV_CON # #tag for satellite a priori constraints
    'SV_X_CON'                    : [],    # Satellite X position parameter constraint
    'SV_Y_CON'                    : [],    # Satellite Y position parameter constraint
    'SV_Z_CON'                    : [],    # Satellite Z position parameter constraint
    'SV_VX_CON'                   : [],    # Satellite X velocity parameter constraint
    'SV_VY_CON'                   : [],    # Satellite Y velocity parameter constraint
    'SV_VZ_CON'                   : [],    # Satellite Z velocity parameter constraint
    'SV_DRAD_CON'                 : [],    # Satellite Direct (D) SRP parameter a priori constraint
    'SV_YRAD_CON'                 : [],    # Satellite Y-Bias (Y) SRP parameter a priori constraint
    'SV_BRAD_CON'                 : [],    # Satellite B-Bias (B) SRP parameter a priori constraint
    
    'SV_CLK_CON'                  : [],    # Satellite clock offset parameter a priori constraint
    
    ##SV_PN # #tag for satellite process noise information
    'SV_X_PN'                     : [],    # Satellite X position parameter process noise (variance/time)
    'SV_Y_PN'                     : [],    # Satellite Y position parameter process noise (variance/time)
    'SV_Z_PN'                     : [],    # Satellite Z position parameter process noise (variance/time)
    'SV_VX_PN'                    : [],    # Satellite X velocity parameter process noise (variance/time)
    'SV_VY_PN'                    : [],    # Satellite Y velocity parameter process noise (variance/time)
    'SV_VZ_PN'                    : [],    # Satellite Z velocity parameter process noise (variance/time)
    'SV_DRAD_PN'                  : [],    # Satellite Direct (D) SRP parameter process noise
    'SV_YRAD_PN'                  : [],    # Satellite Y-Bias (Y) SRP parameter process noise
    'SV_BRAD_PN'                  : [],    # Satellite B-Bias (B) SRP parameter process noise (variance/time)
    
    'SV_CLK_PN'                   : [],    # Satellite clock offset parameter process noise value (variance/time)

    # #SV_EST # #tag for satellite parameter estimates
    'SV_NUM_SITES'                : [],    # Number of sites observed by this satellite at the current epoch (filter step)
    'SV_CLK_EST'                  : [],    # Satellite clock offset parameter estimate
    'SV_CLK_EST_SIG'              : [],    # Satellite clock offset parameter estimate sigma
    'SV_X'                        : [],    # Satellite X position parameter estimate
    'SV_Y'                        : [],    # Satellite Y position parameter estimate
    'SV_Z'                        : [],    # Satellite Z position parameter estimate
    'SV_VX'                       : [],    # Satellite X velocity parameter estimate
    'SV_VY'                       : [],    # Satellite Y velocity parameter estimate
    'SV_VZ'                       : [],    # Satellite Z velocity parameter estimate
    'SV_DRAD'                     : [],    # Satellite Direct (D) SRP parameter estimate
    'SV_YRAD'                     : [],    # Satellite Y-Bias (Y) SRP parameter estimate
    'SV_BRAD'                     : [],    # Satellite B-Bias (B) SRP parameter estimate
    
    'SV_X_SIG'                    : [],    # Satellite X position parameter estimate sigma
    'SV_Y_SIG'                    : [],    # Satellite Y position parameter estimate sigma
    'SV_Z_SIG'                    : [],    # Satellite Z position parameter estimate sigma
    'SV_VX_SIG'                   : [],    # Satellite X velocity parameter estimate sigma
    'SV_VY_SIG'                   : [],    # Satellite Y velocity parameter estimate sigma
    'SV_VZ_SIG'                   : [],    # Satellite Z velocity parameter estimate sigma
    'SV_DRAD_SIG'                 : [],    # Satellite Direct (D) SRP parameter estimate sigma
    'SV_YRAD_SIG'                 : [],    # Satellite Y-Bias (Y) SRP parameter estimate sigma
    'SV_BRAD_SIG'                 : [],    # Satellite B-Bias (B) SRP parameter estimate sigma
    
    'SV_MEAN_PHASE_RESID'         : [],    # Mean LC phase residual for satellite at the current epoch (filter step)
    'SV_PHASE_RESID_RMS'          : [],    # LC phase RMS residual for satellite to observed sites at current epoch (filter step)
    'SV_MEAN_RANGE_RESID'         : [],    # Mean LC range residual for satellite at the current epoch (filter step)
    'SV_RANGE_RESID_RMS'          : []    # LC range RMS residual for satellite to observed sites at current epoch (filter step)
    }
    return sol_sat_t
    

class soln_t:    
    def __init__(self):
        self.meta = {}
        self.rec = {}
        self.sat = {}
        

    
    
def getsoln_t():
    return soln_t()


#******************************************************************************************************************************************
#******************************************************************************************************************************************

def getresid_dict():
    resid_t = {
    'SITE_LAT'                    : [],    # Estimated latitude component of the site position (decimal degrees)
    'SITE_LON'                    : [],    # Estimated longitude component of the site position (dd)
    'SITE_HT'                     : [],    # Estimated height component of the site position (m)
    'SVN_ID'                      : [],    # Name of SV (PRN#_SVN#). E.g. "G01_035"
    'SV_LAT'                      : [],    # Latitude component of the satellite position (dd)
    'SV_LON'                      : [],    # Longitude component of the satellite position (dd)
    'SV_HT'                       : [],    # Height component of the satellite position (km)
    'ELEV_ANG'                    : [],    # Elevation Angle of Satellite wrt Receiver (milli arc sec)
    'AZIM'                        : [],    # Azimuth of Satellite wrt Receiver (mas)
    'NAIDIR_ANG'                  : [],    # Nadir angle of Receiver wrt Satellite (mas)
    'TROP_SLANT_WD'               : [],    # Tropospheric slant wet delay estimate, one-way (site to satellite) (m)
    'TROP_SLANT_TD'               : [],    # Tropospheric slant total delay estimate, one-way (site to satellite) (m)
    #'TROP_MAP_FN;'                         #Mapping functions for Tropospheric wet delay
    'FLOAT_AMB'                   : [],    # The real value of the LC phase ambiguity estimate (m)
    'FLOAT_AMB_SIG'               : [],    # The sigma of the LC phase ambiguity estimate (m)
    'WL_AMB'                      : [],    # The real value of the WL phase ambiguity estimate (m)
    'WL_AMB_SIG'                  : [],    # The sigma of the WL phase ambiguity estimate (m)
    'WL_FIXED_AMB'                : [],    # The integer WL phase ambiguity estimate
    'NL_AMB'                      : [],    # The real value of the NL phase ambiguity estimate (m)
    'NL_AMB_SIG'                  : [],    # The sigma of the NL phase ambiguity estimate (m)
    'NL_FIXED_AMB'                : [],    # The integer NL phase ambiguity estimate
    'LOSS_OF_LOCK_NEW_AMB_FLAG'   : [],    # Flag indicating receiver loss of lock and/or new ambiguity set for this site-satellite one-way observation
    'CYCLE_SLIP_REPAIR_FLAG'      : [],    # Flag indicating cycle slip repaired at the epoch for this site-satellite one-way observation
    'CODE_OBS_IND'                : [],    # Index of code obs in observation vector
    'PHASE_OBS_IND'               : []    # Index of phase obs in observation vector
    }

    return resid_t

class res_t():
    def __init__(self):
        self.resid_t ={}

def getres_t():
    return res_t()
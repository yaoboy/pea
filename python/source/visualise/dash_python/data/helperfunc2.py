import pandas as pd
import os
from os import listdir
from os.path import isfile, join
import csv
import pdb
from csv import reader
from index import get_params



"""
This contains functions common to both 
    a.)get_data_res.py
    b.)get_data_sol.py
"""





"""
 * @brief                      : This converts an Epoch Timestamp, which is in string, into pandas DateTime object. 
 * @param epoch_timestamp      : Epoch time stamp from teh PEA export in string format. e.g : '58682.000000_20190718_000000'
 * @return                     : Returns the pandas Timestamp object.
"""
def get_date_time(epoch_timestamp):
    date_time = str(epoch_timestamp)[13:]
    date = str(date_time)[:8]
    yyyy= str(date)[:4]
    mm= str(date)[4:6]
    dd= str(date)[6:8]
    time = str(date_time)[9:]
    hh = str(time)[0:2]
    minutes=str(time)[2:4]
    ss = str(time)[4:6]
    
    return pd.Timestamp(int(yyyy), int(mm), int(dd), int(hh),int(minutes),int(ss))

    

"""
 * @brief            : This stores the names of the sol files generate by the PEA KF in a list.
 * @return           : 
"""
def get_relevant_files(file_type):
   
    data_source = get_params()
    input_dir = data_source.data
    # pdb.set_trace()

    # if(data_source.data =="pea"):

   
    #     input_dir = "/data/acs/pea/output/examples/Ex02/export/"
    
       
    # else :
    #     input_dir = os.getcwd() + "/raw_data/"

    
    print("Dashboard is reading files from ", input_dir)


  
    onlyfiles = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]


    #Sort the files based on the time the files were created.
    onlyfiles.sort(key=lambda x: os.stat(os.path.join(input_dir, x)).st_ctime)


    ####2
    ## Select only "sol"/"res" files based on file_type: 
    file_names_list = []
    for filename in onlyfiles:
        if(file_type in filename):
            file_names_list.append(filename)




    
    return input_dir, file_names_list




"""
 * @brief                      : This is used to get the dictionary of satellites visible to each reciever. The key of the dictionary will be the  name of the 
                                 reciever and the value will be the list of satellites visible to that reciever. This function is used to update
                                 the DropDown menu in the Dashboard application and is NOT used to parse data from the files.
 * @return                     : void
"""

def get_sat_visible_to_rec():
    ## Take any files and see what satellites are visible to a site in that file. This should remain consistent in all files. 
    
    #Create the empty dictionary, this will be filled subsequently.
    sat_visible_to_rec ={}
    input_dir, res_files = get_relevant_files("res")


    for r in res_files:
#      with open(input_dir + res_files[1]) as read_obj:
      with open(input_dir + r) as read_obj:
        
        csv_reader = reader(read_obj)
        rownum =0
        for row in csv_reader:
            if rownum<2:  #Skip 1st two rows in the csv file.
                    rownum= rownum+1
                    continue

            """
            There are 2 steps:
            1.) Check if the key (reciever's name) does not exist. If it does not exist, create a key with the reciever's name and a value = "ALL".
                This "ALL" value will be used to list all the satellites for that particular reciever in the drop down list.
            2.) Check if the satellite visible to the reciever is added already to the list of visible satellites. If not, simply add. 
            """

            #1.)
            if row[2] not in sat_visible_to_rec.keys()  :
                sat_visible_to_rec[row[2]] =["ALL"]

            #2.)
            if row[6] not in sat_visible_to_rec[row[2]] :
                sat_visible_to_rec[row[2]].append(row[6])

    return sat_visible_to_rec
